<?php

use models\ProjectBillModel;
use traits\SingleTone;

class InvoiceProject
{
    use SingleTone;

    private static $folder = 'invoice';
    private static $MD5_PREFIX = [
        'confirm_bdc_from_email' => 'confirm_bdc_from_email'
    ];

    private function getMPDF($args = [])
    {
        $args = array_merge($args, ['tempDir' => UploadDir::getRootPath() . 'Mpdf/invoice']);
        return new \Mpdf\Mpdf($args);
    }

    public static function baseSelect(array $columns = ['*'], $aggregate = null)
    {
        return ProjectBillModel::baseSelect($columns, $aggregate);
    }

    public static function getTable()
    {
        return BillProject::getTable();
    }

    public static function getFolderName()
    {
        return self::$folder;
    }

    public static function getPathToFolder($bill_id, $project_id)
    {
        return changeSlash(Project::geMainFolder() . DS . $project_id . DS . self::getFolderName() . DS . $bill_id);
    }

    private function createFolder($bill_id, $project_id = 0)
    {
        if (!$project_id) {
            $project_id = $this->getById($bill_id, ['project_id']);
            $project_id = $project_id['project_id'];
        }

        $dir = self::getPathToFolder($bill_id, $project_id);
        Folder::create($dir);
        return $dir;
    }

    public function getFolder($bill_id, $project_id = 0)
    {
        if (!$project_id) {
            $project_id = $this->getById($bill_id, ['project_id']);
            $project_id = $project_id['project_id'];
        }
        return $this->createFolder($bill_id, $project_id);
    }

    public function createFileName($bill_id)
    {

        $name = 'Taskea_';
        if (intval($bill_id) < pow(10, 4)) {
            return $name . (pow(10, 4) + intval($bill_id));
        }
        else {
            $tmp = strlen($bill_id);
            $tmp += 2;
            return $name . (pow(10, $tmp) + intval($bill_id));
        }
    }

    public function createNameForShow($bill_id, $societe)
    {
        return $this->createFileName($bill_id) . '-' . $societe;
    }

    /*
        public function createNameBDC($bill_id, $bill_date)
        {
            $date = new DateTime($bill_date);
            $month = $date->format('m');
            $year = $date->format('Y');
    //        $m = ($month + 1) < 10 ? '0' . ($month + 1) : ($month + 1);
            return $year . '-' . $month . '-' . $bill_id;
        }
    */

    public function update($bill_id, array $data)
    {
        return ProjectBillModel::update($bill_id, $data);
    }

    public function getLastByClientId($client_id, array $columns = ['*'])
    {
        $client_id = escape_string(clear_post($client_id));
        $projects = Project()->getProjectsByClientId($client_id, ['id']);
        $projects = array_column($projects, 'id');
        $bills = [];
        foreach ($projects as $p_id) {
            $bills[] = $this->getLastByProjectId($p_id, $columns);
        }
        return $bills;
    }

    public function getByClientId($client_id, array $columns = ['*'])
    {
        $client_id = escape_string(clear_post($client_id));
        $projects = Project()->getProjectsByClientId($client_id, ['id']);
        if (empty($projects))
            return [];
        $projects = array_column($projects, 'id');
        return $this->getByProjectId($projects, $columns);
    }

    public function getCntByClientId($client_id)
    {
        $res = $this->getByClientId($client_id, ['id']);
        return $res ? count($res) : 0;
    }

    public function getHrefPDF($bill_id, $project_id = 0)
    {
        $res = $this->getFilePathPDF($bill_id, $project_id);
        $res = changeSlash($res);
        $res = explode(MODULE . DS, $res);
        return array_pop($res);
    }

    public function getFilePathPDF($bill_id, $project_id = 0)
    {
        $path = $this->getFolder($bill_id, $project_id);
        return $path . DS . $this->createFileName($bill_id) . '.pdf';
    }

    public function createPDF($data, $as = 'F')
    {
        try {
            $mpdf = $this->getMPDF([
                'setAutoBottomMargin' => 'stretch'
                , 'margin_footer' => 10
            ]);
//            $mpdf->allow_charset_conversion = true;
//            $mpdf->currentfontfamily = 'freesans';
//            $mpdf->FontFamily = 'freesans';
            $mpdf->SetHTMLFooter(Templates::getTemplate('BillInvoice/bill_invoice_footer'));
            $mpdf->writeHTML(Templates::getTemplate('BillInvoice/project_invoice', $data));
            $mpdf->AddPage();
            $mpdf->writeHTML(Templates::getTemplate('BillInvoice/project_company_requisites', $data));
            return $mpdf->Output($this->getFilePathPDF($data['id'], $data['project_id']), $as) == '';
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function getSumPaymentByProjectId($project_id)
    {
        return ProjectBillModel::getByProjectId($project_id, ['SUM(amount) as balance']);
    }

    public function getDataForPDF($bill_id, array $columns = [])
    {
        return ProjectBdcInvoice::getDataForPDF($bill_id, $columns);
    }

    public function createEmailConfirmUrl($client_id, $bill_id)
    {
        $hash = $this->createEmailConfirmHash($client_id);
        return SiteData()->getAll('sitename') . DS . MODULE . DS . Pages::getLogin() . '?confirm=bdc&bdc_id=' . $bill_id . '&hash=' . $hash;
    }

    public function createEmailConfirmHash($client_id)
    {
        return md5(self::$MD5_PREFIX['confirm_bdc_from_email'] . $client_id);
    }

    public function getUserIdByEmailConfirmHash($hash)
    {
        return User::getUserIdByHashAndPrefix($hash, self::$MD5_PREFIX['confirm_bdc_from_email']);
    }

    public static function createTable()
    {
        $sql = 'CREATE TABLE `taskeo`.`project_bill`(
                        `id` INT(11) NOT NULL AUTO_INCREMENT ,
                        `project_id` INT(11) NOT NULL ,
                        `created` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ,
                        `city` VARCHAR(255) NOT NULL ,
                        `discount` DECIMAL(10.2) NOT NULL DEFAULT \'0\' ,
                        `discount_type` TINYINT(1) NOT NULL DEFAULT \'0\' COMMENT \'0-percent, 1-amount\' ,
                        `comment` TEXT NULL DEFAULT NULL , PRIMARY KEY (`id`)
                        ) ENGINE = InnoDB;
                        ';
        //ALTER TABLE `project_bill` ADD FOREIGN KEY (`project_id`) REFERENCES `projects`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;
    }

    public function add($data)
    {
        if (isset($data['created']))
            $data['created'] = date('Y-m-d H:i:s', strtotime($data['created']));
        return ProjectBillModel::add($data);
    }

    public function getById($id, array $columns = ['*'])
    {
        return ProjectBillModel::getById($id, $columns);
    }

    public function getAll(array $columns = ['*'])
    {
        return ProjectBillModel::getAllApproved($columns);
    }

    public function getAllByProjectId($project_id, array $columns = ['*'])
    {
        return ProjectBillModel::getAllApprovedByProjectId($project_id, $columns);
    }

    public function getLastByProjectId($project_id, array $columns = ['*'])
    {
        return ProjectBillModel::getLastApprovedByProjectId($project_id, $columns);
    }

    public function getByProjectId($projects_id, array $columns = ['*'])
    {
        return ProjectBillModel::getApprovedByProjectId($projects_id, $columns);
    }

    public function delete($bill_id, $proj_id = 0)
    {
        try {
            $res = ProjectBillModel::delete($bill_id);
            if ($res) {
                Folder::delete($this->getFolder($bill_id, $proj_id));
            }
            return $res;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function getTasks($bill_id, $asArray = false)
    {
        $bill_id = escape_string(clear_post($bill_id));
        $tasks = $this->getById($bill_id, ['tasks']);
        if (empty($tasks))
            return [];
        $tasks = $tasks['tasks'];
        return $asArray ? array_map(function ($elem) {
            return intval($elem);
        }, explode(',', $tasks)) : $tasks;

    }

}