<?php


class Folder
{
    public static function create($path)
    {
        $res = true;
        if (!file_exists($path)) {
            $res = mkdir($path, 0775, true);
            chmod($path, 0775);
        }
        return $res;
    }

    public static function delete($path)
    {
        if (is_dir($path) === true) {
            $files = array_diff(scandir($path), array('.', '..'));
            foreach ($files as $file)
                self::delete(realpath($path) . DS . $file);
            return rmdir($path);
        } else if (is_file($path) === true) {
            return unlink($path);
        }
        return false;
    }
}