<?php
class Documents
{
    private $docs;
    private $client_id;
    private $dossier_id;
    private $rootPath;
    private $outputDir;
    private $linkPath;

    private function getMPDF()
    {
        return new \Mpdf\Mpdf(['tempDir' => UploadDir::getRootPath() . 'Mpdf/documents']);
    }

    private function createDirectoryClient()
    {
        $path = $this->rootPath;
        $path .= DS . 'documents';
        Folder::create($path);
        if (isset($_COOKIE['debug'])) {
            $path .= DS . 'test';
            Folder::create($path);
        }
        $path .= DS . 'client-' . $this->client_id;
        Folder::create($path);
        return $path;
    }

    private function createDocDirectory($doc)
    {
        $path = $this->createDirectoryClient() . DS . $doc;
        Folder::create($path);
    }

    private function getPath($doc)
    {
        return $this->docs[$doc];
    }

    private function addFileName($doc, $data = [])
    {
        $this->docs[$doc]['data']['files'][] = $this->createFileName($doc, $data);
    }

    private function getPathAll()
    {
        return $this->docs;
    }

    private function createFileName($doc, $data = [])
    {
        switch ($doc) {
            case $this->bill:
                return $this->docs[$doc]['name'] . '-' . $this->dossier_id . $this->client_id . $data['dfp_id'] . '.pdf';
                break;
            case $this->register:
            case $this->cartography:
            case $this->actions_to_be_taken:
            case $this->minimization:
            case $this->finality:
            case $this->contract:
                return $this->docs[$doc]['name'] . '-' . $this->dossier_id . $this->client_id . '.pdf';
                break;
        }
    }

    public function __construct($dossier_id, $client_id = 0)
    {
        $this->dossier_id = $dossier_id;
        if (!$client_id) {
            $client_id = querySingle('SELECT client_id FROM dossiers WHERE id=' . $this->dossier_id . ' LIMIT 1')['client_id'];
        }
        $this->client_id = $client_id;
        $docs = [
            'bill' => [
                'table' => 'dossier_fact_payment',
                'keys' => ['dossier_id' => $this->dossier_id],
                'column' => 'text'
            ],
            'register' => [
                'table' => 'dossiers',
                'keys' => ['id' => $this->dossier_id],
                'column' => 'questionnaire'
            ],
            'cartography' => [
                'table' => 'dossier_documents',
                'keys' => ['dossier_id' => $this->dossier_id, 'name' => 'cartography'],
                'column' => 'text'
            ],
            'actions_to_be_taken' => [
                'table' => 'dossier_documents',
                'keys' => ['dossier_id' => $this->dossier_id, 'name' => 'actions_to_be_taken'],
                'column' => 'text'
            ],
            'minimization' => [
                'table' => 'dossier_documents',
                'keys' => ['dossier_id' => $this->dossier_id, 'name' => 'minimization'],
                'column' => 'text'
            ],
            'finality' => [
                'table' => 'dossier_documents',
                'keys' => ['dossier_id' => $this->dossier_id, 'name' => 'finality'],
                'column' => 'text'
            ],
            'contract' => [
                'table' => 'dossiers',
                'keys' => ['id' => $this->dossier_id],
                'column' => 'result_text'
            ]
        ];
        $this->rootPath = $_SERVER['DOCUMENT_ROOT'] . DS . 'upload';
        $this->outputDir = $this->rootPath . DS . 'documents' . DS;
        $this->linkPath = DS . 'upload' . DS . 'documents' . DS;
        if (isset($_COOKIE['debug'])) {
            $tmp = 'test' . DS;
            $this->linkPath .= $tmp;
            $this->outputDir .= $tmp;
        }
        foreach ($docs as $doc => $data) {
            $this->docs[$doc]['name'] = $doc;
            $this->docs[$doc]['data'] = $data;
            $this->docs[$doc]['output'] = $this->outputDir;
            $this->docs[$doc]['link'] = $this->linkPath;
            $this->docs[$doc]['output'] .= 'client-' . $this->client_id . DS . $doc;
            $this->docs[$doc]['link'] .= 'client-' . $this->client_id . DS . $doc;
        }
    }

    public function __get($name)
    {
        return isset($this->docs[$name]) ? $name : '';
    }

    public function getZIPLinkPath()
    {
        return $this->linkPath . 'client-' . $this->client_id . DS . 'allDocs.zip';
    }

    private function createZipAllDocs_old()
    {
        $zipPath = $this->createDirectoryClient() . DS . 'allDocs.zip';
        $zip = new ZipArchive();
        $zip->open($zipPath, ZipArchive::CREATE);
        foreach ($this->docs as $doc) {
            if (isset($doc['data']['files']) && !empty($doc['data']['files'])) {
                foreach ($doc['data']['files'] as $f_name) {
                    $file = $doc['output'] . DS . $f_name;
                    if (file_exists($file)) {
                        $zip->addFile($file, $f_name);
                    }
                }
            }
        }
        return $zip->close();
    }

    public function createZipAllDocs()
    {
        $zipPath = $this->createDirectoryClient() . DS . 'allDocs.zip';
        $zip = new ZipArchive();
        $zip->open($zipPath, ZipArchive::CREATE);
        foreach ($this->docs as $doc_name => $doc) {
            $arr[]['data'] = $doc['data'];
            $keys = implode(' AND ', array_map(function ($key, $value) {
                return $key . '="' . $value . '"';
            }, array_keys($doc['data']['keys']), $doc['data']['keys']));
            $sql = 'SELECT ' . $doc['data']['column'] . ' AS text, id AS dfp_id  FROM ' . $doc['data']['table'] . ' WHERE ' . $keys;
            $res = query($sql);
            if ($res) {
                foreach ($res as $val) {
                    $file_name = $this->createFileName($doc_name, ['dfp_id' => $val['dfp_id']]);
                    $file_path = $this->getPath($doc_name)['output'] . DS . $file_name;
                    if (!file_exists($file_path) && $val['text']) {
                        $method = 'create' . ucfirst($doc_name);
                        $this->$method(['dfp_id' => $val['dfp_id']]);
                        $zip->addFile($file_path, $file_name);
                    } elseif (file_exists($file_path)) {
                        $zip->addFile($file_path, $file_name);
                    }
                }
            }
        }
        return $zip->close();
    }

    public function getRegisterOutputDir()
    {
        $doc = $this->register;
        $path = $this->getPath($doc);
        return $path['output'] . DS . $this->createFileName($doc);
    }

    public function getRegisterLinkPath()
    {
        $doc = $this->register;
        $path = $this->getPath($doc);
        return $path['link'] . DS . $this->createFileName($doc);
    }

    public function createRegister($data = [])
    {
        $sql = 'SELECT questionnaire FROM dossiers WHERE id=' . $this->dossier_id;
        $res = query($sql);
        if ($res) {
            $res = $res[0];
            $doc = $this->register;
            $this->createDocDirectory($doc);
            try {
                $res = unserialize(htmlspecialchars_decode($res['questionnaire']));
                $file = Formula()->getFile('documents/register.php', ['questionnaire' => $res]);
                $mpdf = $this->getMPDF();
                $mpdf->writeHTML($file);
                $res = $mpdf->Output($this->getRegisterOutputDir($this->dossier_id)) == '';
                if ($res)
                    $this->addFileName($doc);
                return $res;
            } catch (Exception $e) {
                return Response()->error($e->getMessage());
            }
        }
    }

    public function getFileName($doc)
    {
        return $this->createFileName($doc);
    }

    public function getCartographyOutputDir()
    {
        $doc = $this->cartography;
        $path = $this->getPath($doc);
        return $path['output'] . DS . $this->createFileName($doc);
    }

    public function getCartographyLinkPath()
    {
        $doc = $this->cartography;
        $path = $this->getPath($doc);
        return $path['link'] . DS . $this->createFileName($doc);
    }

    public function createCartography($data = [])
    {
        $doc = $this->cartography;
        try {
            $this->createDocDirectory($doc);
            $sql = 'SELECT text FROM dossier_documents WHERE dossier_id=' . $this->dossier_id . ' AND name="' . $doc . '" AND shown=1';
            $res = query($sql);
            if ($res && !empty($res[0]['text'])) {
                $res = $res[0]['text'];
                $res = html_entity_decode($res);
            } else {
                return false;
            }
            $body = $res;
            $mpdf = $this->getMPDF();
//            $mpdf->SetHTMLFooter($footer);
            $mpdf->writeHTML($body);
            $res = $mpdf->Output($this->getCartographyOutputDir($this->dossier_id)) == '';
            if ($res)
                $this->addFileName($doc);
            return $res;

        } catch (Exception $e) {
            consoleJS($body);
            consoleJS($sql);
            consoleJS($e->getMessage());
        }
    }

    public function getMinimizationOutputDir()
    {
        $doc = $this->minimization;
        $path = $this->getPath($doc);
        return $path['output'] . DS . $this->createFileName($doc);
    }

    public function getMinimizationLinkPath()
    {
        $doc = $this->minimization;
        $path = $this->getPath($doc);
        return $path['link'] . DS . $this->createFileName($doc);
    }

    public function createMinimization($data = [])
    {
        $doc = $this->minimization;
        try {
            $this->createDocDirectory($doc);
            $sql = 'SELECT text FROM dossier_documents WHERE dossier_id=' . $this->dossier_id . ' AND name="' . $doc . '" AND shown=1';
            $res = query($sql);
            if ($res && !empty($res[0]['text'])) {
                $res = $res[0]['text'];
                $res = html_entity_decode($res);
            } else {
                return false;
            }
            $body = $res;
            $mpdf = $this->getMPDF();
//            $mpdf->SetHTMLFooter($footer);
            $mpdf->writeHTML($body);
            $res = $mpdf->Output($this->getMinimizationOutputDir($this->dossier_id)) == '';
            if ($res)
                $this->addFileName($doc);
            return $res;
        } catch (Exception $e) {
            consoleJS($body);
            consoleJS($sql);
            consoleJS($e->getMessage());
        }
    }

    public function getActions_to_be_takenOutputDir()
    {
        $doc = $this->actions_to_be_taken;
        $path = $this->getPath($doc);
        return $path['output'] . DS . $this->createFileName($doc);
    }

    public function getActions_to_be_takenLinkPath()
    {
        $doc = $this->actions_to_be_taken;
        $path = $this->getPath($doc);
        return $path['link'] . DS . $this->createFileName($doc);
    }

    public function createActions_to_be_taken()
    {
        $doc = $this->actions_to_be_taken;
        try {
            $this->createDocDirectory($doc);
            $sql = 'SELECT text FROM dossier_documents WHERE dossier_id=' . $this->dossier_id . ' AND name="' . $doc . '" AND shown=1';
            $res = query($sql);
            if ($res && !empty($res[0]['text'])) {
                $res = $res[0]['text'];
                $res = html_entity_decode($res);
            } else {
                return false;
            }
            $body = $res;
            $mpdf = $this->getMPDF();
//            $mpdf->SetHTMLFooter($footer);
            $mpdf->writeHTML($body);
            $res = $mpdf->Output($this->getActions_to_be_takenOutputDir($this->dossier_id)) == '';
            if ($res)
                $this->addFileName($doc);
            return $res;
        } catch (Exception $e) {
            consoleJS($body);
            consoleJS($sql);
            consoleJS($e->getMessage());
        }
    }

    public function getFinalityOutputDir()
    {
        $doc = $this->finality;
        $path = $this->getPath($doc);
        return $path['output'] . DS . $this->createFileName($doc);
    }

    public function getFinalityLinkPath()
    {
        $doc = $this->finality;
        $path = $this->getPath($doc);
        return $path['link'] . DS . $this->createFileName($doc);
    }

    public function createFinality($data = [])
    {
        $doc = $this->finality;
        try {
            $this->createDocDirectory($doc);
            $sql = 'SELECT text FROM dossier_documents WHERE dossier_id=' . $this->dossier_id . ' AND name="' . $doc . '" AND shown=1';
            $res = query($sql);
            if ($res && !empty($res[0]['text'])) {
                $res = $res[0]['text'];
                $res = html_entity_decode($res);
            } else {
                return false;
            }
            $body = $res;
            $mpdf = $this->getMPDF();
//            $mpdf->SetHTMLFooter($footer);
            $mpdf->writeHTML($body);
            $res = $mpdf->Output($this->getFinalityOutputDir($this->dossier_id)) == '';
            if ($res)
                $this->addFileName($doc);
            return $res;
        } catch (Exception $e) {
            consoleJS($body);
            consoleJS($sql);
            consoleJS($e->getMessage());
        }
    }

    public function getBillOutputDir($data = ['dfp_id' => 0])
    {
        $doc = $this->bill;
        $path = $this->getPath($doc);
        return $path['output'] . DS . $this->createFileName($doc, $data);
    }

    public function getBillLinkPath($data = ['dfp_id' => 0])
    {
        $doc = $this->bill;
        $path = $this->getPath($doc);
        return $path['link'] . DS . $this->createFileName($doc, $data);
    }

    public function createBill($data = ['dfp_id' => 0])
    {
        $doc = $this->bill;
        try {
            $this->createDocDirectory($doc);
//            $sql = 'SELECT text FROM dossier_fact_payment WHERE id=' . $data['dfp_id'];
//            $res = query($sql);
            $res = [];
            $fl = false;
//            if ($res && !empty($res[0]['text'])) {
//                $res = $res[0]['text'];
//                $res = unserialize(html_entity_decode($res));
//                $fl = true;
//            }
            $mpdf = $this->getMPDF();
            $footer = $fl ? $res['footer'] : Templates::getTemplate('solution_footer');
            $mpdf->SetHTMLFooter($footer);
            $body = $fl ? $res['body'] : Templates::getTemplate('rgpd_bill', ['$dossier_id' => $this->dossier_id, '$dfp' => $data['dfp_id']]);
            $mpdf->writeHTML($body);
            $res = $mpdf->Output($this->getBillOutputDir($data)) == '';
            if ($res) {
                if (!$fl)
                    query(QueryStr()->update_query_string('dossier_fact_payment', ['text' => serialize(['body' => $body, 'footer' => $footer])]) . ' WHERE id=' . $data['dfp_id']);
                $this->addFileName($doc, $data);
            }
            return $res;
        } catch (Exception $e) {
            return false;
        }
    }

    public function getContractOutputDir()
    {
        $doc = $this->contract;
        $path = $this->getPath($doc);
        return $path['output'] . DS . $this->createFileName($doc);
    }

    public function getContractLinkPath()
    {
        $doc = $this->contract;
        $path = $this->getPath($doc);
        return $path['link'] . DS . $this->createFileName($doc);
    }

    public function createContract($data = [])
    {
        $doc = $this->contract;
        try {
            $this->createDocDirectory($doc);
            $sql = 'SELECT result_text FROM dossiers WHERE id=' . $this->dossier_id;
            $res = query($sql);
            if ($res && !empty($res[0]['result_text'])) {
                $res = $res[0]['result_text'];
                $res = htmlspecialchars_decode($res);
            } else {
                return false;
            }
            $mpdf = $this->getMPDF();
            $mpdf->writeHTML(htmlspecialchars_decode($res));
            $res = $mpdf->Output($this->getContractOutputDir($this->dossier_id)) == '';
            if ($res)
                $this->addFileName($doc);
            return $res;
        } catch (Exception $e) {
            consoleJS($e->getMessage());
        }
    }

    public function createAttestation($data = [])
    {
        $doc = $this->contract;
        try {
            $this->createDocDirectory($doc);
            $sql = 'SELECT result_text FROM dossiers WHERE id=' . $this->dossier_id;
            $res = query($sql);
            if ($res && !empty($res[0]['result_text'])) {
                $res = $res[0]['result_text'];
                $res = htmlspecialchars_decode($res);
            } else {
                return false;
            }
            $mpdf = $this->getMPDF();
            $mpdf->writeHTML(htmlspecialchars_decode($res));
            $res = $mpdf->Output($this->getContractOutputDir($this->dossier_id)) == '';
            if ($res)
                $this->addFileName($doc);
            return $res;
        } catch (Exception $e) {
            consoleJS($e->getMessage());
        }
    }
}