<?php


use traits\GetTable;

class AjaxStat
{
    use GetTable;

    private static $table = 'ajax_key_stat';

    public static function update($method, $ajax_key)
    {
        $sql = insert_query_string(self::getTable(), compact('method', 'ajax_key')) . ' ON DUPLICATE KEY UPDATE cnt=cnt+1';
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}