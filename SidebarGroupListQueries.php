<?php


class SidebarGroupListQueries
{
    private static $groups;

    private static function unshift($need, $tmp)
    {
        $current = '';
        foreach (self::$groups as $key => &$row) {
            if ($row[$need] == $tmp) {
                $current = $row;
                unset(self::$groups[$key]);
                break;
            }
        }
        if (!empty($current))
            array_unshift(self::$groups, $current);
    }

    private static function cntGeneralTasks($group, $cnt = 0)
    {
        $tmp_gr = array_filter(self::$groups, function ($el) use ($group) {
            return intval($el['parent_id']) == intval($group['id']);
        });
        if (empty($tmp_gr))
            return $cnt;
        $cnt += array_reduce($tmp_gr, function ($s, $i) {
            return $s += intval($i['cnt_task']);
        }, 0);
        foreach ($tmp_gr as $g)
            $cnt = self::cntGeneralTasks($g, $cnt);
        return $cnt;
    }

    public static function getGroups($project = null)
    {
        $current_user = User();
        $sql = 'SELECT g.*, (SELECT COUNT(t.id) FROM ' . Task::getTable() . ' AS t WHERE t.group_id = g.id AND t.active = 1';
        if (!($current_user->isSupervisor() || $current_user->isAssistant())) {
            $user_id = $current_user->isCollaborator() ? $current_user->getParentId() : $current_user->getId();
            $sql .= ' AND t.id IN (SELECT task_id FROM ' . TaskAccess::getTable() . ' WHERE user_id=' . $user_id . ')';
        }
        $sql .= ') as cnt_task FROM ' . Group::getTable() . ' AS g WHERE 1 ';
        if (!is_null($project) && is_numeric($project)) {
            $project = escape_string(clear_post($project));
            $sql .= ' AND g.general_project_id = ' . $project;
        }
        $sql .= ' ORDER BY cnt_task';
        /*
        if (!is_null($project) && is_numeric($project)) {
            if ($current_user->isSupervisor() || $current_user->isAssistant()) {
                $sql = "
                    SELECT
                    g.*,
                    (SELECT COUNT(t.id) FROM " . Task::getTable() . " AS t WHERE t.group_id = g.id AND t.active = 1) as cnt_task
                    FROM
                    " . Group::getTable() . " AS g
                    WHERE
                    g.general_project_id = $project
                    ORDER BY
                    cnt_task
                    ";
            }
            else {
//        $groups = Access()->getGroupListForUser($current_user->getId(), true);
                $sql = "
                    SELECT
                    g.*,
                    (SELECT COUNT(t.id) FROM " . Task::getTable() . " AS t WHERE t.group_id = g.id AND t.active = 1 AND t.id IN (SELECT task_id FROM " . TaskAccess::getTable() . " WHERE user_id=$user_id)) as cnt_task
                    FROM
                    " . Group::getTable() . " AS g
                    WHERE
                    g.general_project_id = $project
                    ORDER BY
                    cnt_task
                    ";
            }
        }

        else {
            if ($current_user->isSupervisor() || $current_user->isAssistant()) {
                $sql = "
                    SELECT
                    g.*,
                    (SELECT COUNT(t.id) FROM " . Task::getTable() . " AS t WHERE t.group_id = g.id AND t.active = 1) as cnt_task
                    FROM
                    " . Group::getTable() . " AS g
                    ORDER BY
                    cnt_task
                    ";
            }
            else {
                $sql = "
                    SELECT
                    g.*,
                    (SELECT COUNT(id) FROM " . Task::getTable() . " WHERE group_id = g.id AND active = 1 AND id IN (SELECT task_id FROM " . TaskAccess::getTable() . " WHERE user_id=$user_id)) as cnt_task
                    FROM
                    " . Group::getTable() . " AS g
                    LEFT JOIN
                    " . Task::getTable() . " AS t
                    ON
                    g.id = t.group_id
                    WHERE
                    t.id IN (SELECT task_id FROM " . TaskAccess::getTable() . " WHERE user_id=$user_id)
                    ORDER BY
                    cnt_task
                    ";
            }
        }
        */
        try {
            self::$groups = query($sql);
            if (empty(self::$groups))
                return [];
            self::$groups = array_map(function ($el) {
                $cnt = self::cntGeneralTasks($el, 0);
                $el['cnt_task'] = intval($el['cnt_task']) + $cnt;
                return $el;
            }, self::$groups);
            self::unshift('def_gr', 1);
            return self::$groups;
        } catch (Exception $e) {
            consoleJS($e->getMessage());
            return [];
        }

    }
}