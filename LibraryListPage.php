<?php

use abstractClasses\TableData;

class LibraryListPage extends TableData
{
    protected function addFind()
    {
        $findByName = '';
        if (!empty($this->_findBy)) {
            switch ($this->_findField) {
                default:
                    $findByName = ' AND p.' . $this->_findField . ' LIKE "%' . $this->_findBy . '%" ';
            }
        }
        return $findByName;
    }

    protected function addSort()
    {
        if ($this->_sortWay && $this->_sortBy &&
            $this->_sortWay != 'both'
            &&
            $this->_sortBy != 'done'
        ) {
            switch ($this->_sortBy) {
                case 'cntTasks':
                case 'cntGroups':
                    $name = $this->_sortBy;
                    break;
                case 'category':
                    $name = 'c.name';
                    break;
                default:
                    $name = 'p.' . $this->_sortBy;
            }
            $sort = ' ORDER BY ' . $name . ' ' . $this->_sortWay;
        }
        else
            $sort = ' ORDER BY p.id DESC';
        return $sort;
    }

    public function getTableData($columns = ['*'])
    {
        $columns = clearParams($columns);
        $columns_p = array_map(function ($elem) {
            return 'p.' . $elem;
        }, $columns);

        $findByName = $this->addFind();
        $sql = "
SELECT
" . implode(',', $columns_p) . ",
(SELECT COUNT(t.id) FROM " . Task::getTable() . " AS t LEFT JOIN " . Group::getTable() . " AS g ON g.id=t.group_id LEFT JOIN " . Project::getTable() . " AS gp ON gp.id=g.general_project_id WHERE t.active=1 AND gp.id=p.id) as cntTasks,
c.name AS cat_name
FROM
" . Project::getTable() . " AS p
LEFT JOIN
" . Category::getTable() . " AS c
ON
c.id=p.category
WHERE
p.active = 1
AND
p.is_template=1
$findByName
 ";
        $sql .= $this->addSort();
        try {
            $projects_data = query($sql);
            $projectsDoneData = Project::getDonePercentManyProjects(array_column($projects_data, 'id'));
            $statuses = Statuses();
            foreach ($projects_data as &$v) {
                $v['isDefault'] = $v['id'] == Project::getDefaultId();
                $v['done'] = $projectsDoneData[$v['id']];
                if ($this->issetColumnOrAll('dateCreated', $columns))
                    $v['dateCreated'] = date('d-m-Y, H:i', strtotime($v['dateCreated']));
                if ($this->issetColumnOrAll('start', $columns))
                    $v['start'] = date('d-m-Y, H:i', strtotime($v['start']));
                if ($this->issetColumnOrAll('end', $columns))
                    $v['end'] = date('d-m-Y, H:i', strtotime($v['end']));
                if ($this->issetColumnOrAll('priority', $columns))
                    $v['priority'] = Priorities::getKeyById($v['priority']);
                $v['is_template'] = (bool)$v['is_template'];
                $v['is_offer'] = (bool)$v['is_offer'];
                if ($v['is_offer'])
                    $v['descr'] = $v['banner_descr'];
                if ($this->issetColumnOrAll('status', $columns))
                    $v['status_name'] = $statuses->getStatusesById($v['status'])['aliase'];
                if ($this->issetColumnOrAll('price_type', $columns)) {
                    $v['price_type_tmp'] = $v['price_type'];
                    $v['price_type'] = Project()->getPriceType(intval($v['price_type']));
                    if (isset($v['price_type']['obj']))
                        $v['price_type'] = $v['price_type']['obj'];
                }
            }
            return ['projects' => $projects_data];
        } catch (Exception $e) {
        }
    }
}