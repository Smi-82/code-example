<?php

use abstractClasses\AbstractPaymentSystems;
use traits\SingleTone;

class Bitpay_loc extends AbstractPaymentSystems
{
    use SingleTone;

    private function __construct()
    {
        $this->mode = $this->setTestMode();
        $this->data = [
            'name' => 'bitpay'
        ];
    }
}