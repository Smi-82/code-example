<?php

use abstractClasses\TableData;

class ClientListPage extends TableData
{
    protected function addFilters()
    {
        $filters = '';
        if ($this->_filters != null && is_array($this->_filters)) {

            foreach ($this->_filters as $field => $val) {
                if (!empty($val)) {
                    switch ($field) {
                        default:
                            $filters .= ' AND u.' . $field . ' IN (' . implode(', ', $val) . ') ';
                    }
                }
            }
        }
        return $filters;
    }

    protected function addFind()
    {
        if (!empty($this->_findBy)) {
            switch ($this->_findField) {
                case 'id':
                    $findByName = ' AND u.id LIKE "%' . $this->_findBy . '%" ';
                    break;
                case 'clientName':
                    $findByName = ' AND (u.firstname LIKE "%' . $this->_findBy . '%" OR u.lastname LIKE "%' . $this->_findBy . '%") ';
                    break;
                case 'consultant':
                    $findByName = ' AND (c.firstname LIKE "%' . $this->_findBy . '%" OR c.lastname LIKE "%' . $this->_findBy . '%") ';
                    break;
                case 'societe':
                    $findByName = ' AND u.societe LIKE "%' . $this->_findBy . '%" ';
                    break;
                case 'phones':
                    $findByName = ' AND (u.phone LIKE "%' . $this->_findBy . '%" OR u.phone2 LIKE "%' . $this->_findBy . '%") ';
                    break;
                case 'email':
                    $findByName = ' AND u.email LIKE "%' . $this->_findBy . '%" ';
                    break;
                default:
                    $findByName = '';
            }
        }
        else
            $findByName = '';
        return $findByName;
    }

    protected function addSort()
    {
        if ($this->_sortWay == 'both') {
            $sort = ' ORDER BY client_id DESC';
        }
        else {
            switch ($this->_sortBy) {
                case 'id':
                    $sort = ' ORDER BY client_id ' . $this->_sortWay . ' ';
                    break;
                case 'clientName':
                    $sort = ' ORDER BY client_name ' . $this->_sortWay . ' ';
                    break;
                case 'clientName':
                    $sort = ' ORDER BY u.societe ' . $this->_sortWay . ' ';
                    break;
                case 'phones':
                    $sort = ' ORDER BY phones ' . $this->_sortWay . ' ';
                    break;
                case 'email':
                    $sort = ' ORDER BY u.email ' . $this->_sortWay . ' ';
                    break;
                default:
                    $sort = '';
            }

        }
        return $sort;
    }

    protected function addPage($findByName = '')
    {
        if (!empty($this->_currentPage) && $this->_cntRows) {
            if (
                empty($findByName)
                &&
                $this->_sortWay == 'both'
                ||
                $this->_sortBy != 'ready'
            ) {
                return ' LIMIT ' . ($this->_currentPage * $this->_cntRows - $this->_cntRows) . ', ' . ($this->_cntRows);
            }
        }
        return '';
    }

    public function getTableData()
    {
        $current_user = User();
        $sql = '
SELECT
u.img,
u.id AS client_id,
TRIM(CONCAT(u.phone, IF(u.phone != "" AND u.phone2 != "", ", ", ""), u.phone2)) AS phones,
u.email,
u.societe,
u.usertype,
u.lastlogin,
TRIM(CONCAT(c.firstname," ", c.lastname)) AS curator_name,
TRIM(CONCAT(u.firstname," ", u.lastname)) AS client_name,
u.status
FROM
' . User::getTable() . ' AS u
LEFT JOIN
' . User::getTable() . ' AS c
ON
c.id = u.consultant_id
WHERE u.usertype=' . UserTypes()->getClient();
        $local_filters = '';
        if ($current_user->isRGPDCurator()) {
            $consultant = new Consultant();
            $children_with_current = [];
            $children = $consultant->getChildrensId($current_user->getId());
            if ($children)
                $children_with_current = $children;
            $children_with_current[] = $current_user->getId();
            $local_filters .= ' AND u.consultant_id IN (' . implode(',', $children_with_current) . ')';
            $sql .= $local_filters;
        }
        elseif ($current_user->isRGPDVendor()) {
            $local_filters .= ' AND u.consultant_id = ' . $current_user->getId();
            $sql .= $local_filters;
        }

        $sql .= $findByName = $this->addFind();
        $sql .= $this->addFilters();
        $sql .= $this->addSort();
        $sql .= $this->addPage($findByName);

        try {
            $result = query($sql);
            if (!empty($result)) {
                $statuses = ClientStatus::getAll(['id', 'name']);
                $statuses = valueToKey('id', $statuses);
                $result = array_map(function ($el) use ($statuses) {
                    $el['status_name'] = $statuses[$el['status']]['name'];
                    $lastlogin = strtotime($el['lastlogin']);
                    $el['lastlogin'] = $lastlogin > 0 ? date('d-m-Y H:i', $lastlogin) : 0;
                    return $el;
                }, $result);
            }
            $sql1 = 'SELECT COUNT(u.id) AS cnt FROM ' . User::getTable() . ' AS u WHERE u.usertype=' . UserTypes()->getClient();
            $sql1 .= $findByName;
            $sql1 .= $this->addFilters();
            if (!empty($local_filters))
                $sql1 .= $local_filters;
            $cntData = querySingle($sql1)['cnt'];
        } catch (Exception $e) {
            return $e->getMessage();
        }
        return compact('result', 'cntData');
    }
}