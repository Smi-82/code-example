<?php

use traits\BaseSelect;
use traits\GetTable;

class ProjectClientMessages
{
    private static $table = 'project_client_messages';
    use getTable;
    use baseSelect;

    public static function add($data)
    {
        $sql = insert_query_string(self::getTable(), $data);
        try {
            $res = query($sql);
            return $res ? getLastId() : $res;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function updateById($ids, $data)
    {
        if (isset($data['id']))
            unset($data['id']);
        $ids = getAsArray($ids);
        $sql = update_query_string(self::getTable(), $data) . ' WHERE id IN (' . implode(',', $ids) . ')';
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function getById($message_id, $columns = ['*'])
    {
        $message_id = escape_string($message_id);
        $sql = self::baseSelect($columns) . ' WHERE id = ' . $message_id . ' LIMIT 0, 1';
        try {
            return querySingle($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}