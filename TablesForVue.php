<?php

class TablesForVue
{
    protected $v_if_for_seach;
    protected $th_arr;
    protected $thead;
    protected $radio;
    protected $input_seach;
    protected $th;
    protected $select;
    protected $paginate;
    protected $filter;
    protected $table;

    public function __construct($table_selector = '')
    {
        $this->table = $table_selector;
        $radio_name = '_radio';
        $i = '_i_';
        $this->paginate = [
            'cntPages' => ' SMI_getCntPages(\'' . $this->table . '\')',
            'action' => 'SMI_getTableObjByTable(\'' . $this->table . '\',\'obj\').changePage',
            'v-model' => 'table_obj_for_mixin_smi.' . $this->table . '.currentPage',
//            'current' => 'SMI_getCurrentPage(\'' . $this->table . '\')',
            'inputPage' => ' type="number" min="1" :max="SMI_getCntPages(\'' . $this->table . '\')" @change="changePage(\'' . $this->table . '\', $event.target.value)" ',
        ];
        $this->radio = [
            'id' => ' :id="\'' . $radio_name . '_' . $this->table . '-\'+' . $i . '" ',
            'label_for' => ' :for="\'' . $radio_name . '_' . $this->table . '-\'+' . $i . '" ',
            'name' => ' :name="\'optionsRadios_' . $this->table . '\'" ',
            'checked' => ' :checked="th.field == SMI_getFindField(\'' . $this->table . '\')" ',
            'value' => ' :value="th.field" ',
            'v-model' => ' table_obj_for_mixin_smi.' . $this->table . '.find_field ',
            'event' => ' @click="SMI_changeRadioSearchBy(\'' . $this->table . '\',$event.target.value)"',
        ];
        $this->radio['input_all_attrs'] = $this->radio['id'] . ' ' . $this->radio['value'] . ' ' . $this->radio['v-model'] . ' ' . $this->radio['checked'] . ' ';
        $this->input_seach = [
            'v-model' => ' table_obj_for_mixin_smi.' . $this->table . '.find_by ',
            'value' => ' SMI_getFindBy(\'' . $this->table . '\') ',
            'event' => ' SMI_setFindBy(\'' . $this->table . '\',$event.target.value) '
        ];
        $this->th = [
            'forEach' => ' v-for="(th, ' . $i . ') in SMI_getThArr(\'' . $this->table . '\')" ',
            'show' => ' v-if="th.show" ',
            'sortClass' => ' :class="[{sortable: SMI_getThArr(\'' . $this->table . '\',' . $i . ').sort}, SMI_getThArr(\'' . $this->table . '\',' . $i . ').sort_class]" ',
            'text' => '<div v-if="th.html && th.html != \'\' " v-html="th.html"></div><div v-else>{{th.text_b}}{{ucfirst(lng(th.aliase))}}{{th.text_a}}</div>',
            'event' => ' @click="SMI_changeSort(\'' . $this->table . '\',' . $i . ')" ',
            'html' => ' {{{th.html}}} '
        ];
        $this->body = [
            'forEach' => ' SMI_getResult(\'' . $this->table . '\') '
        ];
        $this->cntData = [
            'cntData' => ' SMI_getCntData(\'' . $this->table . '\')'
        ];
        $this->showRecords = [
            'cnt' => ' SMI_getCntRows(\'' . $this->table . '\')',
            'v-model' => 'table_obj_for_mixin_smi.' . $this->table . '.cntRows',
            'arr' => 'SMI_getCntDataArr(\'' . $this->table . '\')',
            'v-if' => '(SMI_getResultLength(\'' . $this->table . '\'))',
            'change' => 'SMI_changeCntRows(\'' . $this->table . '\', $event.target.value)',

        ];
        $this->select = [
            'event' => ' @change="SMI_selectSort(\'' . $this->table . '\',$event.target.value)" ',
        ];
        $this->v_if_for_seach = ' v-if="th.for_seach" ';
        $this->thead = '<thead>
                            <tr>
                                <th class="text-center v-align-middle" ' . $this->th['forEach'] . '>
                                    <div class="th-inner sortable sorting" ' . $this->th['sortClass'] . ' ' .
            $this->th['event'] . '>' .
            $this->th['text'] . '
                                    </div>
                                </th>
                            </tr>
                        </thead>';
        $this->th_arr = [];
    }

    public function vIfFilter($key)
    {
        return 'SMI_vIfFilter(\'' . $this->table . '\',\'' . $key . '\')';
    }

    public function addSortForSelect($fildName, $way = 'asc')
    {
        return ' value="' . $fildName . ',' . strtoupper($way) . ' " ';
    }

    public function getFilterAction($field, $ind = 1)
    {
        return ' @click="SMI_filterTable(\'' . $this->table . '\',\'' . $field . '\',' . $ind . ',$event.target)" ';
    }

    public function clear_filter()
    {
        return ' v-if="(SMI_getFiltersKeysLength(\'' . $this->table . '\')) != 0" @click="SMI_clear_filter(\'' . $this->table . '\')" ';
    }

    public function getFilterCheckStatus($field, $ind = 1)
    {
        return ' :checked="SMI_getFilterCheckStatus(\'' . $this->table . '\',\'' . $field . '\',' . $ind . ')" ';
    }

    public function __get($name)
    {
        if (property_exists($this, $name))
            return $this->$name;
        else return '';
    }

    public function addTHColumn(array $arr, $toFront = false)
    {
        if ($toFront) {
            array_unshift($this->th_arr, $arr);
            return true;
        }
        $this->th_arr[] = $arr;
    }

    public function getThColumn($ind)
    {
        return $this->th_arr[$ind] ?? [];
    }

    public function addThead(array $arr = [])
    {
        $this->th_arr = array_merge($this->th_arr, $arr);
    }

    public function getThEvent($i)
    {
        return str_replace('_i_', $i, $this->th['event']);

    }

    public function getThSortClass($i)
    {
        return str_replace('_i_', $i, $this->th['sortClass']);
    }

    public function getThArr($asJson = false)
    {
        $fl = false;
        foreach ($this->th_arr as &$v) {
            if (isset($v['is_basic_for_sort'])) {
                $fl = true;
                break;
            }
        }
        if (!$fl) {
            $this->th_arr[0]['is_basic_for_sort'] = true;
        }
        foreach ($this->th_arr as &$v) {
            $v['sort'] = isset($v['sort_class']);
            $v['show'] = $v['show'] ?? true;
        }
        return $asJson ? json_encode($this->th_arr) : $this->th_arr;
    }
}