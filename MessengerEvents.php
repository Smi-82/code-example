<?php

use traits\BaseSelect;
use traits\GetTable;
use traits\SingleTone;

class MessengerEvents
{
    use SingleTone;
    use GetTable;
    use BaseSelect;

    /*
     * CREATE TABLE `event_notifications` (
     `id` int(11) NOT NULL AUTO_INCREMENT,
     `user_id` int(11) NOT NULL COMMENT 'Global id',
     `project_id` int(11) NOT NULL,
     `task_id` int(11) NOT NULL,
     `bill_id` int(11) NOT NULL,
     `data` text NOT NULL,
     `code` varchar(100) NOT NULL,
     `message` text NOT NULL,
     `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
     `watched_date` datetime NOT NULL,
     PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8
     */
    private static $table = 'event_notifications';

    private static $url = 'https://messenger.glooda.com/api/v1/tasks/sendEvent';

    private $params = [];

    private $receivers = [];
    private $data = [];
    private $outerData = [];

    private $dataRequirements = [];

    private $vars = [];

    public $codes;


    private function setOutputData($receivers, $messages, $taskId)
    {
        $this->receivers = $receivers;
        if ($messages) $this->setOutputDataMessages($messages);
        if ($taskId) $this->setOutputDataTaskId($taskId);
    }

    private function setOutputDataReceivers($receivers)
    {
        $this->receivers = $receivers;
    }

    private function setOutputDataMessages($messages)
    {
        $this->data['messages'] = $messages;
    }

    private function setOutputDataTaskId($taskId)
    {
        $this->data['task_id'] = $taskId;
    }

    private function setOutputDataProjectId($projectId)
    {
        $this->data['project_id'] = $projectId;
    }

    private function setOutputDataBillId($billId)
    {
        $this->data['bill_id'] = $billId;
    }

    private function setOutputDataEventCode($code)
    {
        $this->data['code'] = $code;
    }
    private function postRequest($url, $params = [], $data = [])
    {

# Create a connection
        $ch = curl_init($url);
# Form data string
        if (isset($params['headers']))
            if (in_array('Content-Type: application/json', $params['headers']))
                $postString = json_encode($data);
            else
                $postString = http_build_query($data, '', '&');


//    $postString = http_build_query($data, '', '&');
//    $postString = json_encode($data);
# Setting our options
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postString);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if (isset($params['headers'])) curl_setopt($ch, CURLOPT_HTTPHEADER, $params['headers']);
//    if (isset($params['headers']) && is_array($params['headers'])) {
//
//        foreach ($params['headers'] as $header)
//            curl_setopt($ch, CURLOPT_HTTPHEADER, [$header]);
//    }
# Get the response
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }
    public function __get($name)
    {
        if (!isset($this->vars[$name]))
            throw new Exception('No such variable in ' . get_class($this));
        return $this->vars[$name];
    }

    public function __set($name, $value)
    {
        $this->vars[$name] = $value;
    }

    public function setOuterData($key, $data)
    {
        $this->outerData[$key] = $data;
        return $this;
    }

    public function getOutputData()
    {
        return [
            'receivers' => $this->receivers,
            'data' => $this->data
        ];
    }

    private function setDataRequirements()
    {
        $codes = $this->codes;
        $this->dataRequirements[$codes::PROJECT_CREATED] = [
            'project_id'
        ];
        $this->dataRequirements[$codes::CONFIRM_PROJECT_EXPRESSION_OF_NEEDS] = [
            'project_id'
        ];
        $this->dataRequirements[$codes::TASK_STATUS_CHANGE_TO_WAITING_FOR_VALIDATION_BY_THE_CLIENT] = [
            'task_id'
        ];
        $this->dataRequirements[$codes::TASK_STATUS_CHANGE_TO_IN_DEVELOPMENT] = [
            'task_id'
        ];
        $this->dataRequirements[$codes::TASK_STATUS_CHANGE_TO_FINALIZED_NOT_CONTROLLED] = [
            'task_id'
        ];
        $this->dataRequirements[$codes::TASK_STATUS_CHANGE_TO_VALIDATED_TECHNICAL_INSPECTION] = [
            'task_id'
        ];
        $this->dataRequirements[$codes::TASK_STATUS_CHANGE_TO_DOUBLE_CHECK_VALIDATED] = [
            'task_id'
        ];
        $this->dataRequirements[$codes::TASK_STATUS_CHANGE_TO_APPROVED_BY_THE_CUSTOMER] = [
            'task_id'
        ];
        $this->dataRequirements[$codes::CONFIRM_PURCHASE_ORDER] = [
//            'project_id',
            'bill_id'
        ];
        $this->dataRequirements[$codes::PAY_INVOICE] = [
            'project_id'
        ];
        $this->dataRequirements[$codes::START_SCREENSHOT_TASK] = [
            'task_id',
            'user_id'
        ];
        $this->dataRequirements[$codes::END_SCREENSHOT_TASK] = [
            'task_id',
            'user_id'
        ];
    }

    public function __construct()
    {
        $this->params = [
            'headers' => [
                'Content-Type: application/json'
            ]
        ];
        $this->codes = MessengerEventCodes();
        $this->setDataRequirements();
    }

    public static function getUrl()
    {
        return self::$url;
    }

    public function getNotifications()
    {
        $sql = self::baseSelect();
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function getNotificationsCountByGlobalUserId($global_id)
    {
        $sql = 'SELECT COUNT(id) AS cnt FROM ' . self::getTable() . ' WHERE user_id = ' . $global_id;
        try {
            $result = querySingle($sql);
            return $result['cnt'];
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function getNotificationsCountByUserId($user_id)
    {
        $us_obj = User();
        if (!$us_obj->getUser() || $us_obj->getId() != $user_id) {
            $user_id = @User::getById($user_id, ['global_id']);
            $user_id = $user_id['global_id'];
        } else {
            $user_id = $us_obj->getGId();
        }
        return $this->getNotificationsCountByGlobalUserId($user_id);
    }

    public function getNotificationsByGlobalUserId($user_id, $exclude_id = [])
    {
        $sql = self::baseSelect() . ' WHERE `user_id` = ' . $user_id;
        if (!empty($exclude_id)) {
            $exclude_id = clearParams($exclude_id);
            $sql .= ' AND id NOT IN (' . implode(',', $exclude_id) . ')';
        }
        $sql .= ' ORDER BY id DESC';
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function insertNotification($data)
    {
        return query(insert_query_string(self::getTable(), $data));
    }

    public function updateNotification($id, $data)
    {
        return query(update_query_string(self::getTable(), $data) . ' WHERE `id` = ' . escape_string($id));
    }

    public function deleteNotification($ids)
    {
        $ids = getAsArray($ids);
        $sql = 'DELETE FROM ' . self::getTable() . ' WHERE `id` IN (' . implode(',', $ids) . ')';
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function deleteNotificationByBillIdAndCode($billId, $code)
    {
        $billId = escape_string($billId);
        $code = escape_string($code);
        $sql = 'DELETE FROM ' . self::getTable() . ' WHERE bill_id = ' . $billId . ' AND code = \'' . $code . '\'';
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function createOutputData($eventCode, $inputData = [])
    {
        $codes = $this->codes;
        if (!defined(get_class($codes) . '::' . $eventCode)) throw new Exception('Wrong event code.');
        if (!$this->validateData($eventCode, $inputData)) {
            throw new Exception('Wrong data.');
        }
        $receivers = [];
        $outputData = [];
        $messageParams = [];

        switch ($eventCode) {
            case $codes::PROJECT_CREATED:
                $project = Project()->getById($inputData['project_id'], ['name']);
                $client = Project()->getClient($inputData['project_id'], ['global_id', 'usertype', 'firebase_tokens']);
                if (!count($client)) break;
                $receivers[] = $client['global_id'];
                $params = ['notification_event__PROJECT_CREATED', $project['name']];
                $messagesByUsertype = [
                    [
                        'usertype' => Usertypes()->getClient(),
                        'message' => call_user_func_array('lngr', $params),
                        'params' => $params
                    ]
                ];
                $messages = $this->createMessages([$client], $messagesByUsertype);
                break;
            case $codes::TASK_STATUS_CHANGE_TO_WAITING_FOR_VALIDATION_BY_THE_CLIENT:
                if (!isset($this->outerData['old_status'])) throw new Exception('Old status is undefined');
                $task = Task::getById($inputData['task_id'], ['name']);
                $project = Task()->getProject($inputData['task_id'], ['id', 'name']);
                $client = Project()->getClient($project['id'], ['global_id', 'usertype', 'firebase_tokens']);
                if (!count($client)) break;

                $receivers[] = $client['global_id'];
                $params = ['notification_event__TASK_STATUS_CHANGE_TO_WAITING_FOR_VALIDATION_BY_THE_CLIENT', $task['name'], $this->outerData['old_status']];
                $messagesByUsertype = [
                    [
                        'usertype' => Usertypes()->getClient(),
                        'message' => call_user_func_array('lngr', $params),
                        'params' => $params
                    ]
                ];
                $messages = $this->createMessages([$client], $messagesByUsertype);
                break;
            case $codes::TASK_STATUS_CHANGE_TO_IN_DEVELOPMENT:
                if (!isset($this->outerData['old_status'])) throw new Exception('Old status is undefined');
                $task = Task::getById($inputData['task_id'], ['name']);
                $project = Task()->getProject($inputData['task_id'], ['id', 'name']);
                $client = Project()->getClient($project['id'], ['global_id', 'usertype', 'firebase_tokens']);
                if (!count($client)) break;
                $receivers[] = $client['global_id'];
                $params = ['notification_event__TASK_STATUS_CHANGE_TO_IN_DEVELOPMENT', $task['name'], $this->outerData['old_status']];
                $messagesByUsertype = [
                    [
                        'usertype' => Usertypes()->getClient(),
                        'message' => call_user_func_array('lngr', $params),
                        'params' => $params
                    ]
                ];
                $messages = $this->createMessages([$client], $messagesByUsertype);
                break;
            case $codes::TASK_STATUS_CHANGE_TO_FINALIZED_NOT_CONTROLLED:
                if (!isset($this->outerData['old_status'])) throw new Exception('Old status is undefined');
                $task = Task::getById($inputData['task_id'], ['name']);
                $project = Task()->getProject($inputData['task_id'], ['id', 'name']);
                $client = Project()->getClient($project['id'], ['global_id', 'usertype', 'firebase_tokens']);
                if (!count($client)) break;
                $receivers[] = $client['global_id'];
                $params = ['notification_event__TASK_STATUS_CHANGE_TO_FINALIZED_NOT_CONTROLLED', $task['name'], $this->outerData['old_status']];
                $messagesByUsertype = [
                    [
                        'usertype' => Usertypes()->getClient(),
                        'message' => call_user_func_array('lngr', $params),
                        'params' => $params
                    ]
                ];
                $messages = $this->createMessages([$client], $messagesByUsertype);
                break;
            case $codes::TASK_STATUS_CHANGE_TO_VALIDATED_TECHNICAL_INSPECTION:
                if (!isset($this->outerData['old_status'])) throw new Exception('Old status is undefined');
                $task = Task::getById($inputData['task_id'], ['name']);
                $project = Task()->getProject($inputData['task_id'], ['id', 'name']);
                $client = Project()->getClient($project['id'], ['global_id', 'usertype', 'firebase_tokens']);
                if (!count($client)) break;
                $receivers[] = $client['global_id'];
                $params = ['notification_event__TASK_STATUS_CHANGE_TO_VALIDATED_TECHNICAL_INSPECTION', $task['name'], $this->outerData['old_status']];
                $messagesByUsertype = [
                    [
                        'usertype' => Usertypes()->getClient(),
                        'message' => call_user_func_array('lngr', $params),
                        'params' => $params
                    ]
                ];
                $messages = $this->createMessages([$client], $messagesByUsertype);
                break;
            case $codes::TASK_STATUS_CHANGE_TO_DOUBLE_CHECK_VALIDATED:
                if (!isset($this->outerData['old_status'])) throw new Exception('Old status is undefined');
                $task = Task::getById($inputData['task_id'], ['name']);
                $project = Task()->getProject($inputData['task_id'], ['id', 'name']);
                $client = Project()->getClient($project['id'], ['global_id', 'usertype', 'firebase_tokens']);
                if (!count($client)) break;
                $receivers[] = $client['global_id'];
                $params = ['notification_event__TASK_STATUS_CHANGE_TO_DOUBLE_CHECK_VALIDATED', $task['name'], $this->outerData['old_status']];
                $messagesByUsertype = [
                    [
                        'usertype' => Usertypes()->getClient(),
                        'message' => call_user_func_array('lngr', $params),
                        'params' => $params
                    ]
                ];
                $messages = $this->createMessages([$client], $messagesByUsertype);
                break;
            case $codes::TASK_STATUS_CHANGE_TO_APPROVED_BY_THE_CUSTOMER:
                if (!isset($this->outerData['old_status'])) throw new Exception('Old status is undefined');
                $task = Task::getById($inputData['task_id'], ['name']);
                $project = Task()->getProject($inputData['task_id'], ['id', 'name']);
                $client = Project()->getClient($project['id'], ['global_id', 'usertype', 'firebase_tokens']);
                if (!count($client)) break;
                $receivers[] = $client['global_id'];
                $params = ['notification_event__TASK_STATUS_CHANGE_TO_APPROVED_BY_THE_CUSTOMER', $task['name'], $this->outerData['old_status']];
                $messagesByUsertype = [
                    [
                        'usertype' => Usertypes()->getClient(),
                        'message' => call_user_func_array('lngr', $params),
                        'params' => $params
                    ]
                ];
                $messages = $this->createMessages([$client], $messagesByUsertype);
                break;
            case $codes::CONFIRM_PROJECT_EXPRESSION_OF_NEEDS:
                $project = Project()->getById($inputData['project_id'], ['name']);
                $client = Project()->getClient($inputData['project_id'], ['global_id', 'usertype', 'firebase_tokens']);
                if (!count($client)) break;
                $receivers[] = $client['global_id'];
                $params = ['notification_event__CONFIRM_PROJECT_EXPRESSION_OF_NEEDS', $project['name']];
                $messagesByUsertype = [
                    [
                        'usertype' => Usertypes()->getClient(),
                        'message' => call_user_func_array('lngr', $params),
                        'params' => $params

                    ]
                ];
                $messages = $this->createMessages([$client], $messagesByUsertype);
                break;
            case $codes::CONFIRM_PROJECT_EXPRESSION_OF_NEEDS_CLIENT:
                $project = Project()->getById($inputData['project_id'], ['name']);
                $client = Project()->getClient($inputData['project_id'], ['global_id', 'usertype', 'firebase_tokens']);
                if (!count($client)) break;
                $receivers[] = $client['global_id'];
                $params = ['notification_event__CONFIRM_PROJECT_EXPRESSION_OF_NEEDS', $project['name']];
                $messagesByUsertype = [
                    [
                        'usertype' => Usertypes()->getClient(),
                        'message' => call_user_func_array('lngr', $params),
                        'params' => $params
                    ]
                ];
                $messages = $this->createMessages([$client], $messagesByUsertype);
                break;
            case $codes::CONFIRM_PROJECT_EXPRESSION_OF_NEEDS_ADMIN:
                $project = Project()->getById($inputData['project_id'], ['name']);
                $admin = User::getById(User()->getId(), ['global_id', 'usertype', 'firebase_tokens']);
                if (!count($admin)) break;
                $receivers[] = $admin['global_id'];
                $params = ['notification_event__CONFIRM_PROJECT_EXPRESSION_OF_NEEDS_ADMIN', $project['name']];
                $messagesByUsertype = [
                    [
                        'usertype' => $admin['usertype'],
                        'message' => call_user_func_array('lngr', $params),
                        'params' => $params
                    ]
                ];
                $messages = $this->createMessages([$admin], $messagesByUsertype);
                break;
            case $codes::CONFIRM_PURCHASE_ORDER:
                $billId = $inputData['bill_id'];
                $bill = BillProject::getById($billId, ['project_id']);
                if (!$bill) break;
                $projectId = $bill['project_id'];
                $project = Project()->getById($projectId, ['name']);
                $client = Project()->getClient($projectId, ['global_id', 'usertype', 'firebase_tokens']);
                if (!count($client)) break;
                $receivers[] = $client['global_id'];
                $params = ['notification_event__CONFIRM_PURCHASE_ORDER', $project['name']];
                $messagesByUsertype = [
                    [
                        'usertype' => Usertypes()->getClient(),
                        'message' => call_user_func_array('lngr', $params),
                        'params' => $params
                    ]
                ];
                $messages = $this->createMessages([$client], $messagesByUsertype);
                break;
            case $codes::PAY_INVOICE:
                $project = Project()->getById($inputData['project_id'], ['name']);
                $client = Project()->getClient($inputData['project_id'], ['global_id', 'usertype', 'firebase_tokens']);
                if (!count($client)) break;
                $receivers[] = $client['global_id'];
                $params = ['notification_event__PAY_INVOICE', $project['name']];
                $messagesByUsertype = [
                    [
                        'usertype' => Usertypes()->getClient(),
                        'message' => call_user_func_array('lngr', $params),
                        'params' => $params

                    ]
                ];
                $messages = $this->createMessages([$client], $messagesByUsertype);
                break;
            case $codes::START_SCREENSHOT_TASK:
                $user = User::getById($inputData['user_id'], ['global_id', 'firebase_tokens']);
                if (!$user) break;
                $receivers[] = $user['global_id'];
                $taskId = $inputData['task_id'];
                break;
            case $codes::END_SCREENSHOT_TASK:
                $user = User::getById($inputData['user_id'], ['global_id', 'firebase_tokens']);
                if (!$user) break;
                $receivers[] = $user['global_id'];
                break;
        }

        $this->setOutputDataReceivers($receivers);
        $this->setOutputDataEventCode($eventCode);
        if (isset($messages)) $this->setOutputDataMessages($messages);
        if (isset($taskId)) $this->setOutputDataTaskId($taskId);
        if (isset($projectId)) $this->setOutputDataProjectId($projectId);
        if (isset($billId)) $this->setOutputDataBillId($billId);

//        $this->setOutputData($receivers, isset($messages) ? $messages : null, isset($taskId) ? $taskId : null);
        return $this;

    }

    public function send($eventCode, $data)
    {
//        ini_set('display_errors', 1);
//        ini_set('display_startup_errors', 1);
//        error_reporting(-1);
        $this->createOutputData($eventCode, $data);
        if (!count($this->receivers)) return false;
        $outputData = $this->getOutputData();

        $codes = $this->codes;
        $success = false;
        $firebaseData = [];

        switch ($eventCode) {
            case $codes::PROJECT_CREATED:
            case $codes::TASK_STATUS_CHANGE_TO_WAITING_FOR_VALIDATION_BY_THE_CLIENT:
            case $codes::TASK_STATUS_CHANGE_TO_IN_DEVELOPMENT:
            case $codes::TASK_STATUS_CHANGE_TO_FINALIZED_NOT_CONTROLLED:
            case $codes::TASK_STATUS_CHANGE_TO_VALIDATED_TECHNICAL_INSPECTION:
            case $codes::TASK_STATUS_CHANGE_TO_DOUBLE_CHECK_VALIDATED:
            case $codes::TASK_STATUS_CHANGE_TO_APPROVED_BY_THE_CUSTOMER:
            case $codes::CONFIRM_PROJECT_EXPRESSION_OF_NEEDS:
            case $codes::CONFIRM_PROJECT_EXPRESSION_OF_NEEDS_CLIENT:
            case $codes::CONFIRM_PROJECT_EXPRESSION_OF_NEEDS_ADMIN:
            case $codes::CONFIRM_PURCHASE_ORDER:
            case $codes::PAY_INVOICE:
                foreach ($outputData['data']['messages'] as $message) {
                    if (!$message['tokens']) continue;
                    $tokens = explode(',', str_replace('"', '', $message['tokens']));
                    FirebaseEvents::setOutputData($tokens, 'some title', $message['message']);
                    FirebaseEvents::send();
                }
                break;
            default:
                $this->postRequest(self::$url, $this->params, $outputData);
                break;
        }

        $projectId = isset($data['project_id']) ? $data['project_id'] : null;
        $taskId = isset($data['task_id']) ? $data['task_id'] : null;
        $billId = isset($data['bill_id']) ? $data['bill_id'] : null;

        if (!$projectId) $projectId = isset($outputData['data']['project_id']) ? $outputData['data']['project_id'] : null;
        if (!$taskId) $taskId = isset($outputData['data']['task_id']) ? $outputData['data']['task_id'] : null;
        if (!$billId) $billId = isset($outputData['data']['bill_id']) ? $outputData['data']['bill_id'] : null;

        if (isset($outputData['data']['messages'])) foreach ($outputData['data']['messages'] as $item) {
            $insertData = [
                'project_id' => $projectId,
                'task_id' => $taskId,
                'bill_id' => $billId,
                'user_id' => $item['id'],
                'message' => $item['message'],
                'data' => json_encode([
                    'project_id' => $projectId,
                    'task_id' => $taskId,
                    'bill_id' => $billId,
                    'message' => $item['message'],
                    'message_params' => $item['params']
                ], JSON_UNESCAPED_UNICODE),
                'code' => $eventCode
            ];

            $this->insertNotification($insertData);
        }
    }


    private function validateData($eventCode, $data)
    {
        if (!is_array($data) || empty($data)) return false;
        if (!isset($this->dataRequirements[$eventCode])) return false;
        $requirements = $this->dataRequirements[$eventCode];
        return count(array_diff($requirements, array_keys($data))) == 0;
    }

    private function createMessages($users, $messages)
    {
        $output = [];
        foreach ($users as $user) {
            foreach ($messages as $message) {
                if ($message['usertype'] == $user['usertype']) {
                    $output[] = [
                        'id' => $user['global_id'],
                        'tokens' => $user['firebase_tokens'],
                        'message' => $message['message'],
                        'params' => $message['params'] ?? null
                    ];
                }
            }
        }
        return $output;
    }

    public function cronPayInvoice($daysBefore)
    {
        $payment_parts = PaymentPartsProjectBill::getAllUnpaidOfInvoices();
        $today = date_create();
        $todayF = $today->format('Y-m-d H:i:s');
        $todayYMD = $today->format('Y-m-d');
        $today = date_create($todayYMD);
        $test = [];
        foreach ($daysBefore as $_daysBefore) {

            foreach ($payment_parts as $payment_part) {
                $lastNotifiedDate = date_create($payment_part['last_notified_date']);
                if ($lastNotifiedDate->format('Y-m-d') == $todayYMD) {
                    continue;
                }
                $date = date_create($payment_part['payment_date']);
                if (!$date) continue;

                $date_diff = date_diff($today, $date);
                $days = intval($date_diff->days);
                if (
                    (($days == $_daysBefore) && !$date_diff->invert && $_daysBefore >= 0) ||
                    (($days == abs($_daysBefore)) && $date_diff->invert && $_daysBefore < 0)
                ) {
                    $project_id = $payment_part['project_id'];
                    $bill_id = $payment_part['project_bill_id'];
                    $this->send(MessengerEventCodes::PAY_INVOICE, compact('project_id', 'bill_id'));
                    PaymentPartsProjectBill::update($payment_part['id'], ['last_notified_date' => $todayF]);
                }
            }
        }
    }

    public function cronConfirmPurchaseOrder($daysAfter)
    {
        $bills = BillProject()->getAllUnapprovedBills(['id', 'project_id', 'created', 'last_notified_date']);
        $today = date_create();
        $todayF = $today->format('Y-m-d H:i:s');
        $todayYMD = $today->format('Y-m-d');
        $today = date_create($todayYMD);
        $test = [];
        foreach ($daysAfter as $_daysBefore) {

            foreach ($bills as $bill) {
                $lastNotifiedDate = date_create($bill['last_notified_date']);
                if ($lastNotifiedDate->format('Y-m-d') == $todayYMD) {
                    continue;
                }
                $date = date_create($bill['created']);
                $date = date_create($date->format('Y-m-d'));
                if (!$date) continue;

                $date_diff = date_diff($today, $date);
                $days = intval($date_diff->days);
                if (
                    (($days == $_daysBefore) && !$date_diff->invert && $_daysBefore >= 0) ||
                    (($days == abs($_daysBefore)) && $date_diff->invert && $_daysBefore < 0)
                ) {
                    $project_id = $bill['project_id'];
                    $this->send(MessengerEventCodes::CONFIRM_PURCHASE_ORDER, compact('project_id'));
                    BillProject()->update($bill['id'], ['last_notified_date' => $todayF]);
                }
            }
        }
    }
}