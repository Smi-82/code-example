<?php

namespace abstractClasses;
abstract class TableData
{
    protected $_sortBy;
    protected $_cntRows;
    protected $_currentPage;
    protected $_findBy;
    protected $_findField;
    protected $_sortWay;
    protected $_filters;

    protected function addPage($findByName = '')
    {
        if (!empty($this->_currentPage) && $this->_cntRows != 0) {
            return ' LIMIT ' . ($this->_currentPage * $this->_cntRows - $this->_cntRows) . ', ' . ($this->_cntRows);
        }
        return '';
    }

    protected function addFilters()
    {
        $filters = '';
        if ($this->_filters != null && is_array($this->_filters)) {

            foreach ($this->_filters as $fileld => $val) {
                if (!empty($val)) {
                    switch ($fileld) {
                        default:
                            $filters .= ' AND ' . $fileld . ' IN (' . implode(', ', $val) . ') ';
                    }
                }
            }
        }
        return $filters;
    }

    protected function addSort()
    {
        if ($this->_sortWay && $this->_sortBy &&
            $this->_sortWay != 'both'
            &&
            $this->_sortBy != 'done'
        ) {
            switch ($this->_sortBy) {
                default:
                    $name = $this->_sortBy;
            }
            $sort = ' ORDER BY ' . $name . ' ' . $this->_sortWay;
        }
        else
            $sort = ' ORDER BY id ASC ';
        return $sort;
    }

    protected function addFind()
    {
        $findByName = '';
        if (!empty($this->_findBy)) {
            switch ($this->_findField) {
                default:
                    $findByName = ' AND ' . $this->_findField . ' LIKE "%' . $this->_findBy . '%" ';
            }
        }
        return $findByName;
    }

    protected function issetColumnOrAll($column, &$allColumns)
    {
        return in_array($column, $allColumns) || in_array('*', $allColumns);
    }

    public function __construct()
    {
        $data = $_POST['table_data'] ?? [];
        $this->_cntRows = isset($data['cntRows']) ? intval(escape_string(clear_post($data['cntRows']))) : 0;
        $this->_currentPage = isset($data['currentPage']) ? intval(escape_string(clear_post($data['currentPage']))) : '';
        $this->_findBy = isset($data['find_by']) ? escape_string(clear_post($data['find_by'])) : '';
        $this->_findField = isset($data['find_field']) ? escape_string(clear_post($data['find_field'])) : '';
        $this->_sortBy = isset($data['sort_by']) ? escape_string(clear_post($data['sort_by'])) : '';
        $this->_sortWay = isset($data['sort_way']) ? escape_string(clear_post($data['sort_way'])) : 'both';
        $this->_filters = !empty($data['filters']) ? $data['filters'] : null;
    }
}