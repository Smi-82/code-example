<?php
namespace abstractClasses;
abstract class AbstractPaymentSystems
{
    protected $mode;
    protected $data;

    protected function getDataByKey($key)
    {
        if (!isset($this->data[$key]))
            throw new Exception('No such key - "' . $key . '" in ' . strtoupper($this->getName()) . ' payment system ');
        return $this->data[$key];
    }

    public function __get($key)
    {
        return $this->getDataByKey($key);
    }

    public function getData()
    {
        return $this->data;
    }

    public function getMode()
    {
        return $this->mode;
    }

    public function setLiveMode()
    {
        $this->mode = 'live';
        return $this;
    }

    public function setTestMode()
    {
        $this->mode = 'test';
        return $this;
    }

    public function isLiveMode()
    {
        return $this->mode === 'live';
    }

    public function getName()
    {
        return $this->name;
    }
}