<?php

use abstractClasses\TableData;

class NewClientsList extends TableData
{
    protected function addFind()
    {
        if (!empty($this->_findBy)) {
            switch ($this->_findField) {
                default:
                    $findByName = ' AND u.' . $this->_findField . ' LIKE "%' . $this->_findBy . '%" ';
            }
        }
        else
            $findByName = '';
        return $findByName;
    }

    protected function addSort()
    {
        if ($this->_sortWay == 'both') {
            $sort = ' ORDER BY u.id ASC ';
        }
        else {
            $sort = ' ORDER BY ' . $this->_sortBy . ' ' . $this->_sortWay . ' ';
        }
        return $sort;
    }

    protected function addPage($findByName = '')
    {
        return (!empty($this->_currentPage) && $this->_cntRows != 0) ? ' LIMIT ' . ($this->_currentPage * $this->_cntRows - $this->_cntRows) . ', ' . ($this->_cntRows) : '';
    }

    public function getTableData()
    {
        $platforms = implode(',', array_map(function ($el) {
            return '"' . $el . '"';
        }, ['en', 'il']));
        $sql = '
SELECT
u.id,
u.name,
u.mail,
u.phone,
u.whenAdded,
u.platform
FROM
' . Feedback::getTable() . ' AS u WHERE u.platform IN (' . $platforms . ') ';

        $sql .= $findByName = $this->addFind();
        $sql .= $this->addSort();
        $sql .= $this->addPage($findByName);
        try {
            $sql1 = 'SELECT COUNT(u.id) AS cntRows FROM ' . Feedback::getTable() . ' AS u WHERE u.platform IN (' . $platforms . ') ';
            $sql1 .= $findByName;
            $cntRows = querySingle($sql1)['cntRows'];
            $users = query($sql);
            $users = array_map(function ($el) {
                $el['whenAdded'] = date('d-m-Y H:i:s', strtotime($el['whenAdded']));
                return $el;
            }, $users);
            return ['users' => $users, 'cntRows' => $cntRows];
        } catch (Exception $e) {
            return ['error' => $e->getTrace()];
        }
    }
}