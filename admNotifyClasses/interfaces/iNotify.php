<?php

namespace admNotifyClasses\interfaces;
interface iNotify
{
    public function add();

    public function getMsg(array &$val);

    public function getHref(array &$val);

}