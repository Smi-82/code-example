<?php


namespace admNotifyClasses\abstracts;


use admNotifyClasses\interfaces\iNotify;
use traits\GetTable;


abstract class NotifyWorker implements iNotify
{
    protected $worker = null;
    protected static $table = 'notify';
    protected $class_name;
    protected $result;
    use GetTable;

    protected static function createArr($who_do, $who_see, $class_name, $object_id)
    {
        return compact('who_do', 'who_see', 'class_name', 'object_id');
    }

    protected static function addData($data)
    {
        $data = array_filter($data, function ($el) {
            return intval($el['who_do']) != intval($el['who_see']);
        });
        if (empty($data)) return false;
        $sql = multiple_insert_str(self::getTable(), $data);
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function getClassName()
    {
        return $this->class_name;
    }

    public function setDefaults($class_name = __CLASS__)
    {
        $class_name = explode('\\', $class_name);
        $this->class_name = array_pop($class_name);
        $this->result = [];
    }

    public function add()
    {
        return $this->worker->add();
    }

    public function getMsg(array &$val)
    {
        return $this->worker->getMsg($val);
    }

    public function getHref(array &$val)
    {
        return $this->worker->getHref($val);
    }
}