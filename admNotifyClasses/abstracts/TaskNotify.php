<?php

namespace admNotifyClasses\abstracts;

use models\UserModel;
use Task;
use TaskAccess;

abstract class TaskNotify extends NotifyWorker
{
    protected $task_id;
    protected $object = 'task_id';
    protected $class_name;
    protected $result;

    public function __construct($task_id = 0)
    {
        $this->task_id = escape_string(clear_post($task_id));
    }

    protected function deleteByClassAndObj($class, $task_id)
    {
        $sql = 'DELETE FROM ' . self::getTable() . ' WHERE class_name="' . $class . '" AND object_id=' . $task_id;
        try {
            return query($sql);
        } catch (Exception $e) {
            return false;
        }
    }

    protected function createMsgHref($field, &$arr)
    {
        if (!empty($arr) && !isset($this->result[$arr['object_id']][$field])) {
            $this->result[$arr['object_id']][$field] = (Task::getById($arr['object_id'], [$field]))[$field];
            return true;
        }
        else
            return false;
    }

    protected function whoMustSee()
    {
        $current_user = User()->getId();
        $user_ids = [];
        $sql = 'SELECT DISTINCT user_id FROM ' . TaskAccess::getTable() . ' WHERE task_id = ' . $this->task_id . ' AND user_id !=' . $current_user;
        try {
            $res = query($sql);
        } catch (Exception $e) {
            $res = [];
        }
        if (empty($res)) return $res;
        $res = array_column($res, 'user_id');
        $user_ids = array_merge($user_ids, $res);
        if (!empty($res)) {
            $sql = UserModel::baseSelect(['parent_id']) . ' WHERE id IN (' . implode(',', $res) . ') AND parent_id !=0 AND id != ' . $current_user;
            $res = UserModel::query($sql);
            if (!empty($res)) {
                $res = array_column($res, 'parent_id');
                $user_ids = array_merge($user_ids, $res);
            }
        }
        $user_ids = array_merge($user_ids, $res);
        return array_unique($user_ids);
    }
}