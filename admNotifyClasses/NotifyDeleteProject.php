<?php

namespace admNotifyClasses;

use admNotifyClasses\abstracts\NotifyWorker;

class NotifyDeleteProject extends NotifyWorker
{
    private $project_id;

    public function __construct($project_id = 0, $data = [])
    {
        $this->project_id = $project_id;
        $this->setDefaults(__CLASS__);
    }

    public function whoMustSee()
    {
        return [1];
    }

    public function add()
    {
        $who_do = User()->getUser() ? User()->getId() : 0;
        $user_ids = $this->whoMustSee();
        $tmp = [];
        foreach ($user_ids as $who_see)
            $tmp[] = self::createArr($who_do, $who_see, $this->getClassName(), $this->project_id);
        self::addData($tmp);
    }

    public function getMsg(array &$val = [])
    {
        return mb_ucfirst(lng('project')) . ': ' . $val['object_id'] . ' ' . lng('deleted');
    }

    public function getHref(array &$val = [])
    {
        return '';
    }
}