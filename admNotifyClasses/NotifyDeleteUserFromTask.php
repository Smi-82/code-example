<?php

namespace admNotifyClasses;





use admNotifyClasses\abstracts\TaskNotify;

class NotifyDeleteUserFromTask extends TaskNotify
{
    private $_whoMustSee = 0;

    private function _msg(array &$val)
    {
        return $val['who_do'] . ' ' . lng('removed_you_from_the_task') . ' ' . $this->result[$val['object_id']]['name'];
    }

    public function whoMustSee($task_id = 0, $user_id = 0)
    {
        $this->_whoMustSee = escape_string(clear_post($user_id));
        $this->task_id = escape_string(clear_post($task_id));
    }

    public function __construct($task_id = 0, $data = [])
    {
        $this->setDefaults(__CLASS__);
        parent::__construct($task_id);
    }

    public function add()
    {
        $arr = self::createArr(User()->getId(), $this->_whoMustSee, $this->getClassName(), $this->task_id);
        self::addData([$arr]);
    }

    public function getMsg(array &$val = [])
    {
        $res = $this->createMsgHref('name', $val);
        return $res ? $this->_msg($val) : '';
    }

    public function getHref(array &$val = [])
    {
        return '';
    }
}