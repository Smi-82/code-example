<?php

namespace admNotifyClasses;


use admNotifyClasses\abstracts\NotifyWorker;
use User;

class NotifyNewClient extends NotifyWorker
{
    private $clientid;


    public function __construct($clientid = 0, $data = [])
    {
        $this->setDefaults(__CLASS__);
        $this->clientid = escape_string(clear_post($clientid));
    }

    public function whoMustSee()
    {
        $res = User::getByTypeId(UserTypes()->getSupervisor(), ['id']);
        return $res ? array_column($res, 'id') : [1];
//        $sql = 'SELECT id FROM ' . User::getTable() . ' WHERE usertype=' . UserTypes()->getSupervisor();
//        $res = query($sql);
//        return $res ? array_column($res, 'id') : [];
//        return [1];
    }

    public function add()
    {
        $current_user = User()->getUser() ? User()->getId() : 0;
        $user_ids = $this->whoMustSee();
        $tmp = [];
        foreach ($user_ids as $who_see)
            $tmp[] = self::createArr($current_user, $who_see, $this->getClassName(), $this->clientid);
        self::addData($tmp);
    }

    public function getMsg(array &$val = [])
    {
        try {
            if (!isset($this->result[$val['object_id']])) {
                $client = User::getById($val['object_id'], ['firstname', 'lastname']);
                $client['client'] = trim(strtoupper($client['firstname']) . ' ' . strtoupper($client['lastname']));
                unset($client['firstname']);
                unset($client['lastname']);
                $this->result[$val['object_id']] = $client;
            }
            return mb_ucfirst(lng('registered_a_new_client')) . $this->result[$val['object_id']]['client'];
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function getHref(array &$val = [])
    {
        return '';
    }
}