<?php

namespace admNotifyClasses;


use admNotifyClasses\abstracts\TaskNotify;

class NotifyTaskStatus extends TaskNotify
{
    private function _msg(array &$val)
    {
        return $val['who_do'] . ' ' . lng('changed_status_in_the_task') . ' ' . $this->result[$val['object_id']]['name'];
    }

    private function _href(array &$val)
    {
        return 'single_task.php?' . http_build_query(['id' => $val['object_id']]);
    }

    public function __construct($task_id = 0, $data = [])
    {
        $this->setDefaults(__CLASS__);
        parent::__construct($task_id);
    }

    public function add()
    {
        $current_user = User()->getId();
        $user_ids = $this->whoMustSee();
        $tmp = [];
        $this->deleteByClassAndObj($this->getClassName(), $this->task_id);
        foreach ($user_ids as $who_see)
            $tmp[] = self::createArr($current_user, $who_see, $this->getClassName(), $this->task_id);
        self::addData($tmp);
    }

    public function getMsg(array &$val = [])
    {
        $res = $this->createMsgHref('name', $val);
        return $res ? $this->_msg($val) : '';
    }

    public function getHref(array &$val = [])
    {
        $res = $this->createMsgHref('category', $val);
        return $res ? $this->_href($val) : '';
    }
}