<?php

use traits\BaseSelect;
use traits\GetTable;

class TasksMessages
{
    private static $table = 'tasks_messages';
    use getTable;
    use baseSelect;

    public static function getMessagesByTask($task_id)
    {
        $task_id = escape_string(clear_post($task_id));
        $sql = 'SELECT a.*,
                b.`firstname` as `user_name`,
                b.`lastname` as `user_lastname`,
                b.`usertype` as `user_usertype`,
                b.`img` as `user_img`,
                c.`name` as `usertype_name`
                FROM ' . self::getTable() . ' a
                LEFT JOIN ' . User::getTable() . ' b ON a.`user_id` = b.`id`
                LEFT JOIN ' . UserTypes::getTable() . ' c ON b.`usertype` = c.`id`
                WHERE a.`task_id` = ' . $task_id;
        try {
            return query($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function setRead($id, $isread = 1)
    {
        return self::updateById($id, compact('isread'));
    }

    public static function add($data)
    {
        $sql = insert_query_string(self::getTable(), $data);
        try {
            $res = query($sql);
            return $res ? getLastId() : $res;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function getById($id, $columns = ['*'])
    {
        $id = escape_string($id);
        $sql = self::baseSelect($columns) . " WHERE $id = '$id'";
        try {
            return querySingle($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function updateById($ids, $data)
    {
        if (isset($data['id']))
            unset($data['id']);
        $ids = getAsArray($ids);
        $sql = update_query_string(self::getTable(), $data) . ' WHERE id IN (' . implode(',', $ids) . ')';
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function updateByIdAndUserId($ids, $user_ids, $data)
    {
        if (isset($data['id']))
            unset($data['id']);
        $ids = getAsArray($ids);
        $user_ids = getAsArray($user_ids);
        $sql = update_query_string(self::getTable(), $data) . ' WHERE id IN (' . implode(',', $ids) . ') AND user_id IN (' . implode(',', $user_ids) . ')';
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function delete($id)
    {
        $id = escape_string(clear_post($id));
        $sql = 'DELETE FROM' . self::getTable() . 'WHERE id=' . $id;
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function deleteByIdAndUserId($id, $user_id)
    {
        $id = escape_string(clear_post($id));
        $user_id = escape_string(clear_post($user_id));
        $sql = 'DELETE FROM ' . self::getTable() . ' WHERE id=' . $id . ' AND user_id=' . $user_id;
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}