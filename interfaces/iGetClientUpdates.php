<?php

namespace interfaces;
interface iGetClientUpdates
{
    public static function getClientUpdates($row);
}