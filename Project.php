<?php

use appEvents\AppEventsController;
use appEvents\subscribers\controller\AppEventsSubscribersController;
use models\ProjectClientMessagesModel;
use models\ProjectModel;
use traits\BaseSelect;
use traits\ClassName;
use traits\SingleTone;

class Project
{
    use ClassName;
    use SingleTone;
    use BaseSelect;

    private static $mainFolderName = 'projects';
    private static $mainFolder = ROOT . '/upload/projects';
    private static $clientAttachmentsDirName = 'client_attachments';
    private static $questionnaireDirName = 'questionnaire';
    private static $default_id = 243;
    private static $fake_id_prefix = 21000;
    private $price_types;
    private $variables;
    private $upload_dir;
    private $folder;
    private $baseFields;

    const HISTORY_MESSAGES_ACTION_ADD = 'HISTORY_MESSAGES_ACTION_ADD';
    const HISTORY_MESSAGES_ACTION_UPDATE = 'HISTORY_MESSAGES_ACTION_UPDATE';
    const HISTORY_MESSAGES_ACTION_DELETE = 'HISTORY_MESSAGES_ACTION_DELETE';

    private function __construct()
    {
        $this->folder = 'projects';

        $this->price_types = [
            0 => ['aliase' => 'by_tasks', 'obj' => 'by_tasks', 'show' => false],
            1 => ['aliase' => 'hourly_payment', 'obj' => 'hour', 'show' => true],
            2 => ['aliase' => 'package', 'obj' => 'project', 'show' => true],
            3 => ['aliase' => 'subscription', 'obj' => 'period', 'show' => true],
            4 => ['aliase' => 'not_charged', 'obj' => 'not_charged', 'show' => true],
        ];
        $this->baseFields = [
            'name' => 'default name',
            'user_id' => User()->getId()
        ];
        $this->variables = [];
        $this->upload_dir = self::geMainFolder();
    }

    private static function _getPriceTypeId($arr)
    {
        $res = array_keys($arr);
        return array_shift($res);
    }

    private function createDirectory($project_id)
    {
        Folder::create(self::geMainFolder() . DS . $project_id);
        $this->upload_dir .= DS . $project_id . DS;
    }

    private function createClientAttachmentsDirectory($project_id, $user_id)
    {
        $dir = self::getClientAttachmentPath($project_id, $user_id);
        Folder::create($dir);
    }

    public function getBaseFields()
    {
        return $this->baseFields;
    }

    public static function getFakeIdPrefix()
    {
        return self::$fake_id_prefix;
    }

    public static function getFakeId($id)
    {
        return intval($id) + self::getFakeIdPrefix();
    }

    public static function getRealId($fake_id)
    {
        return intval($fake_id) - self::getFakeIdPrefix();
    }

    public static function getTable()
    {
        return ProjectModel::getTable();
    }

    public static function getByWhoCreated($user_id, $columns = ['*'])
    {
        return ProjectModel::getByWhoCreated($user_id, $columns);
    }

    public static function createQuestionnaireDirectory($project_id)
    {
        $inst = self::getInst();
        $inst->createDirectory($project_id);
        $dir = $inst->upload_dir;
        $dir .= self::$questionnaireDirName . DS;
        Folder::create($dir);
        return changeSlash($dir);
    }

    public static function deleteQuestionnaireFiles($project_id, $file_name)
    {
        $project_id = escape_string(clear_post($project_id));
        $dir = self::geMainFolder() . DS . $project_id . DS . self::$questionnaireDirName . DS;
        $dir = changeSlash($dir);
        return !file_exists($dir . $file_name) || Folder::delete($dir . $file_name);
    }

    public static function getQuestionnaire($project_id)
    {
        $res = self::getInst()->getById($project_id, ['questionnaire']);
        $res = $res['questionnaire'];
        return json_decode($res, JSON_OBJECT_AS_ARRAY);
    }

    public static function addQuestionnaire($project_id, $data)
    {
        return self::getInst()->editProjectData($project_id, ['questionnaire' => json_encode($data)]);
    }

    public static function addQuestionnaireFiles($project_id)
    {
        $project_id = escape_string(clear_post($project_id));
        $ret = [];
        if (isset($_FILES['myfile'])) {
            $error = $_FILES['myfile']['error'];
            if (!is_array($_FILES['myfile']['name'])) //single file
            {
                $fileName = $_FILES['myfile']['name'];
                $dir = self::createQuestionnaireDirectory($project_id);
                $dir .= $fileName;
                move_uploaded_file($_FILES['myfile']['tmp_name'], $dir);
                $ret[] = $fileName;
            }
        }
        return $ret;
    }

    public static function getQuestionnaireFiles($project_id)
    {
        $dir = self::createQuestionnaireDirectory($project_id);
        $dir = UploadDir::getRootPath(self::$mainFolderName . DS . $project_id . DS . self::$questionnaireDirName . DS);
        $dir1 = DS . MODULE . UploadDir::getRelativePath() . self::getInst()->folder . DS . $project_id . DS . self::$questionnaireDirName . DS;
        $dir1 = changeSlash($dir1);
        $dir = changeSlash($dir);
        $files = @scandir($dir);
        if (!empty($files)) {
            $files = array_filter($files, function ($elem) use ($dir) {
                return !in_array($elem, ['.', '..']) && !is_dir($dir . DS . $elem);
            });
        }
        $ret = [];
        if (!empty($files)) {
            foreach ($files as $val)
                $ret[] = [
                    'name' => $val,
                    'path' => $dir1 . $val,
                    'size' => filesize($dir . $val),
                ];
            return json_encode($ret);
        }
        return '';
    }

    private function createPath($project_id, $fileName = '')
    {
        $res = self::geMainFolder() . DS . $project_id;
        if (!empty($fileName))
            $res .= DS . $fileName;
        return $res;
    }

    public function __set($name, $value)
    {
        $this->variables[$name] = $value;
    }

    public static function getTimeSpent($id)
    {
        $task_id = self::getOwnTasks($id, ['id']);
        if (empty($task_id)) return 0;
        $task_id = array_column($task_id, 'id');
        $res = Task::getTimeSpent($task_id);
        return array_sum($res);
    }

    public static function getTimePackages($id, array $params = ['*'])
    {
        return TimePackagesProject::getByProjectId($id, $params);
    }

    public function __get($name)
    {
        return !isset($this->variables[$name]) ? false : $this->variables[$name];
    }

    public static function getAsAllTypes($fl = 1)
    {
        return array_merge(self::getAsTemplateType($fl), self::getAsOfferType($fl));
    }

    public static function getAsTemplateType($fl = 1)
    {
        return ['is_template' => $fl];
    }

    public static function getAsOfferType($fl = 1)
    {
        return ['is_offer' => $fl];
    }

    public static function getProjectsSubscrForPayment()
    {
        return ProjectModel::getProjectsSubscrForPayment();
    }

    public static function getDonePercentManyProjects($ids)
    {
        $ids = getAsArray($ids);
        $result = ProjectModel::getDonePercentManyProjects($ids);
        $tmp_arr = [];
        $status_obj = Statuses();
        foreach ($ids as $val) {
            $filter_arr = array_filter($result, function ($el) use ($val) {
                return intval($el['id']) == intval($val);
            });
            $sum_tasks = array_sum(array_column($filter_arr, 'cnt_task'));
            $res = array_map(function ($el) use ($status_obj) {
                return intval($el['cnt_task']) * intval($status_obj->getStatusesById($el['status'], 'weight'));
            }, $filter_arr);
            $tmp_arr[$val] = $sum_tasks ? round(array_sum($res) / $sum_tasks) : 0;
        }
        return $tmp_arr;
    }

    public static function getDonePercent($project_id)
    {
        $res = self::getDonePercentManyProjects($project_id);
        return $res[$project_id];
    }

    public static function createPrice($proj, $tva)
    {
        $price = floatval($proj['price']);
        $tva = floatval($tva);
        $discount = floatval($proj['discount']);
        switch (intval($proj['price_type'])) {
            case intval(self::getIdPriceTypeByHourlyPayment()):
                $price *= ($proj['evaluation'] / 3600);
                break;
        }
        if (intval($proj['prepaid']))
            $price -= $discount;

        $price += $price * $tva / 100;
        switch (intval($proj['price_type'])) {
            case intval(self::getIdPriceTypeByHourlyPayment()):
                return floor($price);
            default:
                return round($price, 2);
        }
    }

    public static function nextPayment($shed, $lastDate)
    {
        $curr = 0;
        if (!is_null($lastDate) && intval(preg_replace('/\D+/', '', $lastDate))) {
            $curr = array_search(strtotime($lastDate), array_map(function ($el) {
                return strtotime($el);
            }, array_column($shed, 'date')));
        }
        return isset($shed[$curr + 1]) ? ['current' => $shed[$curr], 'next' => $shed[$curr + 1]] : ['current' => $shed[$curr], 'next' => false];
    }

    public static function getAllOffers($params = ['*'])
    {
        return ProjectModel::getAllOffers($params);
    }

    public static function getPriceTypeByAliase($aliase)
    {
        return array_filter(self::getInst()->price_types, function ($el) use ($aliase) {
            return $el['aliase'] == $aliase;
        });
    }

    public static function getIdPriceTypeByHourlyPayment()
    {
        return self::_getPriceTypeId(self::getPriceTypeByAliase('hourly_payment'));
    }

    public static function getIdPriceTypeByTasks()
    {
        return self::_getPriceTypeId(self::getPriceTypeByAliase('by_tasks'));
    }

    public static function getIdPriceTypeByPackage()
    {
        return self::_getPriceTypeId(self::getPriceTypeByAliase('package'));
    }

    public static function getIdPriceTypeBySubscription()
    {
        return self::_getPriceTypeId(self::getPriceTypeByAliase('subscription'));
    }

    public static function getIdPriceTypeByNotCharged()
    {
        return self::_getPriceTypeId(self::getPriceTypeByAliase('not_charged'));
    }

    public static function getPeriods()
    {
        return [
            'week' => 1,
            'month' => 2,
            'year' => 3,
        ];
    }

    public static function createPaymentSchedule($schedule, $dateStartPayment, $price, $beforAfter)
    {
        $price = floatval($price);
        $schedule_data = json_decode($schedule, JSON_OBJECT_AS_ARRAY);
        $periods = self::getPeriods();
        $periods = array_flip($periods);
        $schedule_data = array_map(function ($el) use ($periods) {
            $el['radio'] = $periods[$el['radio']];
            return $el;
        }, $schedule_data);
        if (is_null($dateStartPayment)) {
            $dateStartPayment = date('d-m-Y H:i:s');
        }

        $schedule_data['per']['seconds'] = strtotime($dateStartPayment . ' +' . $schedule_data['per']['val'] . ' ' . $schedule_data['per']['radio']) - strtotime($dateStartPayment);
        $schedule_data['freq']['seconds'] = getSeconds(1, $schedule_data['freq']['radio'][0]) / $schedule_data['freq']['val'];
        $cnt = ceil($schedule_data['per']['seconds'] / $schedule_data['freq']['seconds']);
        $part = ceil(($price / $cnt) * 100) / 100;
        $arr = [];
        $dateStartPayment = date('d-m-Y H:i:s', strtotime($dateStartPayment));
        if (intval($beforAfter))
            $date = date('d-m-Y H:i:s', strtotime($dateStartPayment) + $schedule_data['freq']['seconds']);
        else
            $date = $dateStartPayment;
        $rest = $price;
        do {
            $rest -= $part;
            if ($rest <= 0)
                $part += $rest;
            $arr[] = [
                'date' => $date,
                'price' => round($part, 2),
                'rest' => $rest
            ];
            $date = date('d-m-Y H:i:s', round(strtotime($date) + $schedule_data['freq']['seconds']));
        } while ($rest > 0);
        $last = array_pop($arr);
        $last['date'] = @date('d-m-Y H:i:s', strtotime($arr[0]['date'] . ' +' . $schedule_data['per']['val'] . ' ' . $schedule_data['per']['radio']));
        $arr[] = $last;
        return $arr;
    }

    public static function getByPriceType($type, array $columns = ['*'])
    {
        return ProjectModel::getByPriceType($type, $columns);
    }

    public function deleteAccess($project_id, $user_id)
    {
        return ProjectAccess::delete($project_id, $user_id);
    }

    public function addUser($project_id, $user_id, $field = null)
    {
        $res = ProjectModel::addUser($project_id, $user_id, $field);
        if (!$res)
            return false;
        $this->addAccess($project_id, $user_id);
        if (!$this->isTemplate($project_id)) {
            $tasks = self::getOwnTasks($project_id, ['id']);
            $tasks = array_column($tasks, 'id');
            foreach ($tasks as $task) {
                TaskAccess::add($task, $user_id);
            }
        }
        return true;
    }

    public static function getDefaultId()
    {
        return self::$default_id;
    }

    public static function changeCategory($from, $to)
    {
        return ProjectModel::changeCategory($from, $to);
    }

    public function getReady($project_id)
    {
        $project_id = escape_string(clear_post($project_id));
        $cnt = count(self::getOwnTasks($project_id, ['id']));
        $done = ProjectModel::getReady($project_id);
        return round(intval($done) * 100 / $cnt);
    }

    public function getMessageById($message_id, $params = ['*'])
    {
        return ProjectClientMessages::getById($message_id, $params);
    }

    public function getByMessageId($message_id, $params = ['*'])
    {
        return ProjectModel::getByMessageId($message_id, $params);
    }

    public function getBalanceForNewBill($project_id)
    {
        $project_id = escape_string(clear_post($project_id));
        $amount = $this->getById($project_id, ['price'])['price'];
        $balance = BillProject()->getSumPaymentByProjectId($project_id);
        $amount = floatval($amount);
        $balance = floatval($balance);
        $balance -= $balance * floatval(OurCompany()->tva) / 100;
        return $amount - $balance;
    }

    public static function geMainFolder()
    {
        return self::$mainFolder;
    }

    public function getImgLink($project_id, $fileName = '')
    {
        $project_id = escape_string(clear_post($project_id));
        $fileName = escape_string(clear_post($fileName));
        if (empty($fileName)) {
            $fileName = $this->getById($project_id, ['img']);
            if ($fileName)
                $fileName = $fileName['img'];
            if (empty($fileName))
                return '';
        }
        $upl = UploadDir::getRelativePath();
        $upl = ltrim($upl, '/');
        if (file_exists($upl . $this->folder . DS . $project_id . DS . $fileName)) {
            return $upl . $this->folder . DS . $project_id . DS . $fileName;
        }
        return '';
    }

    public function deleteImg($project_id, $file_name = '')
    {
        $project_id = escape_string(clear_post($project_id));
        $dir = self::geMainFolder() . DS . $project_id . DS;
        if (empty($file_name)) {
            $file_name = $this->getById($project_id, ['img']);
            if ($file_name) {
                $file_name = $file_name['img'];
            }
        }
        if ($file_name && file_exists($dir . $file_name)) {
            $res = Folder::delete($dir . $file_name);
            if ($res) {
                $this->editProjectData($project_id, ['img' => '']);
            }
        }
        return true;
    }

    public function getOfferImgDataJS($project_id)
    {
        $project_id = escape_string(clear_post($project_id));
        $dir = UploadDir::getRootPath(self::$mainFolderName . DS . $project_id . DS);
        $dir1 = DS . MODULE . UploadDir::getRelativePath() . $this->folder . DS . $project_id . DS;
        $dir1 = changeSlash($dir1);
        $dir = changeSlash($dir);
        $files = @scandir($dir);
        if (!empty($files)) {
            $files = array_filter($files, function ($elem) use ($dir) {
                return !in_array($elem, ['.', '..']) && !is_dir($dir . DS . $elem);
            });
        }
        $file = '';
        if (!empty($files)) {
            $file = array_shift($files);
            $ret = [[
                'name' => $file,
                'path' => $dir1 . $file,
                'size' => filesize($dir . $file),
            ]];
            return json_encode($ret);
        }
        return '';
    }

    public function getPriceType($id = null)
    {
        if (is_numeric($id) && isset($this->price_types[intval($id)])) {
            return $this->price_types[intval($id)];
        }
        return $this->price_types;
    }

    public function getPriceTypesForJS()
    {
        $res = [];
        foreach ($this->price_types as $k => $v) {
            $v['id'] = $k;
            $res[$v['aliase']] = $v;
        }
        return $res;
    }

    public static function getAccessDataByProjectIdS($project_ids, array $params = ['*'])
    {
        return ProjectAccess::getAccessDataByProjectIdS($project_ids, $params);
    }

    public static function getAccessData($project_id, array $params = ['*'])
    {
        return ProjectAccess::getAccessData($project_id, $params);
    }

    public function addFoto($project_id)
    {
        $project_id = escape_string(clear_post($project_id));
        $this->createDirectory($project_id);
        $ret = [];
        if (isset($_FILES["myfile"])) {

            $error = $_FILES["myfile"]["error"];
            if (!is_array($_FILES["myfile"]["name"])) //single file
            {
                $fileName = $_FILES["myfile"]["name"];
                move_uploaded_file($_FILES["myfile"]["tmp_name"], $this->upload_dir . $fileName);

                //resize image to max height 1200 px
//                resizeImg($this->upload_dir . $fileName);
                $this->editProjectData($project_id, ['img' => $fileName]);
                $ret[] = $fileName;
            }
            return $ret;
        }
    }

    public function add($data1, $withDefGroup = true)
    {
        if (!$this->project_fields)
            $this->project_fields = array_flip(DBManager()->getTableColumns(self::getTable()));
        $current_user = User();
        if (isset($data1['id']))
            unset($data1['id']);
        $user_id = $current_user->isCollaborator() ? $current_user->getParentId() : $current_user->getId();
        $fields = [
            'user_id' => $user_id,
        ];
        $fields = array_merge($data1, $fields);
        if (isset($data1['evaluation']))
            $fields['evaluation'] = @daysHoursMinutesToSeconds($data1['evaluation']['days'], $data1['evaluation']['hours']);

        $fields['is_template'] = $data1['isTemplate'] ?? 0;
        $fields['is_offer'] = $data1['isOffer'] ?? 0;
        if (isset($data1['is_template']))
            $fields['is_template'] = $data1['is_template'];
        if (isset($data1['is_offer']))
            $fields['is_offer'] = $data1['is_offer'];
        if (isset($data1['per_freq']))
            $fields['schedule'] = json_encode($data1['per_freq']);

        if (isset($data1['descr']))
            $fields['descr'] = htmlspecialchars($data1['descr']);
        if (!isset($data1['show_tasks_to_client']))
            $fields['show_tasks_to_client'] = 0;
        if (!isset($data1['lng']))
            $fields['lng'] = Language::getDefault();

        $fields['start'] = isset($data1['start']) ? date('Y-m-d H:i:s', strtotime($data1['start'])) : date('Y-m-d H:i:s');
        $fields['end'] = isset($data1['end']) ? date('Y-m-d H:i:s', strtotime($data1['end'])) : date('Y-m-d H:i:s', strtotime($fields['start'] . ' +1 month'));
        $fields = array_intersect_key($fields, $this->project_fields);

        $res = [
            'error' => '',
            'project' => 0
        ];
        $general_project_id = ProjectModel::add($fields);
        if ($general_project_id && is_numeric($general_project_id)) {
            // If is offer
            if (intval($fields['is_offer']) == 1 && !intval($fields['is_template'])) {
                $tmp = User::getClientsRegisteredByHimself(['id']);
                $tmp = array_column($tmp, 'id');
                Offer::addManyClientsToOffer($tmp, $general_project_id);
            }
            //-------------------
            $res['project'] = $general_project_id;
            if (isset($data1['participants']) && !empty($data1['participants'])) {
                foreach ($data1['participants'] as $part)
                    $this->addAccess($general_project_id, $part);
            }

            if ($current_user->isCollaborator()) {
                CollaboratorProjectAccess::add($current_user->getId(), $general_project_id);
            }
            if ($withDefGroup) {
                if (!isset($data1['def_gr']))
                    $data1['def_gr'] = lng('default_name');
                Group()->setProject($general_project_id)->setName($data1['def_gr'])->setUser($current_user->getId())->setDefault(1)->addGroup();
            }
            AppEventsController::publish(AppEventsSubscribersController::newProjectEvent(), ['project_id' => $general_project_id, 'proj_name' => $fields['name'], 'is_template' => $fields['is_template'], 'is_offer' => $fields['is_offer']]);
        }
        else {
            return $res;
        }
        return $res;
    }

    public static function getCntProjectsForMainClient($client_id)
    {
        return ProjectModel::getCntProjectsForMainClient($client_id);
    }

    public static function getCntProjectsByUserId($user_ids)
    {
        $user_ids = getAsArray($user_ids);
        $user_ids = array_flip($user_ids);
        foreach ($user_ids as $key => $id) {
            $user_ids[$key] = count(array_unique(self::getProjectListForUser($key)));
        }
        return $user_ids;
    }

    public static function getAccessDataForUser($user_id, array $params = ['*'])
    {
        return ProjectAccess::getAccessDataForUser($user_id, $params);
    }

    public static function getProjectListForUser($user_id, $asStr = false)
    {
        $user_id = escape_string(clear_post($user_id));
        $tmp_result = [];
        /**
         * projects created by the user
         */
        $result = ProjectModel::getProjectListForUser($user_id);
        if ($result) {
            $result = array_column($result, 'id');
            $tmp_result = array_merge($tmp_result, $result);
        }

        $result = self::getAccessDataForUser($user_id, ['general_project_id']);

        if ($result) {
            $result = array_column($result, 'general_project_id');
            $tmp_result = array_merge($tmp_result, $result);
        }
        /**
         * projects to which the user has been added
         */
        if ($user_id == User()->getId()) {
            if (User()->isCollaborator()) {
                $result = CollaboratorProjectAccess::getByUserId($user_id, ['general_project_id']);
                $result = array_column($result, 'general_project_id');
                $tmp_result = array_merge($tmp_result, $result);
            }
        }

        /**
         * Projects in which there are tasks to which the user has been added
         */
        $result = ProjectModel::getProjectInWhichThereAreTasksToWhichTheUserHasBeenAdded($user_id);
        if ($result) {
            $tmp_result = array_merge($tmp_result, $result);
        }
        $tmp_result = array_unique($tmp_result);
        if ($asStr)
            $tmp_result = implode(', ', $tmp_result);
        return $tmp_result;
    }

    public function addAccess($project_id, $user_id)
    {
        return ProjectAccess::add($project_id, $user_id);
    }

    public function addMaster($project_id, $user_id)
    {
        return $this->addUser($project_id, $user_id, 'master_id');
    }

    public function addClient($project_id, $client_id)
    {
        $result = $this->addUser($project_id, $client_id, 'client_id');
        if ($result) {
            AppEventsController::publish(AppEventsSubscribersController::addClientToProjectEvent(), compact('project_id', 'client_id'));
            $user = User::getById($client_id);
            $user['usertype'] = UserTypes()->getTypeById($user['usertype']);
            return $user;
        }
        else
            return false;
    }

    public function editProjectData($project_id, $data = [])
    {
        $res = [
            'error' => false,
            'msg' => '',
            'data' => ''
        ];
        if ($project_id && !empty($data)) {
            foreach (['dateCreated', 'start', 'end'] as $dates) {
                if (isset($data[$dates])) {
                    $data[$dates] = date('Y-m-d H:i:s', strtotime($data[$dates]));
                }
            }
            if (isset($data['start']) && isset($data['end']) && $data['end'] < $data['start']) {
                $res['error'] = true;
                $res['msg'] = 'date end < date start';
                return $res;
            }
            if (isset($data['dateCreated'])) {
                $data['dateCreated'] = date('Y-m-d H:i:s', strtotime($data['dateCreated']));
            }
            if (isset($data['evaluation']) && isset($data['evaluation']['days']) && isset($data['evaluation']['hours'])) {
                $data['evaluation'] = daysHoursMinutesToSeconds($data['evaluation']['days'], $data['evaluation']['hours'], 0);
            }
            try {
                $tmp_res = ProjectModel::update($project_id, $data);
                if ($tmp_res) {
                    foreach (['dateCreated', 'start', 'end'] as $dates) {
                        if (isset($data[$dates])) {
                            $data[$dates] = date('d-m-Y H:i', strtotime($data[$dates]));
                        }
                    }
                    $res['data'] = $data;
                    if (isset($res['data']['price_type']))
                        $res['data']['price_type'] = $this->getPriceType($res['data']['price_type'])['obj'];
                    if (isset($res['data']['category'])) {
                        $res['data']['category'] = Category::getOneCategoryById($res['data']['category'], ['name']);
                        if ($res['data']['category'])
                            $res['data']['category'] = $res['data']['category']['name'];
                    }
                    if (isset($res['data']['priority']))
                        $res['data']['priority'] = Priorities::getKeyById($res['data']['priority']);
                    if (isset($res['data']['status']))
                        $res['data']['status'] = Statuses()->getStatusesById($res['data']['status'])['aliase'];
                    if (isset($res['data']['evaluation'])) {
                        $res['data']['evaluation'] = secondsToWorkingTime($res['data']['evaluation']);
                        $tmp = [];
                        $tmp_h = '';
                        if ($res['data']['evaluation']['d'] != 0) {
                            $tmp['days'] = $res['data']['evaluation']['d'];
                        }
                        if ($res['data']['evaluation']['h'] != 0) {
                            $tmp_h = $res['data']['evaluation']['h'];
                        }
                        if ($res['data']['evaluation']['m'] != 0) {
                            $tmp_h = $tmp_h + $res['data']['evaluation']['m'] / 60;
                        }
                        if (!empty($tmp_h)) {
                            $tmp['hours'] = $tmp_h;
                        }
                        $res['data']['evaluation'] = $tmp;
                    }
                    return $res;
                }
                else {
                    $res['error'] = true;
                    $res['msg'] = 'data not updated';
                    return $res;
                }
            } catch (Exception $e) {
                $res['error'] = true;
                $res['msg'] = $e->getTrace();
                return $res;
            }
        }
    }

    public function getAllProjects($params = ['*'])
    {
        return ProjectModel::getAllProjects($params);
    }

    public function getProjectsByClientId($client_ids, $params = ['*'])
    {
        return ProjectModel::getByClientId($client_ids, $params);
    }

    public function getById($id, $params = ['*'])
    {
        try {
            $res = $this->getManyByIds($id, $params);
            return $res ? array_shift($res) : $res;
        } catch (Exception $e) {
            return false;
        }
    }

    public function getManyByIds($ids, $params = ['*'])
    {
        return ProjectModel::getManyByIds($ids, $params);
    }

    public static function getGroups($project_id, $params = ['*'])
    {
        return Group::getGroupsByProjectId($project_id, $params);
    }

    public static function getDefaultGroup($project_id, $params = ['*'])
    {
        $params[] = 'def_gr AS _tmp_def_gr';
        $res = self::getGroups($project_id, $params);
        $res = array_filter($res, function ($el) {
            return intval($el['_tmp_def_gr']) == 1;
        });
        $res = array_shift($res);
        unset($res['_tmp_def_gr']);
        return $res;
    }

    public static function getMainClients($params = ['*'])
    {
        try {
            $res = ProjectModel::getMainClients();
            return User::getManyByIds($res, $params);
        } catch (Exception $e) {
            return [];
        }
    }

    public function getClient($project_id, $params = ['*'])
    {
        $project_id = escape_string(clear_post($project_id));
        try {
            $client_id = self::getById($project_id, ['client_id']);
            $client_id = $client_id['client_id'];
            if (!$client_id)
                return [];
            $res = User::getById($client_id, $params);
            return !empty($res) ? $res : [];
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function getOwnTasks($project_ids, $params = ['*'])
    {
        return ProjectModel::getOwnTasks($project_ids, $params);
    }

    public function getCntTasksForManyProjects($project_ids)
    {
        $project_ids = getAsArray($project_ids);
        $res = ProjectModel::getCntTasksForManyProjects($project_ids);
        $project_ids = array_fill_keys($project_ids, 0);
        if (!empty($res)) {
            $res = valueToKey('id', $res, true);
            $res = array_map(function ($el) {
                return $el['cnt_tasks'];
            }, $res);
            foreach ($res as $k => $val) {
                $project_ids[$k] = $val;
            }
        }
        return $project_ids;
    }

    public function getCntTasksForOneProject($project_id)
    {
        return $this->getCntTasksForManyProjects($project_id)[$project_id];
    }

    public static function getCondition($status_id = 1)
    {
        $obj = Statuses();
        $status_id = intval($status_id);
        switch ($status_id) {
            case $obj->waiting_for_validation_by_the_client():
            case $obj->in_development():
            case $obj->finalized_not_controlled():
            case $obj->validated_technical_inspection():
                return 2;
            case $obj->double_check_validated():
            case $obj->approved_by_the_customer():
                return 3;
        }
        return $status_id;
    }

    public function getOwnParticipants($project_ids, $params = ['*'])
    {
        $project_ids = getAsArray($project_ids);
        try {
            $res = ProjectModel::getOwnParticipants($project_ids, $params);
            $pr_access = [];
            $access_data = self::getAccessDataByProjectIdS($project_ids, ['general_project_id', 'user_id']);
            foreach ($project_ids as $p_id) {
                $tmp = array_filter($access_data, function ($el) use ($p_id) {
                    return $el['general_project_id'] == $p_id;
                });
                $tmp = array_map(function ($el) {
                    $el['p_id'] = $el['general_project_id'];
                    unset($el['general_project_id']);
                    return $el;
                }, $tmp);
                $pr_access = array_merge($pr_access, $tmp);
            }
            $res = array_merge($res, $pr_access);
            $participants = $this->getManyByIds($project_ids, ['id', 'client_id', 'master_id']);
            foreach ($participants as $v) {
                foreach (['client_id', 'master_id'] as $u) {
                    if (intval($v[$u])) {
                        $res[] = ['p_id' => $v['id'], 'user_id' => $v[$u]];
                    }
                }
            }
            $res = array_filter($res, function ($el) {
                return !empty($el['user_id']);
            });
            $tmp = [];
            if ($res) {
                $users = array_column($res, 'user_id');
                $users = array_unique($users, SORT_NUMERIC);
                $users = User::getManyByIds($users, $params);
                foreach ($res as $val) {
                    if (isset($users[$val['user_id']]))
                        $tmp[$val['p_id']][$val['user_id']] = $users[$val['user_id']];
                    else
                        $tmp[$val['p_id']][$val['user_id']] = [];
                }
            }
            return $tmp;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function getMaster($project_id, $params = ['*'])
    {
        $master = $this->getById($project_id, ['master_id']);
        if (empty($master))
            return false;
        $master = $master['master_id'];
        return User::getById($master, $params);
    }

    public function isTemplate($project_id)
    {
        if (!isset($this->project_id)) {
            $res = $this->getById($project_id, ['is_template']);
            $this->project_id = (bool)intval($res['is_template']);
        }
        return $this->project_id;
    }

    public function getTemplateProjects(array $columns = ['*'])
    {
        return ProjectModel::getTemplateProjects($columns);
    }

    public function cloneAsOffer($project_id)
    {
        return $this->clone($project_id, array_merge(self::getAsOfferType(), self::getAsTemplateType(0)));
    }

    public function cloneAsTemplate($project_id)
    {
        return $this->clone($project_id, self::getAsTemplateType());
    }

    public function cloneNotAsTemplate($project_id)
    {
        return $this->clone($project_id, self::getAsTemplateType(0));
    }

    public function cloneAsNormalProject($project_id)
    {
        return $this->clone($project_id, array_merge(self::getAsOfferType(0), self::getAsTemplateType(0)));
    }

    public function fullClone($project_id)
    {
        return $this->clone($project_id);
    }

    /**
     * @param $project_id
     * @param array $types (project types: offer, template, etc...)
     * @return array
     */
    private function clone($project_id, array $types = [])
    {
        $id_old_project = escape_string(clear_post($project_id));
        try {
            $oldProject = $this->getById($id_old_project);
            if (empty($oldProject))
                return false;
        } catch (Exception $e) {
            return $e->getMessage();
        }
        $oldProject['dateCreated'] = date('Y-m-d H:i:s');
        $dif = strtotime($oldProject['end']) - strtotime($oldProject['start']);
        $oldProject['start'] = date('Y-m-d H:i:s');
        $oldProject['end'] = date('Y-m-d H:i:s', strtotime($oldProject['start']) + $dif);
        $oldProject['user_id'] = User()->getId();
        $oldProject['client_id'] = 0;
        $oldProject['cloned_from'] = $id_old_project;
        if (!empty($types)) {
            $oldProject = array_merge($oldProject, $types);
        }
        $id_new_project = $this->add($oldProject, false);
        $id_new_project = $id_new_project['project'];
        if ($oldProject['img']) {
            $this->createDirectory($id_new_project);
            copy($this->createPath($id_old_project, $oldProject['img']), $this->createPath($id_new_project, $oldProject['img']));
        }

        $old_groups = Group::getGroupsByProjectId($id_old_project);
        $old_groups_id = array_column($old_groups, 'id');
        try {
            $old_tasks = Group()->getOwnTasks($old_groups_id);
        } catch (Exception $e) {
            return $e->getMessage();
        }

        foreach ($old_groups as &$group) {
            $group_id = $group['id'];
            $new_group_id = Group()->setParent($group['parent_id'])->setName($group['name'])->setUser($group['user_id'])->setDefault($group['def_gr'])->setProject($id_new_project)->addGroup();

            foreach ($old_groups as &$_group) {
                if (intval($_group['parent_id']) == intval($group_id))
                    $_group['parent_id'] = $new_group_id;
            }
            foreach ($old_tasks as $key => $task) {
                if (intval($task['group_id']) == intval($group_id))
                    $old_tasks[$key]['group_id'] = $new_group_id;
            }
        }
        foreach ($old_tasks as $task) {
            Task()->cloneTask($task['id'], $task);
        }
        return $id_new_project;
    }

    public function deleteParticipants($project_id, $users_id)
    {
        $types = UserTypes();
        $users = User::getManyByIds($users_id, ['usertype']);
        $arr_tmp = [$types->getClient(), $types->getRGPDMaster()];
        $users = array_filter($users, function ($el) use ($arr_tmp) {
            return in_array($el['usertype'], $arr_tmp);
        });
        if (!empty($users)) {
            $arr_tmp = [];
            foreach ($users as $v) {
                switch ($v['usertype']) {
                    case $types->getClient():
                        $arr_tmp['client_id'] = 0;
                        break;
                    case $types->getRGPDMaster():
                        $arr_tmp['master_id'] = 0;
                        break;
                }
            }
            $this->editProjectData($project_id, $arr_tmp);

            /*
            $user_id = $users_id[0];
            $user_to_delete = User::getById($user_id, ['usertype', 'global_id']);
            if($user_to_delete){
                if( in_array($user_to_delete['usertype'], UserTypes()->getSupportTypes()) ){
                    $clientdata = Project()->getClient($project_id,['id', 'global_id']);
                    if( !empty($clientdata) ){
                        require_once($_SERVER['DOCUMENT_ROOT'] . DS . MODULE . DS . 'messenger_tm/VMSG_messenger_functions.php');
                        $dialogid = V_Messenger::getDialogidByProjectidAndClientid($project_id, $clientdata['id']);
                        if($dialogid){
                            require_once($_SERVER['DOCUMENT_ROOT'] . DS . MODULE . DS . 'messenger_tm/glooda_api.php');
                            $ga = new GloodaAPI( 'logged_in_user', array('supportUsersData' => array(), 'supportDialogID'  => 0, 'token' => $_SESSION[MODULE]['user']['accessToken']) );
                            if ($ga->login_OK) {
                                $res_delete = $ga->removeUserFromGroups(array('id'=>$user_to_delete['global_id']), [$dialogid]);
                            }
                        }
                    }
                }
            }
            */

        }
        return ProjectAccess::delete($project_id, $users_id);
    }

    public function deleteClient($project_id, $client_id = null)
    {
        $project_id = escape_string(clear_post($project_id));
        try {
            if (is_null($client_id)) {
                $client_id = $this->getClient($project_id, ['id']);
                if (empty($client_id))
                    return 0;
                $client_id = $client_id['id'];
            }
            $this->editProjectData($project_id, ['client_id' => 0]);
            ProjectAccess::delete($project_id, $client_id);
            return $client_id;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function delete($project_id)
    {
        $project_id = escape_string(clear_post($project_id));
        if ($project_id == self::getDefaultId())
            return false;
        Folder::delete($this->createPath($project_id));
        $res = ProjectModel::delete($project_id);
        if ($res)
            AppEventsController::publish(AppEventsSubscribersController::deleteProjectEvent(), compact('project_id'));
        return $res;
    }

    public function getClientAttachmentPath($project_id, $user_id)
    {
        return $this->upload_dir . DS . $this->getClientAttachmentOutputDir($project_id, $user_id);
    }

    public function getClientAttachmentUrl($project_id, $user_id, $filename)
    {
        $url = SiteData()->getAll('sitename');
        return $url . DS . MODULE . UploadDir::getRelativePath() . self::$mainFolderName . DS . $this->getClientAttachmentOutputDir($project_id, $user_id) . DS . $filename;
    }

    public function getClientAttachmentOutputDir($project_id, $user_id)
    {
        return $project_id . DS . self::$clientAttachmentsDirName . DS . $user_id;
    }

    public function getClientAttachments($project_id)
    {
        return ProjectAttachments::getClientAttachments($project_id);
    }

    public function getAdminAttachments($project_id)
    {
        return ProjectAttachments::getAdminAttachments($project_id);
    }

    public function getAllAttachments($project_id)
    {
        return ProjectAttachments::getAllAttachments($project_id);
    }

    public function addClientAttachment($project_id, $user_id)
    {
        $project_id = escape_string(clear_post($project_id));
        $user_id = escape_string(clear_post($user_id));
        $this->createClientAttachmentsDirectory($project_id, $user_id);
        $ret = [];
        if (isset($_FILES['myfile'])) {

            $error = $_FILES['myfile']['error'];
            if (!is_array($_FILES['myfile']['name'])) //single file
            {
                $fileName = $_FILES['myfile']['name'];
                $filesize = $_FILES['myfile']['size'];
                move_uploaded_file($_FILES['myfile']['tmp_name'], $this->getClientAttachmentPath($project_id, $user_id) . DS . $fileName);
                $ext = explode('.', $fileName);
                $ext = count($ext) ? array_pop($ext) : '';
                $outputDir = $this->getClientAttachmentOutputDir($project_id, $user_id);
                ProjectAttachments::add([
                    'user_id' => $user_id,
                    'output_dir' => $outputDir,
                    'filename' => $fileName,
                    'ext' => $ext,
                    'filesize' => $filesize,
                    'project_id' => $project_id,
                    'added_by_user' => $user_id,
                ]);

                $ret[] = $fileName;
            }
        }
        return $ret;
    }

    public function addClientMessage($project_id, $user_id, $message)
    {
        try {
            $res = ProjectClientMessages::add([
                'user_id' => escape_string($user_id),
                'project_id' => escape_string($project_id),
                'message' => escape_string($message)
            ]);
            if (is_numeric($res)) {
                $message_id = getLastId();
                $this->addToMessagesHistory($project_id, $message_id, self::HISTORY_MESSAGES_ACTION_ADD, $message, null);
                ClientActivity::add($project_id, self::getClassName(), $project_id);
            }
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function editClientMessage($message_id, $message)
    {

        $old_message = $this->getMessageById($message_id, ['message'])['message'];

        try {
            $res = ProjectClientMessages::updateById($message_id, ['message' => $message]);
            if ($res) {
                $project_id = $this->getByMessageId($message_id, ['id'])['id'];
                $this->addToMessagesHistory($project_id, $message_id, self::HISTORY_MESSAGES_ACTION_UPDATE, $message, $old_message);
                ClientActivity::add($project_id, self::getClassName(), $project_id);
            }
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function deleteClientMessage($message_id)
    {
        $message = $this->getMessageById($message_id, ['message'])['message'];
        $project_id = $this->getByMessageId($message_id, ['id'])['id'];
        try {
            $res = ProjectClientMessagesModel::deleteByIdAndUserId($message_id, User()->getId());
            if ($res) {
                $this->addToMessagesHistory($project_id, $message_id, self::HISTORY_MESSAGES_ACTION_DELETE, $message, null);
                ClientActivity::add($project_id, self::getClassName(), $project_id);
            }
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function changeClientAttachmentVisibility($attachment_id, $visible)
    {
        return ProjectAttachments::updateById($attachment_id, ['visible' => $visible]);
    }

    public function getClientMessages($project_id, $user_id = null)
    {
        return ProjectClientMessagesModel::getClientMessagesByProjectId($project_id, $user_id);
    }

    public function getMessagesHistoryByProjectId($project_id, $params = ['*'])
    {
        try {
            return HistoryProjectMessages::getByProjectId($project_id, $params);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function addToMessagesHistory($project_id, $message_id, $action, $message, $old_message)
    {
        $user_id = User()->getId();
        return HistoryProjectMessages::add(compact('project_id', 'message_id', 'action', 'message', 'old_message', 'user_id'));
    }

    public function getClientProjectsCount($client_id = null)
    {
        if (is_null($client_id))
            $client_id = User()->getId();
        return ProjectModel::getClientProjectsCount($client_id);
    }
}