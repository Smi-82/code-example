<?php

use models\HistoryTaskMessagesModel;

class HistoryTaskMessages
{
    public static function baseSelect(array $columns = ['*'], $aggregate = null)
    {
        return HistoryTaskMessagesModel::baseSelect($columns, $aggregate);
    }

    public static function getTable()
    {
        return HistoryTaskMessagesModel::getTable();
    }

    public static function add($data)
    {
        return HistoryTaskMessagesModel::add($data);
    }

    public static function getById($id, $columns = ['*'])
    {
        return HistoryTaskMessagesModel::getById($id, $columns);
    }

    public static function updateById($ids, $data)
    {
        if (isset($data['id']))
            unset($data['id']);
        return HistoryTaskMessagesModel::updateById($ids, $data);
    }

    public static function getByTaskId($task_id, $columns = ['*'])
    {
        return HistoryTaskMessagesModel::getByTaskId($task_id, $columns);
    }
}