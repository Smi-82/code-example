<?php

use abstractClasses\TableData;

class DossierClientListPage extends TableData
{
    protected function addFilters()
    {
        $filters = '';
        if ($this->_filters != null && is_array($this->_filters)) {
            foreach ($this->_filters as $field => $val) {
                if (!empty($val)) {
                    switch ($field) {
                        case 'no_dossier':
                            $filters .= ' ';
                            break;
                        default:
                            $filters .= ' AND u.' . $field . ' IN (' . implode(', ', $val) . ') ';
                    }
                }
            }
        }
        return $filters;
    }

    protected function addFind()
    {
        if (!empty($this->_findBy)) {
            switch ($this->_findField) {
                case 'id':
                    $findByName = ' AND u.id LIKE "%' . $this->_findBy . '%" ';
                    break;
                case 'clientName':
                    $findByName = ' AND (u.firstname LIKE "%' . $this->_findBy . '%" OR u.lastname LIKE "%' . $this->_findBy . '%") ';
                    break;
                case 'consultant':
                    $findByName = ' AND (c.firstname LIKE "%' . $this->_findBy . '%" OR c.lastname LIKE "%' . $this->_findBy . '%") ';
                    break;
                case 'societe':
                    $findByName = ' AND u.societe LIKE "%' . $this->_findBy . '%" ';
                    break;
                case 'phones':
                    $findByName = ' AND (u.phone LIKE "%' . $this->_findBy . '%" OR u.phone2 LIKE "%' . $this->_findBy . '%") ';
                    break;
                case 'email':
                    $findByName = ' AND u.email LIKE "%' . $this->_findBy . '%" ';
                    break;
                case 'dossiers':
                    $findByName = ' AND d.name LIKE "%' . $this->_findBy . '%" ';
                    break;
                default:
                    $findByName = '';
            }

        }
        else
            $findByName = '';
        return $findByName;
    }

    protected function addSort()
    {
        if ($this->_sortWay == 'both') {
            $sort = ' ORDER BY client_id DESC';
        }
        else {
            switch ($this->_sortBy) {
                case 'id':
                    $sort = ' ORDER BY client_id ' . $this->_sortWay . ' ';
                    break;
                case 'clientName':
                    $sort = ' ORDER BY client_name ' . $this->_sortWay . ' ';
                    break;
                case 'clientName':
                    $sort = ' ORDER BY u.societe ' . $this->_sortWay . ' ';
                    break;
                case 'phones':
                    $sort = ' ORDER BY phones ' . $this->_sortWay . ' ';
                    break;
                case 'email':
                    $sort = ' ORDER BY u.email ' . $this->_sortWay . ' ';
                    break;
                case 'dossiers':
                    $sort = ' ORDER BY d.name ' . $this->_sortWay . ' ';
                    break;
                default:
                    $sort = '';
            }

        }
        return $sort;
    }

    public function getTableData()
    {
        $current_user = User();
        $sql = '
SELECT
u.img,
u.id AS client_id,
TRIM(CONCAT(u.phone, IF(u.phone != "" AND u.phone2 != "", ", ", ""), u.phone2)) AS phones,
u.email,
u.societe,
u.usertype,
TRIM(CONCAT(c.firstname," ", c.lastname)) AS curator_name,
TRIM(CONCAT(u.firstname," ", u.lastname)) AS client_name,
u.status
FROM
' . User::getTable() . ' AS u
LEFT JOIN
' . User::getTable() . ' AS c
ON
c.id = u.consultant_id
LEFT JOIN
dossiers as d
ON
d.client_id = u.id
WHERE u.usertype=' . UserTypes()->getClient();

        if ($current_user->isRGPDCurator()) {
            $consultant = new Consultant();
            $children_with_current = [];
            $children = $consultant->getChildrensId($current_user->getId());
            if ($children)
                $children_with_current = $children;
            $children_with_current[] = $current_user->getId();
            $sql .= ' AND u.consultant_id IN (' . implode(',', $children_with_current) . ')';
        }
        elseif ($current_user->isRGPDVendor()) {
            $sql .= ' AND u.consultant_id = ' . $current_user->getId();
        }


        $sql .= $this->addFind();
        $sql .= $this->addFilters();
        $sql .= $this->addSort();
        try {
            $result = query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
        if (!empty($result)) {
            $statuses = ClientStatus::getAll(['id', 'name']);
            $statuses = valueToKey('id', $statuses);
            $result = array_map(function ($el) use ($statuses) {
                $el['status_name'] = $statuses[$el['status']]['name'];
                return $el;
            }, $result);
        }
        $client_ids = array_column($result, 'client_id');
        $sql = 'SELECT d.name, d.id, d.client_id FROM dossiers AS d WHERE d.client_id IN (' . implode(',', $client_ids) . ')';

        try {
            $dossiers = query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
        $tmp = [];
        foreach ($dossiers as $val) {
            $client = $val['client_id'];
            unset($val['client_id']);
            $tmp[$client][] = $val;
        }
        $dossiers = $tmp;
        unset($tmp);
        foreach ($result as $key => &$val) {
            $val['dossiers'] = $dossiers[$val['client_id']] ?? [];
            $val['checked'] = false;
            if (isset($this->_filters['no_dossier']) && !empty($val['dossiers'])) {
                unset($result[$key]);
            }
        }
        return ['result' => $result, 'dossiers' => $client_ids];
    }
}