<?php

use traits\BaseSelect;
use traits\GetTable;

class TaskScreenshot
{
    private static $table = 'task_screenshots';
    use GetTable;
    use BaseSelect;

    public static function getById($id, array $columns = ['*'])
    {
        $id = escape_string(clear_post($id));
        $sql = self::baseSelect($columns) . ' WHERE id = ' . $id . ' LIMIT 0, 1';
        try {
            return querySingle($sql);
        } catch (Exception $e) {
            return $e->getTrace();
        }
    }

    public static function getByTaskIdAndUserId($task_id, $user_id, array $columns = ['*'])
    {
        $task_id = escape_string(clear_post($task_id));
        $user_id = escape_string(clear_post($user_id));
        $sql = self::baseSelect($columns) . ' WHERE task_id = ' . $task_id . ' AND user_id=' . $user_id . ' ORDER BY id DESC';
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getTrace();
        }
    }


    public static function getByTaskId($task_id, array $columns = ['*'])
    {
        $res = self::getByManyTaskId($task_id, $columns);
        return $res[$task_id] ?? [];
    }

    public static function getByManyTaskId($task_ids, array $columns = ['*'])
    {
        $task_ids = getAsArray($task_ids);
        $columns[] = ' task_id AS smi_tmp_id ';
        $sql = self::baseSelect($columns) . ' WHERE task_id IN ( ' . implode(',', $task_ids) . ') ORDER BY id DESC';
        try {
            $res = query($sql);
            $tmp = [];
            if (!empty($res)) {
                foreach ($res as $val) {
                    $tmp_id = $val['smi_tmp_id'];
                    unset($val['smi_tmp_id']);
                    $tmp[$tmp_id][] = $val;
                }
            }
            return $tmp;
        } catch (Exception $e) {
            return $e->getTrace();
        }
    }

    public static function getByManyTaskIdForClient($task_ids, array $columns = ['*'])
    {
        $task_ids = getAsArray($task_ids);
        $columns[] = ' task_id AS smi_tmp_id ';
        $sql = self::baseSelect($columns) . ' WHERE task_id IN ( ' . implode(',', $task_ids) . ') AND show_to_client = 1 ORDER BY id DESC';
        try {
            $res = query($sql);
            $tmp = [];
            if (!empty($res)) {
                foreach ($res as $val) {
                    $tmp_id = $val['smi_tmp_id'];
                    unset($val['smi_tmp_id']);
                    $tmp[$tmp_id][] = $val;
                }
            }
            return $tmp;
        } catch (Exception $e) {
            return $e->getTrace();
        }
    }

    public static function getByTaskIdForClient($task_id, array $columns = ['*'])
    {
        $task_id = escape_string(clear_post($task_id));
        $sql = self::baseSelect($columns) . ' WHERE task_id = ' . $task_id . ' AND show_to_client = 1 ORDER BY id DESC';
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getTrace();
        }
    }

    public static function getMyByTaskId($task_id, array $columns = ['*'])
    {
        return self::getByTaskIdAndUserId($task_id, User()->getId(), $columns);
    }

    public static function update($id, $data)
    {
        $id = escape_string(clear_post($id));
        $sql = update_query_string(self::getTable(), $data) . ' WHERE id = ' . $id;
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getTrace();
        }
    }

    public static function delete($id)
    {
        $id = escape_string(clear_post($id));
        $sql = 'DELETE FROM ' . self::getTable() . ' WHERE id = ' . $id;
        try {
            $file = self::getById($id, ['output_dir', 'filename']);
            $file = $file['output_dir'] . DS . $file['filename'];
            $res = query($sql);
            if ($res) {
                Folder::delete($file);
            }
            return $res;
        } catch (Exception $e) {
            return $e->getTrace();
        }
    }

    public static function add($data)
    {
        $sql = insert_query_string(self::getTable(), $data);
        try {
            $res = query($sql);
            return $res ? getLastId() : $res;
        } catch (Exception $e) {
            return $e->getTrace();
        }
    }
}