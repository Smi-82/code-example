<?php


use traits\BaseSelect;
use traits\GetTable;
use traits\Update;

class UserMessageLink
{
    use GetTable;
    use BaseSelect;
    use Update;

    private static $table = 'user_message_link';

    public static function add($user_id, $message_id, $task_id)
    {
        $sql = insert_query_string(self::getTable(), compact('user_id', 'message_id', 'task_id'));
        try {
            return query($sql) ? getLastId() : false;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function getByUserIdTaskId($user_id, $task_id, $columns = ['*'])
    {
        $user_id = escape_string(clear_post($user_id));
        $task_id = escape_string(clear_post($task_id));
        $sql = self::baseSelect($columns) . ' WHERE user_id=' . $user_id . ' AND task_id=' . $task_id;
        try {
            return querySingle($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}