<?php

use traits\SingleTone;

class Response
{
    private $result;
    private $dataCalling;
    use SingleTone;

    private function __construct()
    {
        $this->result = array();
        $this->dataCalling = false;
    }

    private function getResult()
    {
        if ($this->dataCalling == false) {
            $this->data();
        }
        $this->dataCalling = false;
        $result = $this->result;
        $this->result = array();
        return json_encode($result);
    }

    public function data($data = '')
    {
        $this->dataCalling = true;
        $this->result['data'] = $data;
        return self::$inst;
    }

    public function success($message = '')
    {
        $this->result = array_merge($this->result, array('success' => 'ok', 'message' => $message));
        return $this->getResult();
    }

    public function error($message = '')
    {
        $this->result = array_merge($this->result, array('success' => 'nok', 'message' => $message));
        return $this->getResult();
    }
}