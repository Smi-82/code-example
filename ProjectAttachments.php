<?php

use traits\BaseSelect;
use traits\GetTable;

class ProjectAttachments
{
    private static $table = 'project_attachments';
    use getTable;
    use baseSelect;

    public static function updateById($ids, $data)
    {
        if (isset($data['id']))
            unset($data['id']);
        $ids = getAsArray($ids);
        $sql = update_query_string(self::getTable(), $data) . ' WHERE id IN (' . implode(',', $ids) . ')';
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function add($data)
    {
        $sql = insert_query_string(self::getTable(), $data);
        try {
            $res = query($sql);
            return $res ? getLastId() : $res;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function getClientAttachments($project_id)
    {
        $project_id = escape_string($project_id);
        $sql = "SELECT a.*
                FROM " . self::getTable() . " a
                LEFT JOIN " . User::getTable() . " b ON a.`user_id` = b.`id`
                WHERE `project_id` = '$project_id' AND b.`usertype` = '" . Usertypes()->getClient() . "'";
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function getAdminAttachments($project_id)
    {
        $project_id = escape_string($project_id);
        $sql = "SELECT a.*
                FROM " . self::getTable() . " a
                LEFT JOIN " . User::getTable() . " b ON a.`user_id` = b.`id`
                WHERE `project_id` = '$project_id' AND b.`usertype` = '" . Usertypes()->getSupervisor() . "'";
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function getAllAttachments($project_id)
    {
        $project_id = escape_string($project_id);
        $sql = self::baseSelect() . " WHERE `project_id` = '$project_id'";
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}