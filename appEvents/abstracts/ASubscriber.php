<?php

namespace appEvents\abstracts;

abstract class ASubscriber
{
    public function __call(string $name, array $arguments)
    {
        $this->notify(array_shift($arguments));
    }

    public abstract function notify($data);
}