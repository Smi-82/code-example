<?php

namespace appEvents\subscribers;

use appEvents\interfaces\iSubscriber;
use traits\SingleTone;

class DeleteUserFromTaskS implements iSubscriber
{
    use SingleTone;

    public function notify($data)
    {
        $obj = AdmNotify();
        $obj->setDeleteUserFromTask();
        foreach ($data['tasks_id'] as $task_id) {
            $obj->getWorker()->whoMustSee($task_id, $data['user_id']);
            $obj->add();
        }
    }
}