<?php

namespace appEvents\subscribers;

use appEvents\interfaces\iSubscriber;
use traits\SingleTone;

class NewTaskS implements iSubscriber
{
    use SingleTone;
    public function notify($data)
    {
        taskHistory($data['task_id'], 'Create task', $data['user_id']);
    }
}