<?php

namespace appEvents\subscribers;

use appEvents\interfaces\iSubscriber;
use traits\SingleTone;

class TaskFileS implements iSubscriber
{
    use SingleTone;
    public function notify($data)
    {
        AdmNotify()->setTaskFile($data['task_id'])->add();
    }
}