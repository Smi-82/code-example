<?php

namespace appEvents\subscribers;
use appEvents\interfaces\iSubscriber;
use traits\SingleTone;

class HistoryS implements iSubscriber
{
    use SingleTone;
    public function notify($data)
    {
        history($data['page'], $data['desc']);
    }
}