<?php

namespace appEvents\subscribers;

use appEvents\interfaces\iSubscriber;
use traits\SingleTone;

class UserLoginS implements iSubscriber
{
    use SingleTone;

    public function notify($data)
    {
        if ($data['user']['usertype'] == UserTypes()->getClient()) {
            User::setTimeToShowPopup($data['user']['id']);
        }
        history('Login', User()->signature . ' logged into the dashboard');
    }
}