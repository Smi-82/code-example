<?php

namespace appEvents\subscribers;

use appEvents\interfaces\iSubscriber;
use traits\SingleTone;

class NewUserS implements iSubscriber
{
    use SingleTone;

    public function notify($data)
    {
        history('Utilisateurs', $data['who_add'] . ' added new utilisateur: ' . $data['who_was_added']);
    }
}