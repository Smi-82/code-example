<?php

namespace appEvents\subscribers;

use appEvents\interfaces\iSubscriber;
use traits\SingleTone;

class UserToTaskS implements iSubscriber
{
    use SingleTone;

    public function notify($data)
    {
        $obj = AdmNotify();
        $obj->setUserToTask()->getWorker()->whoMustSee($data['task_id'], $data['user_id']);
        $obj->add();
        taskHistory($data['task_id'], 'User has access to the task', $data['user_id']);
    }
}