<?php

namespace appEvents\subscribers;

use appEvents\interfaces\iSubscriber;
use traits\SingleTone;

class TaskStatusS implements iSubscriber
{
    use SingleTone;
    public function notify($data)
    {
        AdmNotify()->setTaskStatus($data['task_id'])->add();
    }
}