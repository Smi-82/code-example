<?php

namespace appEvents\subscribers;

use appEvents\interfaces\iSubscriber;
use traits\SingleTone;

class NewProjectS implements iSubscriber
{
    use SingleTone;
    public function notify($data)
    {
        AdmNotify()->setNewProject($data['project_id'])->add();
        history('projects_list', User()->getSignature() . ', id: ' . User()->getId() . ' add new project name: ' . $data['proj_name'] . ', id: ' . $data['project_id']);
    }
}