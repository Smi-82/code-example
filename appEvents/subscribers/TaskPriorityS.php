<?php

namespace appEvents\subscribers;

use appEvents\interfaces\iSubscriber;
use traits\SingleTone;

class TaskPriorityS implements iSubscriber
{
    use SingleTone;
    public function notify($data)
    {
        AdmNotify()->setTaskPriority($data['task_id'])->add();
    }
}