<?php

namespace appEvents\subscribers;

use appEvents\interfaces\iSubscriber;
use traits\SingleTone;

class NewClientS implements iSubscriber
{
    use SingleTone;
    public function notify($data)
    {
        AdmNotify()->setNewClient($data['clientid'])->add();
    }
}