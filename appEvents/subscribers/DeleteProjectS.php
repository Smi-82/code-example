<?php

namespace appEvents\subscribers;

use appEvents\interfaces\iSubscriber;
use traits\SingleTone;

class DeleteProjectS implements iSubscriber
{
    use SingleTone;
    public function notify($data)
    {
        AdmNotify()->setDeleteProject($data['project_id'])->add();
    }
}