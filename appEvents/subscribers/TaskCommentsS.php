<?php

namespace appEvents\subscribers;

use appEvents\interfaces\iSubscriber;
use traits\SingleTone;

class TaskCommentsS implements iSubscriber
{
    use SingleTone;
    public function notify($data)
    {
        AdmNotify()->setTaskComments($data['task_id'])->add();
        history('task', User()->getSignature() . ' changed description of the task ID:' . $data['task_id']);
        taskHistory($data['task_id'], 'Changed description');
    }
}