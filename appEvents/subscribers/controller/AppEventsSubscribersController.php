<?php

namespace appEvents\subscribers\controller;

use appEvents\subscribers\AddAccessToProjectS;
use appEvents\subscribers\DeleteProjectS;
use appEvents\subscribers\DeleteUserFromTaskS;
use appEvents\subscribers\NewBillS;
use appEvents\subscribers\NewCategoryS;
use appEvents\subscribers\NewClientS;
use appEvents\subscribers\NewProjectS;
use appEvents\subscribers\NewTaskS;
use appEvents\subscribers\NewUserS;
use appEvents\subscribers\TaskCommentsS;
use appEvents\subscribers\TaskFileS;
use appEvents\subscribers\TaskHistoryS;
use appEvents\subscribers\TaskPriorityS;
use appEvents\subscribers\TaskStatusS;
use appEvents\subscribers\UserLoginS;
use appEvents\subscribers\UserLogoutS;
use appEvents\subscribers\UserToTaskS;

/**
 * @method static changeTaskStatusEvent()
 * @method static deleteProjectEvent()
 * @method static addAccessToProjectEvent()
 * @method static addClientToProjectEvent()
 * @method static newProjectEvent()
 * @method static taskFileEvent()
 * @method static taskCommentsEvent()
 * @method static newClientEvent()
 * @method static deleteUserFromTaskEvent()
 * @method static changeTaskPriorityEvent()
 * @method static userToTaskEvent()
 * @method static userLogout()
 * @method static userLogin()
 * @method static newTask()
 * @method static newBill()
 * @method static newCategory()
 * @method static newUser()
 */
class AppEventsSubscribersController
{
    public static function getSubscribers($event)
    {
        switch ($event) {
            case self::userLogin():
                return [
                    UserLoginS::class
                ];
            case self::userLogout():
                return [
                    UserLogoutS::class
                ];
            case self::deleteProjectEvent():
                return [
                    DeleteProjectS::class
                ];
            case self::addAccessToProjectEvent():
            case self::addClientToProjectEvent():
                return [
                    AddAccessToProjectS::class
                ];
            case self::newCategory():
                return [
                    NewCategoryS::class
                ];
            case self::newBill():
                return [
                    NewBillS::class
                ];
            case self::newTask():
                return [
                    NewTaskS::class
                ];
            case self::newProjectEvent():
                return [
                    NewProjectS::class
                ];
            case self::taskFileEvent():
                return [
                    TaskFileS::class
                ];
            case self::taskCommentsEvent():
                return [
                    TaskCommentsS::class
                ];
            case self::newUser():
                return [
                    NewUserS::class
                ];
            case self::newClientEvent():
                return [
                    NewClientS::class
                ];
            case self::deleteUserFromTaskEvent():
                return [
                    DeleteUserFromTaskS::class
                ];
            case self::changeTaskStatusEvent():
                return [
                    TaskStatusS::class,
                    TaskHistoryS::class,
                ];
            case self::changeTaskPriorityEvent():
                return [
                    TaskPriorityS::class,
                    TaskHistoryS::class,
                ];
            case self::userToTaskEvent():
                return [
                    UserToTaskS::class
                ];
            default:
                return [];
        }
    }

    public static function __callStatic($name, $data = null)
    {
        return $name;
    }
}