<?php

namespace appEvents\subscribers;

use appEvents\interfaces\iSubscriber;
use Task;
use traits\SingleTone;
use User;

class UserLogoutS implements iSubscriber
{
    use SingleTone;

    public function notify($data)
    {
        Task::userNotOnTask(($data['user'])->getId());
        User::setOffLine(($data['user'])->getId());
        history('Logout', ($data['user'])->getSignature() . ' logout');
    }
}