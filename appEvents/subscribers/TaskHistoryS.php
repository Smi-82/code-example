<?php

namespace appEvents\subscribers;

use appEvents\interfaces\iSubscriber;
use TaskHistory;
use traits\SingleTone;

class TaskHistoryS implements iSubscriber
{
    use SingleTone;
    public function notify($data)
    {
        TaskHistory::add([
            'description' => $data['desc'] ?? '',
            'task_id' => $data['task_id'] ?? '',
            'user_id' => $data['user_id'] ?? 0,
            'file_id' => $data['file_id'] ?? 0,
        ]);
    }
}