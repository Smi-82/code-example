<?php
namespace appEvents\interfaces;
interface iSubscriber
{
    public function notify($data);
}