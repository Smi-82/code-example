<?php

namespace appEvents;

use appEvents\subscribers\controller\AppEventsSubscribersController;
use StaticFactory;

class AppEventsController
{
    private static $events;

    public static function subscribe($event, $subscriber)
    {
        if (!is_array($subscriber))
            $subscriber = [$subscriber];
        self::$events[$event] = array_merge(self::$events[$event] ?? [], $subscriber);
    }

    public static function publish($event, $data)
    {
        self::subscribe($event, AppEventsSubscribersController::getSubscribers($event));
        if (!isset(self::$events[$event]) || empty(self::$events[$event]))
            return false;
        foreach (self::$events[$event] as $subscriber)
            StaticFactory::build($subscriber)->notify($data);
    }
}