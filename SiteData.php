<?php


use traits\SingleTone;

class SiteData
{
    private $arr;
    use SingleTone;

    private function __construct()
    {
        $url = @($_SERVER["HTTPS"] != 'on') ? 'http://' . $_SERVER["SERVER_NAME"] : 'https://' . $_SERVER["SERVER_NAME"];
        $url .= (!in_array($_SERVER["SERVER_PORT"], [80, 443])) ? ":" . $_SERVER["SERVER_PORT"] : "";
        $fullurl = $url . $_SERVER["REQUEST_URI"];
        $moduleUrl = $url . '/' . MODULE;
        $this->arr = [
            'sitename' => $url,
            'sitenamelight' => $_SERVER["HTTP_HOST"],
            'fullurl' => $fullurl,
            'upload' => rtrim($moduleUrl . UploadDir::getRelativePath(), '/'),
            'img' => $moduleUrl . '/img'
        ];
    }

    public function __get($key)
    {
        return $this->getAll($key);
    }

    public function getAll($key = null)
    {
        if (is_null($key) || empty($key))
            return $this->arr;
        elseif (isset($this->arr[$key]))
            return $this->arr[$key];
        else
            throw new Exception('not such key ' . $key);
    }

    public function getSiteName()
    {
        return $this->getAll('sitename');
    }

    public function getSiteNameLight()
    {
        return $this->getAll('sitenamelight');
    }

    public function getFullUrl()
    {
        return $this->getAll('fullurl');
    }

    public function getUpload()
    {
        return $this->getAll('upload');
    }

    public function getImg()
    {
        return $this->getAll('img');
    }
}