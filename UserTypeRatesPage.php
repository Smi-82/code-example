<?php


use abstractClasses\TableData;

class UserTypeRatesPage extends TableData
{
    public function getTableData($columns = ['*'])
    {
        $exception_types = [UserTypes()->getClient()];
        $sql = UserTypes::baseSelect($columns) . ' WHERE id NOT IN ( ' . implode(',', $exception_types) . ') ';
        $filters = $this->addFilters();
        $sql .= $findByName = $this->addFind();
        $sql .= $filters;
        $sql .= $this->addSort();
        $sql .= $this->addPage($findByName);
        try {
            $types = query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
        $sql = 'SELECT COUNT(id) AS cnt FROM ' . UserTypes::getTable() . ' WHERE id NOT IN ( ' . implode(',', $exception_types) . ') ';
        $sql .= $findByName;
        $sql .= $filters;
        try {
            $cntRows = querySingle($sql)['cnt'];
        } catch (Exception $e) {
            return $e->getMessage();
        }
        return compact('types', 'cntRows');
    }

    public function getTableDataForClientSide($columns = ['*'])
    {
        $res = $this->getTableData($columns);
        $types = $res['types'];
        $cntRows = $res['cntRows'];

        $show = User::getById(User()->getId(), ['show_the_client']);
        $show = htmlspecialchars_decode($show['show_the_client']);
        if (!empty($show)) {
            $show = json_decode($show, true);
            $types = array_map(function ($el) use ($show) {
                if (isset($show[$el['id']])) {
                    $el['show_the_client'] = $show[$el['id']];
                }
                return $el;
            }, $types);
        }
        $all = count($types);
        $types = array_filter($types, function ($el) {
            return intval($el['show_the_client'] == 1);
        });
        $cntRows -= $all - count($types);
        return compact('types', 'cntRows');
    }
}