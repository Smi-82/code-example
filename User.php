<?php

use appEvents\AppEventsController;
use appEvents\subscribers\controller\AppEventsSubscribersController;
use models\UserModel;
use traits\BaseSelect;
use traits\ClassName;
use traits\GetTable;
use traits\SingleTone;

class User
{
    use SingleTone;
    use ClassName;
    use GetTable;
    use BaseSelect;

    private $types;
    private $upload_dir;
    private $folder;
    private $fieldsForCSV;
    private static $columns;
    private $csvFileName = 'import_clients_example.csv';

    private function __construct()
    {
        $this->folder = 'users';
        $this->types = [];
        $this->upload_dir = UploadDir::getRootPath($this->folder);
        self::$columns = [];
        $this->fieldsForCSV = [
            'lastname' => [
                'title' => '',
                'value' => 'Example_lastname',
            ],
            'firstname' => [
                'title' => '',
                'value' => 'Example_firstname',
            ],
            'gender' => [
                'title' => 'gender(mr,mrs)',
                'value' => 'mr',
            ],
            'email' => [
                'title' => '',
                'value' => 'example@gmail.com',
            ],
            'societe' => [
                'title' => '',
                'value' => '',
            ],
            'phone' => [
                'title' => '',
                'value' => '12.34.56.78.91',
            ],
            'country' => [
                'title' => '',
                'value' => 'France',
            ],
            'zip' => [
                'title' => '',
                'value' => '94210',
            ],
            'city' => [
                'title' => '',
                'value' => 'Saint Maur',
            ],
            'address' => [
                'title' => '',
                'value' => '15 rue de Paris',
            ],
            'login' => [
                'title' => '',
                'value' => 'example',
            ],
            'password' => [
                'title' => '',
                'value' => 'xxxxxxxx',
            ],
        ];
        foreach ($this->fieldsForCSV as $k => &$v)
            if (empty($v['title']))
                $v['title'] = $k;
    }

    private function getTypes()
    {
        if (empty($this->types))
            $this->types = UserTypes();
        return $this->types;
    }

    private function import_clients($data)
    {
        $errors = [];
        $data = array_map(function ($el) {
            return htmlentities(urldecode(str_replace('%C2%A0', '', urlencode($el))));
        }, $data);
        $data['usertype'] = UserTypes()->getClient();
//        $id = $data['id'] ?? 0;
        if (isset($data['id']))
            unset($data['id']);
        if (isset($data['gender(mr,mrs)']))
            unset($data['gender(mr,mrs)']);
        $data['password1'] = $data['password'];
        $data['registeredBy'] = 'csv';
        return $data;
    }

    private function csvToArray($file, $delimiter)
    {
        $arr = [];
        if (($handle = fopen($file, 'r')) !== FALSE) {
            $i = 0;
            while (($lineArray = fgetcsv($handle, 4000, $delimiter, '"')) !== FALSE) {
                foreach ($lineArray as $j => $v)
                    $arr[$i][$j] = urldecode(str_replace('%EF%BB%BF', '', str_replace('%C2%A0', '', urlencode($v))));
                $i++;
            }
            fclose($handle);
        }
        return $arr;
    }

    private static function checkUser($param = null)
    {
        if (!isset($_SESSION[MODULE]['user']))
            return false;
        return is_null($param) ? $_SESSION[MODULE]['user'] : $_SESSION[MODULE]['user'][$param];
    }

    public static function baseSelect($param = ['*'])
    {
        return UserModel::baseSelect($param);
    }

    public static function getTable()
    {
        return UserModel::getTable();
    }

    public static function customer_availability($filters, $columns = ['*'])
    {
        return UserModel::customer_availability($filters, $columns);
    }

    public static function getAllUsers(array $columns = ['*'])
    {
        return UserModel::getAll($columns);
    }

    public static function getClientsRegisteredByHimself(array $columns = ['*'])
    {
        return UserModel::getClientsRegisteredByHimself($columns);
    }

    public static function getAllClients(array $columns = ['*'])
    {
        return self::getByManyTypeIds(UserTypes()->getClient(), $columns);
    }

    public static function editAllUsersData($data)
    {
        return UserModel::updateAll($data);
    }

    public static function getOnLine(array $columns = ['*'], array $types = [])
    {
        return UserModel::getOnLine($columns, $types);
    }

    public static function isMainClient($id)
    {
        return (bool)Project::getCntProjectsForMainClient($id);
    }

    public static function setOnLine($id, $regenerate = false)
    {
        $arr = ['is_online' => 1, 'last_login_ip' => $_SERVER['REMOTE_ADDR'], 'session_id' => Login()->encrypt_password(session_id())];
        if (!$regenerate) {
            $arr['lastlogin'] = date('Y-m-d H:i:s');
        }
        return UserModel::update($id, $arr);
    }

    public static function setSuperAdmins($ids, $id)
    {
        $_SESSION[MODULE]['superadmins']['GIds'] = $ids;
        $_SESSION[MODULE]['superadmins']['GId_id'] = $id;
    }

    public static function setOffLine($ids)
    {
        return UserModel::update($ids, ['is_online' => 0, 'session_id' => null]);
    }

    public function update($user_ids, $data)
    {
        return UserModel::update($user_ids, $data);
    }

    public static function getColumns($asAccos = false)
    {
        if (empty(self::$columns))
            self::$columns = DBManager()->getTableColumns(self::getTable());
        return $asAccos ? array_flip(self::$columns) : self::$columns;
    }

    public function updateHimself()
    {
        $current_user = self::getById($this->getId());
        $current_user['accessToken'] = $this->accessToken;
        $current_user['profileid'] = $this->profileid;
        self::setSession($current_user);
        $this->__construct();
    }

    public static function setSession(&$arr)
    {
        $userColumns = DBManager()->getTableColumns(self::getTable());
        foreach ($userColumns as $col) {
            $_SESSION[MODULE]['user'][$col] = $arr[$col] ?? '';
        }
        $_SESSION[MODULE]['user'] = array_merge($_SESSION[MODULE]['user'], [
            'login' => $arr['email'],
            'signature' => $arr['firstname'] . ' ' . $arr['lastname'],
            'usertype_signature' => UserTypes()->getTypeById($arr['usertype'])
        ]);

        $_SESSION[MODULE]['user']['accessToken'] = $arr['accessToken'] ?? false;
        $_SESSION[MODULE]['user']['global_login'] = $arr['login'] ?? '';
        $_SESSION[MODULE]['user']['profileid'] = $arr['profileid'] ?? 0;

        if (!isset($_SESSION[MODULE]['user']['keepMe']))
            $_SESSION[MODULE]['user']['keepMe'] = false;
        $forUnset = [
            'password',
            'encrypt',
            'cookie',
        ];
        $_SESSION[MODULE]['user'] = array_diff_key($_SESSION[MODULE]['user'], array_flip($forUnset));
    }

    public function addClient($data, $fromCSV = false)
    {
        if (!isset($data['usertype']))
            $data['usertype'] = UserTypes()->getClient();

        $data['password'] = trim($data['password']);
//        $data['password1'] = trim($data['password1']);
        $arr = ['email', 'firstname', 'lastname'];
        if (isset($data['registeredBy']) && !in_array($data['registeredBy'], ['himself', 'himself_from_landing']) && !$fromCSV)
            $arr[] = 'position_held_id';
        foreach ($arr as $val) {
            if (empty($data[$val])) {
                return mb_ucfirst(lng('fill_in_required_fields'));
            }
        }
//        if ($data['password'] != $data['password1']) {
//            return mb_ucfirst(lng('these_passwords_are_different'));
//        }
        $password = $data['password'];
        $data['password'] = Login()->encrypt_password($data['password']);
        if (filter_var($data['email'], FILTER_VALIDATE_EMAIL) === false) {
            return mb_ucfirst(lng('this_email_is_not_valid'));
        }

        /* !!! удалено согласно новому способу регистрации */
        /* if (!email_is_unique($data['email'])) {
            return mb_ucfirst(lng('this_login_is_already_taken'));
        }
        /**/
        if (!self::global_login_is_unique($data['login'])) {
            return mb_ucfirst(lng('this_login_is_already_taken'));
        }

        $collaborator = $data['collaborator'] ?? 0;
        $employe = $data['employe'] ?? 0;
        if ($collaborator == 1)
            $data['parent_id'] = $this->getId();
        $data['registered'] = date('Y-m-d');
        if ($this->getUser()) {
            if ($this->isRGPDUser()) {
                $data['consultant_id'] = $this->getId();
            }
            $data['who_registered'] = $this->getId();
        }
        $data = array_map(function ($elem) {
            return is_array($elem) ? json_encode($elem) : $elem;
        }, $data);
        if (isset($data['position_held_id']) && isset($data['position_held_name']) && intval($data['position_held_id']) == 0 && !empty(trim($data['position_held_name']))) {
            $data['position_held_id'] = PositionHeld::create($data['position_held_name']);
        }
        $clientid = UserModel::add(array_intersect_key($data, self::getColumns(true)));
        if (!is_numeric($clientid))
            return false;

        AppEventsController::publish(AppEventsSubscribersController::newClientEvent(), compact('clientid'));
        $offers_checked = $data['offers_checked'] ?? '';
        if ($this->getUser() && $this->isRGPDMaster() && !$fromCSV && !empty($offers_checked)) {
            $offer_ids = [];
            $offer_ids = explode(',', $offers_checked);
            $offer_ids = clearParams($offer_ids);
            $master_offers = Offer::getOffersForMaster($this->getId(), ['id']);
            if ($master_offers) {
                $master_offers = array_column($master_offers, 'id');
                $offer_ids = array_intersect($offer_ids, $master_offers);
            }
            Offer::addManyOffersForClient($offer_ids, $clientid);
        }
        elseif (($this->getUser() && $this->isSupervisor()) || (isset($data['registeredBy']) && in_array($data['registeredBy'], ['himself', 'himself_from_landing']) && !$fromCSV)) {
            $all_offer_ids = Offer::getAllOffers(['id']);
            $all_offer_ids = array_column($all_offer_ids, 'id');
            Offer::addManyOffersForClient($all_offer_ids, $clientid);
        }
        //Временно
        if (!is_array($offers_checked))
            $offers_checked = explode(',', $offers_checked);
        $offers_checked = array_diff([491, 492, 493], $offers_checked);
        if (!empty($offers_checked)) {
            Offer::addManyOffersForClient($offers_checked, $clientid);
        }

        //---------------------------------------------------------------------------
        if (isset($data['sent_to_user']) && $data['sent_to_user'] == 1) {
            $this->sendMailWithPassword($password, $data);
        }
        else {
            $this->sendMailWithPassword($password, $data, false);
        }
        if ($collaborator == 1)
            CollaboratorProjectAccess::add(getLastId(), (isset($data['general_project_id'])) ? $data['general_project_id'] : 0);

//        history('Utilisateurs', $this->getSignature() . ' added new client: ' . clear_post($data['firstname'] . ' ' . $data['lastname']));
        return $clientid;
    }

    public function sendMailWithPassword($password, $data, $with_login_pass = true)
    {
        $to_email = clear_post($data['email']);
        $to_name = clear_post(trim($data['firstname'] . ' ' . $data['lastname']));
        $subject = mb_ucfirst(lng('your_login_credentials'));
        if (!isset($data['message_to_user']))
            $data['message_to_user'] = '';
        if ($password != '')
            $data['message_to_user'] .= ' <br> ' . lng('password') . ' : ' . $password;
        $message_to_user = $data['message_to_user'] ?? '';
        $from_name = 'Accueil';

        $mail_template = $with_login_pass ? 'mail_password1' : 'mail_register_user';
        $mailbody_html = Templates::getTemplate($mail_template, [
            'login' => $data['login'],
            'password' => $password,
        ]);
        return Mails()->send_one_mail('contact@taskeo.net', $from_name, $to_email, $to_name, $subject, $mailbody_html, $message_to_user);
    }

    public function updateClient($clientid, $data, $by_client_himself = false)
    {
        $data_for_GL = $data;

        $user_to_update = self::getById($clientid);

        $clientid = escape_string(clear_post($clientid));
        if (isset($data['status']) && $user_to_update['status'] !== $data['status']) {
            \classSchedule::setScheduleEmail(intval($user_to_update['status']), intval($data['status']), intval($clientid));
        };
        if (!isset($data['usertype']))
            $data['usertype'] = UserTypes()->getClient();
        if (isset($data['offers_checked'])) {
            $offers_checked = $data['offers_checked'];
            unset($data['offers_checked']);
        }
        if (isset($data['password']) && $data['password'] != '') {
            $data['password'] = Login()->encrypt_password($data['password']);  // deprecated!!! pass from Tackea DB do not used
        }
        else {
            unset($data['password']);
        }
        $collaborator = $data['collaborator'] ?? 0;
        $employe = $data['employe'] ?? 0;
        unset($data['employe']);
        if ($collaborator == 1)
            $data['parent_id'] = $this->getId();
        unset($data['collaborator']);
        if (isset($data['services']))
            $services = $data['services'];
        unset($data['services']);
        $data = array_map(function ($elem) {
            return is_array($elem) ? json_encode($elem) : $elem;
        }, $data);

        require_once($_SERVER['DOCUMENT_ROOT'] . DS . MODULE . DS . 'messenger_tm/glooda_api.php');
        $ga = new GloodaAPI('logged_in_user', array('supportUsersData' => array(), 'supportDialogID' => 0, 'token' => $_SESSION[MODULE]['user']['accessToken']));
        $updateGLsuccess = true;
        if ($ga->login_OK) {
            if (isset($data_for_GL['birthday'])) $data_for_GL['birthday'] = strtotime($data_for_GL['birthday']); //todo: проверить (ошибка на месяц)
            if ($by_client_himself) {
                $updateGLsuccess = $ga->auth_saveProfile($data_for_GL);
                if (isset($data_for_GL['password']) && $data_for_GL['password'] != '') {
                    $updateGLsuccess = $ga->auth_updateSkey(
                        array('pass' => $data_for_GL['password'])
                    );
                }

            }
            else {
                $updateGLsuccess = $ga->auth_updateAccountProfile(array('id' => $user_to_update['global_id']), $data_for_GL);
                if (isset($data_for_GL['password']) && $data_for_GL['password'] != '') {
                    $updateGLsuccess = $ga->auth_updateAccountSkey(
                        array('id' => $user_to_update['global_id']),
                        array('pass' => $data_for_GL['password'])
                    );
                }
            }
        }

        try {
            if ($updateGLsuccess || is_null($updateGLsuccess))
                UserModel::update($clientid, array_intersect_key($data, self::getColumns(true)));
            if ($by_client_himself)
                $this->updateHimself();
        } catch (Exception $e) {
            return false;
        }

        if (isset($services) && is_array($services) && $data['usertype'] == UserTypes()->getClient()) {
//            ClientServices::updateServicesForClient($clientid, $services);
        }
        if ($collaborator == 1) {
            $general_project_id = (isset($data['general_project_id'])) ? $data['general_project_id'] : 0;
            $id = getLastId();
            CollaboratorProjectAccess::add($id, $general_project_id);
        }
        if (isset($data['firstname']) && isset($data['lastname']))
            history('Utilisateurs', $this->getSignature() . ' updated client information: ' . clear_post($data['firstname'] . ' ' . $data['lastname']));

        //return array($_SESSION[MODULE]['user']['accessToken'], $resGL, $data_for_GL); ///////////////////////test//////////////////////////
        return $clientid;
    }

    public function addUser($data)
    {
        $res = ['user' => 0, 'errors' => ''];
        if ($data['firstname'] == '' || $data['lastname'] == '' || $data['email'] == '' || $data['login'] == '' || $data['usertype'] == '') {
            $res['errors'] = mb_ucfirst(lng('fill_in_required_fields'));
            return $res;
        }

        /* !!!удалено согласно новому способу регистрации */
        /* if (!email_is_unique($data['email'])) {
            $res['errors'] = mb_ucfirst(lng('this_login_is_already_taken'));
            return $res;
        }*/
        if (!self::global_login_is_unique($data['login'])) {
            return mb_ucfirst(lng('this_login_is_already_taken'));
        }

        if ($data['usertype'] == '') {
            $res['errors'] = 'Not valid type';
            return $res;
        }
        if ($data['password'] != '') {
//            if ($data['password'] != $data['re_password']) {
//                $res['errors'] = mb_ucfirst(lng('wrong_repeat_password'));
//                return $res;
//            }
            $data['password'] = Login()->encrypt_password($data['password']);
        }
        else {
            $data['password'] = Login()->encrypt_password('12345678');
        }
        unset($data['re_password']);
        $data['img'] = $_SESSION['mainImage'] ?? '';
        $data['who_registered'] = $this->getId();
        if ($this->isRGPDUser()) {
            $data['parent_id'] = $this->getId();
            $data['consultant_id'] = $this->getId();
        }
        $new_id = UserModel::add($data);
        if (is_numeric($new_id)) {
            $res['user'] = $new_id;
            AppEventsController::publish(AppEventsSubscribersController::newUser(), ['id' => $new_id, 'who_add' => $this->getSignature(), 'who_was_added' => clear_post($data['firstname']) . ' ' . clear_post($data['lastname'])]);
        }
        else {
            $res['errors'] = 'User was not added!';
            return $res;
        }
        return $res;
    }

    public function deleteFoto($user_id)
    {
        $user_id = escape_string(clear_post($user_id));
        try {
            $res = UploadDir::delete($this->folder . DS . $user_id);
            if ($res)
                self::update($user_id, ['img' => '']);
            return $res;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function addDocForClient($user_id, $doc_name = '')
    {
        $user_id = escape_string(clear_post($user_id));
        $output_dir = $this->upload_dir . DS . $user_id . '/docs/';
        $upload_dir = $output_dir;
        $doc_name = escape_string(clear_post($doc_name));
        Folder::create($upload_dir);

        if (isset($_FILES['myfile'])) {
            $fileName = $_FILES['myfile']['name'];
            move_uploaded_file($_FILES['myfile']['tmp_name'], $upload_dir . $fileName);
            $path_info = pathinfo($upload_dir . $fileName);
            $extension = $path_info['extension'];
            $new_name = $upload_dir . $doc_name . '.' . $extension;
            rename($upload_dir . $fileName, $new_name);
            try {
                $res = UserModel::update($user_id, [$doc_name => $new_name]);
                return ['output_dir' => $output_dir, 'doc' => $doc_name, 'filename' => $doc_name . '.' . $extension];
            } catch (Exception $e) {
                return $e->getMessage();
            }
        }
    }

    public function deleteDocForClient($user_id, $doc_name)
    {
        $user_id = escape_string(clear_post($user_id));
        $doc_name = escape_string(clear_post($doc_name));
        if (empty($doc_name))
            return false;
        $dir = UploadDir::getRootPath() . $this->folder . DS . $user_id . '/docs/' . $doc_name;
        return Folder::delete($dir);
    }

    public function getDocsDataForJS($user_id, $doc_name)
    {
        $user_id = escape_string(clear_post($user_id));
        $doc_name = escape_string(clear_post($doc_name));
        $dir = UploadDir::getRootPath() . $this->folder . DS . $user_id . '/docs/';
        $files = @scandir($dir);
        $file = '';
        if (!$files)
            return '';
        foreach ($files as $f) {
            if (strpos($f, $doc_name) !== false) {
                $file = $f;
                break;
            }
        }
        if (empty($file))
            return '';
        $info = new SplFileInfo($dir . $file);
        $ret = [[
            'name' => $info->getFilename(),
            'path' => $info->getRealPath(),
            'size' => $info->getSize(),
        ]];
        return $ret;
    }

    public function getFotoDataForJS($user_id)
    {
        $user_id = escape_string(clear_post($user_id));
        $dir = UploadDir::getRootPath($this->folder . DS . $user_id . DS);
        $dir1 = UploadDir::getRelativePath() . $this->folder . DS . $user_id . DS;
        $dir = changeSlash($dir);
        $dir1 = changeSlash($dir1);
        $dir1 = ltrim($dir1, DS);
        $file = self::getById($user_id, ['img']);
        $file = $file['img'];
        $ret = [[
            'name' => $file,
            'path' => $dir1 . $file,
            'size' => !empty($file) ? filesize($dir . $file) : 0,
        ]];
        return json_encode($ret);
    }

    public function getPathToDocs($user_id, $doc_name)
    {
        return DS . MODULE . UploadDir::getRelativePath() . $this->folder . DS . $user_id . DS . 'docs' . DS . $doc_name;
    }

    public function delete($user_id)
    {
        $user_id = escape_string(clear_post($user_id));
        history('Users', $this->getSignature() . ' deleted user ID:' . $user_id);
        $path = UploadDir::getRootPath() . $this->folder . DS . $user_id . DS;
        Folder::delete($path);

//        $dossier_ids = query('SELECT `id` FROM ' . Dossier::getTable() . ' WHERE `client_id` = ' . $user_id);
//        if (count($dossier_ids)) {
//            foreach ($dossier_ids as $dossier_id) {
//                history('Utilisateurs', $this->getSignature() . ' deleted dossier ID:' . $dossier_id['id']);
//                Dossier::delete($dossier_id['id']);
//            }
//        }

        $user_to_delete = User::getById($user_id, ['usertype', 'global_id']);
        $res = query('DELETE FROM ' . self::getTable() . ' WHERE id =' . $user_id);

        if ($res > 0 && $user_to_delete) {
            require_once($_SERVER['DOCUMENT_ROOT'] . DS . MODULE . DS . 'messenger_tm/glooda_api.php');
            $ga = new GloodaAPI('logged_in_user', array('supportUsersData' => array(), 'supportDialogID' => 0, 'token' => $this->accessToken));
            if ($ga->login_OK) {
                //$profileid = $ga->auth_getProfiles([$user_to_delete['global_id']])[0]['profileid'];
                require_once($_SERVER['DOCUMENT_ROOT'] . DS . MODULE . DS . 'messenger_tm/VMSG_messenger_functions.php');

                if ($user_to_delete['usertype'] == UserTypes()->getClient()) {
                    $dialods = V_Messenger::getAllDialogsbyClientid($user_id);
                    if ($dialods) $removed_from_arrdialogs = $ga->removeUserFromGroups(array('id' => $user_to_delete['global_id'])/*$profileid*/, $dialods, true);

                }
                else {
                    $dialods = V_Messenger::getAllDialogsbyClientid(false);
                    if ($dialods) $removed_from_arrdialogs = $ga->removeUserFromGroups(array('id' => $user_to_delete['global_id'])/*$profileid*/, $dialods);
                }
            }
        }

        return $res;

    }

    public function addFoto($user_id)
    {
        $user_id = escape_string(clear_post($user_id));
        $this->upload_dir = changeSlash($this->upload_dir);
        Folder::delete($this->upload_dir . DS . $user_id);
        if (!file_exists($this->upload_dir . DS . $user_id)) {
            $this->upload_dir .= DS . $user_id . DS;
            Folder::create($this->upload_dir);
        }
        if (isset($_FILES['myfile'])) {
            $ret = array();

//	This is for custom errors;
            /*	$custom_error= array();
                $custom_error['jquery-upload-file-error']="File already exists";
                echo json_encode($custom_error);
                die();
            */
            $error = $_FILES['myfile']['error'];
            //You need to handle  both cases
            //If Any browser does not support serializing of multiple files using FormData()
            if (!is_array($_FILES['myfile']['name'])) //single file
            {
                $fileName = $_FILES['myfile']['name'];
                //if(!isset($_SESSION['mainImage']) || $_SESSION['mainImage']=='') {
                $_SESSION['mainImage'] = $fileName;
                //}
                move_uploaded_file($_FILES['myfile']['tmp_name'], $this->upload_dir . $fileName);

                //resize image to max height 1200 px
                resizeImg($this->upload_dir . $fileName);
                $this->update($user_id, ['img' => $fileName]);
                $ret[] = $fileName;
            }
            return $ret;
        }
    }

    public function createCSVTemplateAddClientFile()
    {
        $this->fieldsForCSV;
        foreach (['title', 'value'] as $v)
            $tmp[] = array_column($this->fieldsForCSV, $v);
        $path = UploadDir::getRootPath() . $this->csvFileName;
        $fp = fopen($path, 'w');
        foreach ($tmp as $fields)
            fputcsv($fp, $fields, ';');
        fclose($fp);
        return true;
    }

    public function getCSVLink()
    {
        return UploadDir::getSrc($this->csvFileName);
    }

    public static function global_login_is_unique($login)
    {
        $res = self::getByLogin($login, ['id']);
        return empty($res) || !is_array($res);
    }

    public static function getByLogin($login, array $columns = ['*'])
    {
        return UserModel::getByLogin($login, $columns);
    }

    public static function SynchronizeUserDataByGlobalID($global_id, $data)  //Synchronize User data with Global Account
    {
        return UserModel::updateByGlobalIds($global_id, $data);
    }

    public static function setDateForPopup($id, $date = null)
    {
        return UserModel::update($id, ['dateForPopup' => $date]);
    }

    public static function setTimeToShowPopup($client_id)
    {
        $duration = ClientUsers::getDurationCurrentByClientId($client_id);
        if (!$duration)
            return false;

        $dateForPopup = self::getById($client_id, ['dateForPopup']);
        $dateForPopup = strtotime($dateForPopup['dateForPopup']);
        if ($dateForPopup <= 0)
            self::getInst()->update($client_id, ['dateForPopup' => date('Y-m-d H:i:s')]);
        return true;
    }

    public static function checkToShowPopup($client_id)
    {
        $duration = ClientUsers::getDurationCurrentByClientId($client_id);
        if (!$duration)
            return false;
        $dateForPopup = self::getById($client_id, ['dateForPopup']);
        $dateForPopup = strtotime($dateForPopup['dateForPopup']);
        $dateForPopup = $dateForPopup > 0 ? $dateForPopup : 0;
        $currentTime = strtotime(date('Y-m-d H:i:s'));
        $res = $currentTime > ($dateForPopup + $duration);
        if ($res && $dateForPopup != 0)
            ClientUsers::removeBasicByClientId($client_id);
        return !$res;
    }

    public static function addNewClientAccount($data)
    {

        if (!isset($data['usertype']))
            $data['usertype'] = UserTypes()->getClient();

        $data['password'] = trim($data['password']);
        $data['password1'] = trim($data['password1']);
        $arr = ['email', 'firstname', 'lastname', 'password', 'password1'];

        /*if (isset($data['registeredBy']) && $data['registeredBy'] != 'himself')
            $arr[] = 'position_held_id';
        foreach ($arr as &$val) {
            if (empty($data[$val])) {
                return mb_ucfirst(lng('fill_in_required_fields'));
            }
        }
        if ($data['password'] != $data['password1']) {
            return mb_ucfirst(lng('these_passwords_are_different'));
        }*/
        $password = $data['password'];
        $data['password'] = Login()->encrypt_password($data['password']);
        /*if (filter_var($data['email'], FILTER_VALIDATE_EMAIL) === false) {
            return mb_ucfirst(lng('this_email_is_not_valid'));
        }*/

        /* !!! удалено согласно новому способу регистрации */
        /**
         * if (!email_is_unique($data['email'])) {
         * return mb_ucfirst(lng('this_login_is_already_taken'));
         * }
         * /**/
        if (!self::global_login_is_unique($data['login'])) {
            return mb_ucfirst(lng('this_login_is_already_taken'));
        }

        /*$collaborator = isset($data['collaborator']) ? $data['collaborator'] : 0;
        $employe = isset($data['employe']) ? $data['employe'] : 0;
        if ($collaborator == 1)
            $data['parent_id'] = $this->getId(); */

        $data['registered'] = date('Y-m-d');

        /*if ($this->getUser()) {
            if ($this->isRGPDUser()) {
                $data['consultant_id'] = $this->getId();
            }
            $data['who_registered'] = $this->getId();
        }*/

        $data = array_map(function ($elem) {
            return is_array($elem) ? json_encode($elem) : $elem;
        }, $data);
        if (isset($data['position_held_id']) && isset($data['position_held_name']) && intval($data['position_held_id']) == 0 && !empty(trim($data['position_held_name']))) {
            $data['position_held_id'] = PositionHeld::create($data['position_held_name']);
        }

        //Сохранение в БД
        return UserModel::add(array_intersect_key($data, self::getColumns(true)));
    }

    public static function getParticipantsWithoutTypes($params = ['id', 'TRIM(CONCAT(firstname, SPACE(1), lastname)) AS user_name', 'firstname', 'lastname', 'img', 'usertype'])
    {
        return UserModel::getParticipantsWithoutTypes(
            $params,
            [UserTypes()->getClient()]
        );
    }

    public static function getParticipantsForClient($client_id, $params = ['*'])
    {
        $res = Project()->getProjectsByClientId($client_id, ['id']);
        $tmp = [];
        if ($res) {
            try {
                $res = array_column($res, 'id');
                $res = Project()->getOwnParticipants($res, $params);
                foreach ($res as $proj => $users) {
                    foreach ($users as $id => $user) {
                        $user['projects'][] = $proj;
                        if (!isset($tmp[$id]) && $user['id'] != $client_id) {
                            $tmp[$id] = $user;
                        }
                    }
                }
            } catch (Exception $e) {

            }
        }
        return $tmp;
    }

    public function importClientsFromXLS($cloneProjectForClient = false)
    {
        if (!isset($_POST['import_xls']))
            return false;
        $filename = pathinfo($_FILES['file']['name'], PATHINFO_FILENAME);

        $temp_data = $this->csvToArray($_FILES['file']['tmp_name'], ';');

        //Use first row for names
        $labels = array_shift($temp_data);
        $keys = array_values($labels);
        $data = [];

        // Bring it all together
        foreach ($temp_data as $v)
            $data[] = array_combine($keys, $v);

        $offer_ids = [];
        if ($this->isRGPDMaster() && isset($_POST['offers_checked'])) {
            $offer_ids = explode(',', $_POST['offers_checked']);
            $offer_ids = clearParams($offer_ids);
            $master_offers = Offer::getOffersForMaster($this->getId(), ['id']);
            if ($master_offers) {
                $master_offers = array_column($master_offers, 'id');
                $offer_ids = array_intersect($offer_ids, $master_offers);
            }
        }
        foreach ($data as $row) {
            $client_data = $this->import_clients($row);
            if (empty($client_data['login']) || empty($client_data['password']) || empty($client_data['email'])) continue;
            if (filter_var($client_data['email'], FILTER_VALIDATE_EMAIL) === false) continue;

            //Global registration
            require_once($_SERVER['DOCUMENT_ROOT'] . DS . MODULE . DS . 'messenger_tm/glooda_api.php');
            $ga = new GloodaAPI(
                'registration',
                array(
                    'login' => $client_data['login'],
                    'pass' => $client_data['password'],
                    'email' => $client_data['email']
                )
            );

            if ($ga->registration_OK) {
                $global_id = $ga->get_ID();
                $client_data['global_id'] = $global_id;

                $client_id = $this->addClient($client_data, true);
                if (!is_numeric($client_id))
                    continue;
            }
            else {
                $res = $ga->getNotificationsArr();
                continue;
            }

            if ($this->isRGPDMaster() && !empty($offer_ids)) {
                foreach ($offer_ids as $val) {
                    Offer::addOfferForClient($val, $client_id);
                }
            }

            if ($cloneProjectForClient) {

            }
        }
        redirect($_SERVER['REQUEST_URI']);
    }

    public static function getUserByEmail($email, $params = ['*'])
    {
        return UserModel::getByEmail($email, $params);
    }

//    public static function getUserByLogin($login, $params = ['*'])
//    {
//        $params = clearParams($params);
//        $login = escape_string(clear_post($login));
//        $sql = self::baseSelect($params) . ' WHERE login = "' . $login . '" LIMIT 1';
//        $res = query($sql);
//        return $res ? $res[0] : [];
//    }
    public static function getUserIdByHashAndPrefix($hash, $prefix)
    {
        return UserModel::getUserIdByHashAndPrefix($hash, $prefix);
    }

    public static function getUserByGlobalID($global_id, $params = ['*'])
    {
        return UserModel::getByGlobalID($global_id, $params);
    }

    public function updateByGlobalId($global_id, $data)
    {
        $id = $this->getUserByGlobalId($global_id, ['id'])['id'];
        return $this->update($id, $data);
    }

    public static function getHierarchyById($user_id)
    {
        return UserModel::getHierarchyById($user_id);
    }

    public static function getByManyTypeIds($ids, $params = ['*'])
    {
        return UserModel::getByManyTypeIds($ids, $params);
    }

    public static function getByEncrypt($encrypt, $params = ['*'])
    {
        return UserModel::getByEncrypt($encrypt, $params);
    }

    public static function getByTypeId($id, $params = ['*'])
    {
        return self::getByManyTypeIds($id, $params);
    }

    public static function getCntByType($ids)
    {
        return UserModel::getCntByType($ids);
    }

    public static function findBy($arr, $columns = ['*'])
    {
        return UserModel::findBy($arr, $columns);
    }

    public static function getClientsForConsultant($consultants_id, array $params = ['*'])
    {
        return UserModel::getClientsForConsultant($consultants_id, $params);
    }

    public static function getById($id, array $params = ['*'])
    {
        return UserModel::getById($id, $params);
    }

    public static function getManyByIds($ids, $params = ['*'])
    {
        return UserModel::getManyByIds($ids, $params);
    }

    public static function getManyUsersByGlobalIds($ids, $params = ['*'])
    {
        return UserModel::getManyUsersByGlobalIds($ids, $params);
    }

    public function getCollaboratorsByClientId($clientId, $params = ['*'])
    {
        return UserModel::getCollaboratorsByClientId($clientId, $params);
    }

    public function getCollaboratorsByProjectId($projectId, $params = ['*'])
    {
        if (is_scalar($projectId)) $projectId = [$projectId];
        $clientId = Project()->getManyByIds($projectId, ['client_id']);
        if (!$clientId) return [];
        $clientId = array_column($clientId, 'client_id');
        return $this->getCollaboratorsByClientId($clientId, $params);
    }

    public function getAllCollaborators($params = ['*'])
    {
        $projects = Project()->getAllProjects(['id']);

        if (empty($projects)) return [];
        $projectId = array_column($projects, 'id');
        return $this->getCollaboratorsByProjectId($projectId, $params);
    }


    public function getConsultantId()
    {
        return $this->user['consultant_id'];
    }

    public function getAllMyConsultants()
    {
        $obj = new Consultant();
        return $obj->getMasterIds($this->getId());
    }

    public function __set($name, $val)
    {
        $_SESSION[MODULE]['user'][$name] = $val;
        $this->updateHimself();
    }

    public function __get($name)
    {
        return self::checkUser($name);
    }

    public function getFName()
    {
        return self::checkUser('firstname');
    }

    public function getNomero()
    {
        return self::checkUser('nomero');
    }

    public function getNomeroTVA()
    {
        return self::checkUser('nomero_tva');
    }

    public function getFieldOfActivity()
    {
        return self::checkUser('field_of_activity');
    }

    public function getGender()
    {
        return self::checkUser('gender');
    }

    public function getType()
    {
        return self::checkUser('usertype');
    }

    public function getId()
    {
        return self::checkUser('id');
    }

    public function getGId()
    {
        return self::checkUser('global_id');
    }

    public function getMail()
    {
        return self::checkUser('email');
    }

    public function getUsertypeSignature()
    {
        return self::checkUser('usertype_signature');
    }

    public function getSignature()
    {
        return self::checkUser('signature');
    }

    public function getSociete()
    {
        return self::checkUser('societe');
    }

    public function getAddress()
    {
        return self::checkUser('address');
    }

    public function getLogin()
    {
        return self::checkUser('login');
    }

    public function getLName()
    {
        return self::checkUser('lastname');
    }

    public function getLastlogin()
    {
        return self::checkUser('lastlogin');
    }

    public function getParentId()
    {
        return self::checkUser('parent_id');
    }

    public function getPhone()
    {
        return self::checkUser('phone');
    }

    public function getPhone2()
    {
        return self::checkUser('phone2');
    }

    public function getUser()
    {
        return self::checkUser();
    }

    public function getImg()
    {
        return self::checkUser('img');
    }

    public function isChef()
    {
        return $this->usertype == $this->getTypes()->getChef();
    }

    public function isAssistant()
    {
        return $this->usertype == $this->getTypes()->getAssistant();
    }

    public function isTeamLead()
    {
        return $this->usertype == $this->getTypes()->getTeamLead();
    }

    public function isDeveloper1()
    {
        return $this->usertype == $this->getTypes()->getDeveloper1();
    }

    public function isDeveloper2()
    {
        return $this->usertype == $this->getTypes()->getDeveloper2();
    }

    public function isDeveloper3()
    {
        return $this->usertype == $this->getTypes()->getDeveloper3();
    }

    public function isDesigner1()
    {
        return $this->usertype == $this->getTypes()->getDesigner1();
    }

    public function isDesigner2()
    {
        return $this->usertype == $this->getTypes()->getDesigner2();
    }

    public function isEngineer1()
    {
        return $this->usertype == $this->getTypes()->getEngineer1();
    }

    public function isEngineer2()
    {
        return $this->usertype == $this->getTypes()->getEngineer2();
    }

    public function isContentEditor()
    {
        return $this->usertype == $this->getTypes()->getContentEditor();
    }

    public function isTranslator()
    {
        return $this->usertype == $this->getTypes()->getTranslator();
    }

    public function isCommunityManager1()
    {
        return $this->usertype == $this->getTypes()->getCommunityManager1();
    }

    public function isClient()
    {
        return ($this->parent_id == 0 && $this->usertype == $this->getTypes()->getClient());
    }

    public function isArtDirector()
    {
        return $this->usertype == $this->getTypes()->getArtDirector();
    }

    public function isAnimator()
    {
        return $this->usertype == $this->getTypes()->getAnimator();
    }

    public function isMarketingDirector()
    {
        return $this->usertype == $this->getTypes()->getMarketingDirector();
    }

    public function isTeleprospector()
    {
        return $this->usertype == $this->getTypes()->getTeleprospector();
    }

    public function isSecretory()
    {
        return $this->usertype == $this->getTypes()->getSecretory();
    }

    public function isMarketingAssistant()
    {
        return $this->usertype == $this->getTypes()->getMarketingAssistant();
    }

    public function isSupervisor()
    {
        return $this->usertype == $this->getTypes()->getSupervisor();
    }

    public function isRGPDSenior()
    {
        return $this->usertype == $this->getTypes()->getRGPDSenior();
    }

    public function isRGPDJunior()
    {
        return $this->usertype == $this->getTypes()->getRGPDJunior();
    }

    public function isRGPDManager()
    {
        return $this->usertype == $this->getTypes()->getRGPDManager();
    }

    public function isRGPDTechnician()
    {
        return $this->usertype == $this->getTypes()->getRGPDTechnician();
    }

    public function isRGPDAdministrator()
    {
        return $this->usertype == $this->getTypes()->getRGPDAdministrator();
    }

    public function isRGPDCurator()
    {
        return in_array($this->usertype, $this->getTypes()->getRGPDCurators());
    }

    public function isRGPDVendor()
    {
        return in_array($this->usertype, $this->getTypes()->getRGPDVendors());
    }

    public function isRGPDMaster()
    {
        return $this->usertype == $this->getTypes()->getRGPDMaster();
    }

    public function isManager()
    {
        return in_array($this->usertype, $this->getTypes()->getManagers());
    }

    public function isRGPDUser()
    {
        return in_array($this->usertype, $this->getTypes()->getRGPDUsers());
    }

    public function hasRGPDManagementRights()
    {
        return $this->isRGPDUser() || $this->isSupervisor() || $this->isRGPDAdministrator();
    }

    public function isEmploye()
    {
        return in_array($this->usertype, $this->getTypes()->getEmployes());
    }

    public function isCollaborator()
    {
        return ($this->parent_id != 0 && $this->usertype == $this->getTypes()->getClient());
    }

    public function isSuperAdmin($id = null)
    {
        if (is_null($id))
            $id = $this->getGId();
        return isset($_SESSION[MODULE]['superadmins']['GIds']) && in_array($id, $_SESSION[MODULE]['superadmins']['GIds']);
    }

    private function _setType($type)
    {
        $this->user['usertype'] = $type;
    }

    public function getNewArrivals($count, $params = ['*'])
    {
        return UserModel::getNewArrivals($count, $params);
    }

    public function getGLdataArr()
    {
        return [
            'tkn' => $this->accessToken,
            'aboutme' => $this->aboutme,
            'address' => $this->address,
            //'admin' => $_SESSION[MODULE]['user']['admin.........'],
            'birthday' => $this->birthday,
            'city' => $this->city,
            'country' => $this->country,
            'email' => $this->email,
            'firstname' => $this->firstname,
            'gender' => $this->gender,
            'id' => $this->global_id,
            //'image' => $_SESSION[MODULE]['user']['image........'],
            'lastname' => $this->lastname,
            'login' => $this->global_login,
            'phone' => $this->phone,
            //'pints' => $_SESSION[MODULE]['user']['pints......'],
            'profileid' => $this->profileid,
            //'settings' => $_SESSION[MODULE]['user']['address'], // {notifications: 1, removeAfter: 0, removeAfterUnlock: 0}
            'zip' => $this->zip,
        ];
    }

}