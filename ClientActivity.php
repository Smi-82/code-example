<?php

use models\ClientActivityModel;
use traits\SingleTone;

class ClientActivity
{
    private $classes;
    use SingleTone;

    private static function setShown()
    {
        $sql = update_query_string(ClientActivityModel::getTable(), [[], ['dateShow' => 'dateUpdated']]) . ' WHERE dateUpdated>dateShow';
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    private function __construct()
    {
        $this->classes = [
            BillProject::getClassName() => [
                'table_name' => 'validation du Bon de commande',
                'obj_href' => 'bill_invoice.php?project='
            ],
            ExpressionOfNeeds::getClassName() => [
                'table_name' => 'commentaires à l\'expression des besoins',
                'obj_href' => 'expression-of-needs.php?project='
            ],
            Project::getClassName() => [
                'table_name' => 'commentaires au projet',
                'obj_href' => 'task_list.php?project='
            ],
            Task::getClassName() => [
                'table_name' => 'commentaires aux tâches',
                'obj_href' => 'single_task.php?id='
            ],
        ];
    }

    public static function baseSelect(array $columns = ['*'], $aggregate = null)
    {
        return ClientActivityModel::baseSelect($columns, $aggregate);
    }

    public static function getTable()
    {
        return ClientActivityModel::getTable();
    }

    public static function getGetedByUserId($id, array $columns = ['*'])
    {
        return ClientActivityModel::getGetedByUserId($id, $columns);
    }

    public static function freeTask($id)
    {
        try {
            $res = ClientActivityModel::getById($id, ['IF(dateUpdated>=dateGet,"1","0") AS updated']);
            if ($res) {
                ClientActivityModel::updateById($id, [
                    'dateGet' => date('Y-m-d H:i:s'),
                ]);
                return 'open';
            }
            else {
                ClientActivityModel::updateById($id, [
                    'dateFree' => date('Y-m-d H:i:s'),
                    'isClose' => 1,
                    'whoGet' => 0
                ]);
                return 'closed';
            }
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function updateById($id, $data)
    {
        return ClientActivityModel::updateById($id, $data);
    }

    public function getTables()
    {
        return $this->classes;
    }

    public static function getData()
    {
        $cash = [];
        $sql = ClientActivityModel::baseSelect([
                'id',
                'project_id',
                'obj_id',
                'className',
                'dateUpdated',
                'whoGet',
                'dateGet'
            ]) . ' WHERE isClose=0 ';

        if (!(User()->isSupervisor())) {
            $pr_ids = Project::getProjectListForUser(User()->getId(), true);
            asort($pr_ids);
            $sql .= ' AND project_id IN (' . $pr_ids . ')';
        }
        $sql .= ' ORDER BY dateUpdated DESC';
        try {
            $result = query($sql);
            $result = array_map(function ($elem) {
                $elem['dateUpdated'] = Timing::asString(Timing::get(time() - strtotime($elem['dateUpdated'])));
                if ($elem['dateGet'])
                    $elem['dateGet'] = Timing::asString(Timing::get(time() - strtotime($elem['dateGet'])));
                return $elem;
            }, $result);
        } catch (Exception $e) {
            return $e->getMessage();
        }
        $users_needs_ids = [];
        $pr_ids = array_column($result, 'project_id');
        $pr_ids = array_unique($pr_ids);
        asort($pr_ids);
        $projects = Project()->getManyByIds($pr_ids, ['id', 'client_id', 'name']);
        $projects = valueToKey('id', $projects);
        foreach ($result as &$row) {
            if (!isset($cash[$row['project_id']]['client'])) {
                $proj = $projects[$row['project_id']];
                $cash[$row['project_id']]['project_name'] = $proj['name'];
                $cash[$row['project_id']]['client'] = $proj['client_id'];
                if (!in_array($proj['client_id'], $users_needs_ids))
                    $users_needs_ids[] = $proj['client_id'];
            }
            if (intval($row['whoGet'])) {
                $users_needs_ids[] = $row['whoGet'];
            }

        }
        $project_ids_withoutAssistents = array_keys(array_filter($cash, function ($elem) {
            return !isset($elem['assistents']);
        }));
        $ass_id = [];
        if (!empty($project_ids_withoutAssistents)) {
            $assistants = Assistant::getByManyProjectId($project_ids_withoutAssistents, ['id']);
            foreach ($cash as $p_id => $val) {
                $cash[$p_id]['assistents'] = !empty($assistants[$p_id]) ? array_column($assistants[$p_id], 'id') : [];
            }
            $ass_id = array_unique(array_reduce(array_map(function ($elem) {
                return array_column($elem, 'id');
            }, $assistants), function ($res, $elem) {
                return array_merge($res, $elem);
            }, []));
        }
        $ut_obj = UserTypes();
        $client_onLine = [];
        if (User()->isSupervisor()) {
            $client_onLine = User::getOnLine(['id'], [$ut_obj->getClient()]);
            $client_onLine = array_column($client_onLine, 'id');
        }
        else {
            if (!empty($pr_ids)) {
                $client_onLine = Project()->getManyByIds($pr_ids, ['client_id']);
                $client_onLine = array_column($client_onLine, 'client_id');
            }
        }

        $users_needs_ids = array_unique(array_merge($users_needs_ids, $ass_id, $client_onLine));
        $users = [];
        if (!empty($users_needs_ids)) {
            $users = User::getManyByIds($users_needs_ids, ['id', 'img', 'firstname', 'lastname', 'is_online', 'lastlogin', 'usertype']);
            $users = array_map(function ($elem) {
                $elem['img'] = getImg($elem, $elem['id']);
                $elem['fullName'] = trim(mb_ucfirst($elem['firstname']) . ' ' . mb_ucfirst($elem['lastname']));
                return $elem;
            }, $users);
        }
        $client_onLine = array_filter($users, function ($el) use ($ut_obj) {
            return ($el['is_online'] == 1 && $el['usertype'] == $ut_obj->getClient());
        });

        if (!empty($client_onLine)) {
            $client_onLine = array_values($client_onLine);
            usort($client_onLine, function ($a, $b) {
                $a = $a['lastlogin'];
                $b = $b['lastlogin'];
                if ($a == $b) {
                    return 0;
                }
                return ($a > $b) ? -1 : 1;
            });
            $client_onLine = array_map(function ($el) {
                $el['lastlogin'] = Timing::asString(Timing::get(time() - strtotime($el['lastlogin'])));
                return $el;
            }, $client_onLine);
        }
        foreach ($cash as $p_id => $val) {
            if (isset($users[$val['client']]))
                $cash[$p_id]['client'] = $users[$val['client']];
            if (!empty($cash[$p_id]['assistents']))
                $cash[$p_id]['assistents'] = array_map(function ($elem) use ($users) {
                    return $users[$elem];
                }, $cash[$p_id]['assistents']);
        }
        foreach ($result as $key => &$row) {
            $result[$key] = array_merge($row, $cash[$row['project_id']]);
            if (intval($row['whoGet'])) {
                $result[$key]['whoGetObj'] = $users[$row['whoGet']];
            }
        }
        self::setShown();
        return ['general' => $result, 'clients_onLine' => $client_onLine];
    }

    public static function add($project_id, $className, $obj_id = 0)
    {
        $className = escape_string(clear_post($className));
        $project_id = escape_string(clear_post($project_id));
        $obj_id = escape_string(clear_post($obj_id));
        $token = md5($className . $obj_id . $project_id);
        $res = ClientActivityModel::getByToken($token, ['COUNT(token) AS cnt']);
        $res = intval($res['cnt']);
        if ($res) {
            return self::update($token, [[
                'dateUpdated' => date('Y-m-d H:i:s'),
                'whoUpdated' => User()->getId(),
                'isClose' => 0
            ], ['cntUpdates' => 'cntUpdates +1']]);
        }
        else {
            return ClientActivityModel::add([
                'className' => $className,
                'obj_id' => $obj_id,
                'project_id' => $project_id,
                'token' => $token,
                'whoCreated' => User()->getId(),
                'whoUpdated' => User()->getId()
            ]);
        }
    }

    private static function checkGeted($id)
    {
        $res = ClientActivityModel::getById($id, ['whoGet']);
        return $res ? (bool)$res['whoGet'] : $res;
    }

    public static function getTask($id)
    {
        if (self::checkGeted($id))
            return false;
        try {
            return ClientActivityModel::updateById($id, [
                'whoGet' => User()->getId(),
                'dateGet' => date('Y-m-d H:i:s'),
            ]);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function update($token, $data)
    {
        return ClientActivityModel::updateByToken($token, $data);
    }
}