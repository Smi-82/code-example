<?php


class Logger
{
    private static $dir = 'log';

    private static function createDirectory()
    {
        $path = UploadDir::getRootPath(self::$dir);
        if (!file_exists($path))
            Folder::create($path);
        return $path;
    }

    private static function getDirectory()
    {
        return UploadDir::getRootPath(self::$dir);
    }

    private static function getFilePath($file, $exp = 'txt')
    {
        $path = self::getDirectory();
        $path .= DS . $file . '.' . $exp;
        return $path;
    }

    public static function add($file, $text, $exp = 'txt')
    {
        self::createDirectory();
        $path = self::getFilePath($file, $exp);
        return file_put_contents($path, $text . PHP_EOL, FILE_APPEND | LOCK_EX);
    }

    public static function delete($file)
    {
        $path = self::getFilePath($file);
        return Folder::delete($path);
    }
}