<?php

use traits\SingleTone;

class QueryStr
{
    use SingleTone;

    private function escape_string($str)
    {
        return mysqli_real_escape_string(DBManager()->getLink(), $str);
    }

    private function clear_post($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return checkallowtags($data);
    }

    private function query_string($type, $table, array $data)
    {
        $str = $type . ' `' . $table . '` SET ';
        reset($data);
        $data1 = []; // key => val
        $data2 = []; // key => mysql entities
        if (key($data) === 0) {
            if ($data[0]) $data1 = $data[0];
            if ($data[1]) $data2 = $data[1];
        }
        else {
            $data1 = $data;
        }

        if (count($data1)) foreach ($data1 as $key1 => &$val1)
            $str .= '`' . trim($this->escape_string($key1)) . '` = "' . trim($this->escape_string($val1)) . '", ';
        if (count($data2)) foreach ($data2 as $key2 => &$val2)
            $str .= '`' . trim($this->escape_string($key2)) . '` = ' . trim($val2) . ', ';
        $str = trim($str);
        return rtrim($str, ',');
    }

    public function insert_query_string($table, array $data = array(), $ignore = false)
    {
        $ignore = $ignore ? ' IGNORE ' : ' ';
        if ($table && !empty($data))
            return $this->query_string('INSERT' . $ignore . 'INTO', $table, $data);
        return false;
    }

    public function multiple_insert($table, array $values, $ignore = false)
    {
        if (empty($values)) {
            throw new Exception('empty array values in multiple_insert function');
        }
        $values = array_values($values);
        $columns = array_keys($values[0]);
        $ignore = $ignore ? ' IGNORE ' : ' ';
        $columns = clearParams($columns);
        $columns = implode(',', $columns);
        $values = array_map(function ($el) {
            $el = array_map(function ($e) {
                $e = escape_string(clear_post($e));
                return '"' . $e . '"';
            }, $el);
            return '(' . implode(',', $el) . ')';
        }, $values);
        $values = implode(',', $values);
        return 'INSERT' . $ignore . 'INTO ' . $table . ' (' . $columns . ') VALUES ' . $values;
    }

    public function update_query_string($table, array $data = array())
    {
        if ($table && !empty($data))
            return $this->query_string('UPDATE', $table, $data);
        return false;
    }

    public function explodeForSearch($phrase)
    {
        $inBracketsPattern = "/\(([^\(\)]*?)\)/";
        $inBracketsMatches = [];
        preg_match_all($inBracketsPattern, $phrase, $inBracketsMatches);
        $phrase = preg_replace($inBracketsPattern, '', $phrase);
        $inBracketsParts = [];
        if (isset($inBracketsMatches[1])) $inBracketsParts = array_map(function ($item) {
            return array_filter(explode(' ', $item));
        }, $inBracketsMatches[1]);
        $otherParts = array_filter(explode(' ', $phrase));
        return compact('inBracketsMatches', 'inBracketsParts', 'otherParts');
    }
}