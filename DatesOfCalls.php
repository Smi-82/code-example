<?php

use models\DatesOfCallsModel;

class DatesOfCalls
{
    private static function _prepareData($data)
    {
        $res = array_map(function ($el) {
            $el['date_for_calendar'] = $el['d_calls'];
            $el['d_calls'] = date('d-m-Y H:i', strtotime($el['d_calls']));
            return $el;
        }, $data);
        $user_ids = array_reduce($res, function ($r, $el) {
            $r[] = intval($el['client_id']);
            $r[] = intval($el['who_add']);
            $r[] = intval($el['seller']);
            return $r;
        }, []);
        $user_ids = array_unique($user_ids);
        $users = User::getManyByIds($user_ids, ['lastname', 'firstname', 'id', 'img']);
        $users = array_map(function ($el) {
            $el['img'] = getImg($el, $el['id']);
            return $el;
        }, $users);
        foreach ($res as &$v)
            foreach (['client_id', 'who_add', 'seller'] as $field)
                $v[$field] = $users[$v[$field]];
        return $res;
    }

    public static function add(array $data)
    {
        if (isset($data['id']))
            unset($data['id']);
        $data['d_calls'] = date('Y-m-d H:i:s', strtotime($data['date']));
        unset($data['date']);
        $data['who_add'] = User()->getId();
        $data['seller'] = intval($data['seller']) ? $data['seller'] : $data['who_add'];
        return DatesOfCallsModel::add($data);
    }

    public static function update($id, $data)
    {
        return DatesOfCallsModel::update($id, $data);
    }

    public static function getAll(array $params = ['*'])
    {
        $res = DatesOfCallsModel::getAll();
        $res = self::_prepareData($res);
        return $res;
    }

    public static function getByClientId($id, array $params = ['*'])
    {
        $res = DatesOfCallsModel::getByClientId($id, $params);
        $res = self::_prepareData($res);
        return $res;
    }

    public static function getByWhoAdd($id, array $params = ['*'])
    {
        $res = DatesOfCallsModel::getByWhoAdd($id, $params);
        $res = self::_prepareData($res);
        return $res;
    }

    public static function deleteById($id)
    {
        return DatesOfCallsModel::deleteById($id);
    }
}