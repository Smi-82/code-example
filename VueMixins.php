<?php


class VueMixins
{
    public static function includeScripts($mixins = [])
    {
        if (!is_array($mixins))
            $mixins = [$mixins];
        $str = '';
        if (!empty($mixins))
            foreach ($mixins as $mixin)
                $str .= '<script type="text/javascript" src="js/vueMixins/' . $mixin . '.js"></script>';
        return $str;
    }
}