<?php


use admNotifyClasses\abstracts\NotifyWorker;
use admNotifyClasses\NotifyDeleteProject;
use admNotifyClasses\NotifyDeleteUserFromTask;
use admNotifyClasses\NotifyNewClient;
use admNotifyClasses\NotifyNewProject;
use admNotifyClasses\NotifyTaskComments;
use admNotifyClasses\NotifyTaskFile;
use admNotifyClasses\NotifyTaskPriority;
use admNotifyClasses\NotifyTaskStatus;
use admNotifyClasses\NotifyUserToTask;


class AdmNotify extends NotifyWorker
{
    public function setDeleteProject($project_id = 0, $data = [])
    {
        $this->worker = new NotifyDeleteProject($project_id, $data);
        return $this;
    }

    public function setNewProject($project_id = 0, $data = [])
    {
        $this->worker = new NotifyNewProject($project_id, $data);
        return $this;
    }

    public function setNewClient($client_id = 0, $data = [])
    {
        $this->worker = new NotifyNewClient($client_id, $data);
        return $this;
    }

    public function setDeleteUserFromTask($task_id = 0, $data = [])
    {
        $this->worker = new NotifyDeleteUserFromTask($task_id, $data);
        return $this;
    }

    public function setTaskComments($task_id = 0, $data = [])
    {
        $this->worker = new NotifyTaskComments($task_id, $data);
        return $this;
    }

    public function setTaskFile($task_id = 0, $data = [])
    {
        $this->worker = new NotifyTaskFile($task_id, $data);
        return $this;
    }

    public function setTaskPriority($task_id = 0, $data = [])
    {
        $this->worker = new NotifyTaskPriority($task_id, $data);
        return $this;
    }

    public function setTaskStatus($task_id = 0, $data = [])
    {
        $this->worker = new NotifyTaskStatus($task_id, $data);
        return $this;
    }

    public function setUserToTask($task_id = 0, $data = [])
    {
        $this->worker = new NotifyUserToTask($task_id, $data);
        return $this;
    }

    public function getWorker()
    {
        return $this->worker;
    }

    public function get(array $exclude = [])
    {
        $sql = 'SELECT n.*, TRIM(CONCAT(wd.firstname," ",wd.lastname)) AS who_do FROM ' . self::getTable() . ' AS n LEFT JOIN ' . User::getTable() . ' AS wd ON wd.id=n.who_do WHERE n.who_see=' . User()->getId();
        if (!empty($exclude)) {
            $exclude = clearParams($exclude);
            $sql .= ' AND n.id NOT IN (' . implode(', ', $exclude) . ')';
        }
        $sql .= ' ORDER BY n.class_name, n.object_id';
        try {
            $res = query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
        $obj = '';
        foreach ($res as &$val) {
            $key = $val['class_name'];
            unset($val['class_name']);
            unset($val['who_see']);
            try {
                if (empty($obj) || get_class($obj) != $key) {
                    $key = '\\admNotifyClasses\\' . $key;
                    $obj = new $key();
                }

                $val['msg'] = $obj->getMsg($val);
                $val['href'] = $obj->getHref($val);
            } catch (Exception $e) {
                $val['msg'] = '';
                $val['href'] = '';
            }
            unset($val['object_id']);
        }
        usort($res, function ($a, $b) {
            if ($a['id'] == $b['id']) {
                return 0;
            }
            return ($a['id'] < $b['id']) ? -1 : 1;
        });
        return $res;
    }

    public function update($ids, $data)
    {
        $ids = getAsArray($ids);
        $sql = update_query_string(self::getTable(), $data) . ' WHERE id IN (' . implode(',', $ids) . ')';
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function close(array $ids = [0])
    {
        $ids = clearParams($ids);
        $sql = 'DELETE FROM ' . self::getTable() . ' WHERE id IN (' . implode(', ', $ids) . ') AND who_see=' . User()->getId();
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}