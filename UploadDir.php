<?php


class UploadDir
{
    private static $relativePath = DS . 'upload' . DS;
    private static $path = ROOT . DS . 'upload' . DS;

    public static function createDir($path)
    {
        $path = self::getRootPath() . $path;
        Folder::create($path);
        return file_exists($path);
    }

    public static function delete($path)
    {
        $path = self::getRootPath() . $path;
        if (file_exists($path)) {
            @unlink($path);
            Folder::delete($path);
        }
        return !file_exists($path);
    }

    public static function getRootPath($p = '')
    {
        $p = ltrim($p, '\\');
        $p = ltrim($p, '/');
        return changeSlash(self::$path . $p);
    }

    public static function getSrc($path = '')
    {
        $path = DS . MODULE . self::getRelativePath() . $path;
        return changeSlash($path);
    }

    public static function getRelativePath($path = '')
    {
        return changeSlash(self::$relativePath . $path);
    }
}