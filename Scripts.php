<?php

use traits\SingleTone;

class Scripts
{
    use SingleTone;

    const KEY_URL_KEY = 'key';
    private $defaults = [];

    private function __construct()
    {
        $this->defaults['url'] = SiteData()->getAll('sitename') . DS . MODULE . '/scripts.php';
    }

    public function buildUrl($key, $params, $url = null)
    {
        if (!$url) $url = $this->defaults['url'];
        $url_parts = parse_url($url);
        if (!isset($url_parts['query'])) $url .= '?';
        $params[self::KEY_URL_KEY] = $key;
        return $url . http_build_query($params);
    }
}