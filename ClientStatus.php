<?php

use models\ClientStatusModel;
use traits\SingleTone;

class ClientStatus
{
    private static $baseId = 1;
    use SingleTone;

    public static function baseSelect(array $columns = ['*'], $aggregate = null)
    {
        return ClientServicesModel::baseSelect($columns, $aggregate);
    }

    public static function getTable()
    {
        return ClientServicesModel::getTable();
    }

    public static function getAll(array $params = ['*'])
    {
        return ClientStatusModel::getAll($params);
    }

    public static function getById($ids, array $params = ['*'])
    {
        return ClientStatusModel::getById($ids, $params);
    }

    public static function getOneById($id, array $params = ['*'])
    {
        try {
            $res = self::getById($id, $params);
            return $res ? array_shift($res) : '';
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function add($name)
    {
        $whoCreate = User()->getId();
        return ClientStatusModel::add(compact('name', 'whoCreate'));
    }

    public static function getDefault()
    {
        return self::$baseId;
    }

    public static function update($id, $data)
    {
        return ClientStatusModel::update($id, $data);
    }
}