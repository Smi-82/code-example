<?php
use traits\BaseSelect;
use traits\GetTable;
use traits\SingleTone;

class OurCompany
{
    use SingleTone;
    use GetTable;
    use BaseSelect;

    private static $table = 'our_company';
    private $data;
    private $default_logo;
    const UPLOAD_FOLDER = 'our_company/logo';
    const UPLOAD_TMP_FOLDER = self::UPLOAD_FOLDER . '/tmp';
    const UPLOAD_DIR = ROOT . DS . self::UPLOAD_FOLDER;
    const UPLOAD_TMP_DIR = ROOT . DS . self::UPLOAD_TMP_FOLDER;

    private function __construct()
    {
        $this->data = [];
        $this->default_logo = SiteData()->getAll('img') . '/Logo-Taskeo_approve_approve_Done.png';
    }

    private static function getURL($file, $filename)
    {
        $url = @($_SERVER["HTTPS"] != 'on') ? 'http://' . $_SERVER["SERVER_NAME"] : 'https://' . $_SERVER["SERVER_NAME"];
        $url .= (!in_array($_SERVER["SERVER_PORT"], [80, 443])) ? ":" . $_SERVER["SERVER_PORT"] : "";
        return changeSlash($url . DS . MODULE .UploadDir::getRelativePath() . self::UPLOAD_FOLDER . DS . $file . DS . $filename);
    }

    public function __get($key)
    {
        if (empty($this->data)) {
            $this->data = self::getDetails();
            $logos = self::getLogoFileData();
            $logos = array_keys($logos);
            foreach ($logos as $val) {
                $this->data[$val] = self::getImageDetails($val);
            }
        }
        switch ($key) {
            case 'iban_usd':
                return self::getCurrentBankIbanValue('usd');
            case 'iban_eur':
                return self::getCurrentBankIbanValue('eur');
            case 'bank_name':
                return self::getCurrentBank('name');
            case 'bank_address':
                return self::getCurrentBank('address');
        }
        return $this->data[$key] ?? '';
    }

    public static function getFullData()
    {
        $res = self::getDetails();
        $res['banks'] = OurCompanyBank::getAll();
        if (!empty($res['banks'])) {
            $bank_ids = array_column($res['banks'], 'id');
            $ibans = OurCompanyBankIban::getManyByBankIds($bank_ids);
            $res['banks'] = array_map(function ($el) use ($ibans) {
                $el['ibans'] = $ibans[$el['id']] ?? [];
                return $el;
            }, $res['banks']);
        }
        return $res;
    }

    public function getLogo()
    {
        $res = $this->logo_black;
        if (empty($res))
            return $this->default_logo;
        return self::getURL('logo_black', $res['name']);
    }

    public function getFullAddress()
    {
        return implode(', ', array_filter([
            $this->address,
            $this->city && $this->zip ? $this->city . ' ' . $this->zip : '',
            $this->country
        ]));
    }

    public static function getDetails($params = ['*'])
    {
        $sql = self::baseSelect(['company_data']) . ' LIMIT 1';
        $res = query($sql);
        $res = array_shift($res);
        $company_data = json_decode($res['company_data'], JSON_OBJECT_AS_ARRAY);
        if (!isset($company_data['isDefaultBank']))
            $company_data['isDefaultBank'] = 0;
        if (!isset($company_data['banks']))
            $company_data['banks'] = [];
        return $company_data;
    }

    public static function getCurrentBank($key = null)
    {
        $res = self::getDetails();
        return !is_null($key) && isset($res['banks'][intval($res['isDefaultBank'])][$key]) ? $res['banks'][intval($res['isDefaultBank'])][$key] : $res['banks'][intval($res['isDefaultBank'])];
    }

    public static function getCurrentBankIbans($currency, $key = null)
    {
        $res = self::getCurrentBank('ibans');
        $res = array_filter($res, function ($el) use ($currency) {
            return $el['currency'] == $currency;
        });
        if (empty($res)) return false;
        $res = array_shift($res);
        return $res[$key] ?? $res;
    }

    public static function getCurrentBankIbanValue($currency)
    {
        return self::getCurrentBankIbans($currency, 'iban');
    }

    public static function getImageDetails($file)
    {
        $upload_folder = changeSlash(ROOT . UploadDir::getRelativePath() . self::UPLOAD_FOLDER . DS . $file);
        $files = @scandir($upload_folder);
        if (!empty($files)) {
            $files = array_filter($files, function ($elem) use ($upload_folder) {
                return !in_array($elem, ['.', '..']) && !is_dir($upload_folder . DS . $elem);
            });
        }
        if (empty($files))
            return false;
        $filename = array_shift($files);
        $dir1 = changeSlash(DS . MODULE . UploadDir::getRelativePath() . self::UPLOAD_FOLDER . DS . $file . DS . $filename);
        return [
            'name' => $filename,
            'path' => $dir1,
            'size' => filesize($upload_folder . DS . $filename),
        ];
    }

    public static function updateDetails($data)
    {
        $arr['company_data'] = json_encode($data);
        $sql = update_query_string(self::getTable(), $arr) . ' WHERE id = 1';
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function addFileToDetailsTemp($file)
    {
        $upload_dir = changeSlash(ROOT . UploadDir::getRelativePath() . self::UPLOAD_FOLDER . DS . $file);
        if (file_exists($upload_dir))
            Folder::delete($upload_dir);
        Folder::create($upload_dir);
        $filename = escape_string($_FILES['myfile']['name']);
        $file_path = $upload_dir = changeSlash($upload_dir . DS . $filename);
        $moved = move_uploaded_file($_FILES['myfile']['tmp_name'], $file_path);

        if (!$moved) return false;
        $path_info = pathinfo($file_path);
        return [
            'path_info' => $path_info,
            'upload_folder' => $upload_dir,
            'file' => $file
        ];
    }

    public static function getBankRequisitesPDF()
    {
        return self::getCurrentBank();
    }
//    public static function getBankRequisitesPDF()
//    {
//        try {
//            $mpdf = self::getMPDF();
//            $body = Templates::getTemplate('bankRequisites');
//            $mpdf->writeHTML($body);
//            $mpdf->Output('BankRequisites.pdf', 'D');
//        } catch (Exception $e) {
//            return $e->getMessage();
//        }
//    }

//    public static function deleteFileFromDetailsTemp($filename, $file)
//    {
//        return unlink(self::UPLOAD_TMP_DIR . DS . $file . DS . $filename) && Folder::delete(self::UPLOAD_TMP_DIR . DS . $file);
//    }

    public static function deleteFileFromDetails($filename, $file)
    {
        $upload_dir = changeSlash(ROOT . UploadDir::getRelativePath() . self::UPLOAD_FOLDER . DS . $file . DS . $filename);
        if (file_exists($upload_dir))
            return Folder::delete($upload_dir);
        return false;
    }

    public static function getLogoFileData($file = null)
    {
        $arr = [
            'logo_white' => [
                'aliase' => 'udb_client_our_company_details_field_13'
            ],
            'logo_black' => [
                'aliase' => 'udb_client_our_company_details_field_14'
            ],
            'favicon' => [
                'aliase' => 'udb_client_our_company_details_field_15'
            ],
        ];
        return !is_null($file) && isset($arr[$file]) ? $arr[$file] : $arr;
    }

    public function getBankRequisites()
    {
        $data = $this->getDetails();
        $data['banks'] = $data['banks'][$data['isDefaultBank']] ?? [];
        $arr = [
            'company_name' => $data['company_name'],
            'company_address' => $data['address'],
            'bank_name' => $data['banks']['name'] ?? '',
            'bank_address' => $data['banks']['address'] ?? '',
            'bank_swift' => $data['banks']['swift'] ?? '',
            'ibans' => $data['banks']['ibans'] ?? []
        ];
        return $arr;
    }

    private static function createTable()
    {
        $table = self::getTable();
        $sql = "CREATE TABLE `$table` (
                 `site_name` varchar(100) NOT NULL,
                 `domain_name` varchar(255) NOT NULL,
                 `trade_name` varchar(255) NOT NULL,
                 `company_name` varchar(255) NOT NULL,
                 `email` varchar(100) NOT NULL,
                 `phone` varchar(50) NOT NULL,
                 `address` varchar(255) NOT NULL,
                 `zip` varchar(10) NOT NULL,
                 `city` varchar(100) NOT NULL,
                 `country` varchar(100) NOT NULL,
                 `director_name` varchar(100) NOT NULL,
                 `director_signature_encrypt` text NOT NULL,
                 `logo_white_filename` varchar(255) NOT NULL,
                 `logo_black_filename` varchar(255) NOT NULL,
                 `favicon_filename` varchar(255) NOT NULL
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8";
    }
}