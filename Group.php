<?php

use models\GroupModel;
use traits\SingleTone;

class Group
{
    use SingleTone;

    private $_parentGroup;
    private $_groups;
    private static $table = 'groups';
    private $table_fields;
    private $defFields;
    private $tmpValues;
//    private static $level = 0;
//    private static $template = '';
    private static $_forTree = [
        'level' => 0,
        'template' => '',
        'html' => [
            'wrap' => [
                'tag' => 'ul',
                'attr' => [
                    'class' => []
                ]
            ],
            'child' => [
                'tag' => 'li',
                'attr' => [
                    'class' => []
                ]
            ]
        ]

    ];

    private static function _get_item_str(&$group)
    {
        $pr_id = isset($group['general_project_id']) ? "project={$group['general_project_id']}&" : '';
        if (intval($group['def_gr']) == 1 || User()->isEmploye())
            $del = 'style="display: none;"';
        else
            $del = '';

        return Templates::getTemplate(self::$_forTree['template'],
            array(
                'id' => $group['id'],
                'level' => self::$_forTree['level'],
                'def_gr' => $group['def_gr'],
                'pr_id' => $pr_id,
                'cntTasks' => $group['cnt_task'],
                'name' => $group['name'],
                'delete' => $del,
//            'arr_for_transl' => &$lang_keys_for_transl
            )
        );
    }

    private function getChildGroup($id)
    {
        try {
            $result = GroupModel::getByParentId($id, ['id']);
            $result = array_column($result, 'id');
        } catch (Exception $e) {
            $result = false;
        }
        if ($result) {
            $this->_groups = array_merge($this->_groups, $result);
            foreach ($result as $v)
                $this->getChildGroup($v);
        }
    }

    public static function baseSelect(array $columns = ['*'], $aggregate = null)
    {
        return GroupModel::baseSelect($columns, $aggregate);
    }

    public static function getTable()
    {
        return GroupModel::getTable();
    }

    public static function getByWhoCreated($user_id, $columns = ['*'])
    {
        return GroupModel::getByWhoCreated($user_id, $columns);
    }

    public static function updateParent($id, $parent_id)
    {
        return self::update($id, compact('parent_id'));
    }

    public static function update($id, $data)
    {
        return GroupModel::update($id, $data);
    }


    private static function _getTreeTag($el = 'wrap')
    {
        return self::$_forTree['html'][$el]['tag'];
    }

    private static function _getTreeAttr($el = 'wrap')
    {
        $str = ' ';
        if (!empty(self::$_forTree['html'][$el]['attr'])) {
            foreach (self::$_forTree['html'][$el]['attr'] as $k => $v) {
                if (!empty($v))
                    $str .= $k . '="' . $v . '"' . ' ';
            }
        }
        return $str;
    }

    private static function _setTreeAttr($attr, $el = 'wrap')
    {
        self::$_forTree['html'][$el]['attr'] = array_merge(self::$_forTree['html'][$el]['attr'], $attr);
    }

    private static function _removeTreeAttr($attr, $el = 'wrap')
    {
        unset(self::$_forTree['html'][$el]['attr'][$attr]);
    }

    private static function _get_tree($groups, $pid)
    {
        $html = '';
        foreach ($groups as $group) {
            if (!isset($group['cnt_task']))
                $group['cnt_task'] = 0;
            if (intval($group['parent_id']) == $pid) {
                self::$_forTree['level']++;
                $html .= '<' . self::_getTreeTag('child') . ' ' . self::_getTreeAttr('child') . '>' . "\n";
                $html .= self::_get_item_str($group) . "\n";
                $p_id = intval($group['id']);
                $html .= '  ' . self::_get_tree($groups, $p_id);
                $html .= '</' . self::_getTreeTag('child') . '>' . "\n";
            }
        }
        self::$_forTree['level']--;
        if ($html) {
            self::_setTreeAttr(['style' => $pid != 0 ? 'display:none;' : '']);
            return '<' . self::_getTreeTag() . ' ' . self::_getTreeAttr() . '>' . $html . '</' . self::_getTreeTag() . '>' . "\n";
        }
        else {
            return '';
        }
    }

    private function getParentGroup($id)
    {
        try {
            $result = GroupModel::getById($id, ['parent_id']);
            $result = array_column($result, 'parent_id');
        } catch (Exception $e) {
            $result = [];
        }
        if ($result) {
            $this->_groups[] = $result[0];
            $this->getParentGroup($result[0]);
        }
    }

    private function __construct()
    {
        $this->_groups = [];
        $this->defFields = [
            'parent_id' => 0,
            'name' => 'default group',
            'user_id' => 0,
            'def_gr' => 0,
            'general_project_id' => 0,
        ];
        $this->tmpValues = [];
    }

    public static function get_tree($template, $groups, $pid, array $wrapper = [])
    {
        self::$_forTree['template'] = $template;
        if (!empty($wrapper))
            self::$_forTree['html'] = array_merge(self::$_forTree['html'], $wrapper);
        return self::_get_tree($groups, $pid);
    }

    public function setName($val)
    {
        $val = escape_string(clear_post($val));
        $this->tmpValues['name'] = $val;
        return $this;
    }

    public function setParent($val)
    {
        $val = escape_string(clear_post($val));
        $this->tmpValues['parent_id'] = $val;
        return $this;
    }

    public function setUser($val)
    {
        $val = escape_string(clear_post($val));
        $this->tmpValues['user_id'] = $val;
        return $this;
    }

    public function setDefault($val)
    {
        $val = escape_string(clear_post($val));
        if (!in_array($val, [0, 1]))
            $val = 0;
        $this->tmpValues['def_gr'] = $val;
        return $this;
    }

    public function setProject($val)
    {
        $val = escape_string(clear_post($val));
        $this->tmpValues['general_project_id'] = $val;
        return $this;
    }

    public function getColumns()
    {
        return $this->table_fields;
    }

    public function set($parentGroup_id)
    {
        $this->_parentGroup = $parentGroup_id;
        return $this;
    }

    public static function getGroupsById($ids, array $params = ['*'])
    {
        return GroupModel::getByManyIds($ids, $params);
    }

    public static function getOneGroupById($id, array $params = ['*'])
    {
        $res = self::getGroupsById($id, $params);
        return $res ? $res[0] : $res;
    }

    public static function getGroupsByProjectId(int $project_id, array $params = ['*'])
    {
        $res = self::getByManyProjectsId($project_id, $params);
        if (!empty($res))
            return $res[$project_id];
        return $res;
    }

    public static function getByManyProjectsId($project_id, array $params = ['*'])
    {
        try {
            $res = GroupModel::getByManyProjectsId($project_id, $params);
            if (empty($res))
                return $res;
            $tmp = [];
            foreach ($res as $v) {
                $tmp_k = $v['_tmp_p_id'];
                unset($v['_tmp_p_id']);
                $tmp[$tmp_k][] = $v;
            }
            return $tmp;
        } catch (Exception $e) {
            return [];
        }
    }

    public function getThisAndChildGroups($id, array $params = ['*'])
    {
        $id = escape_string(clear_post($id));
        $params = clearParams($params);
        $this->_groups[] = $id;
        $this->getChildGroup($id);
        $res = self::getGroupsById($this->_groups, $params);
        $this->_groups = [];
        return $res;
    }

    public function getChildGroupsOnly($id, array $params = ['*'])
    {
        $id = escape_string(clear_post($id));
        $params = clearParams($params);
        $this->getChildGroup($id);
        $res = self::getGroupsById($this->_groups, $params);
        $this->_groups = [];
        return $res;
    }

    public function getThisAndParentGroups($id, array $params = ['*'])
    {
        $id = escape_string(clear_post($id));
        $params = clearParams($params);
        $this->_groups[] = $id;
        $this->getParentGroup($id);
        $res = self::getGroupsById($this->_groups, $params);
        $this->_groups = [];
        return $res;
    }

    public function getOwnTasks($group_ids, $params = ['*'])
    {
        return Task::getByGroupIds($group_ids, $params);
    }

    public function getParentGroupsOnly($id, array $params = ['*'])
    {
        $id = escape_string(clear_post($id));
        $params = clearParams($params);
        $this->getParentGroup($id);
        $res = self::getGroupsById($this->_groups, $params);
        $this->_groups = [];
        return $res;
    }

    public function addGroup($data = [])
    {
        try {
            $res = GroupModel::add(array_merge($this->defFields, $this->tmpValues, $data));
            $this->defFields = [];
            return $res;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function cloneGroup($id, $withTasks = false)
    {
        $id = escape_string(clear_post($id));
        $groups = $this->getThisAndChildGroups($id);
        if ($withTasks) {
            $groups_ids = array_column($groups, 'id');
            $tasks = $this->getOwnTasks($groups_ids);
        }
        foreach ($groups as $key => &$group) {
            $_id = $group['id'];
            unset($group['id']);
            $group['user_id'] = User()->getId();
            $group['def_gr'] = 0;
            $new_id = $this->addGroup($group);
            if (!is_numeric($new_id))
                continue;
            unset($groups[$key]);
            foreach ($groups as &$_group) {
                if (intval($_group['parent_id']) == intval($_id))
                    $_group['parent_id'] = $new_id;
            }
            if ($withTasks) {
                foreach ($tasks as &$task) {
                    if (intval($task['group_id']) == intval($_id))
                        $task['group_id'] = $new_id;
                }
            }
        }
        if ($withTasks) {
            foreach ($tasks as $task) {
                Task()->cloneTask($task['id'], $task);
            }
        }
        return true;
    }

    public static function deleteByProjectIds($ids)
    {
        return GroupModel::deleteByProjectIds($ids);
    }

    public function delete($id)
    {
        $id = escape_string(clear_post($id));
        $group_ids = [];
        $this->getChildGroup($id);
        $group_ids = $this->_groups;
        $this->_groups = [];
        $group_ids[] = $id;
        return GroupModel::delete($group_ids);
    }
}