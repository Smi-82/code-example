<?php

use traits\BaseSelect;
use traits\GetTable;

class ProjectFunctions
{
    private static $table = 'project_functions';
    use GetTable;
    use BaseSelect;

    public static function add($data)
    {
        $data['whoCreate'] = User()->getId();
        $data['name'] = trim($data['name']);
        $data['descr'] = htmlspecialchars(trim($data['descr']));
        $sql = insert_query_string(self::getTable(), $data);
        try {
            $res = query($sql);
            return $res ? getLastId() : $res;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function multiAdd($data)
    {
        $current_user = User()->getId();
        $data = array_values($data);
        $data = array_map(function ($el) use ($current_user) {
            $el['whoCreate'] = $current_user;
            $el['name'] = trim($el['name']);
            $el['descr'] = htmlspecialchars(trim($el['descr']));
            if (isset($el['id']))
                unset($el['id']);
            return $el;
        }, $data);
        $sql = multiple_insert_str(self::getTable(), $data);
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function getOneById($id, array $columns = ['*'])
    {
        $res = self::getManyById($id, $columns);
        return array_shift($res);
    }

    public static function getManyById($ids, array $columns = ['*'])
    {
        $ids = getAsArray($ids);
        $sql = self::baseSelect($columns) . ' WHERE id IN (' . implode(',', $ids) . ')';
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function getOneByProjectId($project_id, array $columns = ['*'])
    {
        $res = self::getManyByProjectId($project_id, $columns);
        return array_shift($res);
    }

    public static function getManyByProjectId($projects_id, array $columns = ['*'])
    {
        $projects_id = getAsArray($projects_id);
        $columns[] = ' project_id AS _p_id';
        $sql = self::baseSelect($columns) . ' WHERE project_id IN (' . implode(',', $projects_id) . ') AND active=1';
        try {
            $res = query($sql);
            if (empty($res))
                return $res;
            $tpl = [];
            foreach ($res as $v) {
                $p_id = $v['_p_id'];
                unset($v['_p_id']);
                $tpl[$p_id][] = $v;
            }
            return $tpl;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function update($id, $data)
    {
        $id = escape_string(clear_post($id));
        $curr = self::getOneById($id, ['project_id', 'name', 'descr']);
        unset($data['id']);
        $fl = true;
        foreach ($curr as $key => $val) {
            if (isset($data[$key]) && $data[$key] != $val) {
                $fl = false;
                break;
            }
        }
        if ($fl)
            return true;

        $row = array_merge($curr, $data);
        $row['whoUpdated'] = User()->getId();
        $row['whenUpdated'] = date('Y-m-d H:i:s');
        $row['parent_id'] = $id;
        self::add($row);
        self::addToHistory($id);
        return true;
    }

    public static function addToHistory($ids)
    {
        $ids = getAsArray($ids);
        $sql = update_query_string(self::getTable(), ['active' => 0, 'whenUpdated' => date('Y-m-d H:i:s'), 'whoUpdated' => User()->getId()]) . ' WHERE id IN (' . implode(',', $ids) . ')';
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function delete($ids)
    {
        return self::addToHistory($ids);
    }
}