<?php

use traits\SingleTone;

class VueDynamicParams
{
    use SingleTone;

    private $params;

    private function __construct()
    {
        $this->params = [];
    }

    public function set(array $params)
    {
        $str = '<script> ';
        if (empty($this->params))
            $str .= ' var ';
        $this->params = array_merge($this->params, $params);
        $str .= 'vueAppParams = ';
        $params_str = ['dynamicParams' => $this->params];
        $str .= json_encode($params_str);
        $str .= '</script>';
        return $str;
    }
}