<?php

namespace safetyAnalysis\ajax;

use controllers\ajax\BaseAjaxController;
use Project;
use safetyAnalysis\models\SafetyAnalysisModel;
use safetyAnalysis\SafetyAnalysisController;

class SafetyAnalysis extends BaseAjaxController
{
    public function generateProjectFromSafetyAnalys()
    {
        $proj_name = SafetyAnalysisController::getById($_POST['id'], ['url']);
        $proj_name = $proj_name['url'];
        $data = Project()->getBaseFields();
        $data['name'] = $proj_name;
        $data['price_type'] = Project::getIdPriceTypeByPackage();
        $proj_id = Project()->add($data);
        if (empty($proj_id['error']) && is_numeric($proj_id['project'])) {
            $proj_id = $proj_id['project'];
            $res = SafetyAnalysisController::updateProjectById($_POST['id'], $proj_id);
        }
        return is_numeric($proj_id) ? Response()->data($proj_id)->success() : Response()->error($proj_id);
    }

    public function uploadXLSAnalys()
    {
        $res = SafetyAnalysisController::uploadFileAndSaveData();
        return $res ? Response()->success() : Response()->error($res);
    }

    public function deleteSafetyDomain()
    {
        $res = SafetyAnalysisModel::delete($_POST['id']);
        return $res ? Response()->success() : Response()->error($res);
    }

    public function run()
    {
        $res = SafetyAnalysisController::getByUserId();
        return is_array($res) ? Response()->data($res)->success() : Response()->error($res);
    }
}