<?php

namespace safetyAnalysis;

use Exception;
use Folder;
use PhpOffice\PhpSpreadsheet\IOFactory;
use safetyAnalysis\models\SafetyAnalysisMetaModel;
use safetyAnalysis\models\SafetyAnalysisModel;
use Templates;
use UploadDir;

class SafetyAnalysisController
{
    private static $dir = 'safetyAnalysis';
    private const OFFSET_FOR_SHEET1 = 15;

    private static function getCellParams($cell, $param)
    {
        return is_array($cell) ? $cell[$param] : $cell;
    }

    public static function getCellVal($cell)
    {
        return self::getCellParams($cell, 'val');
    }

    public static function getCellBgColor($cell)
    {
        return self::getCellParams($cell, 'bg_color');
    }

    public static function getCellTextColor($cell)
    {
        return self::getCellParams($cell, 'color');
    }

    /**
     * @param array $data
     * @return array
     */
    private static function prepareThreatLevelData(array $data): array
    {
        $data = $data['meta_data']['sheet4'];
        array_shift($data);
        return array_reduce($data, function ($carry, $item) {
            $carry[self::getCellVal($item['A'])] = trim(self::getCellVal($item['B']));
            return $carry;
        }, []);
    }

    /**
     * @param array $data
     * @param int $filter
     * @return array
     */
    private static function prepareProbsData(array $data, int $filter = 0): array
    {
        $arr = array_filter($data['url_data'], function ($el) use ($filter) {
            return $filter === intval(self::getCellVal($el));
        }, ARRAY_FILTER_USE_BOTH);
        $arr = array_keys($arr);
        $arr = array_map(function ($el) {
            return self::prepare_str($el);
        }, $arr);
        $arr = array_filter($data['meta_data']['sheet2'], function ($el) use ($arr) {
            return in_array(self::prepare_str(self::getCellVal($el['A'])), $arr);
        });
        usort($arr, function ($a, $b) {
            $a = floatval(self::getCellVal($a['E']));
            $b = floatval(self::getCellVal($b['E']));
            if ($a == $b) {
                return 0;
            }
            return ($a > $b) ? -1 : 1;
        });
        return $arr;
    }

    /**
     * @param string $str
     * @return string
     */
    private static function prepare_str(string $str): string
    {
        return strtolower(preg_replace('/\s+/', '', $str));
    }

    /**
     * @param array $args
     * @return \Mpdf\Mpdf
     * @throws \Mpdf\MpdfException
     */
    private static function getMPDF(array $args = [])
    {
        $args = array_merge($args, ['tempDir' => UploadDir::getRootPath() . 'Mpdf' . DS . self::$dir]);
        return new \Mpdf\Mpdf($args);
    }

    /**
     * @param array $data
     * @return array
     */
    public static function getDataForBasicPDF(array $data): array
    {
        $url_data = array_slice($data['url_data'], 0, self::OFFSET_FOR_SHEET1);
        $data['url_data'] = array_slice($data['url_data'], self::OFFSET_FOR_SHEET1);
        $probs = self::prepareProbsData($data, 1);
        $no_probs = self::prepareProbsData($data);
        $threat_level = self::prepareThreatLevelData($data);
        $all_probs = $data['url_data'];
        foreach ($data['meta_data']['sheet2'] as $val) {
            $key = self::getCellVal($val['A']);
            if (isset($all_probs[$key])) {
                $all_probs[$key]['bg_color'] = self::getCellBgColor($val['A']);
                $all_probs[$key]['color'] = self::getCellTextColor($val['A']);
            }
        }
        return compact('url_data', 'probs', 'no_probs', 'threat_level', 'all_probs');
    }

    /**
     * @param int $id
     * @param int $project_id
     * @return array|bool
     */
    public static function updateProjectById(int $id, int $project_id)
    {
        return SafetyAnalysisModel::update($id, compact('project_id'));
    }

    /**
     * @param array $data
     * @param string $as
     * @return bool|string
     */
    public static function createPDF(array $data, string $as = 'D')
    {
        try {
            $mpdf = self::getMPDF([
                'setAutoBottomMargin' => 'stretch'
                , 'margin_footer' => 10
            ]);

//            $mpdf->SetHTMLFooter(Templates::getTemplate('BillInvoice/bill_invoice_footer'));
//            $mpdf->writeHTML(Templates::getTemplate('BillInvoice/project_bill', $data));
//            $mpdf->AddPage();
            $mpdf->writeHTML(Templates::getTemplate(self::$dir . '/basicPDFv2', $data));
            $file_name = explode('//', self::getCellVal($data['url_data']['URL']));
            $file_name = array_pop($file_name);
            return $mpdf->Output($file_name . '.pdf', $as) == '';
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param int $id
     * @param array $params
     * @return array
     */
    public static function getById(int $id, array $params = ['*']): array
    {
        $res = SafetyAnalysisModel::getById($id, $params);
        if (empty($res))
            return $res;
        $res['url_data'] = json_decode(htmlspecialchars_decode($res['url_data']), true);
        $res['meta_data'] = json_decode(htmlspecialchars_decode($res['meta_data']), true);
        return $res;
    }

    /**
     * @param int|null $user_id
     * @param array $params
     * @return array
     */
    public static function getByUserId(int $user_id = null, array $params = ['*']): array
    {
        if (is_null($user_id))
            $user_id = User()->getId();
        return SafetyAnalysisModel::getByWhoAdd($user_id, $params);
    }

    /**
     * @return false|string
     */
    public static function uploadFileAndSaveData()
    {
        if (isset($_FILES['myfile'])) {
            $dir = SafetyAnalysisController::createTmpFile();
            $arr = SafetyAnalysisController::parseFile($dir);//
            SafetyAnalysisController::add($arr);
            if (file_exists($dir))
                Folder::delete($dir);
            return json_encode($dir);
        }
        return false;
    }

    public static function getSheet1(array $data)
    {
        $firstList = $data['sheet1'];
        $titles = array_shift($firstList);
        $titles = array_reduce($titles, function ($curr, $item) {
            $curr[] = trim(self::getCellVal($item));
            return $curr;
        }, []);
        $res = array_map(function ($el) use ($titles) {
            return array_diff_key(array_combine($titles, $el), ['' => '']);
        }, $firstList);
        return array_reduce($res, function ($curr, $item) {
            $curr[self::getCellVal($item['URL'])] = $item;
            return $curr;
        }, []);
    }

    /**
     * @param array $data
     * @param int|null $project_id
     * @return bool|void
     */
    public static function add(array $data, int $project_id = null)
    {
        $arr = array_map(function ($el) {
            return json_encode($el, JSON_UNESCAPED_UNICODE);
        }, self::getSheet1($data));
        unset($data['sheet1']);
        $tmp_data = [];
        $user_id = User()->getId();
        foreach ($arr as $key => $val) {
            $tmp_data[] = [
                'url' => $key,
                'url_data' => $val,
                'who_add' => $user_id,
                'project_id' => $project_id
            ];
        }

        $urls = array_column($tmp_data, 'url');
        $urls = array_map(function ($el) {
            return '"' . $el . '"';
        }, $urls);
        $urls = SafetyAnalysisModel::checkUrls($urls);
        if (!empty($urls)) {
            $urls = array_column($urls, 'url');
            $tmp_data = array_filter($tmp_data, function ($el) use ($urls) {
                return !in_array($el['url'], $urls);
            });
        }
        if (empty($tmp_data))
            return true;
        $data = array_map(function ($sheet) {
            return array_map(function ($row) {
                return array_map(function ($cell) {
                    if (is_array($cell)) {
                        $res = self::getCellVal($cell);
                        $cell['val'] = is_numeric($res) ? strval($res) : (empty($res) ? '0' : str_replace('"', '\'', $res));
                    }
                    return $cell;
                }, $row);
            }, $sheet);
        }, $data);
        $data = json_encode($data, JSON_UNESCAPED_UNICODE);
        $meta_id = SafetyAnalysisMetaModel::addMetaData($data);
        if (!is_numeric($meta_id))
            return $meta_id;

        return SafetyAnalysisModel::multiple_add(array_map(function ($el) use ($meta_id) {
            $el['safety_analysis_meta_id'] = $meta_id;
            return $el;
        }, $tmp_data));
    }

    /**
     * @return array|string|string[]
     */
    public static function createTmpFile()
    {
        $dir = changeSlash(ROOT . UploadDir::getRelativePath() . 'xls_analys_tmp');
        if (isset($_FILES['myfile'])) {
            if (!file_exists($dir))
                Folder::create($dir);
            $dir .= DS;
            $error = $_FILES['myfile']['error'];
            if (!is_array($_FILES['myfile']['name'])) //single file
            {
                $fileName = $_FILES['myfile']['name'];
                $ext = explode('.', $fileName);
                $ext = array_pop($ext);
                $dir .= md5(uniqid(session_id())) . '.' . $ext;
                $dir = changeSlash($dir);
                move_uploaded_file($_FILES['myfile']['tmp_name'], $dir);
            }
        }
        return $dir;
    }

    /**
     * @param string $file_path
     * @return array
     * @throws \PhpOffice\PhpSpreadsheet\Calculation\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public static function parseFile(string $file_path): array
    {
        $inputFileName = $file_path;
        /** Load $inputFileName to a Spreadsheet Object  **/
        /**  Identify the type of $inputFileName  **/
        $inputFileType = IOFactory::identify($inputFileName);
        /**  Create a new Reader of the type that has been identified  **/
        $reader = IOFactory::createReader($inputFileType);
        /**  Load $inputFileName to a Spreadsheet Object  **/
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($inputFileName);

        $arr = [];
        foreach ($spreadsheet->getWorksheetIterator() as $sh_k => $sheet)
            foreach ($sheet->getRowIterator() as $r_k => $row)
                foreach ($row->getCellIterator() as $c_k => $cell)
                    $arr['sheet' . ($sh_k + 1)][$r_k][$c_k] = $cell->getCalculatedValue();
        return $arr;
    }

    public static function parseFileV2(string $file_path): array
    {
        $inputFileName = $file_path;
        /** Load $inputFileName to a Spreadsheet Object  **/
        /**  Identify the type of $inputFileName  **/
        $inputFileType = IOFactory::identify($inputFileName);
        /**  Create a new Reader of the type that has been identified  **/
        $reader = IOFactory::createReader($inputFileType);
        /**  Load $inputFileName to a Spreadsheet Object  **/
        $reader->setReadDataOnly(false);
        $spreadsheet = $reader->load($inputFileName);

        $arr = [];
        foreach ($spreadsheet->getWorksheetIterator() as $sh_k => $sheet)
            foreach ($sheet->getRowIterator() as $r_k => $row)
                foreach ($row->getCellIterator() as $c_k => $cell)
                    $arr['sheet' . ($sh_k + 1)][$r_k][$c_k] = [
                        'val' => $cell->getCalculatedValue(),
                        'bg_color' => $cell->getStyle()->getFill()->getStartColor()->getRGB(),
                        'color' => $cell->getStyle()->getFont()->getColor()->getRGB(),
                    ];
        return $arr;
    }
}