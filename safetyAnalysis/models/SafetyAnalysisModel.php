<?php

namespace safetyAnalysis\models;

use Exception;
use traits\BaseSelect;
use traits\GetTable;

class SafetyAnalysisModel
{
    use GetTable;
    use BaseSelect;

    private static $table = 'safety_analysis';

    private static function prepareParams($params)
    {
        $params = clearParams($params);
        return array_map(function ($el) {
            return 's.' . $el;
        }, $params);
    }

    public static function update($id, $data)
    {
        $id = escape_string(clear_post($id));
        $sql = update_query_string(self::getTable(), $data) . ' WHERE id=' . $id;
        try {
            return query($sql);
        } catch (Exception $e) {
            return false;
        }
    }

    public static function delete($ids)
    {
        $ids = getAsArray($ids);
        $sql = 'DELETE FROM ' . self::getTable() . ' WHERE id IN (' . implode(', ', $ids) . ')';
        try {
            return query($sql);
        } catch (Exception $e) {
            return false;
        }
    }

    public static function getById($id, $params = ['*'])
    {
        $id = escape_string(clear_post($id));
        $params = self::prepareParams($params);
        $sql = 'SELECT ' . implode(',', $params) . ', sm.meta_data FROM ' . self::getTable() . ' AS s LEFT JOIN ' . SafetyAnalysisMetaModel::getTable() . ' AS sm ON s.safety_analysis_meta_id=sm.id WHERE s.id=' . $id;
        try {
            return querySingle($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function getByWhoAdd($user_id, $params = ['*'])
    {
        $user_id = escape_string(clear_post($user_id));
        $params = self::prepareParams($params);
        $sql = 'SELECT ' . implode(',', $params) . ', sm.meta_data FROM ' . self::getTable() . ' AS s LEFT JOIN ' . SafetyAnalysisMetaModel::getTable() . ' AS sm ON s.safety_analysis_meta_id=sm.id WHERE s.who_add=' . $user_id;
        try {
            return query($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function add($data)
    {
        $sql = insert_query_string(self::getTable(), $data);
        try {
            $res = query($sql);
            return $res ? getLastId() : $res;
        } catch (Exception $e) {
            return false;
        }
    }

    public static function multiple_add($data)
    {
        $sql = multiple_insert_str(self::getTable(), $data);
        try {
            query($sql);
        } catch (Exception $e) {
            return false;
        }
    }

    public static function checkUrls($urls)
    {
        $sql = 'SELECT url FROM ' . self::getTable() . ' WHERE url IN (' . implode(',', $urls) . ') GROUP BY url';
        try {
            return query($sql);
        } catch (Exception $e) {
            return [];
        }
    }
}