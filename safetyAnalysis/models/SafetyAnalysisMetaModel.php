<?php

namespace safetyAnalysis\models;

use Exception;
use traits\BaseSelect;
use traits\GetTable;

class SafetyAnalysisMetaModel
{
    use GetTable;
    use BaseSelect;

    private static $table = 'safety_analysis_meta';

    public static function addMetaData($meta_data)
    {
        return self::add(compact('meta_data'));
    }

    public static function add($data)
    {
        $sql = insert_query_string(self::getTable(), $data);
        try {
            $res = query($sql);
            return $res ? getLastId() : $res;
        } catch (Exception $e) {
            return false;
        }
    }
}