<?php

use traits\BaseSelect;
use traits\GetTable;

//deprecated
class Feedback
{
    private static $table = 'feedback';
    use GetTable;
    use BaseSelect;

    public static function add($data)
    {
        $sql = insert_query_string(self::getTable(), $data);
        try {
            $res = query($sql);
            return $res ? getLastId() : $res;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function delete($ids)
    {
        $ids = getAsArray($ids);
        $sql = 'DELETE FROM ' . self::getTable() . ' WHERE id IN (' . implode(',', $ids) . ')';
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function update($id, $data)
    {
        $id = escape_string(clear_post($id));
        $sql = update_query_string(self::getTable(), $data) . ' WHERE id = ' . $id;
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getTrace();
        }
    }

    public static function getAll(array $params = ['*'])
    {
        try {
            return query(self::baseSelect($params));
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function getById($id, array $params = ['*'])
    {
        $res = self::getByManyId($id, $params);
        return empty($res) ? [] : array_shift($res);
    }

    public static function getByManyId($ids, array $params = ['*'])
    {
        $ids = getAsArray($ids);
        try {
            return query(self::baseSelect($params) . ' WHERE id IN (' . implode(',', $ids) . ')');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function createTable()
    {
        $sql = "CREATE TABLE `taskeotest`.`feedback` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `name` VARCHAR(100) NOT NULL , `mail` VARCHAR(100) NOT NULL , `phone` VARCHAR(100) NOT NULL , `whenAdded` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , `platform` VARCHAR(10) NOT NULL DEFAULT 'en' , PRIMARY KEY (`id`)) ENGINE = InnoDB";
    }
}