<?php

use traits\BaseSelect;
use traits\GetTable;
use traits\SingleTone;

class PositionHeld
{
    private static $table = 'position_held';
    private static $default_id = 6;
    use SingleTone;
    use GetTable;
    use BaseSelect;

    public static function getDefaultId()
    {
        return self::$default_id;
    }

    public static function search($val, array $columns = ['*'])
    {
        $val = escape_string(clear_post($val));
        $sql = self::baseSelect($columns) . ' WHERE name LIKE "%' . $val . '%"';
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function create($name)
    {
        $name = escape_string(clear_post($name));
        $whoCreate = User()->getId();
        $sql = insert_query_string(self::getTable(), compact('name', 'whoCreate'));
        try {
            $res = query($sql);
            return $res ? getLastId() : $res;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function update($id, $data)
    {
        $id = escape_string(clear_post($id));
        $sql = update_query_string(self::getTable(), $data) . ' WHERE id=' . $id;
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function delete($id)
    {
        $id = escape_string(clear_post($id));
        $sql = 'DELETE FROM ' . self::getTable() . ' WHERE id=' . $id;
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function getAll(array $columns = ['*'])
    {
        try {
            return query(self::baseSelect($columns) . ' ORDER BY name');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function getOneById($id, array $columns = ['*'])
    {
        $res = self::getManyByIds($id, $columns);
        return $res ? array_shift($res) : $res;
    }

    public static function getManyByIds($ids, array $columns = ['*'])
    {
        $ids = getAsArray($ids);
        $sql = self::baseSelect($columns) . ' WHERE id IN (' . implode(',', $ids) . ')';
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}