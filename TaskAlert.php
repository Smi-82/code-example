<?php


use traits\BaseSelect;
use traits\GetTable;

class TaskAlert
{
    use GetTable;
    use BaseSelect;

    private static $table = 'tasks_alerts';

    public static function add($user_id, $task_id, $type)
    {
        $sql = insert_query_string(self::getTable(), [
            'type' => $type,
            'user_id' => $user_id,
            'task_id' => $task_id
        ], true);
        try {
            $res = query($sql);
            return $res ? getLastId() : $res;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function deleteByUserTaskType($user_id, $task_id, $type)
    {
        $sql = 'DELETE FROM ' . self::getTable() . ' WHERE type = ' . $type . ' and user_id = ' . $user_id . ' and task_id =' . $task_id;
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function taskHasAlertType2($task_id)
    {
        $task_id = escape_string(clear_post($task_id));

        $sql = "SELECT COUNT(ta.user_id) as cnt FROM " . TaskAccess::getTable() . " AS ta LEFT JOIN " . User::getTable() . " AS u ON ta.user_id = u.id
                    WHERE ta.task_id = '$task_id' AND u.usertype != " . UserTypes()->getClient() . "
                    AND ta.user_id NOT IN ((SELECT tal.user_id FROM " . self::getTable() . ' AS tal WHERE tal.task_id = ' . $task_id . ' and tal.type = 1))';
        try {
            $res = querySingle($sql);
            return (int)$res['cnt'];
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}