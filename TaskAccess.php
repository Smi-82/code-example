<?php

use appEvents\AppEventsController;
use appEvents\subscribers\controller\AppEventsSubscribersController;
use models\TaskAccessModel;

class TaskAccess
{
    public static function baseSelect(array $columns = ['*'], $aggregate = null)
    {
        return TaskAccessModel::baseSelect($columns, $aggregate);
    }

    public static function getTable()
    {
        return TaskAccessModel::getTable();
    }

    public static function delete($tasks, $users)
    {
        return TaskAccessModel::delete($tasks, $users);
    }

    public static function add($task_id, $user_id, $last_timestamp = '0000-00-00')
    {
        try {
            $res = TaskAccessModel::add($task_id, $user_id, $last_timestamp);
            if ($res)
                AppEventsController::publish(AppEventsSubscribersController::userToTaskEvent(), compact('task_id', 'user_id'));
            return $res;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function update($ids, $data)
    {
        return TaskAccessModel::update($ids, $data);
    }

    public static function updateByTaskAndUserIds($data, $user_id, $task_id = 0)
    {
        return TaskAccessModel::updateByTaskAndUserIds($data, $user_id, $task_id);
    }

    private static function updateTimeSpend($res, $interval)
    {
        if (!empty($res)) {
            $res1 = TaskAccessModel::update($res['id'], ['time_spent' => intval($res['time_spent']) + $interval, 'last_timestamp' => time()]);
            return $res1 ? intval($res['time_spent']) + $interval : $res1;
        }
        else
            return false;
    }

    public static function updateTimeSpendById($id, $interval = 10)
    {
        $res = self::getOneById($id, ['id', 'time_spent']);
        return self::updateTimeSpend($res, $interval);
    }

    public static function updateTimeSpendByTaskAndUserIds($task_id, $user_id, $interval = 10)
    {
        $res = self::getByTaskAndUserIds($task_id, $user_id, ['id', 'time_spent']);
        return self::updateTimeSpend($res, $interval);
    }

    public static function getOneById($id, $columns = ['*'])
    {
        $res = self::getManyByIds($id, $columns);
        return array_shift($res);
    }

    public static function getManyByIds($ids, $columns = ['*'])
    {
        $res = TaskAccessModel::getManyByIds($ids, $columns);
        return $res ? valueToKey('_tmp_id', $res, true) : $res;
    }

    public static function isBindUserToTask($user_id, $task_id = 0)
    {
        $arr = ['COUNT(id) AS cnt'];
        if ($task_id == 0)
            $res = TaskAccessModel::getByUserId($user_id, $arr);
        else
            $res = TaskAccessModel::getByUserIdAndTaskId($user_id, $task_id, $arr);
        if (empty($res))
            return 0;
        $res = array_shift($res);
        return $res['cnt'];
    }

    public static function isUserOnTask($user_id, $task_id = 0)
    {
        $arr = ['task_id', 'is_on_task'];
        if ($task_id == 0)
            $res = TaskAccessModel::getByUserId($user_id, $arr);
        else
            $res = TaskAccessModel::getByUserIdAndTaskId($user_id, $task_id, $arr);
        try {
            if (empty($res))
                return 0;
            $res = array_filter($res, function ($el) {
                return intval($el['is_on_task']);
            });
            if (empty($res))
                return 0;
            $res = array_shift($res);
            return $res['task_id'];
        } catch (Exception $e) {
            return 0;
        }
    }

    public static function getByTaskAndUserIds($task_id, $user_id, $columns = ['*'])
    {
        return TaskAccessModel::getByTaskAndUserIds($task_id, $user_id, $columns);
    }

    public static function getManyByTaskAndUserIds($task_ids, $user_ids, $columns = ['*'])
    {
        return TaskAccessModel::getByTaskAndUserIds($task_ids, $user_ids, $columns);
    }

    public static function getManyByTaskIds($task_ids, $columns = ['*'])
    {
        return TaskAccessModel::getManyByTaskIds($task_ids, $columns);
    }

    public static function getByUserId($user_id, $columns = ['*'])
    {
        return TaskAccessModel::getByUserId($user_id, $columns);
    }
}