<?php


class StaticFactory
{
    public static function build($class)
    {
        return method_exists($class, 'getInst') ? $class::getInst() : new $class();
    }
}