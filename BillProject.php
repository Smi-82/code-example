<?php

use appEvents\AppEventsController;
use appEvents\subscribers\controller\AppEventsSubscribersController;
use interfaces\iGetClientUpdates;
use models\ProjectBillModel;
use traits\ClassName;
use traits\SingleTone;

class BillProject implements iGetClientUpdates
{
    use SingleTone;
    use ClassName;

    private static $folder = 'bills';
    private static $MD5_PREFIX = [
        'confirm_bdc_from_email' => 'confirm_bdc_from_email',
        'pay_invoice_from_email' => 'pay_invoice_from_email'
    ];

    private function getMPDF($args = [])
    {
        $args = array_merge($args, ['tempDir' => UploadDir::getRootPath() . 'Mpdf' . DS . self::getFolderName()]);
        return new \Mpdf\Mpdf($args);
    }

    public static function baseSelect(array $columns = ['*'], $aggregate = null)
    {
        return ProjectBillModel::baseSelect($columns, $aggregate);
    }

    public static function getTable()
    {
        return ProjectBillModel::getTable();
    }

    public static function getFolderName()
    {
        return self::$folder;
    }

    public static function getPathToFolder($bill_id, $project_id)
    {
        return Project::geMainFolder() . DS . $project_id . DS . self::getFolderName() . DS . $bill_id;
    }

    private function createFolder($bill_id, $project_id = 0)
    {
        if (!$project_id) {
            $project_id = self::getById($bill_id, ['project_id']);
            $project_id = $project_id['project_id'];
        }

        $dir = self::getPathToFolder($bill_id, $project_id);
        Folder::create($dir);
        return $dir;
    }

    public function readTOS($id, $val = 1)
    {
        return $this->update($id, ['readTOS' => $val]);
    }

    public function getFolder($bill_id, $project_id = 0)
    {
        if (!$project_id) {
            $project_id = self::getById($bill_id, ['project_id']);
            $project_id = $project_id['project_id'];
        }
        return $this->createFolder($bill_id, $project_id);
    }

    public function createFileName($bill_id)
    {

        $name = 'Taskea_order_';
        if (intval($bill_id) < pow(10, 4)) {
            return $name . (pow(10, 4) + intval($bill_id));
        }
        else {
            $tmp = strlen($bill_id);
            $tmp += 2;
            return $name . (pow(10, $tmp) + intval($bill_id));
        }
    }

    public function createNameBDC($bill_id, $bill_date)
    {
        $date = new DateTime($bill_date);
        $month = $date->format('m');
        $year = $date->format('Y');
//        $m = ($month + 1) < 10 ? '0' . ($month + 1) : ($month + 1);
        return $year . '-' . $month . '-' . $bill_id;
    }


    public function approve($bill_id)
    {
        $bill_id = escape_string(clear_post($bill_id));
        MessengerEvents()->deleteNotificationByBillIdAndCode($bill_id, MessengerEventCodes::CONFIRM_PURCHASE_ORDER);
        $res = $this->update($bill_id, ['approved' => 1, 'when_approved' => date('Y-m-d H:i:s')]);
        $p_id = self::getById($bill_id, ['project_id']);
        $p_id = $p_id['project_id'];
        ClientActivity::add($p_id, self::getClassName(), $p_id);
        $pdfData = InvoiceProject()->getDataForPDF($bill_id);
        $pdfData['bank_requisites'] = OurCompany()->getBankRequisites();
        $res = InvoiceProject()->createPDF($pdfData);

        $pdfPath = InvoiceProject()->getFilePathPDF($pdfData['id'], $pdfData['project_id']);

        $emailHtmlData = $pdfData;
        $emailHtmlData['pay_invoice_url'] = SiteData()->getAll('sitename') . '/' . MODULE . '/personal.php#factures';
        $emailHtmlData['pay_invoice_url'] = Scripts()->buildUrl(
            'payInvoiceFromClientEmail',
            $this->createPayInvoiceParams($emailHtmlData['client']['id'], $bill_id)
        );

        $emailHtml = Templates::getTemplate('invoice_email', $emailHtmlData);
        $clientEmail = Project()->getClient($emailHtmlData['project_id'], ['email']);
        $clientEmail = $clientEmail['email'];
        $mailed = @Mails()->send_one_mail_with_attachments(
            OurCompany()->email,
            OurCompany()->trade_name,
            $clientEmail,
            'asd',
            lng('invoice_email_topic'),
            $emailHtml,
            ' ',
            [['path' => $pdfPath]]);
        return ['href' => $pdfPath, 'res' => $res];
    }

    public function update($bill_id, array $data)
    {
        return ProjectBillModel::update($bill_id, $data);
    }

    public function getLastBillsByClientId($client_id, array $columns = ['*'])
    {
        $client_id = escape_string(clear_post($client_id));
        $projects = Project()->getProjectsByClientId($client_id, ['id']);

        if (empty($projects) || !is_array($projects))
            return [];

        $projects = array_column($projects, 'id');
        $bills = $this->getLastBDCByProjectId($projects, $columns);
        return array_filter($bills);
    }

    public function getBillsByClientId($client_id, array $columns = ['*'])
    {
        $client_id = escape_string(clear_post($client_id));
        $projects = Project()->getProjectsByClientId($client_id, ['id']);
        $projects = array_column($projects, 'id');
        $bills = [];
        foreach ($projects as $p_id) {
            $bills[] = $this->getByProjectId($p_id, $columns);
        }
        return $bills;
    }

    public function getHrefPDF($bill_id, $project_id = 0)
    {
        $res = $this->getFilePathPDF($bill_id, $project_id);
        $res = changeSlash($res);
        $res = explode(MODULE . DS, $res);
        return array_pop($res);
    }

    public function getFilePathPDF($bill_id, $project_id = 0)
    {
        $path = $this->getFolder($bill_id, $project_id);
        return $path . DS . $this->createFileName($bill_id) . '.pdf';
    }

    public function createPDF($data, $as = 'F')
    {
        try {
            $mpdf = $this->getMPDF([
                'setAutoBottomMargin' => 'stretch'
                , 'margin_footer' => 10
            ]);

            $mpdf->SetHTMLFooter(Templates::getTemplate('BillInvoice/bill_invoice_footer'));
            $mpdf->writeHTML(Templates::getTemplate('BillInvoice/project_bill', $data));
            $mpdf->AddPage();
            $mpdf->writeHTML(Templates::getTemplate('BillInvoice/project_company_requisites', $data));
            return $mpdf->Output($this->getFilePathPDF($data['id'], $data['project_id']), $as) == '';
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function getSumPaymentByProjectId($project_id)
    {
        return ProjectBillModel::getSumPaymentByProjectId($project_id);
    }

    public function getBillDataForBill($project)
    {
        $project_id = $project['id'];
        $client = User()->getUser();
        $city = $client['city'];
        $created = $project['dateCreated'];
        $amount = Project::createPrice($project, OurCompany()->tva);
        $discount = 0;
        $comment = '';
        $tasks = Project::getOwnTasks($project_id);
        if (!empty($tasks)) {
//            usort($tasks, function ($a, $b){
//                $a = $a['ord'];
//                $b = $b['ord'];
//                if ($a == $b) {
//                    return 0;
//                }
//                return ($a < $b) ? -1 : 1;
//            });
            $tasks = Task::order0ToTheEnd($tasks);
        }

        return compact('project_id', 'tasks', 'amount', 'created', 'city', 'discount', 'comment');
    }

    public function getDataForPDF($bill_id, array $columns = [])
    {
        return ProjectBdcInvoice::getDataForPDF($bill_id, $columns);
    }

    public function createBdcEmailConfirmUrl($client_id, $bill_id)
    {
        $hash = $this->createBdcEmailConfirmHash($client_id);
        $url = SiteData()->getAll('sitename') . DS . MODULE . DS . Pages::getLogin() . '?' . http_build_query([
                'confirm' => 'bdc',
                'bdc_id' => $bill_id,
                'hash' => $hash
            ]);
        return $url;
    }

    public function createBdcEmailConfirmHash($client_id)
    {
        return md5(self::$MD5_PREFIX['confirm_bdc_from_email'] . $client_id);
    }

    public function createPayInvoiceHash($client_id)
    {
        return md5(self::$MD5_PREFIX['pay_invoice_from_email'] . $client_id);
    }

    public function createBdcEmailConfirmParams($client_id, $bill_id)
    {
        return [
            'hash' => $this->createBdcEmailConfirmHash($client_id),
            'id' => $bill_id
        ];
    }

    public function createPayInvoiceParams($client_id, $invoice_id)
    {
        return [
            'hash' => $this->createPayInvoiceHash($client_id),
            'id' => $invoice_id
        ];
    }

    public function getUserIdByBdcEmailConfirmHash($hash)
    {
        return User::getUserIdByHashAndPrefix($hash, self::$MD5_PREFIX['confirm_bdc_from_email']);
    }

    public function getUserIdByPayInvoiceHash($hash)
    {
        return User::getUserIdByHashAndPrefix($hash, self::$MD5_PREFIX['pay_invoice_from_email']);
    }

    public function add($data)
    {
        if (isset($data['created']))
            $data['created'] = date('Y-m-d H:i:s', strtotime($data['created']));
        $data['whoCreate'] = User()->getId();
        if (is_array($data['tasks'])) {
            $data['tasks'] = implode(',', array_column($data['tasks'], 'id'));
        }
        $data['token'] = md5($data['project_id'] . $data['tasks']);
        $res = ProjectBillModel::add($data);
        if ($res && is_numeric($res))
            AppEventsController::publish(AppEventsSubscribersController::newBill(), ['bill_id' => $res]);
        return $res;
    }

    public static function getById($id, array $columns = ['*'])
    {
        return ProjectBillModel::getById($id, $columns);
    }

    public static function isApproved($id)
    {
        $res = self::getById($id, ['approved']);
        return (boolean)$res['approved'];
    }

    public function getAll(array $columns = ['*'])
    {
        return ProjectBillModel::getAll($columns);
    }

    public function getLastBDCByProjectId($projects_id, array $columns = ['*'])
    {
        return ProjectBillModel::getLastBDCByProjectId($projects_id, $columns);
    }

    public function getByProjectId($project_id, array $columns = ['*'])
    {
        return ProjectBillModel::getByProjectId($project_id, $columns);
    }

    public function delete($bill_id, $proj_id = 0)
    {
        try {
            $res = ProjectBillModel::delete($bill_id);
            if ($res) {
                Folder::delete(InvoiceProject::getPathToFolder($bill_id, $proj_id));
                Folder::delete(self::getPathToFolder($bill_id, $proj_id));
            }
            return $res;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function getTasksFromBill($bill_id, $asArray = false)
    {
        $tasks = self::getById($bill_id, ['tasks']);
        if (empty($tasks))
            return [];
        $tasks = $tasks['tasks'];
        return $asArray ? array_map(function ($elem) {
            return intval($elem);
        }, explode(',', $tasks)) : $tasks;
    }

    public static function getClientUpdates($row)
    {
        return 'qwe';
    }

    public function getAllUnapprovedBills($params = ['*'])
    {

        $sql = 'SELECT ' . columnsArrayToQueryStringWithAlias('pb1', $params) . 'FROM ' . self::getTable() . ' pb1 LEFT JOIN ' . self::getTable() . ' pb2 ON (pb1.project_id = pb2.project_id AND pb1.id < pb2.id) WHERE pb2.id is null and pb1.approved = 0 order by pb1.id desc';
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}