<?php

use interfaces\iGetClientUpdates;
use models\ExpressionOfNeedsModel;
use traits\ClassName;
use traits\SingleTone;

class ExpressionOfNeeds implements iGetClientUpdates
{
    use ClassName;
    use SingleTone;

    public static function baseSelect(array $columns = ['*'], $aggregate = null)
    {
        return ExpressionOfNeedsModel::baseSelect($columns, $aggregate);
    }

    public static function getTable()
    {
        return ExpressionOfNeedsModel::getTable();
    }

    public static function getClientUpdates($row)
    {
        $data = self::getById($row['obj_id'], ['project_id']);
        $data['project'] = Project()->getById($data['project_id'], ['id', 'name']);
        $data['assistants'] = Assistant::getByOneProjectId($data['project_id'], ['id', 'firstname', 'lastname', 'img']);
        unset($data['project_id']);
        return $data;
    }


    public static function create($data)
    {
        $last_id = ExpressionOfNeedsModel::add([
            'data' => json_encode($data['data']),
            'project_id' => $data['project_id'],
            'is_show' => $data['is_show'],
            'whoCreate' => User()->getId(),
            'as_draft' => $data['as_draft'] ?? 1,
        ]);
        try {
            if (isset($data['as_draft']) && $data['as_draft'] == 0 && $last_id) {
                /*
                $client_id = User()->getId();
                if (!User()->isClient()) {
                    $client_id = Project()->getClient($data['project_id'], ['id']);
                    $client_id = $client_id['id'];
                }
                */
                ClientActivity::add($data['project_id'], self::getClassName(), $last_id);
            }
            return $last_id;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function getById($id, $columns = ['*'])
    {
        return ExpressionOfNeedsModel::getById($id, $columns);
    }

    public static function update($id, $data)
    {
        try {
            $res = ExpressionOfNeedsModel::update($id, [
                'data' => json_encode($data['data']),
                'project_id' => $data['project_id'],
                'is_show' => $data['is_show'],
                'as_draft' => $data['as_draft'] ?? 1,
            ]);
            if (isset($data['as_draft']) && $data['as_draft'] == 0 && $res) {
                /*
                $client_id = User()->getId();
                if (!User()->isClient()) {
                    $client_id = Project()->getClient($data['project_id'], ['id']);
                    $client_id = $client_id['id'];
                }
                */
                ClientActivity::add($data['project_id'], self::getClassName(), $data['project_id']);
            }
            return $res;
        } catch (Exception $e) {
            return $e->getTrace();
        }
    }

    public static function getByProjectId($id, array $columns = ['*'])
    {
        try {
            $res = ExpressionOfNeedsModel::getByProjectId($id, $columns);
            if (!empty($res))
                $res['data'] = json_decode(html_entity_decode($res['data']), true);
            return $res;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function getForAdminByProjectId($id, array $columns = ['*'])
    {
        try {
            $res = ExpressionOfNeedsModel::getForAdminByProjectId($id, $columns);
            if (!empty($res))
                $res['data'] = json_decode(html_entity_decode($res['data']), true);
            return $res;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}