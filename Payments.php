<?php

use traits\BaseSelect;
use traits\GetTable;

class Payments
{
    private static $table = 'payments';
    use getTable;
    use baseSelect;

    public static function add($data)
    {
        $sql = insert_query_string(self::getTable(), $data);
        try {
            $res = query($sql);
            return $res ? getLastId() : $res;
        } catch (Exception $e) {
            return $e->getTraceAsString();
        }
    }

    public static function getBySystemAndTransactionId($system, $transaction_id, $columns = ['*'])
    {
        $system = escape_string(clear_post($system));
        $transaction_id = escape_string(clear_post($transaction_id));
        $columns[] = 'whenAdded';
        $sql = self::baseSelect($columns) . ' WHERE system LIKE "' . $system . '" AND transaction_id="' . $transaction_id . '"';
        try {
            $res = querySingle($sql);
            $res['whenAdded'] = date('d-m-Y H:i:s', strtotime($res['whenAdded']));
            return $res;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}