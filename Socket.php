<?php

class Socket
{
    private $func;
    private $exitFunc;
    private $id;
    private $sleep;
    private $variables;

    private function closeFunc($obj = null)
    {

    }

    public function __construct($func, $sleep = 5)
    {
        $this->func = $func;
        $this->id = 0;
        $this->sleep = $sleep;
        $this->variables = [];
    }

    private function sendMessage($json_result)
    {
        echo "id: $this->id\n";
        echo "data: " . $json_result . "\n\n";
        ob_flush();
        flush();
    }

    public function setDestructFunc($func = null)
    {
        $this->exitFunc = $func;
        return $this;
    }

    public function setSleep($sleep = 5)
    {
        $this->sleep = $sleep;
        return $this;
    }

    public function __destruct()
    {
        if (is_callable($this->exitFunc)) {
            $exit = $this->exitFunc;
            $exit($this);
        }
    }

    public function __set($name, $value)
    {
        $this->variables[$name] = $value;
    }

    public function __get($name)
    {
        return $this->variables[$name] ?? false;
    }

    public function getSleep()
    {
        return $this->sleep;
    }

    public function __toString()
    {
        return http_build_query($this->variables);
    }

    public function run()
    {
//        set_time_limit($this->getSleep() + 3);
//        ini_set('max_execution_time', $this->getSleep() + 3);
//        if (session_status() == PHP_SESSION_NONE) {
            @session_start();
//        }
        session_write_close();
//        ignore_user_abort(true);
        header("Content-Type: text/event-stream");
        header("Cache-Control: no-cache");
        header("Connection: keep-alive");
//        ignore_user_abort();
        if (isset($_SERVER["HTTP_LAST_EVENT_ID"])) {
            $this->id = $_SERVER["HTTP_LAST_EVENT_ID"];
            if (isset($this->id) && !empty($this->id) && is_numeric($this->id)) {
                $this->id = intval($this->id);
                $this->id++;
            }
            else {
                $this->id = 0;
            }
        }
        else {
            $this->id = 0;
        }
        if (is_callable($this->func)) {
            $func = $this->func;
            while (true) {
                if (connection_status() != CONNECTION_NORMAL) {
//                    Logger::add('socket', 'connection aborted in SOCKET. Status - ' . connection_status() . '. Line ' . __LINE__);
                    exit(0);
                }
                $res = $func($this);
                $res = json_encode($res, JSON_UNESCAPED_UNICODE);
                $this->sendMessage($res);
                $this->id++;
                sleep($this->sleep);
            }
        }
    }
}