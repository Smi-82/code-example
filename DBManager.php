<?php


use dbmanager\interfaces\iDBManager;
use dbmanager\Native;
use dbmanager\ORM;
use traits\SingleTone;

class DBManager implements iDBManager
{
    use SingleTone;

    private $worker;

    private function __construct()
    {
        $this->setNative();
    }

    private function logQueries($sql)
    {
        $sql = preg_replace('/\s+/', ' ', $sql);
        $sql = trim($sql);
        Logger::add('queries', $sql . ';');
        Logger::add('queries', str_repeat('-', strlen($sql)) . ' ' . date('d.m.Y H:i:s'));
        return $sql;
    }

    public function setNative()
    {
        $this->worker = Native::getInst();
        return $this;
    }

    public function setORM()
    {
        $this->worker = ORM::getInst();
        return $this;
    }

    public function setPDO()
    {
        $this->worker = \DBManager\PDO::getInst();
        return $this;
    }

    public function getLink()
    {
        return $this->worker->getLink();
    }

    public function getTableColumns($table)
    {
        $table = escape_string(clear_post($table));
        $res = query('SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA="' . $this->worker->getDbname() . '" AND TABLE_NAME="' . $table . '"');
        return array_column($res, 'COLUMN_NAME');
    }

    public static function incrementToVal($table, $val = 0)
    {
        try {
            return query('ALTER TABLE ' . $table . ' AUTO_INCREMENT=' . $val);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function addTextField($table, $fieldName, $type = 'TEXT')
    {
        $table = escape_string(clear_post($table));
        $fieldName = escape_string(clear_post($fieldName));
        $sql = 'ALTER TABLE ' . $table . ' ADD IF NOT EXISTS ' . $fieldName . ' ' . $type . ' DEFAULT NULL';
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function query($sql, $log = false)
    {
        if ($log)
            $sql = $this->logQueries($sql);
        $link = $this->getLink();
        if (is_null($link)) {
            return false;
        }
        $result = mysqli_query($link, $sql);
        if (mysqli_error($link)) {
            throw new Exception(mysqli_error($link));
        }
        if (is_bool($result)) {
            return $result;
        }

        $data = array();

        while ($row = mysqli_fetch_assoc($result))
            $data[] = $row;
        return $data;
    }

    public function querySingle($sql)
    {
        $result = $this->query($sql);
        return empty($result) ? $result : array_shift($result);
    }
}
