<?php

use traits\BaseSelect;
use traits\GetTable;
use traits\SingleTone;

class Position
{
    private static $table = 'positions';
    use SingleTone;
    use GetTable;
    use BaseSelect;

    public static function getDefaultId()
    {
        return self::$default_id;
    }

    public function getAll($params = ['*'])
    {
        $sql = self::baseSelect($params);
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function getById($id, $params = ['*'])
    {
        if (is_scalar($id)) $id = [$id];
        $sql = self::baseSelect($params) . ' WHERE id in (' . implode(',', $id) . ')';
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function add($data)
    {
        $sql = insert_query_string(self::getTable(), $data);
        try {
            query($sql);
            return getLastId();
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function update($id, $data)
    {
        $id = escape_string($id);
        $sql = update_query_string(self::getTable(), $data) . ' WHERE id = ' . $id;
        try {
            query($sql);
            return getLastId();
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function delete($id)
    {
        $id = escape_string(clear_post($id));
        $sql = 'DELETE FROM ' . self::getTable() . ' WHERE id = ' . $id;
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function find($search, $params = ['*'])
    {
        $lang = Language()->getLanguage();
        $nameLangKey = 'name_' . $lang;
        $searchParts = explode(' ', $search);
        $where = array_map(function ($item) use ($nameLangKey) {
            $item = escape_string($item);
            $parts = [
                'p.level like \'%' . $item . '%\'',
                'pt.' . $nameLangKey . ' LIKE \'%' . $item . '%\'',
                'pc.' . $nameLangKey . ' LIKE \'%' . $item . '%\'',
                'ps.' . $nameLangKey . ' LIKE \'%' . $item . '%\''
            ];
            return '(' . implode(' OR ', $parts) . ')';
        }, $searchParts);

        $sql = 'SELECT ' . columnsArrayToQueryStringWithAlias('p', $params) . ' FROM ' . self::getTable() . ' p
         LEFT JOIN ' . PositionTitle::getTable() . ' pt ON p.position_title_id = pt.id
         LEFT JOIN ' . PositionCategory::getTable() . ' pc ON p.position_category_id = pc.id
         LEFT JOIN ' . PositionSpeciality::getTable() . ' ps ON p.position_speciality_id = ps.id
         WHERE 1 AND ' . implode(' OR ', $where);

        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}