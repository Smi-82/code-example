<?php


class Pages
{
    private static $defaults = [
        'admin' => 'project_list.php',
        'client' => 'dashboard.php',
        'RGPDUsers' => 'client_list.php',
        'login' => 'login.php'
    ];

    public static function getLogin()
    {
        return self::$defaults['login'];
    }

    public static function getRGPDUsersDefault()
    {
        return self::$defaults['RGPDUsers'];
    }

    public static function getClientDefault()
    {
        return self::$defaults['client'];
    }

    public static function getAdminDefault()
    {
        return self::$defaults['admin'];
    }

    public static function getClientSide()
    {
        return [
            'dashboard',
            'my-project-page',
            'my-projects',
            'personal',
            'my-project-detail-page',
            'my-project-detail-view',
            'library-project-detail-view',
            'payment',
            'questions',
            'add_new_project',
            'group_list',
            'add_tasks',
            'votre-assistant',
            'notifications',
            'library',
            'offers',
            'stripe',
            'bitpay',
            'payment_systems',
            'worker-rates',
            'payment_new',
            'test-inv',
            'collaborator_rates'
        ];
    }

    public static function withoutRegistering()
    {
        return [
            'validateUserData',
            'translation_adm',
            'uploadClientsFilesUponRegistration',
            'dictionary',
            'order-now',
            'order-now_new',
            'registration',
            'scripts',
            'login',
            'cron',
            'backref',
            'client_login',
            'logout',
            'for_anna',
            'forgot-password',
            'index',
            'redirect_outside',
            'pdf_digital',
            'contract-signature',
            'scripts_u2df',
            '404',
            'clear_tables',
            'formulaire-inscription-developpeur',
            'formular-project',
            'page',
            'sitemap',
            'ipn',
            'pay_by_link',
            'test',
            'test_smi',
            'stripe_response',
            'test_invoice',
            'animation',
            'successful', //gateway payment system
            'unsuccessful', //gateway payment system
        ];
    }
}