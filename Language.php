<?php

use traits\SingleTone;
use translation\classes\TranslationManager;

class Language
{
    private $_current_lng;
    private $_json;
    private static $default_lng = 'en';
    use SingleTone;

    private function _getJson()
    {
        $this->_json = [];
        $filename = ROOT . '/language/' . $this->_current_lng . '/' . $this->_current_lng . '.json';
        if (file_exists($filename)) {
            $language_json_origin = file_get_contents($filename);
            $this->_json = json_decode($language_json_origin);
        }
        else {
            $this->_json = false;
        }
    }

    private function __construct()
    {
        $this->_current_lng = self::$default_lng;
        $this->setLang($this->_current_lng);
        $this->getTranslateFromDB();
    }

    public function getTranslateFromDB()
    {
        if (!$this->_json) {
            $this->_json = TranslationManager::getInst()->getAsKeyValue($this->_current_lng);
        }
    }

    public function getJson($lng = null)
    {
        return $this->_json;
    }

    public static function getDefault()
    {
        return self::$default_lng;
    }

    public function setLang($lng = '')
    {
        if (!empty($lng)) {
            $this->_current_lng = $lng;
        }
        try {
            $_SESSION['language'] = $this->_current_lng;
            setcookie('lang', $this->_current_lng, time() + getSeconds(1, 'y'));
        } catch (Exception $e) {

        }
    }

    public function exceptions($langs)
    {
        try {
            if (User()->isClient()) {
                unset($langs['ru']);
            }

        } catch (Exception $e) {
        }

        return $langs;
    }

    public function getLangData()
    {
        $arr = [
            'en' => [
                'img' => '002-united-kingdom.png',
                'name' => 'En',
                'active' => 0,
                'fullname' => 'english',
            ],
            'fr' => [
                'img' => '001-france.png',
                'name' => 'Fr',
                'active' => 0,
                'fullname' => 'french',
            ],
            'ru' => [
                'img' => 'Russia.png',
                'name' => 'Ру',
                'active' => 0,
                'fullname' => 'russian',
            ],
//    'ge' => [
//        'img' => 'Germany.png',
//        'name' => 'Deutsch',
//        'active' => 0
//    ],
        ];
        foreach ($arr as $key => &$val)
            $val['key'] = $key;
        $arr[$this->getLanguage()]['active'] = 1;
        return $this->exceptions($arr);
    }

    public function getLangs()
    {
        return array_keys($this->getLangData());
    }

    public function getLanguage()
    {
        return $this->_current_lng;
    }

    public function lng($key, $lng = '')
    {
        if (empty($lng))
            $lng = $this->_current_lng;
        $key = trim($key);
        if ($lng != $this->_current_lng) {
            $this->_current_lng = $lng;
        }
        $this->getTranslateFromDB();
        try {
            if (isset($this->_json[$key])) {
                return trim($this->_json[$key]);
            }
            else {
                throw new Exception('There is no such key ("' . $key . '") in the "' . $this->_current_lng . '" language.');
            }
        } catch (Exception $e) {
            if (!(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest')) {

            }
            return $key;
        }
    }

    public function lngr()
    {
        $args = func_get_args();
        $key = array_shift($args);
        $string = vsprintf($this->lng($key), $args);
        return $string !== false ? $string : $key;
    }
}