<?php
use Kreait\Firebase;
use Kreait\Firebase\Factory;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\Notification;
use Kreait\Firebase\Messaging\MessageTarget;
class FirebaseEvents
{

    static $outputData = [];


    static function send()
    {


        $factory = (new Factory)
            ->withServiceAccount($_SERVER['DOCUMENT_ROOT'] . '/' . MODULE . '/firebase/credentials.json')
            ->createMessaging();

        $notification = Notification::create(self::$outputData['notification']['title'], self::$outputData['notification']['body']);
        $deviceTokens = self::$outputData['tokens'];

        $message = CloudMessage::new()
            ->withNotification($notification);

//        console($message);

        $sendReport = $factory->sendMulticast($message, $deviceTokens);

//        console($sendReport);
        return $sendReport;

//        echo 'Successful sends: '.$sendReport->successes()->count().PHP_EOL;
//        echo 'Failed sends: '.$sendReport->failures()->count().PHP_EOL;
//
//        if ($sendReport->hasFailures()) {
//            foreach ($sendReport->failures()->getItems() as $failure) {
//                echo $failure->error()->getMessage().PHP_EOL;
//            }
//        }

    }

    static function addOutputData($token, $title, $body)
    {
        self::$outputData[] = [
            'message' => [
                'token' => $token,
                'notification' => [
                    'title' => $title,
                    'body' => $body
                ]
            ]
        ];
    }

    static function setOutputData($token, $title, $body)
    {
        self::$outputData = [
            'tokens' => $token,
            'notification' => [
                'title' => $title,
                'body' => $body
            ]
        ];
    }
}