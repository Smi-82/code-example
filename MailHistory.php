<?php

use models\MailHistoryModel;
use traits\SingleTone;

class MailHistory
{
    use SingleTone;

    public static function baseSelect(array $columns = ['*'], $aggregate = null)
    {
        return MailHistoryModel::baseSelect($columns, $aggregate);
    }

    public static function getTable()
    {
        return MailHistoryModel::getTable();
    }

    public static function create($data)
    {
        return MailHistoryModel::add([
            'to' => $data['to'],
            'from' => $data['from'],
            'data' => json_encode($data['data']),
            'search_payment_part_id' => $data['search_payment_part_id'],
            'search_project_id' => $data['search_project_id'],
            'search_invoice_id' => $data['search_invoice_id']
        ]);
    }

    public static function getById($id, array $columns = ['*'])
    {
        try {
            $res = MailHistoryModel::getById($id, $columns);
            if ($res)
                $res['data'] = json_decode(html_entity_decode($res['data']), true);
            return $res;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function getAll(array $columns = ['*'])
    {
        return MailHistoryModel::getAll($columns);
    }
}