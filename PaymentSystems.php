<?php

use traits\SingleTone;

class PaymentSystems
{
    use SingleTone;

    private $_paymentByName;
    private $_settings;
    private $_dontShow;

    private function getPayment($name, $param)
    {
        return is_null($param) ? $this->_settings[$this->_paymentByName[$name]] : $this->_settings[$this->_paymentByName[$name]]->$param;
    }

    private function __construct()
    {
        $this->_dontShow = [];
        $this->_settings = [
            Simplepay_loc(),
            Stripe_loc(),
            Easytransac_loc(),
            Gateway_loc(),
            Bitpay_loc()
        ];
        foreach ($this->_settings as $key => $sys) {
            $this->_paymentByName[$sys->getName()] = $key;
        }
        $this->exclude_ids([0, 2, 3, 4]);
    }

    public function __get($name)
    {
        if (isset($this->_paymentByName[$name])) {
            return $name;
        }
        throw new Exception('qwe');
    }

    public function getSimplepay($param = null)
    {
        return $this->getPayment($this->simplepay, $param);
    }

    public function getGateway($param = null)
    {
        return $this->getPayment($this->gateway, $param);
    }

    public function getStripe($param = null)
    {
        return $this->getPayment($this->stripe, $param);
    }

    public function getEasytransac($param = null)
    {
        return $this->getPayment($this->easytransac, $param);
    }

    public function exclude_ids($arr = [], $rewrite = false)
    {
        if (!empty($arr))
            $this->_dontShow = $rewrite ? array_merge($this->_dontShow, $arr) : $arr;
        return self::$_inst;
    }

    public function exclude_names($names = [], $rewrite = false)
    {
        $arr = [];
        foreach ($names as $val) {
            if (isset($this->_paymentByName[$val]))
                $arr[] = $this->_paymentByName[$val];
        }
        if (!empty($arr))
            $this->_dontShow = $rewrite ? array_merge($this->_dontShow, $arr) : $arr;
        return self::$_inst;
    }

    public function getSystemById($id, $param = '')
    {
        return isset($this->_settings[$id]) ? $this->_settings[$id][$param] ?? $this->_settings[$id] : [];
    }

    public function getAllPaySys()
    {
        return empty($this->_dontShow) ? $this->_settings : array_diff_key($this->_settings, array_flip($this->_dontShow));
    }
}