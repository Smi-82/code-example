<?php

use traits\BaseSelect;
use traits\GetTable;
use traits\SingleTone;

class Offer
{
    private static $table = 'offer_client';
    use SingleTone;
    use GetTable;
    use BaseSelect;

    public static function getById($id, $params = ['*'])
    {
        $id = escape_string(clear_post($id));
        $sql = self::baseSelect($params) . ' WHERE id=' . $id . ' LIMIT 0, 1';
        try {
            return querySingle($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function getAllOffers1($params = ['*'])
    {
        $sql = self::baseSelect($params);
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function getAllOffers($params = ['*'])
    {
        $sql = Project::baseSelect($params) . ' WHERE is_offer = 1 AND is_template=0';
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function getOffersForMaster($master_id, $params = ['*'])
    {
        $master_id = escape_string(clear_post($master_id));
        $sql = Project::baseSelect($params) . ' WHERE master_id=' . $master_id . ' AND is_offer = 1 AND is_template=0';
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function getOfferForClient($client_id, $params = ['*'])
    {
        $client_id = escape_string(clear_post($client_id));
        $sql = self::baseSelect($params) . ' WHERE displayed=1 AND client_id=' . $client_id;
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function getNotAgreedOfferForClient($client_id, $params = ['*'])
    {
        $client_id = escape_string(clear_post($client_id));
        $sql = self::baseSelect($params) . ' WHERE displayed=1 AND client_id=' . $client_id . ' AND client_agree=0';
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function getOffersForClientByRegistrator($client_id, $params = ['*'])
    {
        $client_id = escape_string(clear_post($client_id));
        $registrator_id = User::getById($client_id, ['who_registered'])['who_registered'];
        if (!$registrator_id) return [];
        $sql = self::baseSelect($params) . ' WHERE displayed=1 AND client_id = ' . $client_id . ' AND who_add = ' . $registrator_id;
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function update($id, $data)
    {
        $id = escape_string(clear_post($id));
        unset($data['id']);
        $sql = update_query_string(self::getTable(), $data) . ' WHERE id=' . $id;
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function changeClientAgree($project_id, $client_id, $agree = 0)
    {
        $client_id = escape_string(clear_post($client_id));
        $project_id = escape_string(clear_post($project_id));
        $agree = escape_string(clear_post($agree));
        $sql = update_query_string(self::getTable(), ['client_agree' => $agree]) . ' WHERE project_id=' . $project_id . ' AND client_id=' . $client_id;
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function changeClientAgreeById($id, $agree = 0)
    {
        $id = escape_string(clear_post($id));
        $agree = escape_string(clear_post($agree));
        return self::update($id, ['client_agree' => $agree]);
    }

    public static function addOfferForClient($project_id, $client_id)
    {
        $client_id = escape_string(clear_post($client_id));
        $project_id = escape_string(clear_post($project_id));
        $sql = insert_query_string(self::getTable(), ['client_id' => $client_id, 'project_id' => $project_id, 'who_add' => User()->getId()], true);
        try {
            $res = query($sql);
            return $res ? getLastId() : $res;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function addManyOffersForClient($project_ids, $client_id)
    {
        $client_id = escape_string(clear_post($client_id));
        $project_ids = getAsArray($project_ids);
        $who_add = User()->getUser() ? User()->getId() : 0;
        $project_ids = array_map(function ($el) use ($client_id, $who_add) {
            return ['client_id' => $client_id, 'project_id' => $el, 'who_add' => $who_add];
        }, $project_ids);
        $sql = multiple_insert_str(self::getTable(), $project_ids, true);
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function addManyClientsToOffer($client_ids, $offer_id)
    {
        $offer_id = escape_string(clear_post($offer_id));
        $client_ids = getAsArray($client_ids);
        $who_add = User()->getUser() ? User()->getId() : 0;
        $client_ids = array_map(function ($el) use ($offer_id, $who_add) {
            return ['client_id' => $el, 'project_id' => $offer_id, 'who_add' => $who_add];
        }, $client_ids);
        $sql = multiple_insert_str(self::getTable(), $client_ids, true);
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function deleteClientFromOffer($client_id, $project_ids = '')
    {
        $client_id = escape_string(clear_post($client_id));
        $sql = 'DELETE FROM ' . self::getTable() . ' WHERE client_id=' . $client_id;
        $project_ids = getAsArray($project_ids);
        $project_ids = array_filter($project_ids);
        if (!empty($project_ids)) {
            $project_ids = clearParams($project_ids);
            $sql .= ' AND project_id IN (' . implode(',', $project_ids) . ')';
        }
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function changeOffersForClient($client_id, array $offer_ids = [])
    {
        $offer_ids = clearParams($offer_ids);

        if (empty($offer_ids))
            return true;

        $offer_ids = array_map(function ($elem) {
            return intval($elem);
        }, $offer_ids);

        $client_id = escape_string(clear_post($client_id));
        $old_offers = self::getOfferForClient($client_id);

        $old_offers_ids = array_column($old_offers, 'project_id');
        $old_offers_ids = array_map(function ($elem) {
            return intval($elem);
        }, $old_offers_ids);
        $forDelete = array_diff($old_offers_ids, $offer_ids);
        if ($forDelete)
            self::deleteClientFromOffer($client_id, $forDelete);
        foreach ($offer_ids as $val) {
            self::addOfferForClient($val, $client_id);
        }
        return true;
    }

    public static function getCntDisplayedByProjects($projects_id)
    {
        $projects_id = getAsArray($projects_id);
        $sql = 'SELECT COUNT(id) AS cnt, project_id FROM ' . self::getTable() . ' WHERE displayed=1 AND project_id IN (' . implode(',', $projects_id) . ') GROUP BY project_id';
        try {
            $res = query($sql);
            if (!empty($res)) {
                $res = valueToKey('project_id', $res, true);
                $res = array_map(function ($el) {
                    return $el['cnt'];
                }, $res);
            }
            return $res;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function deleteByProjectId($projects_id)
    {
        $projects_id = getAsArray($projects_id);
        $sql = 'DELETE FROM ' . self::getTable() . ' WHERE project_id IN (' . implode(',', $projects_id) . ')';
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function updateByProjectsId($projects_id, $data = [])
    {
        $projects_id = getAsArray($projects_id);
        $sql = update_query_string(self::getTable(), $data) . ' WHERE project_id IN (' . implode(',', $projects_id) . ')';
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function displayByProjectId($projects_id, $fl = 1)
    {
        return self::updateByProjectsId($projects_id, ['displayed' => $fl]);
    }
}