<?php


namespace controllers\ajax\request;


use controllers\ajax\BaseAjaxController;
use User;

class ConfirmBdcFromClientEmail extends BaseAjaxController
{
    public function run()
    {
        $userId = BillProject()->getUserIdByBdcEmailConfirmHash($_REQUEST['hash']);
        Login()->authorize(User::getById($userId));
        BillProject()->approve($_REQUEST['id']);
    }
}