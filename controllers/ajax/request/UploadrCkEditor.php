<?php

namespace controllers\ajax\request;
use controllers\ajax\BaseAjaxController;
use Exception;
use Intervention\Image\ImageManager;
use UploadDir;

class UploadrCkEditor extends BaseAjaxController
{
    public function run()
    {
        try {
            $upload_dir = UploadDir::getRootPath() . 'email-files/';
            $alias_dir = @($_SERVER["HTTPS"] != 'on') ? 'http://' . $_SERVER["SERVER_NAME"] : 'https://' . $_SERVER["SERVER_NAME"];
            $alias_dir .= '/' . MODULE . UploadDir::getRelativePath() . 'email-files/';
            $imageManager = new ImageManager(['driver' => 'imagick']);

            if (isset($_FILES["upload"])) {


                $error = $_FILES["upload"]["error"];
                $fileName = time() . $_FILES["upload"]["name"];

                $fileAlias = $alias_dir . time() . $fileName;
                $fileServer = $upload_dir . time() . $fileName;


                $img = $imageManager->make($_FILES["upload"]["tmp_name"]);
                $img->resize(400, null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
                $img->save($fileServer, 60);

                $insert_file = insert_query_string('sent_mails_files', [
                    'output_dir' => $fileServer,
                    'filename' => $fileName
                ]);


                return json_encode([
                    'fileName' => $fileName,
                    'uploaded' => 1,
                    'url' => $fileAlias,
                ]);
                return false;
            }
        } catch (Exception $e) {
            return Response()->error($e->getMessage());
        }
    }
}