<?php


namespace controllers\ajax\request;


use controllers\ajax\BaseAjaxController;
use ExpressionOfNeeds AS EON;

class ExpressionOfNeeds extends BaseAjaxController
{
    public function run()
    {
        $data = $_POST['data'];
        if ($_POST['draft_id'] == 0) {
            $res = EON::create([
                'data' => $data,
                'project_id' => $_POST['project'],
                'as_draft' => $_POST['asDraft'],
                'is_show' => $_POST['is_show'],
            ]);
        }
        else {
            $res = EON::update($_POST['draft_id'], [
                'data' => $data,
                'project_id' => $_POST['project'],
                'as_draft' => $_POST['asDraft'],
                'is_show' => $_POST['is_show'],
            ]);
        }
        return $res ? Response()->data($res)->success() : Response()->data($res)->error();
    }
}