<?php


namespace controllers\ajax\request;


use controllers\ajax\BaseAjaxController;
use User;

class PayInvoiceFromClientEmail extends BaseAjaxController
{
    public function run()
    {
        $userId = BillProject()->getUserIdByPayInvoiceHash($_REQUEST['hash']);
        Login()->authorize(User::getById($userId));
        redirect('personal.php#factures');
    }
}