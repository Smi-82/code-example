<?php


namespace controllers\ajax\request;


use controllers\ajax\BaseAjaxController;
use PositionHeld;

class FindPositions extends BaseAjaxController
{
    public function run()
    {
        return Response()->data(PositionHeld::search($_POST['filter'], ['id', 'name']))->success();
    }
}