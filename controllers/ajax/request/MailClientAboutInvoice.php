<?php


namespace controllers\ajax\request;


use controllers\ajax\BaseAjaxController;
use PaymentPartsProjectBill;
use Templates;

class MailClientAboutInvoice extends BaseAjaxController
{
    public function run()
    {
        $project_id = clear_post($_POST['project_id']);
        $invoice = InvoiceProject()->getLastByProjectId($project_id, ['id']);
        $project = Project()->getById($project_id, ['name']);
        $client = Project()->getClient($project_id, ['id', 'email']);
        $pdfPath = InvoiceProject()->getFilePathPDF($invoice['id'], $project_id);

        $payment_part = PaymentPartsProjectBill::getLastUnpaidByBillId($invoice['id'], ['payment_date']);

        $emailData = [
            'project' => [
                'name' => $project['name']
            ],
            'invoice' => [
                'payment_date' => $payment_part['payment_date']
            ]
        ];
        $emailHtml = Templates::getTemplate('invoice_email2', $emailData);
        $clientEmail = $client['email'];
        $testEmail = 'gleb.godovanyuk@gmail.com';

        $mailed = Mails()->send_one_mail_with_attachments(
            OurCompany()->email,
            OurCompany()->trade_name . ' ' . OurCompany()->company_name,
            $clientEmail,
            'asd',
            lng('invoice_email2_topic'),
            $emailHtml,
            ' ',
            [['path' => $pdfPath]]
        );
        return Response()->data(compact('mailed'))->success();
    }
}