<?php


namespace controllers\ajax;


use Exception;

class BaseAjaxController
{
    public function run()
    {
        return null;
    }

    public function __call($name, $arguments)
    {
        if (!method_exists($this, 'run'))
            throw new Exception('Methods "' . $name . '" or "run" does not exists in ' . get_class() . ' class!');
        $this->run();
    }
}