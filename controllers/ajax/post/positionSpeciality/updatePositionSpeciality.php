<?php


namespace controllers\ajax\post\positionSpeciality;


use controllers\ajax\BaseAjaxController;

class updatePositionSpeciality extends BaseAjaxController
{
    public function run()
    {
        return Response()->data(PositionSpeciality()->update(clear_post($_POST['id']), $_POST['data']))->success();
    }
}