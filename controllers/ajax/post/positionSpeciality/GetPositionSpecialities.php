<?php

namespace controllers\ajax\post\positionSpeciality;

use controllers\ajax\BaseAjaxController;

class GetPositionSpecialities extends BaseAjaxController
{
    public function run()
    {
        $lang = Language()->getLanguage();
        $nameKey = 'name_' . $lang;
        return Response()->data(array_map(function ($item) use ($nameKey) {
            $item['name'] = $item[$nameKey];
            return $item;
        }, PositionSpeciality()->getAll()))->success();
    }
}