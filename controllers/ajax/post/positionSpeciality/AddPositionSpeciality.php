<?php


namespace controllers\ajax\post\positionSpeciality;


use controllers\ajax\BaseAjaxController;

class AddPositionSpeciality extends BaseAjaxController
{
    public function run()
    {
        return Response()->data(PositionSpeciality()->add($_POST['data']))->success();
    }
}