<?php


namespace controllers\ajax\post\positionSpeciality;


use controllers\ajax\BaseAjaxController;

class DeletePositionSpeciality extends BaseAjaxController
{
    public function run()
    {
        return Response()->data(PositionSpeciality()->delete(clear_post($_POST['id'])))->success();
    }
}