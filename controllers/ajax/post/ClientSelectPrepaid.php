<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use MessengerEventCodes;
use Offer;
use ProjectBdcInvoice;

class ClientSelectPrepaid extends BaseAjaxController
{
    public function run()
    {
        $client_data = [
            'firstname' => isset($_POST['bill_client_data']['firstname']) ? clear_post($_POST['bill_client_data']['firstname']) : null,
            'lastname' => isset($_POST['bill_client_data']['lastname']) ? clear_post($_POST['bill_client_data']['lastname']) : null,
            'address' => isset($_POST['bill_client_data']['address']) ? clear_post($_POST['bill_client_data']['address']) : null,
            'societe' => isset($_POST['bill_client_data']['societe']) ? clear_post($_POST['bill_client_data']['societe']) : null,
            'nomero' => isset($_POST['bill_client_data']['nomero']) ? clear_post($_POST['bill_client_data']['nomero']) : null
        ];
        $client_data = array_filter($client_data, function ($el) {
            return !is_null($el);
        });

        if (!empty($client_data))
            $client_data = User()->updateClient(User()->getId(), $client_data, true);
        $new_proj = Project()->fullClone($_POST['project']);
        $res = Project()->addClient($new_proj, User()->getId());
        MessengerEvents()->send(MessengerEventCodes::PROJECT_CREATED, ['project_id' => $new_proj]);
        Project()->editProjectData($new_proj, ['prepaid' => $_POST['prepaid'], 'is_offer' => 0, 'offer_client_id' => $_POST['offer_id']]);
        Offer::changeClientAgree($_POST['project'], User()->getId(), 1);
        $prepaid = boolval(clear_post($_POST['prepaid']));
        if ($res) {
            ProjectBdcInvoice::createByProjectPriceType($new_proj);
        }
        return $res !== false ? Response()->success() : Response()->error();
    }
}