<?php


namespace controllers\ajax\post;


use Category;
use controllers\ajax\BaseAjaxController;
use Group;
use Priorities;
use Project;
use Task;

class GetSingleTaskPageDataClientSide extends BaseAjaxController
{
    public function run()
    {
        $taskId = escape_string(clear_post($_POST['task_id']));
        $projectId = escape_string(clear_post($_POST['project_id']));
        $task = Task::getById($taskId);

        $projectGroupNames = implode(', ', array_map(function ($item) {
            return mb_ucfirst($item);
        }, array_column(Group::getGroupsByProjectId($projectId, ['name']), 'name')));

        $taskGroupName = Group()->getOneGroupById([$task['group_id']], ['name'])['name'];

        $taskCategory = Category::getOneCategoryById($task['category']);
        $taskCategory = $taskCategory['name'] ?? '';
        $project = Project()->getById($projectId);
        $projectCategory = Category::getOneCategoryById($project['category']);
        $projectCategory = $projectCategory['name'] ?? '';
        $projectStatus = Statuses()->getStatusesById($project['status']);
        $projectStatus = $projectStatus ? mb_ucfirst(lng($projectStatus['name'])) : '';

        $projectPriority = Priorities::getKeyById($project['priority']);
        $taskPriority = Priorities::getKeyById($task['priority']);

        $taskTerms = call_user_func(function () use ($task) {
            $today = date_create(date('Y-m-d'));
            $created = date_create($task['created']);
            $estimateTime = date_create($task['estimateTime']);
            $working = date_diff($created, $today);
            $totaldays = date_diff($estimateTime, $created);
            $daysToEnd = date_diff($estimateTime, $today);
            $daysToEnd = $daysToEnd->format('%R%a');
            $expired = false;
            if ($daysToEnd < 0) {/*not expired*/
                $daysToEnd = substr($daysToEnd, 1, 5);
                $totaldays = intval($totaldays->format('%a'));
                $totalworking = intval($working->format('%a'));
                $done = round($totalworking * 100 / $totaldays);
            }
            else {
                $daysToEnd = intval($daysToEnd);
                $expired = true;
                $done = 100;
            }

            return compact('daysToEnd', 'expired', 'done');

        });

        $projectTerms = call_user_func(function () use ($project) {
            $today = date_create(date('Y-m-d'));
            $created = date_create($project['dateCreated']);
            $estimateTime = date_create($project['end']);
            $working = date_diff($created, $today);
            $totaldays = date_diff($estimateTime, $created);
            $daysToEnd = date_diff($estimateTime, $today);
            $daysToEnd = $daysToEnd->format('%R%a');
            $expired = false;
            if ($daysToEnd < 0) {/*not expired*/
                $daysToEnd = substr($daysToEnd, 1, 5);
                $totaldays = intval($totaldays->format('%a'));
                $totalworking = intval($working->format('%a'));
                $done = round($totalworking * 100 / $totaldays);
            }
            else {
                $daysToEnd = intval($daysToEnd);
                $expired = true;
                $done = 100;
            }

            return compact('daysToEnd', 'expired', 'done');
        });
        $priorityColorClass = Priorities::getColorClassById($task['priority']);
        $taskTerms['expiredText'] = $taskTerms['expired'] ? ucfirst(lngr('delivery_expired_x_days_ago', $taskTerms['daysToEnd'], $taskTerms['daysToEnd'] == 1 ? lng('day') : lng('days'))) : ucfirst(lngr('delivery_in_x_days', $taskTerms['daysToEnd'], $taskTerms['daysToEnd'] == 1 ? lng('day') : lng('days')));
        $task['start'] = date_create($task['start'])->format('d-m-Y H:i');
        $task['estimateTime'] = date_create($task['estimateTime'])->format('d-m-Y H:i');
        $task['description'] = html_entity_decode($task['description']);
        $task['status_aliase'] = Statuses()->getStatusesById($task['status'])['aliase'];
        $task['status_descr'] = Statuses()->getDescrById($task['status'], Task::getTable());
        $tmp = Task::getTimeSpentWithUsers($task['id']);
        $res = Task::getFullDataTimeSpentWithUsers($tmp);
        $res = $res[$task['id']];
        unset($res['users'][User()->getId()]);
        return Response()->data(compact(
            'priorityColorClass',
            'res',
            'project',
            'task',
            'projectTerms',
            'taskTerms',
            'taskPriority',
            'projectPriority',
            'projectStatus',
            'projectCategory',
            'taskCategory',
            'taskGroupName',
            'projectGroupNames'
        ))->success();

    }
}