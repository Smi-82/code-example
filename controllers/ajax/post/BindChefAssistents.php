<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class BindChefAssistents extends BaseAjaxController
{
    public function run()
    {
        $users = $_POST['users'];
        foreach ($users as $us_id)
            Project()->addUser($_POST['project'], $us_id);
        return Response()->success();
    }
}