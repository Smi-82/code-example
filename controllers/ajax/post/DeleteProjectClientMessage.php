<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class DeleteProjectClientMessage extends BaseAjaxController
{
    public function run()
    {
        return Response()->data(Project()->deleteClientMessage(clear_post($_POST['msg_id'])))->success();
    }
}