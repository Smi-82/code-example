<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Exception;
use Task;

class DeleteTask extends BaseAjaxController
{
    public function run()
    {
        if ($_POST['task_id'] == '') {
            return Response()->error();
        }
        try {
            Task::delete($_POST['task_id']);
        } catch (Exception $e) {
            return Response()->data(['place' => 'console'])->error($e->getMessage());
        }
        history('task', User()->getSignature() . ' delete task (ID:' . clear_post($_POST['task_id']));
        return Response()->success();
    }
}