<?php


namespace controllers\ajax\post;


use Category;
use controllers\ajax\BaseAjaxController;

class AddNewCategoryForTaskManager extends BaseAjaxController
{
    public function run()
    {
        $res = Category::add($_POST['data']);
        return !is_bool($res) && intval($res) != 0 ? Response()->data($_POST['data'])->success() : Response()->error($res);
    }
}