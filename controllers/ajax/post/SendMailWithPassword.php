<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use User;

class SendMailWithPassword extends BaseAjaxController
{
    public function send($pass, $login)
    {
        $user = User::getByLogin($login, ['email', 'firstname', 'lastname', 'login']);
        $res = User()->sendMailWithPassword($pass, $user);
        return $res ? Response()->success() : Response()->error();
    }

    public function run()
    {
        return $this->send($_POST['password'], $_POST['login']);
    }
}