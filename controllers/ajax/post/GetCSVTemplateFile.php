<?php

namespace controllers\ajax\post;

use controllers\ajax\BaseAjaxController;

class GetCSVTemplateFile extends BaseAjaxController
{
    public function run()
    {
        $res = User()->createCSVTemplateAddClientFile();
        if ($res)
            $res = User()->getCSVLink();
        else
            $res = '';

        return Response()->data($res)->success();
    }
}