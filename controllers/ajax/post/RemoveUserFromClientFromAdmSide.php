<?php

namespace controllers\ajax\post;

use controllers\ajax\BaseAjaxController;
use models\ClientUsersModel;

class RemoveUserFromClientFromAdmSide extends BaseAjaxController
{
    public function run()
    {
        $res = ClientUsersModel::delete($_POST['id']);
        return $res ? Response()->data($res)->success() : Response()->error($res);
    }
}