<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Exception;

class ChangeConsultantForClients extends BaseAjaxController
{
    public function run()
    {
        if (!(User()->isRGPDMaster() || User()->isSupervisor())) {
            return Response()->error();
        }
        $res = User()->update($_POST['to'], ['consultant_id' => $_POST['master']]);
        try {
            return $res === true ? Response()->success() : Response()->error(mb_ucfirst(lng('the_consultant_has_not_been_replaced')));
        } catch (Exception $e) {
            return Response()->error($e->getMessage());
        }
    }
}