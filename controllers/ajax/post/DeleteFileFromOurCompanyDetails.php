<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use OurCompany;

class DeleteFileFromOurCompanyDetails extends BaseAjaxController
{
    public function run()
    {
        return Response()->data(OurCompany::deleteFileFromDetails($_POST['filename'], $_POST['file']))->success();
    }
}