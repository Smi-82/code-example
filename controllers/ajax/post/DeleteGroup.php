<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class DeleteGroup extends BaseAjaxController
{
    public function run()
    {
        $res = Group()->delete($_POST['group_id']);
        return is_bool($res) && $res ? Response()->success() : Response()->error(mb_ucfirst(lng('error')) . ': ' . mb_ucfirst(lng('group_was_not_deleted')) . '!');
    }
}