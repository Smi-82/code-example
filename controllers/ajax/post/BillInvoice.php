<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Exception;
use Group;
use Project;
use Task;
use TaskAccess;
use TasksMessages;
use TasksMessagesFiles;
use User;
use UserTypes;

class BillInvoice extends BaseAjaxController
{
    public function run()
    {
        $general_project_id = escape_string(clear_post($_POST['project_id']));
        $user_id = User()->isCollaborator() ? User()->getParentId() : User()->getId();
        $query_str = '
            SELECT
            t.id,
            t.name,
            t.created,
            t.group_id,
            t.estimate,
            t.category,
            t.start,
            t.status,
            t.price,
            t.price_type,
            t.estimateTime,
            t.ord,
            g.name AS group_name,
            (SELECT name FROM ' . UserTypes::getTable() . ' WHERE id=t.collaborator_type_id) AS ut_name,
            (SELECT COUNT(id) FROM ' . TasksMessages::getTable() . ' WHERE task_id=t.id) AS cntMessage,
            (SELECT COUNT(id) FROM ' . TasksMessagesFiles::getTable() . ' WHERE task_id=t.id) AS cntFile
            FROM
            ' . Task::getTable() . ' AS t
            LEFT JOIN
            ' . Group::getTable() . ' AS g
            ON
            g.id = t.group_id
            LEFT JOIN
            ' . Project::getTable() . ' AS p
            ON
            g.general_project_id = p.id
            WHERE
            t.active=1
            AND
            p.active = 1
            AND
            p.id = ' . $general_project_id;
        if (!(User()->isSupervisor() || User()->isAssistant())) {
            $query_str .= " AND ( $user_id IN (SELECT ta.user_id FROM " . TaskAccess::getTable() . ' AS ta WHERE ta.task_id = t.id)) ';
        }
        $query_str .= ' ORDER BY t.ord ASC, t.id';
        try {
            $tasks = query($query_str);
        } catch (Exception $e) {
            return $e->getMessage();
        }
        $tasks = Task::order0ToTheEnd($tasks);
        $task_ids = array_column($tasks, 'id');
        $sql = 'SELECT
ta.user_id,
ta.task_id,
ta.time_spent,
TRIM(CONCAT(u.firstname, " ", u.lastname)) AS u_name,
u.img
FROM
' . TaskAccess::getTable() . ' AS ta
LEFT JOIN
' . User::getTable() . ' AS u
ON
u.id = ta.user_id
WHERE
ta.task_id IN (' . implode(',', $task_ids) . ')
AND
u.usertype != ' . UserTypes()->getClient();
        $participants = query($sql);
        $users = [];
        foreach ($participants as $user) {
            $user['img'] = getImg($user, $user['user_id']);
            $user['time_spent'] = secondsToWorkingTime($user['time_spent']);
            array_pop($user['time_spent']);
            $str = '';
            foreach ($user['time_spent'] as $k => $v)
                $str .= $v . $k . ' ';
            $str = trim($str);
            $user['time_spent'] = $str;
            $task_id = $user['task_id'];
            unset($user['task_id']);
            unset($user['user_id']);
            $users[$task_id][] = $user;
        }
        $groups = [];
        foreach ($tasks as $task) {
            $task['estimate'] = secondsToWorkingTime($task['estimate']);
            array_pop($task['estimate']);
            $str = '';
            foreach ($task['estimate'] as $key => $val)
                $str .= $val . $key . ' ';
            $task['estimate'] = $str;
            $group_id = $task['group_id'];
            $groups[$group_id]['group_name'] = $task['group_name'];
//            unset($task['group_name']);
            unset($task['group_id']);
            $task['users'] = isset($users[$task['id']]) && $users[$task['id']] ? $users[$task['id']] : [];
            $task['status'] = Statuses()->getStatusesById($task['status'], 'name');
            $task['checked'] = true;
            $task['user_show'] = false;
            $task['task_details'] = false;

            $today = date_create(date('Y-m-d'));
            $created = date_create($task['created']);
            $estimateTime = date_create($task['estimateTime']);
            $working = date_diff($created, $today);
            $totaldays = date_diff($estimateTime, $created);
            $daysToEnd = date_diff($estimateTime, $today);
            $daysToEnd = $daysToEnd->format('%R%a');

            if ($daysToEnd < 0) {/*not expired*/
                $daysToEnd = substr($daysToEnd, 1, 5);
                $totaldays = intval($totaldays->format('%a'));
                $totalworking = intval($working->format('%a'));
                $task['done'] = round($totalworking * 100 / $totaldays);
            }
            else {
                $task['done'] = 100;
            }
            $task['daysToEnd'] = intval($daysToEnd);
            $groups[$group_id]['tasks'][] = $task;
        }
        usort($groups, function ($a, $b) {
            $a = $a['group_name'];
            $b = $b['group_name'];
            if ($a == $b) {
                return 0;
            }
            return ($a < $b) ? -1 : 1;
        });
        return Response()->data(['groups' => $groups, 'cntTasks' => count($tasks)])->success();
    }
}