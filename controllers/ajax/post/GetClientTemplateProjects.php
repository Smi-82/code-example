<?php


namespace controllers\ajax\post;


use abstractClasses\TableData;
use controllers\ajax\BaseAjaxController;
use Exception;
use LibraryListPage;
use Project;

class GetClientTemplateProjects extends BaseAjaxController
{
    public function run()
    {
        $obj = new LibraryListPage();
        $result = $obj->getTableData();
        $project_ids = array_column($result['projects'], 'id');
        $tasks = Project::getOwnTasks($project_ids, ['id', 'status']);
        foreach ($tasks as $key => $val) {
            $p_id = $val['_p_id'];
            unset($val['_p_id']);
            $tasks[$p_id][] = $val;
            unset($tasks[$key]);
        }
        $statuses = Statuses();
        try {
            foreach ($result['projects'] as &$val) {
                $val['dateCreated'] = date('d-m-Y H:i', strtotime($val['dateCreated']));
                $val['id'] = intval($val['id']);
                if (isset($tasks[$val['id']])) {
                    $val['tasks_total'] = count($tasks[$val['id']]);
                    $val['tasks_ongoing'] = count(array_filter($tasks[$val['id']], function ($task) use ($statuses) {
                        return !in_array($task['status'], [$statuses->double_check_validated(), $statuses->approved_by_the_customer()]);
                    }));
                    $val['tasks_compl'] = $val['tasks_total'] - $val['tasks_ongoing'];
                }
                else {
                    $val['tasks_total'] = 0;
                    $val['tasks_ongoing'] = 0;
                    $val['tasks_compl'] = 0;
                }
                $val['new_id'] = Project::getFakeId($val['id'] );
                switch ($val['status']) {
                    case $statuses->in_preparation():
                    case $statuses->waiting_for_validation_by_the_client():
                    case $statuses->in_development():
                    case $statuses->finalized_not_controlled():
                    case $statuses->validated_technical_inspection():
                        $val['status'] = 1;
                        break;
                    case $statuses->double_check_validated():
                    case $statuses->approved_by_the_customer():
                        $val['status'] = 2;
                        break;
                }
            }
        } catch (Exception $e) {
            return Response()->data($e->getMessage())->error();
        }
        return Response()->data($result)->success();
    }
}