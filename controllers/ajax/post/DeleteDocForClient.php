<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class DeleteDocForClient extends BaseAjaxController
{
    public function run()
    {
        $res = User()->deleteDocForClient($_POST['user_id'], $_POST['doc']);
        return $res ? Response()->data($res)->success() : Response()->error($res);
    }
}