<?php


namespace controllers\ajax\post;


use CollaboratorProjectAccess;
use controllers\ajax\BaseAjaxController;

class DeleteCollaborator extends BaseAjaxController
{
    public function run()
    {
        $res = CollaboratorProjectAccess::deleteByUserIdProjectId($_POST['id'], $_POST['general_project_id']);
        return $res ? Response()->success() : Response()->error(mb_ucfirst(lng('error')) . ': collaborator was deletes from "collaborator_project_access" table');
    }
}