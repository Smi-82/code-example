<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class GetSession extends BaseAjaxController
{
    public function run()
    {
        return json_encode($_SESSION, JSON_UNESCAPED_UNICODE);
    }
}