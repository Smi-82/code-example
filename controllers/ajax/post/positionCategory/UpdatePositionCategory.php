<?php


namespace controllers\ajax\post\positionCategory;


use controllers\ajax\BaseAjaxController;

class UpdatePositionCategory extends BaseAjaxController
{
    public function run()
    {
        return Response()->data(PositionCategory()->update(clear_post($_POST['id']), $_POST['data']))->success();
    }
}