<?php


namespace controllers\ajax\post\positionCategory;


use controllers\ajax\BaseAjaxController;

class AddPositionCategory extends BaseAjaxController
{
    public function run()
    {
        return Response()->data(PositionCategory()->add($_POST['data']))->success();
    }
}