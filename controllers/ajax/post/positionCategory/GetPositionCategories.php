<?php


namespace controllers\ajax\post\positionCategory;


use controllers\ajax\BaseAjaxController;

class GetPositionCategories extends BaseAjaxController
{
    public function run()
    {
        $lang = Language()->getLanguage();
        $nameKey = 'name_' . $lang;
        return Response()->data(array_map(function ($item) use ($nameKey) {
            $item['name'] = $item[$nameKey];
            return $item;
        }, PositionCategory()->getAll()))->success();
    }
}