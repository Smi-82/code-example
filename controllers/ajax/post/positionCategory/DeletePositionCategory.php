<?php


namespace controllers\ajax\post\positionCategory;


use controllers\ajax\BaseAjaxController;

class DeletePositionCategory extends BaseAjaxController
{
    public function run()
    {
        return Response()->data(PositionCategory()->delete(clear_post($_POST['id'])))->success();
    }
}