<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class GetProjectMessages extends BaseAjaxController
{
    public function run()
    {
        $data = array_map(function ($item) {

            $item['user_img_url'] = getImg(['img' => $item['user_img']], $item['user_id']);
            $item['message_f'] = htmlspecialchars_decode($item['message']);
            return $item;
        }, Project()->getClientMessages($_POST['project_id']));
        usort($data, function ($a, $b) {
            return date($b['sentat']) >= date($a['sentat']);
        });
        return Response()->data($data)->success();
    }
}