<?php


namespace controllers\ajax\post;


use Category;
use controllers\ajax\BaseAjaxController;

class AddEntityToCat extends BaseAjaxController
{
    public function run()
    {
        $res = Category::addEntity($_POST['cat'], $_POST['ent']);
        return $res ? Response()->data($res)->success() : Response()->error($res);
    }
}