<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class ChangeProjectClientAttachmentVisibility extends BaseAjaxController
{
    public function run()
    {
        return Response()->data(Project()->changeClientAttachmentVisibility(clear_post($_POST['attachment_id']), clear_post($_POST['visible'])))->success();
    }
}