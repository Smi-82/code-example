<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class DeleteClientForBill extends BaseAjaxController
{
    public function run()
    {
        $res = Project()->deleteClient($_POST['project_id']);
        return !is_numeric($res) ? Response()->error($res) : Response()->success();
    }
}