<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use User;

class GetUserById extends BaseAjaxController
{
    public function run()
    {
        $user = isset($_POST['params']) && !empty($_POST['params']) ? User::getById($_POST['user_id'], $_POST['params']) : User::getById($_POST['user_id']);
        return $user ? Response()->data($user)->success() : Response()->error();
    }
}