<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Project;

class GetSelectedProjecWithTasksForClient extends BaseAjaxController
{
    public function run()
    {
        $project = Project()->getById($_POST['id']);
        $project['banner_descr'] = htmlspecialchars_decode($project['banner_descr']);
        $project['dateCreated'] = date('d-m-Y H:i', strtotime($project['dateCreated']));
        $project['price_type'] = Project()->getPriceType($project['price_type'])['obj'];
        $tasks = Project::getOwnTasks($_POST['id']);
        $project['tasks'] = $tasks;
        foreach ($project['tasks'] as &$v) {
            $v['description'] = htmlspecialchars_decode($v['description']);
        }
        return Response()->data(['project' => $project])->success();
    }
}