<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class JsDataAction extends BaseAjaxController
{
    public function run()
    {
        $action = escape_string(clear_post($_POST['action_name']));
        $params = $_POST['data'] ?? '';
        if (JsData()->$action($params))
            return Response()->data(JsData()->getJsData())->success();
        else
            return Response()->error('no jsData ' . $action);
    }
}