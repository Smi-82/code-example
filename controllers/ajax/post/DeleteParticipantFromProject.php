<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class DeleteParticipantFromProject extends BaseAjaxController
{
    public function run()
    {
        $res = Project()->deleteParticipants($_POST['project_id'], $_POST['user_id']);
        return is_bool($res) ? Response()->success() : Response()->error($res);
    }
}