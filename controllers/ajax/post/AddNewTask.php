<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class AddNewTask extends BaseAjaxController
{
    public function run()
    {
        $res = Task()->addNewTask($_POST);
        return empty($res['errors']) ? Response()->data($res)->success() : Response()->error($res['errors']);
    }
}