<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class BindMasterToProject extends BaseAjaxController
{
    public function run()
    {
        $master_ids = clearParams($_POST['masters']);
        $proj_obj = Project();
        $errors = [];
        foreach ($master_ids as $m_id) {
            $newProjectId = $proj_obj->cloneAsTemplate($_POST['project']);
            if (!is_numeric($newProjectId)) {
                $errors[] = 'project ' . $_POST['project'] . ' not cloned';
                continue;
            }
            $proj_obj->addMaster($newProjectId, $m_id);
        }
        return empty($errors) ? Response()->success() : Response()->data($errors)->error();
    }
}