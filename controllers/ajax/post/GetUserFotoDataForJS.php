<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class GetUserFotoDataForJS extends BaseAjaxController
{
    public function run()
    {
        return User()->getFotoDataForJS($_POST['user_id']);
    }
}