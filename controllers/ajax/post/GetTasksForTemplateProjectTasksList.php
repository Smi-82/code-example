<?php


namespace controllers\ajax\post;


use abstractClasses\TableData;
use controllers\ajax\BaseAjaxController;
use TaskListPage;

class GetTasksForTemplateProjectTasksList extends BaseAjaxController
{
    public function run()
    {
        $obj = new TaskListPage();
        $general_project_id = isset($_POST['project_id']) ? escape_string(clear_post($_POST['project_id'])) : null;
        $result = $obj->getDataForTemplateProject($general_project_id);
        return $result ? Response()->data($result)->success() : Response()->error();
    }
}