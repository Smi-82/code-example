<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use TaskAccess;
use User;
use UserTypes;

class GetSuggestsClientsTask extends BaseAjaxController
{
    public function run()
    {
        $data['search_key'] = $_POST['search_key'];
        $search_key = clear_post($data['search_key']);
        $strict = substr($search_key, 0, 1) == '(' && substr($search_key, -1) == ')';
        $oneword = count(explode(' ', $search_key)) == 1;
        if ($strict) {
            $search_key = ltrim($search_key, '(');
            $search_key = rtrim($search_key, ')');
        }
        if ($strict) {
            $sql = "SELECT a.firstname as name,
                a.lastname as lastname,
                a.img as img,
                a.id as id
                FROM " . User::getTable() . " a
                WHERE 1 and (a.firstname LIKE '$search_key' OR a.lastname LIKE '$search_key' OR CONCAT_WS(' ', a.firstname, a.lastname) LIKE '$search_key') and a.usertype = " . UserTypes()->getClient();
        }
        elseif ($oneword) {
            $sql = "SELECT a.firstname as name,
                a.lastname as lastname,
                a.img as img,
                a.id as id
                FROM " . User::getTable() . " a
                WHERE 1 and (CONCAT_WS(' ', a.firstname, a.lastname) LIKE '%$search_key%') and a.usertype = " . UserTypes()->getClient();
        }
        else {
            $sql = "SELECT CONCAT_WS(' ', a.firstname, a.lastname) as name,
                a.id as id
                FROM " . User::getTable() . " a WHERE 1 and a.usertype = " . UserTypes::getTable();
            if (isset($_POST['task_id']))
                $sql .= ' and a.id NOT IN (' . TaskAccess::baseSelect(['user_id']) . ' WHERE task_id IN (' . $_POST['task_id'] . ')) ';
            $sql .= ' ORDER BY firstname, lastname ';
            $result = query($sql);

            $ids = getIdsByKey($result, $search_key);

            $sql = "SELECT a.firstname as name,
                a.lastname as lastname,
                a.img as img,
                a.id as id
                FROM " . User::getTable() . " a WHERE a.id IN ($ids) ORDER BY firstname, lastname
            ";
        }

        $result_clients = query($sql);


        $items = array(
            'clients' => array()
        );

        $html = '';
        $item_empty = "<li class = 'search-suggest-item-empty'>Aucun résultat trouvé</li>";

        if (count($result_clients)) {
            foreach ($result_clients as $row2) {
                $row2['img'] = getImg($row2, $row2['id']);
                $items['clients'][] = "<li class = 'search-suggest-item-found' data-id = '" . $row2['id'] . "' ><span class='user-item'><img class='round32' src='" . $row2['img'] . "'> " . $row2['name'] . ' ' . $row2['lastname'] . " </span></li>";
            }
        }
        $items['clients'] = count($items['clients']) ? $items['clients'] : array($item_empty);
        $html = "<li class = 'search-suggest-category'>Clients</li>%clients%";
        $html = str_replace('%clients%', implode('', $items['clients']), $html);
        return Response()->data($html)->success();
    }
}