<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use GloodaAPI;
use User;

class EditUser extends BaseAjaxController
{
    public function run()
    {
        if (!isset($_POST['data'])) {
            $data = $_POST;
            unset($data['key']);
        }
        elseif (isset($_POST['data']) && !is_array($_POST['data'])) {
            parse_str($_POST['data'], $data);
        }
        else {
            $data = $_POST['data'];
        }
        $id = $data['id'];
        unset($data['id']);

        unset($data['login']); //login can't be changed and the field in form is locked
        $password_for_GL = '';


        $user = User::getById($id, ['email', 'global_id']);
        if ($data['firstname'] == '' || $data['lastname'] == '' || $data['email'] == '' || $data['usertype'] == '' /*|| $data['login'] == ''*/) {
            return Response()->error(mb_ucfirst(lng('fill_in_required_fields')));
        }


        if ($data['usertype'] == '') {
            return Response()->error(mb_ucfirst(lng('not_valid_type')));
        }

        // todo: change password though email to user!!!
        if ($data['password'] != '' and $data['re_password'] != '') {
            if ($data['password'] != $data['re_password']) {
                return Response()->error(mb_ucfirst(lng('wrong_repeat_password')));
            }
            $password_for_GL = $data['password'];
            $data['password'] = Login()->encrypt_password($data['password']);
        }
        else {
            unset($data['password']);
        }
        unset($data['re_password']);
        if (isset($_SESSION['mainImage']))
            $data['img'] = $_SESSION['mainImage'];

        require_once($_SERVER['DOCUMENT_ROOT'] . DS . MODULE . DS . 'messenger_tm/glooda_api.php');
        $ga = new GloodaAPI('logged_in_user', array('supportUsersData' => array(), 'supportDialogID' => 0, 'token' => User()->accessToken));
        $updateGLsuccess = true;
        $by_client_himself = false;
        if (User()->getGId() == $user['global_id']) { $by_client_himself = true; }
        if ($ga->login_OK) {
            if (isset($data['birthday'])) $data['birthday'] = strtotime($data['birthday']); //todo: проверить (ошибка на месяц)

            if ($by_client_himself) {
                $updateGLsuccess = $ga->auth_saveProfile($data);
                if (!empty($password_for_GL)) {
                    $updateGLsuccess = $ga->auth_updateSkey(
                        array('pass' => $password_for_GL)
                    );
                }

            } else {
                $updateGLsuccess = $ga->auth_updateAccountProfile(array('id' => $user['global_id']), $data);
                if (!empty($password_for_GL)) {
                    $updateGLsuccess = $ga->auth_updateAccountSkey(
                        array('id' => $user['global_id']),
                        array('pass' => $password_for_GL)
                    );
                }
            }
        }

        if ($updateGLsuccess) {
            $res = User()->update($id, $data);
            if ($res) {
                if ($id == User()->getId())
                    User()->updateHimself();
                history('Utilisateurs', User()->getSignature() . ' updated user information: ' . clear_post($data['firstname']) . ' ' . clear_post($data['lastname']));
                return Response()->success(mb_ucfirst(lng('success')));
            }
        }
    }
}