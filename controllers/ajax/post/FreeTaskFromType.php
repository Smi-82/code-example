<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Task;

class FreeTaskFromType extends BaseAjaxController
{
    public function run()
    {
        $res = Task::updateTasksData($_POST['task_id'], ['collaborator_type_id' => 0]);
        return $res === false ? Response()->error($e->getMessage()) : Response()->success();
    }
}