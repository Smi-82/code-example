<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Project;
use User;

class GetDataForBindUsersToProjects extends BaseAjaxController
{
    public function run()
    {
        $users = User::getByTypeId($_POST['type'], ['id', 'img', 'firstname', 'lastname']);
        $user_ids = array_column($users, 'id');
        $user_ids = array_map(function ($elem) {
            return intval($elem);
        }, $user_ids);
        $cnt_p = Project::getCntProjectsByUserId($user_ids);
        $users = array_map(function ($elem) use ($cnt_p) {
            $elem['img'] = getImg($elem, $elem['id']);
            $elem['fullName'] = trim($elem['firstname'] . ' ' . $elem['lastname']);
            $elem['cnt_p'] = $cnt_p[$elem['id']];
            return $elem;
        }, $users);
        usort($users, function ($a, $b) {
            $a = $a['cnt_p'];
            $b = $b['cnt_p'];
            if ($a == $b) {
                return 0;
            }
            return ($a < $b) ? -1 : 1;
        });
        $users_in_proj = Project()->getOwnParticipants($_POST['projects'], ['id']);

        $users_in_proj = array_map(function ($elem) use ($user_ids) {
            $elem = array_keys($elem);
            $elem = array_intersect($elem, $user_ids);
            return array_values($elem);
        }, $users_in_proj);
        return Response()->data(compact('users', 'users_in_proj', 'cnt_p'))->success();
    }
}