<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class DeleteTechFromProject extends BaseAjaxController
{
    public function run()
    {
        $current = Project()->getById($_POST['project'], ['languages']);
        $current = $current['languages'];
        if ($current != 'null') {
            $current = json_decode($current);
        }
        else
            $current = [];
        $current = array_values($current);
        $tech = [$_POST['tech']];
        $tech = array_values($tech);
        $current = array_diff($current, $tech);
        $current = array_values($current);
        $current = empty($current) ? null : json_encode($current);
        Project()->editProjectData($_POST['project'], ['languages' => $current]);
        return Response()->success();
    }
}