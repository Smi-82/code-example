<?php

namespace controllers\ajax\post;

use ClientUsers;
use controllers\ajax\BaseAjaxController;

class GetRelatedUsersToClientForAdmSide extends BaseAjaxController
{
    public function run()
    {
        $res = ClientUsers::getByClientId($_POST['client_id']);
        return is_array($res) ? Response()->data($res)->success() : Response()->error($res);
    }
}