<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use ProjectBdcInvoice;
use TimePackagesProject;

class GenerateBDCInvoiceForTimePackage extends BaseAjaxController
{
    public function run()
    {
        $res = ProjectBdcInvoice::createByProjectPriceType($_POST['project'], [TimePackagesProject::getTable() => $_POST['package_id']]);
        $bill_id = TimePackagesProject::getById($_POST['package_id'], ['project_bill_id']);
        $bill_id = $bill_id['project_bill_id'];
        $res = BillProject()->readTOS($bill_id);
        $res = BillProject()->approve($bill_id);
        return Response()->data($res)->success();
    }
}