<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class ChangeActiveForMasters extends BaseAjaxController
{
    public function run()
    {
        $res = User()->update($_POST['data']['masters_id'], ['active' => $_POST['data']['status']]);
        return $res ? Response()->success() : Response()->error();
    }
}