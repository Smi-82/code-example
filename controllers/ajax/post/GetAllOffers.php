<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Offer;

class GetAllOffers extends BaseAjaxController
{
    public function run()
    {
        $offers = Offer::getAllOffers(['id']);
        $tmp = [];
        foreach ($offers as $v) {
            $tmp[$v['id']] = $v;
        }
        $offers = array_column($offers, 'id');
        $offers = Project()->getManyByIds($offers, ['id', 'banner_descr', 'banner_title', 'img']);
        foreach ($offers as &$val) {
            $val['img'] = Project()->getImgLink($val['id'], $val['img']);
            $val['banner_descr'] = htmlspecialchars_decode($val['banner_descr']);
            $val['offer_id'] = isset($tmp[$val['id']]) ? $tmp[$val['id']]['id'] : 0;
        }
        unset($tmp);
        return Response()->data($offers)->success();
    }
}