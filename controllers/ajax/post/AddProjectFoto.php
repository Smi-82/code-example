<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class AddProjectFoto extends BaseAjaxController
{
    public function run()
    {
        $res = Project()->addFoto($_POST['id']);
        return $res ? Response()->data($res)->success() : Response()->error();
    }
}