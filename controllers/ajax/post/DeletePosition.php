<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class DeletePosition extends BaseAjaxController
{
    public function run()
    {
        return Response()->data(Position()->delete(clear_post($_POST['id'])))->success();
    }
}