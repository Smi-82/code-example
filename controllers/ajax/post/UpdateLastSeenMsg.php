<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use UserMessageLink;

class UpdateLastSeenMsg extends BaseAjaxController
{
    public function run()
    {
        $res = UserMessageLink::getByUserIdTaskId(User()->getId(), $_POST['task_id'], ['id']);
        if (!empty($res)) {
            UserMessageLink::update($res['id'], ['message_id' => $_POST['lastMessage']]);
        }
        else {
            if (isset($_POST['lastMessage']) && clear_post($_POST['lastMessage']) != 0) {
                UserMessageLink::add(User()->getId(), $_POST['lastMessage'], $_POST['task_id']);
            }
        }
        return Response()->success();
    }
}