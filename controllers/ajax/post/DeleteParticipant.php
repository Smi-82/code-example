<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use User;

class DeleteParticipant extends BaseAjaxController
{
    public function run()
    {
        $res = Task()->deleteAccess($_POST['task_id'], $_POST['user_id']);
        $user = [];
        if ($res) {
            $user = User::getById($_POST['user_id'], ['firstname', 'lastname', 'usertype']);
            $obj = AdmNotify();
            $obj->setDeleteUserFromTask()->getWorker()->whoMustSee($_POST['task_id'], $_POST['user_id']);
            $obj->add();
            taskHistory($_POST['task_id'], 'Delete participant : ' . $user['firstname'] . " " . $user['lastname']);
            $user['usertype'] = UserTypes()->getTypeById($user['usertype']);
            history('task', User()->getSignature() . ' deleted participant : ' . $user['firstname'] . ' ' . $user['lastname'] . ' from the task ID: ' . $_POST['task_id']);
        }
        return Response()->data($user)->success();
    }
}