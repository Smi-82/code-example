<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Group;

class GetGroupsByProjectId extends BaseAjaxController
{
    public function run()
    {
        $res = Group::getGroupsByProjectId($_POST['project'], ['id', 'name']);
        usort($res, function ($a, $b) {
            if ($a['name'] == $b['name']) {
                return 0;
            }
            return ($a['name'] < $b['name']) ? -1 : 1;
        });
        return Response()->data($res)->success();
    }
}