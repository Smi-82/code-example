<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Exception;
use Group;
use Project;
use Task;
use TaskAccess;
use UserTypes;

class BillInvoiceReport extends BaseAjaxController
{
    public function run()
    {
        if ($_SESSION[$_POST['key']]['project']['price_type'] == 2) {
            $task_ids = Project::getOwnTasks($_SESSION[$_POST['key']]['project']['id'], ['id']);
            $task_ids = array_column($task_ids, 'id');
        }
        else
            $task_ids = $_SESSION[$_POST['key']]['tasks'];
        $task_ids = clearParams($task_ids);
        $sql = '
SELECT
t.id,
t.name,
t.description,
t.estimate,
t.price_type,
t.price,
       t.ord,
COUNT(ta.user_id) AS cnt_users,
((t.estimate/3600)*ut.tax_min) AS tht,
(((t.estimate/3600)*ut.tax_min) + ((t.estimate/3600)*ut.tax_min)*0.2) AS ttc,
ut.id AS type_id,
ut.name AS type_name,
ut.tax_min,
g.name AS group_name,
g.id AS group_id
FROM
' . Task::getTable() . ' AS t
LEFT JOIN
' . TaskAccess::getTable() . ' AS ta
ON
ta.task_id = t.id
LEFT JOIN
' . UserTypes::getTable() . ' AS ut
ON
ut.id = t.collaborator_type_id
LEFT JOIN
' . Group::getTable() . ' AS g
ON
t.group_id = g.id
WHERE
t.id IN (' . implode(',', $task_ids) . ')
GROUP BY t.id ORDER BY t.ord ASC, t.id ';
        try {
            $tasks = query($sql);
        } catch (Exception $e) {
            return Response()->data($e->getMessage())->error();
        }
        $tasks = Task::order0ToTheEnd($tasks);
        foreach ($tasks as &$task) {
            $task['description'] = strip_tags(html_entity_decode($task['description']));
            $task['group_name'] = strip_tags(html_entity_decode($task['group_name']));
            $task['price_type'] = intval($task['price_type']);
            if ($task['price_type'] == 1) {
                $task['date_obj'] = secondsToWorkingTime($task['task_estimation']);
            }
            if (is_null($task['tht']))
                $task['tht'] = 0;
            if (is_null($task['ttc']))
                $task['ttc'] = 0;

        }
        return Response()->data($tasks)->success();
    }
}