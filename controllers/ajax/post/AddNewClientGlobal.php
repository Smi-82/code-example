<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use GloodaAPI;

class AddNewClientGlobal extends BaseAjaxController
{
    public function run()
    {
        ### new registration into global Taskea account
        ### used#1: www/udb_u2df/registration.php
        ### used#2: www/udb_u2df/fiche-client.php

        if (isset($_POST['data']) && !is_array($_POST['data'])) {
            parse_str($_POST['data'], $data);
        }
        else {
            $data = $_POST;
        }
        if (isset($data['clientid']) && intval($data['clientid'])) {
            if (isset($data['by_whom']) && $data['by_whom'] == 'client') {
                $res = User()->updateClient($data['clientid'], $data, true);
            }
            else {
                $res = User()->updateClient($data['clientid'], $data);
            }
        }
        else {
            require_once($_SERVER['DOCUMENT_ROOT'] . DS . MODULE . DS . 'messenger_tm/glooda_api.php');
            $ga = new GloodaAPI(
                'registration',
                array(
                    'login' => $data['login'],
                    'pass' => $data['password'],
                    're_pass' => $data['password1'],
                    'email' => $data['email']
                )
            );
            if ($ga->registration_OK) {
                $ga->auth_saveProfile($data);
                $global_id = $ga->get_ID();
                $data['global_id'] = $global_id;
                $glres = array(
                    'TM_token' => $ga->get_accessToken(),
                    'TM_global_id' => $global_id,
                    'TM_profileid' => $ga->get_profileid(),
                    'TM_login' => $data['login']
                );
                $res = User()->addClient($data);
            }
            else {
                $res = $ga->getErrorsString();
            }
        }
        if (intval($res) == intval(User()->getId())) {
            User()->updateHimself();
        }
        if(!is_numeric($res)){
            return Response()->error($res);
        }
        return Response()->data($res)->success();
    }
}