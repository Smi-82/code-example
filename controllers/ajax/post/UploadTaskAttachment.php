<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class UploadTaskAttachment extends BaseAjaxController
{
    public function run()
    {
        return Response()->data(Task()->addAttachment($_POST['task_id']))->success();
    }
}