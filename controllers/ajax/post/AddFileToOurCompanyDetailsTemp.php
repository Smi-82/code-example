<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use OurCompany;

class AddFileToOurCompanyDetailsTemp extends BaseAjaxController
{
    public function run()
    {
        return Response()->data(OurCompany::addFileToDetailsTemp($_POST['file']))->success();
    }
}