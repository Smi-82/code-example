<?php


namespace controllers\ajax\post;


use ClientActivity;
use controllers\ajax\BaseAjaxController;

class GetTaskInClientEvents extends BaseAjaxController
{
    public function run()
    {
        $res = ClientActivity::getTask($_POST['id']);
        return is_numeric($res) ? Response()->data($res)->success() : Response()->error($res);
    }
}