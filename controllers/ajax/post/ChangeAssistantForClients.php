<?php


namespace controllers\ajax\post;


use Assistant;
use controllers\ajax\BaseAjaxController;

class ChangeAssistantForClients extends BaseAjaxController
{
    public function run()
    {
        if (!User()->isSupervisor()) {
            return Response()->error();
        }
        $res = Assistant::bindAssistantWithClient($_POST['to'], $_POST['assistant']);
        return $res ? Response()->success() : Response()->error(mb_ucfirst(lng('the_consultant_has_not_been_replaced')));
    }
}