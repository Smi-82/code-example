<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Exception;
use Group;
use Task;

class CloseGeneralProject extends BaseAjaxController
{
    public function run()
    {
        $res = Project()->editProjectData($_POST['general_project_id'], ['active' => 0]);
        if ($res['error'] === false) {
            $sql = update_query_string(Task::getTable(),
                    array(
                        'active' => 0
                    )) . ' WHERE active = 1 AND user_id=' . User()->getId() . ' AND group_id IN (' . Group::baseSelect(['id']) . ' WHERE general_project_id = "' . clear_post($_POST['general_project_id']) . '")';
            try {
                if (query($sql)) {
                    return Response()->success();
                }
                else {
                    Project()->editProjectData($_POST['general_project_id'], ['active' => 1]);
                    return Response()->error(mb_ucfirst(lng('error')) . ': ' . mb_ucfirst(lng('tasks_is_not_updated')));
                }
            } catch (Exception $e) {
                return Response()->error($e->getMessage());
            }
        }
        else {
            return Response()->error(mb_ucfirst(lng('error')) . ': ' . mb_ucfirst(lng('project_is_not_updated')));
        }
    }
}