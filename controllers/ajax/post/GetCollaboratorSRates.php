<?php


namespace controllers\ajax\post;


use abstractClasses\TableData;
use controllers\ajax\BaseAjaxController;
use UserTypeRatesPage;

class GetCollaboratorSRates extends BaseAjaxController
{
    public function run()
    {
        $res = new UserTypeRatesPage();
        $res = $res->getTableData();
        return Response()->data($res)->success();
    }

    public function forClientSide()
    {
        $res = new UserTypeRatesPage();
        $res = $res->getTableDataForClientSide();
        return Response()->data($res)->success();
    }
}