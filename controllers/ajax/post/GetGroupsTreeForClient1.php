<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Group;

class GetGroupsTreeForClient1 extends BaseAjaxController
{
    public function run()
    {
        $arr = Group::getGroupsByProjectId($_POST['project']);
        $html = Group::get_tree('singleGroupClientSide', $arr, 0);
        return Response()->data(compact('arr', 'html'))->success();
    }
}