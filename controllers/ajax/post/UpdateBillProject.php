<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class UpdateBillProject extends BaseAjaxController
{
    public function run()
    {
        $res = BillProject()->update($_POST['bill'], $_POST['data']);
        return $res ? Response()->data($res)->success() : Response()->error();
    }
}