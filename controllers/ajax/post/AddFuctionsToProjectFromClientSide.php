<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use ProjectFunctions;

class AddFuctionsToProjectFromClientSide extends BaseAjaxController
{
    public function run()
    {
        $functions = $_POST['data'];
        $functions = array_filter($functions, function ($el) {
            return !empty(trim($el['name']));
        });
        if (!empty($functions)) {
            $old = array_filter($functions, function ($el) {
                return intval($el['id']) != 0;
            });
            $new = array_filter($functions, function ($el) {
                return intval($el['id']) == 0;
            });
            if (!empty($new)) {
                ProjectFunctions::multiAdd($new);
            }
            if (!empty($old)) {
                foreach ($old as $f) {
                    ProjectFunctions::update($f['id'], $f);
                }
            }
        }
        return Response()->success();
    }
}