<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use TimePackagesProject;

class AddProjectTimePackage extends BaseAjaxController
{
    public function run()
    {
        $res = TimePackagesProject::add($_POST['data']);
        return Response()->data($res)->success();
    }
}