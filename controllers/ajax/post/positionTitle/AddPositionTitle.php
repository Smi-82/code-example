<?php


namespace controllers\ajax\post\positionTitle;


use controllers\ajax\BaseAjaxController;

class AddPositionTitle extends BaseAjaxController
{
    public function run()
    {
        return Response()->data(PositionTitle()->add($_POST['data']))->success();
    }
}