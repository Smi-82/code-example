<?php


namespace controllers\ajax\post\positionTitle;


use controllers\ajax\BaseAjaxController;

class DeletePositionTitle extends BaseAjaxController
{
    public function run()
    {
        return Response()->data(PositionTitle()->delete(clear_post($_POST['id'])))->success();
    }
}