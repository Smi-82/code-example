<?php


namespace controllers\ajax\post\positionTitle;


use controllers\ajax\BaseAjaxController;

class UpdatePositionTitle extends BaseAjaxController
{
    public function run()
    {
        return Response()->data(PositionTitle()->update(clear_post($_POST['id']), $_POST['data']))->success();
    }
}