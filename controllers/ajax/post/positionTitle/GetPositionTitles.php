<?php


namespace controllers\ajax\post\positionTitle;


use controllers\ajax\BaseAjaxController;

class GetPositionTitles extends BaseAjaxController
{
    public function run()
    {
        $lang = Language()->getLanguage();
        $nameKey = 'name_' . $lang;
        return Response()->data(array_map(function ($item) use ($nameKey) {
            $item['name'] = $item[$nameKey];
            return $item;
        }, PositionTitle()->getAll()))->success();
    }
}