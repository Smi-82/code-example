<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Exception;
use User;
use UserTypes;

class SeachUsersForTask extends BaseAjaxController
{
    public function run()
    {
        $sql = 'SELECT u.id, u.usertype, u.firstname, u.lastname, TRIM(CONCAT(u.firstname," ",u.lastname)) AS u_name, img, ut.name AS ut_name FROM ' . User::getTable() . ' AS u LEFT JOIN ' . UserTypes::getTable() . ' AS ut ON u.usertype=ut.id WHERE u.usertype!=' . UserTypes()->getClient();
        $u_name = escape_string(clear_post($_POST['name']));
        $tmp = ' AND (';
        $fields = ['firstname', 'lastname'];
        foreach ($fields as $field) {
            $tmp .= $field . ' LIKE "%' . $u_name . '%"';
            if (next($fields) !== false)
                $tmp .= ' OR ';
        }
        $tmp .= ') ';
        $sql .= $tmp;
        $sql .= ' ORDER BY u.usertype, u_name';
        try {
            $res = query($sql);
            $res = array_map(function ($elem) {
                $elem['img'] = getImg($elem, $elem['id']);
                return $elem;
            }, $res);
            $tmp = [];
            foreach ($res as $val) {
                $tmp[$val['ut_name']][] = $val;
            }
            return Response()->data($tmp)->success($sql);
        } catch (Exception $e) {
            return Response()->data($e->getMessage())->error();
        }
    }
}