<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use MessengerEventCodes;
use Pages;
use PaymentPartsProjectBill;
use Templates;

class GenerateInvoice extends BaseAjaxController
{
    public function run()
    {
        $bill_id = BillProject()->add($_POST['billData']);
        MessengerEvents()->send(MessengerEventCodes::CONFIRM_PURCHASE_ORDER, ['bill_id' => $bill_id]);
        if (!intval($bill_id)) {
            echo Response()->error();
            exit();
        }
        $payment_parts = $_POST['payment_parts'];
        $new_parts = $payment_parts['new'] ?? [];
        $old_parts = $payment_parts['old'] ?? [];
        if (!empty($new_parts)) {
            foreach ($new_parts as $val) {
                $val['project_id'] = $_POST['project_id'];
                $val['project_bill_id'] = $bill_id;
                PaymentPartsProjectBill::add($val);
            }
        }
        if (!empty($old_parts)) {
            foreach ($old_parts as $val) {
                PaymentPartsProjectBill::update($val['id'], $val);
            }
        }

        $client = Project()->getClient($_POST['project_id'], ['id', 'email']);
        $pdfData = BillProject()->getDataForPDF($bill_id);
        $pdfData['bank_requisites'] = OurCompany()->getBankRequisites();
        $file_create = BillProject()->createPDF($pdfData);
        $pdfPath = BillProject()->getFilePathPDF($pdfData['id'], $pdfData['project_id']);

        $clientId = $client['id'];

        $emailHtml = Templates::getTemplate('bdc_email', [
            'confirm_bdc_href' => Scripts()->buildUrl(
                'confirmBdcFromClientEmail',
                Login()->redirectParam('/personal.php?#comandes'),
                SiteData()->getAll('sitename') . DS . MODULE . DS . Pages::getLogin()
            )
        ]);
        $test = false;
        $clientEmail = $client['email'];
        $testEmail = 'gleb.godovanyuk@gmail.com';
        $mailed = Mails()->send_one_mail_with_attachments(
            OurCompany()->email,
            OurCompany()->trade_name . ' ' . OurCompany()->company_name,
            $test ? $testEmail : $clientEmail,
            'asd',
            lng('bdc_email_topic'),
            $emailHtml,
            ' ',
            [['path' => $pdfPath]]);
        MessengerEvents()->send(MessengerEventCodes::CONFIRM_PURCHASE_ORDER, ['bill_id' => $bill_id]);
        return Response()->data($bill_id)->success();
    }
}