<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Exception;
use TaskAccess;
use Timing;
use User;

class ParticipantList extends BaseAjaxController
{
    public function run()
    {
        $sql = '
SELECT
ta.time_spent,
ta.last_timestamp,
ta.user_id,
u.firstname,
u.lastname,
u.usertype,
u.img,
t_al.id AS alert
FROM ' .
            TaskAccess::getTable() . ' AS ta
LEFT JOIN ' .
            User::getTable() . ' AS u
ON
u.id=ta.user_id
LEFT JOIN
tasks_alerts AS t_al
ON
t_al.user_id = ta.user_id
AND
t_al.task_id = ta.task_id
AND
t_al.type = 1
WHERE
ta.task_id=' . $_POST['task_id'];
        try {
            $participants = query($sql);
        } catch (Exception $e) {
            $participants = $e->getMessage();
        }
        $current_user = User();
        $participants = array_map(function ($el) use ($current_user) {
            $el['online_participant'] = (time() - $el['last_timestamp'] > 20) ? (($el['user_id'] == $current_user->getID()) ? 'online_participant' : '') : 'online_participant';
            $el['type'] = 'participant';
            $el['alert_add'] = ($current_user->isSupervisor() || $current_user->isChef() || $current_user->isAssistant());
            if ($el['alert'])
                $el['alert'] = 'alerted';
            $el['src'] = getImg($el, $el['user_id']);
            $el['time_spent1'] = Timing::asString(Timing::get(intval($el['time_spent'])));
            return $el;
        }, $participants);
        $clients = array_filter($participants, function ($el) {
            return $el['usertype'] == UserTypes()->getClient();
        });
        $clients_ids = array_flip(array_keys($clients));
        $participants = array_diff_key($participants, $clients_ids);
        $clients = array_values($clients);
        return Response()->data(compact('participants', 'clients'))->success();
    }
}