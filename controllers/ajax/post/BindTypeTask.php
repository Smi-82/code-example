<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Task;

class BindTypeTask extends BaseAjaxController
{
    public function run()
    {
        $res = Task::updateTasksData($_POST['task_id'], ['collaborator_type_id' => $_POST['collaborator_type']]);
        return $res ? Response()->success() : Response()->error();
    }
}