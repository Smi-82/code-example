<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class DeleteBdcInvBySuperAdmin extends BaseAjaxController
{
    public function run()
    {
        if (!User()->isSuperAdmin()) {
            return Response()->error('You have no rights');
        }
        switch ($_POST['class_key']) {
            case 'bdc':
                $res = BillProject()->delete($_POST['id'], $_POST['project_id']);
                break;
            case 'inv':
                $res = InvoiceProject()->delete($_POST['id'], $_POST['project_id']);
                break;
        }
        return Response()->data($res)->success();
    }
}