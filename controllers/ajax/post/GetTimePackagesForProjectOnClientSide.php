<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use TimePackagesProject;

class GetTimePackagesForProjectOnClientSide extends BaseAjaxController
{
    public function run()
    {
        $res = TimePackagesProject::getStatisticsByProjectId($_POST['project']);
        return Response()->data($res)->success();
    }
}