<?php


namespace controllers\ajax\post;


use Category;
use controllers\ajax\BaseAjaxController;
use Exception;
use Project;

class GetCategoriesForProjects extends BaseAjaxController
{
    public function run()
    {
        try {
            $category = Category::getCategoriesByEntity(Project::getTable());
            return Response()->data($category)->success();
        } catch (Exception $e) {
            return Response()->error($e->getMessage());
        }
    }
}