<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class AddNewGroup extends BaseAjaxController
{
    public function run()
    {
        $res = Group()->addGroup([
            'parent_id' => $_POST['parent_group_id'],
            'name' => $_POST['new_group_name'],
            'general_project_id' => $_POST['general_project_id'],
            'user_id' => User()->getId()
        ]);
        return is_numeric($res) ? Response()->success() : Response()->error(mb_ucfirst(lng('error')) . ': Group was not saved!');
    }
}