<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Response;

class GetDocsDataForJS extends BaseAjaxController
{
    public function run()
    {
        $res = User()->getDocsDataForJS($_POST['user_id'], $_POST['doc']);
        return is_array($res) || empty($res) ? Response()->data($res)->success() : Response()->error($res);
    }
}