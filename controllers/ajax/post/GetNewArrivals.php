<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class GetNewArrivals extends BaseAjaxController
{
    public function run()
    {
        $count = clear_post($_POST['count']);
        $newArrivals = User()->getNewArrivals($count);
        $nameColumn = 'name_' . Language()->getLanguage();

        $newArrivalsPositionTitles = arrayToAssocByKey(PositionTitle()->getById(array_unique(array_column($newArrivals, 'position_title_id'))), 'id');
        $newArrivalsPositionCategories = arrayToAssocByKey(PositionCategory()->getById(array_unique(array_column($newArrivals, 'position_category_id'))), 'id');
        $newArrivalsPositionSpecialities = arrayToAssocByKey(PositionSpeciality()->getById(array_unique(array_column($newArrivals, 'position_speciality_id'))), 'id');

        $newArrivals = array_map(function ($item) use ($nameColumn, $newArrivalsPositionTitles, $newArrivalsPositionCategories, $newArrivalsPositionSpecialities) {
            $item['position_title_name'] = @$newArrivalsPositionTitles[$item['position_title_id']][$nameColumn];
            $item['position_category_name'] = @$newArrivalsPositionCategories[$item['position_category_id']][$nameColumn];
            $item['position_speciality_name'] = @$newArrivalsPositionSpecialities[$item['position_speciality_id']][$nameColumn];
            $item['img'] = getImg($item, $item['id']);
            return $item;
        }, $newArrivals);

        return Response()->data($newArrivals)->success();
    }
}