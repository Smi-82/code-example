<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Exception;
use models\UserModel;
use User;

class GetMastersByPaymentSystem extends BaseAjaxController
{
    public function run()
    {
        $sql = UserModel::baseSelect(['id']) . ' WHERE usertype = ' . UserTypes()->getRGPDMaster() . ' AND payment_system_id = ' . clear_post($_POST['payment_id']);
        try {
            $res = UserModel::query($sql);
            return !empty($res) ? Response()->data(array_map(function ($elem) {
                return $elem['id'];
            }, $res))->success() : Response()->data([])->success();
        } catch (Exception $e) {
            return Response()->error($e->getMessage());
        }
    }
}