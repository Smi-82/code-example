<?php


namespace controllers\ajax\post;


use Category;
use controllers\ajax\BaseAjaxController;
use TaskAccess;

class CreateOrClonMassTask extends BaseAjaxController
{
    public function run()
    {
        $data = $_POST['data'];
        foreach ($data as $val) {
            if (intval($val['group_id']) == 0) {
                $val['group_id'] = Group()->addGroup(['general_project_id' => $val['project'], 'name' => $val['group_name']]);
            }
            if (intval($val['category_id']) == 0 && isset($val['cat_name']) && !empty(trim($val['cat_name']))) {
                $res = Category::add(['name' => $val['cat_name'], 'entity' => [$val['cat_entity']]]);
                $val['category_id'] = (!is_bool($res) && intval($res) != 0) ? $res : Category::getBaseCategory();
            }
            if (intval($val['task_id']) == 0) {
                $task_id = Task()->addNewTask([
                    'name' => $val['task_name'],
                    'category' => intval($val['category_id']) ? intval($val['category_id']) : Category::getBaseCategory(),
                    'estimate' => 0,
                    'estimateTime' => date("Y-m-d H:i:s", strtotime("+1 month")),
                    'for_cloning' => $val['for_cloning'],
                    'group_id' => $val['group_id'],
                    'description' => $val['task_description'],
                    'priority' => $val['priority'],
                    'start' => date('Y-m-d H:i:s', strtotime($val['date_start'])),
                    'end' => date('Y-m-d H:i:s', strtotime($val['date_end'])),
                ]);
            }
            else {
                $task_id = Task()->cloneTask(intval($val['task_id']), [
                    'name' => $val['task_name'],
                    'group_id' => $val['group_id'],
                    'description' => $val['task_description'],
                    'priority' => $val['priority'],
                    'start' => date('Y-m-d H:i:s', strtotime($val['date_start'])),
                    'end' => date('Y-m-d H:i:s', strtotime($val['date_end'])),

                ]);
            }
            $task_id = empty($task_id['errors']) ? $task_id['responce']['task'] : $task_id['errors'];
            if (!empty($task_id['errors'])) {
                return Response()->data($task_id['errors'])->error();
            }
            foreach ($val['users'] as $u_id)
                TaskAccess::add($task_id, intval($u_id));
        }
        return Response()->data($task_id)->success();
    }
}