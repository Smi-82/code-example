<?php


namespace controllers\ajax\post;


use appEvents\AppEventsController;
use appEvents\subscribers\controller\AppEventsSubscribersController;
use controllers\ajax\BaseAjaxController;
use Exception;
use Folder;
use UploadDir;

class UploadTaskMessageFiles extends BaseAjaxController
{
    public function run()
    {
        $upload_dir = UploadDir::getRootPath() . 'messages/projects/';
        $upload_dir_for_U2df = UploadDir::getRootPath() . 'messages/';
        Folder::create(UploadDir::getRootPath() . 'messages/projects');
        Folder::create($upload_dir . date("Y"));

        $folder = date('H-i-s', time());
        Folder::create($upload_dir . date("Y") . '/' . date("Y") . '-' . date("m") . '-' . date("d"));
        Folder::create($upload_dir . date("Y") . '/' . date("Y") . '-' . date("m") . '-' . date("d") . '/' . $folder);

        $output_dir = 'projects/' . date("Y") . '/' . date("Y") . '-' . date("m") . '-' . date("d") . '/' . $folder . '/';
        $allowedTypes = 'jpg,jpeg,png,gif,doc,docx,xls,xlsx,csv,odt,pdf,zip,rar,psd,mp3,mp4,3gp,wma,ogg,wav,webm,mpeg,acc,txt';
//$allowedTypes = 'qwe';
        $allowedTypes = explode(',', $allowedTypes);
        $fl = false;
        if (isset($_FILES["myfile"])) {

            $result = array();

            $result['output_dir'] = $output_dir;
            $error = $_FILES["myfile"]["error"];
            //You need to handle  both cases
            //If Any browser does not support serializing of multiple files using FormData()
            if (!is_array($_FILES["myfile"]["name"])) //single file
            {
                $extension = pathinfo($_FILES["myfile"]["name"], PATHINFO_EXTENSION);
                if (!in_array($extension, $allowedTypes)) {
                    throw new Exception('this extension is not allowed');
//            echo json_encode('qwe', JSON_UNESCAPED_UNICODE);
                    exit();
                }

                $fileName = $_FILES["myfile"]["name"];
                $filesize = $_FILES["myfile"]["size"];

                move_uploaded_file($_FILES["myfile"]["tmp_name"], $upload_dir_for_U2df . $output_dir . $fileName);

                $query_str = insert_query_string('tasks_messages_files',
                    array(
                        'output_dir' => $output_dir,
                        'filename' => $fileName,
                        'filesize' => $filesize,
                        'task_id' => $_POST['task_id'],
                        'added_by_user' => $_POST['user_id'],
                    )
                );

                if (query($query_str)) {
                    $lastID = getLastId();
                }


            }
            else  //Multiple files, file[]
            {
                $fileCount = count($_FILES["myfile"]["name"]);
                for ($i = 0; $i < $fileCount; $i++) {
                    $extension = pathinfo($_FILES["myfile"]["name"][$i], PATHINFO_EXTENSION);
                    if (!in_array($extension, $allowedTypes)) {
                        $fl = true;
                        continue;
                    }
                    $fileName = $_FILES["myfile"]["name"][$i];
                    $filesize = $_FILES["myfile"]["size"][$i];
                    move_uploaded_file($_FILES["myfile"]["tmp_name"][$i], $upload_dir_for_U2df . $output_dir . $fileName);
                    $query_str = insert_query_string('tasks_messages_files',
                        array(
                            'output_dir' => $output_dir,
                            'filename' => $fileName,
                            'filesize' => $filesize,
                            'task_id' => $_POST['task_id'],
                            'added_by_user' => $_POST['user_id'],
                        )
                    );
                    if (query($query_str)) {
                        $lastID = getLastId();
                    }
                }
            }
            if ($fl)
                throw new Exception('this extension is not allowed');
            $ext = pathinfo($output_dir . $fileName, PATHINFO_EXTENSION);
            switch (strtolower($ext)) {
                case 'jpg':
                case 'jpeg':
                case 'png':
                case 'gif':
                    $fileimg = 1;
                    break;
                case 'doc':
                case 'docx':
                case 'odt':
                case 'xls':
                case 'xlsx':
                case 'csv':
                case 'pdf':
                case 'zip':
                case 'rar':
                case 'psd':
                    $fileimg = 0;
                    break;
                default:
                    $fileimg = 0;
                    break;
            }
            $result['id'] = $lastID;
            $result['if_img'] = $fileimg;
            $result['filesize'] = $filesize;
            $result['filesize_formatted'] = formatBytes($filesize);
            if ($fileimg == 1) {
                $msg = '<div><a href="/' . MODULE . UploadDir::getRelativePath() . 'messages/' . $output_dir . $fileName . '" target="_blank">Attached image<br><img src="/getImage?w=200&t=udb/upload/messages/projects&fromfile=udb/upload/messages/' . $output_dir . $fileName . '" alt="message user image"></a></div>';
            }
            else {
                $msg = '<div><a href="/' . MODULE . UploadDir::getRelativePath() . 'messages/' . $output_dir . $fileName . '" download="' . $fileName . '">Attached file</a></div>';
            }
            $query_str = insert_query_string(Tas,
                array(
                    'user_id' => User()->getId(),
                    'sentat' => date('Y-m-d'),
                    'name' => User()->getSignature(),
                    'message' => $msg,
                    'task_id' => $_POST['task_id'],
                    'file_id' => $lastID,
                )
            );
            query($query_str);

            AppEventsController::publish(AppEventsSubscribersController::taskFileEvent(), ['task_id' => $_POST['task_id']]);
            $result['tasks_messages_last_id'] = getLastId();
            $result['ext'] = $ext;
            $result['who_add'] = User()->getSignature();
            $result['user_img'] = getImg(User()->getImg(), User()->getId());
            $result['time'] = date('d-m-Y H:i:s');

            $query_str = insert_query_string('tasks_history',
                array(
                    'user_id' => User()->getId(),
                    'task_id' => $_POST["task_id"],
                    'file_id' => $lastID,
                    'description' => '<i class="font-icon font-icon-page"></i> <a href="/' . MODULE . UploadDir::getRelativePath() . 'messages"' . $output_dir . $fileName . '" target="_blank">' . $fileName . '</a>',
                )
            );
            query($query_str);
            $result['tasks_history_last_id'] = getLastId();

            history('Upload files', User()->getSignature() . ' uploaded file: <a href="' . UploadDir::getRootPath() . 'messages/' . $output_dir . $fileName . '" download="' . $fileName . '">' . $fileName . '</a>');
            return json_encode($result, JSON_UNESCAPED_UNICODE);
        }

    }
}