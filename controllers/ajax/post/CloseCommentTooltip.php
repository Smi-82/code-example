<?php


namespace controllers\ajax\post;



use controllers\ajax\BaseAjaxController;
use Task;

class CloseCommentTooltip extends BaseAjaxController
{
    public function run()
    {
        if ($_POST['section_target'] == '' || $_POST['task_id'] == '') {
            return Response()->error(mb_ucfirst(lng('error')) . ': ' . mb_ucfirst(lng('missed_required_fields')));
        }
        $section = clear_post($_POST['section_target']);
        $task_id = clear_post($_POST['task_id']);

        if (Task::updateSectionComment($task_id, $section, false)) {
            history('task', User()->getSignature() . ' closed message for "' . $section . '" cell. Task ID : ' . $task_id);
            returnResponse()->success();
        }
        else {
            return Response()->error(mb_ucfirst(lng('error_occured')) . ' ' . lng('during_insert_data_in_database'));
        }
    }
}