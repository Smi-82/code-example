<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use PaymentPartsProjectBill;
use TimePackagesProject;

class SetPaidTimePackage extends BaseAjaxController
{
    public function run()
    {
        $res = 1;
        if ($res && is_numeric($res)) {
            $res = PaymentPartsProjectBill::setPaid($_POST['pp_id']);
            $res = TimePackagesProject::setPaid($_POST['package_id']);
//            $bill_id = PaymentPartsProjectBill::getById($_POST['pp_id'], ['project_bill_id']);
//            $bill_id = $bill_id['project_bill_id'];
//            $tasks = BillProject::getById($bill_id, ['tasks']);
//            $tasks = explode(',', $tasks['tasks']);
//            $res = TimePackagesProject::updateTimeSpent($_POST['package_id'], Task::getTimeSpentWithUsers($tasks));
        }

        return is_bool($res) && $res ? Response()->success() : Response()->error($res);
    }
}