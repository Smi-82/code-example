<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class SetJsData extends BaseAjaxController
{
    public function run()
    {
        JsData()->set(clear_post($_POST['JsData']));
    }
}