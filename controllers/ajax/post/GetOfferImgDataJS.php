<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class GetOfferImgDataJS extends BaseAjaxController
{
    public function run()
    {
        return Project()->getOfferImgDataJS($_POST['id']);
    }
}