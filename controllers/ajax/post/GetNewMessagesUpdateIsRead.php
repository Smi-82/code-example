<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use TasksMessages;

class GetNewMessagesUpdateIsRead extends BaseAjaxController
{
    public function run()
    {
        if ($_POST['id'] == '' || $_POST['user_id'] == '') {
            return Response()->error();
        }
        $update_note_exe = TasksMessages::setRead($_POST['id']);
        if ($update_note_exe === false) {
            return Response()->error();
        }
        else {
            return Response()->success();
        }
    }
}