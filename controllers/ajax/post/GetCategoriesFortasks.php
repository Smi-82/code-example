<?php


namespace controllers\ajax\post;


use Category;
use controllers\ajax\BaseAjaxController;
use Task;

class GetCategoriesFortasks extends BaseAjaxController
{
    public function run()
    {
        $res = Category::getCategoriesByEntity(Task::getTable());
        return is_array($res) ? Response()->data($res)->success() : Response()->error($res);
    }
}