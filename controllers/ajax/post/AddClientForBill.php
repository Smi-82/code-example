<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use MessengerEventCodes;

class AddClientForBill extends BaseAjaxController
{
    public function run()
    {
        $user = Project()->addClient($_POST['project_id'], $_POST['user_id']);
        MessengerEvents()->send(MessengerEventCodes::PROJECT_CREATED, ['project_id' => $_POST['project_id']]);
        return Response()->data($user)->success();
    }
}