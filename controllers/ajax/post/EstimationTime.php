<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Task;

class EstimationTime extends BaseAjaxController
{
    public function run()
    {
        $days = escape_string(clear_post($_POST["days"]));
        $hours = escape_string(clear_post($_POST["hours"]));
        $minutes = escape_string(clear_post($_POST["minutes"]));
        $task_id = escape_string(clear_post($_POST["task_id"]));

        $seconds = daysHoursMinutesToSeconds($days, $hours, $minutes);
        $result = Task::getById($task_id, ['start', 'estimateTime', 'estimate']);
        if ((strtotime($result['start']) + $seconds + (($seconds / 8) / 3600) * 57600) > strtotime($result['estimateTime'])) {
            $obj = secondsToWorkingTime((strtotime($result['estimateTime']) - strtotime($result['start'])) / 3);
            return Response()->error(mb_ucfirst(lng('the_interval_is_longer_than_the_scheduled_time')) . ' ' . $obj['d'] . ' ' . mb_ucfirst(lng('days')) . ', ' . $obj['h'] . ' ' . mb_ucfirst(lng('hours')) . ', ' . $obj['m'] . mb_ucfirst(lng('minutes')) . ' !');
        }
        if (Task::updateTasksData($task_id, ['estimate' => $seconds, 'price_type' => $seconds ? 1 : 0])) {
            taskHistory($task_id, 'Change estimation');
            history('task', User()->getSignature() . ' changed estimation time to: ' . showWorkingDate($seconds) . ' - task ID: ' . $task_id);
            return Response()->data($seconds ? 1 : 0)->success(mb_ucfirst(lng('estimation_time_added_successfully')));
        }
        else {
            return Response()->error(mb_ucfirst(lng('error_occured_during_insert_estimation_time')));
        }
    }
}