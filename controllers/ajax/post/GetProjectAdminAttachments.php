<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class GetProjectAdminAttachments extends BaseAjaxController
{
    public function run()
    {
        $data = array_map(function ($item) {
            $item['src'] = Project()->getClientAttachmentUrl($item['project_id'], $item['user_id'], $item['filename']);
            $item['filesize_f'] = formatBytes($item['filesize'], 0);
            return $item;
        }, Project()->getAdminAttachments($_POST['project_id']));

        $data = array_values(array_filter($data));

        return Response()->data($data)->success();
    }
}