<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Exception;
use TasksMessages;
use User;

class GetNewMessages extends BaseAjaxController
{
    public function run()
    {
        if ($_POST['user_id'] == '' || $_POST['lastMessage'] == '' || $_POST['reply_of'] == '') {
            return Response()->error(mb_ucfirst(lng('missed_required_data')));
        }
        $table = TasksMessages::getTable();
        if (isset($_POST['from']) && $_POST['from'] == 'task') {
            $query = 'SELECT tm.*,u.img FROM ' . $table . ' AS tm LEFT JOIN ' . User::getTable() . ' AS u ON u.id = tm.user_id WHERE tm.user_id <>' . clear_post($_POST['user_id']) . ' AND tm.task_id=' . clear_post($_POST['task_id']) . ' AND tm.id >' . clear_post($_POST['lastMessage']);
        }
        else {
            $query = TasksMessages::baseSelect() . $table . ' WHERE reply_of=' . clear_post($_POST['reply_of']) . ' and id>' . clear_post($_POST['lastMessage']);
        }

        try {
            $exeQuery = query($query);
        } catch (Exception $e) {
            return $e->getMessage();
        }
        if (!$exeQuery) {
            $json = Response()->error();
        }
        else {
            if (!empty($exeQuery)) {
                foreach ($exeQuery as &$data)
                    $data['img'] = getImg($data, $data['user_id']);
                $json = Response()->data($exeQuery)->success();
            }
            else {
                $json = Response()->error(mb_ucfirst(lng('missed_required_data')));
            }
        }
        return $json;
    }
}