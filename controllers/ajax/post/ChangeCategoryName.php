<?php


namespace controllers\ajax\post;


use Category;
use controllers\ajax\BaseAjaxController;

class ChangeCategoryName extends BaseAjaxController
{
    public function run()
    {
        $res = Category::update($_POST['id'], ['name' => $_POST['name']]);
        return is_bool($res) && $res ? Response()->success() : Response()->error($res);
    }
}