<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class CloneGroupWithTasks extends BaseAjaxController
{
    public function run()
    {
        Group()->cloneGroup($_POST['group'], true);
        return Response()->success();
    }
}