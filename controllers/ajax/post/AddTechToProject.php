<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class AddTechToProject extends BaseAjaxController
{
    public function run()
    {
        $current = Project()->getById($_POST['project'], ['languages']);
        $current = $current['languages'];
        if (!empty($current) && $current != 'null') {
            $current = json_decode($current);
        }
        else
            $current = [];

        $current = array_merge($current, $_POST['tech']);
        $current = json_encode($current);
        Project()->editProjectData($_POST['project'], ['languages' => $current]);
        return Response()->success();
    }
}