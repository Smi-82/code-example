<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use TaskAccess;
use TaskAlert;

class TaskCompletionStatusUpdate extends BaseAjaxController
{
    public function run()
    {
        $status = escape_string($_POST['status']);
        $userid = User()->getId();
        $task_id = escape_string($_POST['task_id']);
        if (TaskAccess::updateByTaskAndUserIds(['completion_status' => $status], $userid, $task_id)) {
            history('task', User()->getSignature() . ' updated his status on the task ID: ' . $task_id . ' to "' . Statuses()->getStatusesById($status, 'aliase') . '"');
            $res = TaskAlert::deleteByUserTaskType($userid, $task_id, 1);
            return $res ? Response()->success() : Response()->error();
        }
        else
            return Response()->error();
    }
}