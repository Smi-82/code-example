<?php


namespace controllers\ajax\post;


use ConsultantHierarchePage;
use controllers\ajax\BaseAjaxController;

class GetConsultants extends BaseAjaxController
{
    public function run()
    {
        $res = new ConsultantHierarchePage();
        return Response()->data($res->getTableData())->success();
    }
}