<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class GetTaskAttachments extends BaseAjaxController
{
    public function run()
    {
        $data = array_map(function ($item) {
            $item['src'] = Task()->getAttachmentUrl($item['output_dir'], $item['filename']);
            $item['filesize_f'] = formatBytes($item['filesize'], 0);

            $item['ext'] = getFileExtension($item['filename']);
            return $item;
        }, Task()->getAttachments($_POST['task_id']));
        $data = array_values(array_filter($data));
        return Response()->data($data)->success();
    }
}