<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class AddTaskClientComment extends BaseAjaxController
{
    public function run()
    {
        return Response()->data(Task()->addClientMessage(clear_post($_POST['task_id']), clear_post($_POST['user_id']), $_POST['desc']))->success();
    }
}