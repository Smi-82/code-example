<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use UserTypes;

class AddEditType extends BaseAjaxController
{
    public function run()
    {
        return UserTypes::addType() ? Response()->success() : Response()->error();
    }
}