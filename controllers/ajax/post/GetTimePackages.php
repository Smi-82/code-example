<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use TimePackagesProject;

class GetTimePackages extends BaseAjaxController
{
    public function run()
    {
        $res = TimePackagesProject::getByProjectIdFullData($_POST['project']);
        return is_array($res) ? Response()->data($res)->success() : Response()->error($res);
    }
}