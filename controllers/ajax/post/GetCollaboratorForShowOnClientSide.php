<?php

namespace controllers\ajax\post;

use ClientUsers;
use controllers\ajax\BaseAjaxController;
use models\ClientUsersModel;
use User;

class GetCollaboratorForShowOnClientSide extends BaseAjaxController
{
    public function run()
    {
        $user = [];
        $show = User::checkToShowPopup(User()->getId());
        if ($show) {
            $user = ClientUsersModel::getCurrentByClientId(User()->getId(), ['user_id']);
            $user = User::getById($user['user_id'], ['id', 'firstname', 'lastname', 'motto', 'img']);
            $user['img'] = getImg($user, $user['id'], ['maxw' => 200, 'maxh' => 200]);
        }
        return Response()->data(compact('show', 'user'))->success();
    }

    public function removeBasicByClientId()
    {
        $res = ClientUsers::removeBasicByClientId(User()->getId());
        return $res ? Response()->success() : Response()->error($res);
    }
}