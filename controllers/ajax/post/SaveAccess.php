<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Task;

class SaveAccess extends BaseAjaxController
{
    public function run()
    {
        Task::updateTasksData($_POST['task_id'], ['system_access' => $_POST['access']]);
        taskHistory($_POST['task_id'], 'Changed access');
        history('task', User()->getSignature() . ' added access information to the taks ID: ' . clear_post($_POST["task_id"]));
        if (isset($_POST['unset_section']))
            Task::updateSectionComment(clear_post($_POST['task_id']), clear_post($_POST['unset_section']), false);
        return Response()->success();
    }
}