<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class ApproveBill extends BaseAjaxController
{
    public function run()
    {
        $res = BillProject()->approve($_POST['bill']);
        return $res ? Response()->data($res)->success() : Response()->error();
    }
}