<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class EditTaskInsideComment extends BaseAjaxController
{
    public function run()
    {
        return Response()->data(Task()->editInsideMessage(clear_post($_POST['task_id']), clear_post($_POST['message_id']), $_POST['desc']))->success();
    }
}