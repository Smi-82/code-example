<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class DeleteTaskMessage extends BaseAjaxController
{
    public function run()
    {
        return Response()->data(Task()->deleteClientMessage(clear_post($_POST['msg_id'])))->success();
    }
}