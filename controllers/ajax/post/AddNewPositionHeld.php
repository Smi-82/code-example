<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use PositionHeld;

class AddNewPositionHeld extends BaseAjaxController
{
    public function run()
    {
        $res = PositionHeld::create($_POST['name']);
        return !is_bool($res) && intval($res) != 0 ? Response()->data($res)->success() : Response()->error($res);
    }
}