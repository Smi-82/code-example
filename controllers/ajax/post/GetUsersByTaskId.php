<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class GetUsersByTaskId extends BaseAjaxController
{
    public function run()
    {
        $res = Task()->getOwnParticipants($_POST['task'], ['id', 'firstname', 'lastname', 'img']);
        $res = array_map(function ($elem) {
            $elem['img'] = getImg($elem, $elem['id']);
            return $elem;
        }, $res);
        usort($res, function ($a, $b) {
            $a = trim($a['firstname'] . ' ' . $a['lastname']);
            $b = trim($b['firstname'] . ' ' . $b['lastname']);
            if ($a == $b) {
                return 0;
            }
            return ($a < $b) ? -1 : 1;
        });
        return Response()->data($res)->success();
    }
}