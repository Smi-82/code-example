<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class DeleteProjectPackage extends BaseAjaxController
{
    public function run()
    {
        $res = TimePackagesProject::delete($_POST['id']);
        return $res ? Response()->success() : Response()->error($res);
    }
}