<?php


namespace controllers\ajax\post;


use Category;
use controllers\ajax\BaseAjaxController;

class GetAllCategoriesWithEntity extends BaseAjaxController
{
    public function run()
    {
        $res = Category::getAllCategoriesWithEntity();
        return is_array($res) ? Response()->data($res)->success() : Response()->error($res);
    }
}