<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class ChangeProjectName extends BaseAjaxController
{
    public function run()
    {
        $res = Project()->editProjectData($_POST['id'], ['name' => $_POST['name']]);
        return empty($res['error']) ? Response()->success() : Response()->error($res['msg']);
    }
}