<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Exception;
use GloodaAPI;
use UploadDir;
use User;

class AddUser extends BaseAjaxController
{
    public function run()
    {
        if (!isset($_POST['data'])) {
            $data = $_POST;
            unset($data['key']);
        }
        elseif (isset($_POST['data']) && !is_array($_POST['data'])) {
            parse_str($_POST['data'], $data);
        }
        else {
            $data = $_POST['data'];
        }
        require_once($_SERVER['DOCUMENT_ROOT'] . DS . MODULE . DS . 'messenger_tm/glooda_api.php');
        $ga = new GloodaAPI(
            'registration',
            array(
                'login' => $data['login'],
                'pass' => $data['password'],
                're_pass' => $data['re_password'],
                'email' => $data['email']
            )
        );

        if ($ga->registration_OK) {
            $global_id = $ga->get_ID();
            $data['global_id'] = $global_id;
            /*glres = array(
            'TM_token' => $ga->get_accessToken(),
            'TM_global_id' => $global_id,
            'TM_profileid' => $ga->get_profileid(),
            'TM_login' => $data['login']
        );*/
            $res = User()->addUser($data);
            return empty($res['errors']) ? Response()->data($res['user'])->success() : Response()->error($res['errors']);

        }
        else {
            $res = $ga->getErrorsString();
            return Response()->error($res);
        }


        if ($data['firstname'] == '' || $data['lastname'] == '' || $data['email'] == '' || $data['login'] == '' || $data['usertype'] == '') {
            return Response()->error(mb_ucfirst(lng('fill_in_required_fields')));
        }
        if (!email_is_unique($data['email'])) {
            return Response()->error(mb_ucfirst(lng('this_login_is_already_taken')));
        }

        if ($data['usertype'] == '') {
            return Response()->error('Not valid type');
        }
        if ($data['password'] != '') {
            if ($data['password'] != $data['re_password']) {
                return Response()->error(mb_ucfirst(lng('wrong_repeat_password')));
            }
            $data['password'] = Login()->encrypt_password($data['password']);
        }
        else {
            $data['password'] = Login()->encrypt_password('1234');
        }
        unset($data['re_password']);
        $data['img'] = $_SESSION['mainImage'];

        $sql = insert_query_string(User::getTable(), $data);

        try {
            if (query($sql)) {
                $id = getLastId();
                $path = UploadDir::getRootPath() . User::getTable() . '/';
                if (file_exists($path . $_SESSION['temporary_folder'])) {
                    rename($path . $_SESSION['temporary_folder'], $path . $id);
                    unset($_SESSION['temporary_folder']);
                    unset($_SESSION['mainImage']);
                }
                history('Utilisateurs', User()->getSignature() . ' added new utilisateur: ' . clear_post($data['firstname']) . ' ' . clear_post($data['lastname']));
                return Response()->success(mb_ucfirst(lng('success')));
            }
            else
                return Response()->error('User was not added!');
        } catch (Exception $e) {
            return Response()->error($e->getMessage());
        }
    }
}