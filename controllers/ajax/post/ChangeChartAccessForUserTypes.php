<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use UserTypes;

class ChangeChartAccessForUserTypes extends BaseAjaxController
{
    public function run()
    {
        return UserTypes::changeChatAccess() ? Response()->success() : Response()->error();
    }
}