<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Task;

class GetNewCommentsFromAssistants extends BaseAjaxController
{
    public function run()
    {
        if ($_POST['task_id'] == '') {
            return Response()->error(mb_ucfirst(lng('error')) . ": " . lng('missed_required_fields'));
        }

        $task_id = clear_post($_POST["task_id"]);
        $section_comments = Task::getById($task_id, ['section_comments']);
        $section_comments = $section_comments['section_comments'];
        $section_comments = unserialize($section_comments);

        if (!empty($section_comments)) {
            return Response()->data($section_comments)->success();
        }
        else {
            return Response()->error(mb_ucfirst(lng('empty_data_from_database')));
        }
    }
}