<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Task;

class FreeTask extends BaseAjaxController
{
    public function run()
    {
        $res = Task::userNotOnTask(User()->getId(), $_POST['task_id']);
        return $res === false ? Response()->error($e->getMessage()) : Response()->success();
    }
}