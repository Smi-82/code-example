<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class DeleteUserphoto extends BaseAjaxController
{
    public function run()
    {
        if (isset($_POST['op']) && $_POST['op'] == 'delete' && isset($_POST['name'])) {
            return User()->deleteFoto($_POST['user_id']);
        }
        return false;
    }
}