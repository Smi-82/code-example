<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Project;
use TaskAccess;

class CloneTask extends BaseAjaxController
{
    public function run()
    {
        $def_group = Project::getDefaultGroup($_POST['project'], ['id']);
        $def_group = array_column($def_group, 'id')[0];
        $taskObj = Task();
        $task = $taskObj->cloneTask($_POST['task'], ['group_id' => $def_group]);
        if (!empty($task['errors'])) {
            return Response()->data($task['errors'])->error();
        }
        $task = $task['responce']['task'];
        if (intval($_POST['withUsers'])) {
            $users = $taskObj->getOwnParticipants($_POST['task'], ['id']);
            if ($users) {
                $users = array_column($users, 'id');
                foreach ($users as $u) {
                    TaskAccess::add($task, $u);
                }
            }
        }
        return Response()->success();
    }
}