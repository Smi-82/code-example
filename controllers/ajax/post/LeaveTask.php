<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Task;

class LeaveTask extends BaseAjaxController
{
    public function run()
    {
        return Task::userNotOnTask(User()->getId(), $_POST['task']);
    }
}