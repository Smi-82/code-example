<?php


namespace controllers\ajax\post;


use appEvents\AppEventsController;
use appEvents\subscribers\controller\AppEventsSubscribersController;
use controllers\ajax\BaseAjaxController;
use Exception;
use TaskAccess;

class UpdateEmployeAccess extends BaseAjaxController
{
    public function run()
    {
        $for_delete_arr = $_POST['for_delete_arr'];
        $for_add_arr = $_POST['for_add_arr'];
        if (!empty($for_delete_arr)) {
            try {
                foreach ($for_delete_arr as $task_id)
                    TaskAccess::delete($task_id, $_POST['user_id']);
            } catch (Exception $e) {
                return Response()->error($e->getMessage());
            }
            AppEventsController::publish(AppEventsSubscribersController::deleteUserFromTaskEvent(), ['tasks_id' => $for_delete_arr, 'user_id' => $_POST['user_id']]);
        }
        if (!empty($for_add_arr)) {
            foreach ($for_add_arr as $task_id) {
                TaskAccess::add($task_id, $_POST['user_id']);
            }
        }
        return Response()->success();
    }
}