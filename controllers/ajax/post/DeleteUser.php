<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class DeleteUser extends BaseAjaxController
{
    public function run()
    {
        return User()->delete($_POST['id']) ? Response()->success() : Response()->error();
    }
}