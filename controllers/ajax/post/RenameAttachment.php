<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class RenameAttachment extends BaseAjaxController
{
    public function run()
    {
        $res = Task()->renameAttachment($_POST['id'], $_POST['filename']);
        return $res !== false ? Response()->data()->success() : Response()->error();
    }
}