<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use LoginContracts;
use User;

class GetLoginContracts extends BaseAjaxController
{
    public function run()
    {
        $user_data = User::getById($_POST['user']);
        $obj = new LoginContracts();
        $docs = $obj->getContractsData($user_data);
        foreach ($docs as $key => $val)
            $docs[$key] = $val['text'];
        return Response()->data($docs)->success();
    }
}