<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Exception;
use TasksMessages;
use User;

class GetTaskNotes extends BaseAjaxController
{
    public function run()
    {
        $sql = 'SELECT u.img, tm.user_id, tm.id, tm.changeat AS created, IF(tm.sentat<tm.changeat,1,0) AS changed, tm.message AS text, IF(tm.user_id = "' . User()->getId() . '",1,0) AS editAction, TRIM(CONCAT(u.firstname," ", UPPER(u.lastname))) AS user FROM ' . TasksMessages::getTable() . ' AS tm LEFT JOIN ' . User::getTable() . ' AS u ON u.id=tm.user_id WHERE tm.file_id=0 AND tm.task_id=' . escape_string(clear_post($_POST['task_id'])) . ' ORDER BY id DESC';
        try {
            $res = query($sql);
        } catch (Exception $e) {
            return Response()->error($e->getMessage());
        }
        if (!empty($res))
            $res = array_map(function ($elem) {
                $elem['img'] = getImg($elem, $elem['user_id']);
                unset($elem['user_id']);
                $elem['text'] = htmlspecialchars_decode($elem['text']);
                $elem['created'] = date('d-m-Y, H:i', strtotime($elem['created']));
                return $elem;
            }, $res);
        return Response()->data($res)->success();
    }
}