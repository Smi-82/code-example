<?php

namespace controllers\ajax\post;

use ClientUsers;
use controllers\ajax\BaseAjaxController;

class SetBasicAndDurationUserForClientOnAdmSide extends BaseAjaxController
{
    public function run()
    {
        $res = ClientUsers::setAsBasic($_POST['id'], $_POST['is_basic']);
        $res = ClientUsers::setDuration($_POST['id'], $_POST['duration']);
        return $res ? Response()->data($res)->success() : Response()->error($res);
    }
}