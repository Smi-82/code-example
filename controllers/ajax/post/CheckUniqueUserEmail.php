<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use User;

class CheckUniqueUserEmail extends BaseAjaxController
{
    public function run()
    {
        if ($_POST['email'] == '') {
            return Response()->error();
        }
        if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) // Validate email address
        {
            return Response()->error(mb_ucfirst(lng('enter_valid_email')));
        }

        $email = clear_post($_POST['email']);
        $checkOrExistRow = User::getUserByEmail($email, ['email']);
        if (!empty($checkOrExistRow)) {
            $rjson = Response()->error(mb_ucfirst(lng('this_email_is_already_registered')) . '. ' . mb_ucfirst(lng('please_insert_another_email')) . '.');
        }
        else {
            $rjson = Response()->success();
        }
        return $rjson;
    }
}