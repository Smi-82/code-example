<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use TasksMessagesFiles;
use TasksMessages;
use TaskHistory;
use UploadDir;

class DeleteProjectUploadedFile extends BaseAjaxController
{
    public function run()
    {
        if (isset($_POST['op']) && $_POST['op'] == 'delete' && isset($_POST['name'])) {

            $upload_dir = UploadDir::getRootPath() . 'messages/';
            $todel = TasksMessagesFiles::getById($_POST['name']);
            $path = $upload_dir . $todel['output_dir'] . $todel['filename'];
            if (file_exists($path)) {
                UploadDir::delete('messages/' . $todel['output_dir']);
            }
            $arr = array(
                'TasksMessagesFiles' => clear_post($_POST['name']),
                'TasksMessages' => clear_post($_POST['tasks_messages_last_id']),
                'TaskHistory' => clear_post($_POST['tasks_history_last_id'])
            );
            foreach ($arr as $class => $id)
                $class::delete($id);
            history('Upload files', User()->getSignature() . ' deleted file: ' . $todel['filename']);
        }
    }
}