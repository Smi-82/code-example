<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use PositionHeld;

class ChangePositionHeldName extends BaseAjaxController
{
    public function run()
    {
        $res = PositionHeld::update($_POST['id'], ['name' => $_POST['name']]);
        return is_bool($res) && $res ? Response()->success() : Response()->error($res);
    }
}