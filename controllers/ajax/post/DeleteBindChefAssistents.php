<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class DeleteBindChefAssistents extends BaseAjaxController
{
    public function run()
    {
        Project()->deleteAccess($_POST['project'], $_POST['users']);
        return Response()->success();
    }
}