<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use UsersStatisticPage;

class GetUserStatisticsDetails extends BaseAjaxController
{
    public function run()
    {
        $res = UsersStatisticPage::getUserStatisticsDetails($_POST['id']);
        return Response()->data($res)->success();
    }
}