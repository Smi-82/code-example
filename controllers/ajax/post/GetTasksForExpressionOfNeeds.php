<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Project;

class GetTasksForExpressionOfNeeds extends BaseAjaxController
{
    public function run()
    {
        $result = Project::getOwnTasks($_POST['project_id']);
        return $result ? Response()->data($result)->success() : Response()->error();
    }
}