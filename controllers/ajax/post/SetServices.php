<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class SetServices extends BaseAjaxController
{
    public function run()
    {
        unset($_POST['service']['services']['old']);
        $services_for_update = isset($_POST['service']['services']['for_update']) ? $_POST['service']['services']['for_update'] : [];
        $services_new = isset($_POST['service']['services']['new']) ? $_POST['service']['services']['new'] : [];
        $dossier_id = $_POST['service']['dossier'];
        $total_discount = $_POST['service']['total_discount'];
        $total_comment = $_POST['service']['total_comment'];
        foreach ($services_for_update as $key => $val) {
            $sql = update_query_string('dossier_services', $val) . ' WHERE id=' . clear_post($key);
            try {
                query($sql);
            } catch (Exception $e) {
                return Response()->error($e->getMessage());
            }
        }
        $call_back = [];
        foreach ($services_new as $key => $val) {
            $val['dossier_id'] = $dossier_id;
            $sql = insert_query_string('dossier_services', $val);
            try {
                query($sql);
                $call_back[] = getLastId();
            } catch (Exception $e) {
                return Response()->error($e->getMessage());
            }
        }
        $sql = update_query_string('dossiers', [
                'total_discount' => $total_discount,
                'total_comment' => $total_comment,
            ]) . ' WHERE id=' . clear_post($dossier_id);
        try {
            query($sql);
        } catch (Exception $e) {
            return Response()->error($e->getMessage());
        }
        return Response()->data($call_back)->success();
    }
}