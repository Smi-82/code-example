<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Exception;
use models\UserModel;
use TaskAccess;
use User;

class GetParticipantForSingeTaskPage extends BaseAjaxController
{
    public function run()
    {
        $sql = UserModel::baseSelect() . " WHERE `usertype` NOT IN (" . implode(',', [
                UserTypes()->getClient(),
            ]) . ") and `id` NOT IN (" . TaskAccess::baseSelect(['user_id']) . " WHERE `task_id` = " . $_POST['task_id'] . ") ORDER by `usertype` ASC, firstname ASC";
        try {
            $participants = UserModel::query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
        $html = getSelect2Foto($participants, ['selector' => 'add_participant']);
        return Response()->data(compact('html'))->success();
    }
}