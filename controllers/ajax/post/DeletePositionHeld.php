<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Exception;
use PositionHeld;
use User;

class DeletePositionHeld extends BaseAjaxController
{
    public function run()
    {
        $id = escape_string(clear_post($_POST['id']));
        $res = PositionHeld::delete($id);
        if ($res) {
            $sql = update_query_string(User::getTable(), ['position_held_id' => PositionHeld::getDefaultId()]) . ' WHERE position_held_id=' . $id;
            try {
                $res = query($sql);
                return $res ? Response()->data(getLastId())->success() : Response()->error();
            } catch (Exception $e) {
                return Response()->error($e->getMessage());
            }
        }
        else {
            return Response()->error();
        }
    }
}