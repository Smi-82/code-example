<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Offer;

class GetMastersClientOffers extends BaseAjaxController
{
    public function run()
    {
        $params = ['id', 'name', 'banner_title'];
        $offers = [];
        if (User()->isSupervisor())
            $offers = Offer::getAllOffers($params);
        if (User()->isRGPDMaster()) {
            $offers = Offer::getOffersForMaster(User()->getId(), $params);
        }
        $offers = array_map(function ($elem) {
            $elem['checked'] = !isset($_POST['client']);
            return $elem;
        }, $offers);
        if (!isset($_POST['client'])) {
            return Response()->data($offers)->success();
        }
        $client_offers = Offer::getOfferForClient($_POST['client'], ['id', 'project_id', 'client_agree']);
        $tmp = [];
        foreach ($client_offers as $v) {
            $tmp[$v['project_id']] = $v;
        }
        if (!empty($client_offers)) {
            $client_offers_id = array_column($client_offers, 'project_id');
            foreach ($offers as &$val) {
                if (in_array($val['id'], $client_offers_id))
                    $val['checked'] = true;
                $val['client_agree'] = isset($tmp[$val['id']]) ? intval($tmp[$val['id']]['client_agree']) : 0;
                $val['table_id'] = isset($tmp[$val['id']]) ? intval($tmp[$val['id']]['id']) : 0;
            }
        }
        return Response()->data($offers)->success();
    }
}