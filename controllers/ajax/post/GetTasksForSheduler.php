<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Exception;
use Group;
use Task;
use TaskAccess;

class GetTasksForSheduler extends BaseAjaxController
{
    public function run()
    {
        if (intval($_POST['group_id']) == 0)
            $group = 'group_id IN ( ' . Group::baseSelect(['id']) . ' WHERE general_project_id = ' . escape_string(clear_post($_POST['project_id'])) . ')';
        else
            $group = 'group_id = ' . escape_string(clear_post($_POST['group_id']));
        if (!(User()->isSupervisor() || User()->isAssistant())) {
            $current_user_id = User()->isCollaborator() ? User()->getParentId() : User()->getId();
            $sql = Task::baseSelect(['id', 'name', 'start', 'category', 'color', 'estimateTime']) . ' WHERE ' . $group . ' AND id IN(' . TaskAccess::baseSelect(['task_id']) . ' WHERE user_id = ' . $current_user_id . ')';
        }
        else
            $sql = Task::baseSelect(['id', 'name', 'start', 'category', 'color', 'estimateTime']) . ' WHERE 1 AND ' . $group;

        try {
            $result = query($sql);
            return $result ? Response()->data($result)->success() : Response()->error();
        } catch (Exception $e) {
            return Response()->data($sql)->error($e->getMessage());
        }
    }
}