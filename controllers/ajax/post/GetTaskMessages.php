<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class GetTaskMessages extends BaseAjaxController
{
    public function run()
    {
        $data = array_map(function ($item) {

            $item['user_img_url'] = getImg(['img' => $item['user_img']], $item['user_id']);
            $item['message_f'] = htmlspecialchars_decode($item['message']);
            return $item;
        }, Task()->getMessages(clear_post($_POST['task_id'])));

        usort($data, function ($a, $b) {
            return date($b['sentat']) >= date($a['sentat']);
        });

        return Response()->data($data)->success();
    }
}