<?php


namespace controllers\ajax\post;


use Consultant;
use controllers\ajax\BaseAjaxController;
use Exception;
use User;

class GetConsultantMaster extends BaseAjaxController
{
    public function run()
    {
        try {
            $obj = new Consultant();
            $res = $obj->getMasterIds(clear_post($_POST['master']));

            $res2 = $obj->getAllHierarchyHigher(clear_post($_POST['master']));

            $res3 = query('SELECT id FROM ' . User::getTable() . ' WHERE consultant_id=' . escape_string(clear_post($_POST['master'])));
            $res3 = array_column($res3, 'id');

            $res = array_unique(array_merge($res, $res2, $res3), SORT_NUMERIC);
            $res = array_map(function ($elem) {
                return strval($elem);
            }, $res);
            $res = array_values($res);
            return Response()->data($res)->success();
        } catch (Exception $e) {
            return Response()->error($e->getMessage());
        }
    }
}