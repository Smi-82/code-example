<?php


namespace controllers\ajax\post;


use ClientListPage;
use controllers\ajax\BaseAjaxController;

class GetClientList extends BaseAjaxController
{
    public function run()
    {
        $obj = new ClientListPage();
        return Response()->data($obj->getTableData())->success();
    }
}