<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class CommentAttachment extends BaseAjaxController
{
    public function run()
    {
        $res = Task()->commentAttachment(clear_post($_POST['id']), clear_post($_POST['comment']));
        return $res ? Response()->success() : Response()->error($res);
    }
}