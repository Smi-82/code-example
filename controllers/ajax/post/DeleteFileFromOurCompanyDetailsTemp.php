<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use OurCompany;

class DeleteFileFromOurCompanyDetailsTemp extends BaseAjaxController
{
    public function run()
    {
        return Response()->data(OurCompany::deleteFileFromDetailsTemp($_POST['filename'], $_POST['file']))->success();
    }
}