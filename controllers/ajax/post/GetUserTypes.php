<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class GetUserTypes extends BaseAjaxController
{
    public function run()
    {
        $res = UserTypes()->getAll();
        $res = array_map(function ($elem) {
            $elem['allow_сhat_with_client'] = (bool)$elem['allow_сhat_with_client'];
            return $elem;
        }, $res);
        return Response()->data($res)->success();
    }
}