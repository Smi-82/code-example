<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use TaskAccess;
use User;

class AddNewParticipant extends BaseAjaxController
{
    public function run()
    {
        if (!is_array($_POST['task_id']))
            $_POST['task_id'] = [$_POST['task_id']];

        foreach ($_POST['task_id'] as $task_id)
            foreach ($_POST['user_id'] as $u_id)
                TaskAccess::add($task_id, $u_id);

        $users = [];
        foreach ($_POST['user_id'] as $u_id) {
            $tmp = User::getById($u_id, ['id', 'firstname', 'lastname', 'usertype', 'img']);
            $tmp['usertype'] = UserTypes()->getTypeById($tmp['usertype']);
            $tmp['img'] = getImg($tmp, $tmp['id']);
            $users[] = $tmp;
        }
        return Response()->data($users)->success();
    }
}