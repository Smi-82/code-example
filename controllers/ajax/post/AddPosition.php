<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class AddPosition extends BaseAjaxController
{
    public function run()
    {
        return Response()->data(Position()->add($_POST['data']))->success();
    }
}