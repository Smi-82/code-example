<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Exception;

class CloseNotify extends BaseAjaxController
{
    public function run()
    {
        try {
            return Response()->data(AdmNotify()->close(!is_array($_POST['id']) ? [$_POST['id']] : $_POST['id']))->success();
        } catch (Exception $e) {
            return Response()->error($e->getMessage());
        }
    }
}