<?php

namespace controllers\ajax\post\position;
use controllers\ajax\BaseAjaxController;

class FindPositions2 extends BaseAjaxController
{
    public function run()
    {
        $lang = Language()->getLanguage();
        $nameKey = 'name_' . $lang;

        $positions = Position()->find(clear_post($_POST['search']));

        $positionTitleId = array_unique(array_column($positions, 'position_title_id'));
        $positionCategoryId = array_unique(array_column($positions, 'position_category_id'));
        $positionSpecialityId = array_unique(array_column($positions, 'position_speciality_id'));

        $positionTitles = PositionTitle()->getById($positionTitleId, ['id', $nameKey]);
        $positionCategories = PositionCategory()->getById($positionCategoryId, ['id', $nameKey]);
        $positionSpecialities = PositionSpeciality()->getById($positionSpecialityId, ['id', $nameKey]);

        $positionTitlesAssoc = arrayToAssocByKey($positionTitles, 'id');
        $positionCategoriesAssoc = arrayToAssocByKey($positionCategories, 'id');
        $positionSpecialitiesAssoc = arrayToAssocByKey($positionSpecialities, 'id');

        foreach ($positions as &$position) {
            $position['title_name'] = $positionTitlesAssoc[$position['position_title_id']][$nameKey];
            $position['category_name'] = $positionCategoriesAssoc[$position['position_category_id']][$nameKey];
            $position['speciality_name'] = $positionSpecialitiesAssoc[$position['position_speciality_id']][$nameKey];
        }
        return Response()->data($positions)->success();
    }
}