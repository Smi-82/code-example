<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use ProjectFunctions;

class DeleteProjectFuctions extends BaseAjaxController
{
    public function run()
    {
        $res = ProjectFunctions::delete($_POST['id']);
        return $res ? Response()->success() : Response()->error();
    }
}