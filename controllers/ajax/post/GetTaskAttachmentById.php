<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class GetTaskAttachmentById extends BaseAjaxController
{
    public function run()
    {
        $data = Task()->getAttachmentById($_POST['id']);
        $data['src'] = Task()->getAttachmentUrl($data['output_dir'], $data['filename']);
        $data['filesize_f'] = formatBytes($data['filesize'], 0);
        $data['ext'] = getFileExtension($data['filename']);
        return Response()->data($data)->success();
    }
}