<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class GetNotify extends BaseAjaxController
{
    public function run()
    {
        $tmp_arr = isset($_POST['exclude']) && !empty($_POST['exclude']) ? $_POST['exclude'] : [];
        try {
            return Response()->data(AdmNotify()->get($tmp_arr))->success();
        } catch (Exception $e) {
            return Response()->error($e->getMessage());
        }
    }
}