<?php


namespace controllers\ajax\post;


use Category;
use controllers\ajax\BaseAjaxController;
use Task;

class GetStaticDataForMassTasksInsertion extends BaseAjaxController
{
    public function run()
    {
        $proj_data = ['id', 'name', 'cloned_from'];
        if (User()->isSupervisor()) {
            $projects = Project()->getAllProjects($proj_data);
        }
        else {
            $projects = Project()->getProjectListForUser(User()->getId());
            $projects = Project()->getManyByIds($projects, $proj_data);
        }
        usort($projects, function ($a, $b) {
            if ($a['name'] == $b['name']) {
                return 0;
            }
            return ($a['name'] < $b['name']) ? -1 : 1;
        });
        $categories = Category::getCategoriesByEntity(Task::getTable(), ['id', 'name']);
        return Response()->data(['proj' => $projects, 'cat' => $categories])->success();
    }
}