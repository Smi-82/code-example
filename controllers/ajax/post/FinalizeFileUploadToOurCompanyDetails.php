<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use OurCompany;

class FinalizeFileUploadToOurCompanyDetails extends BaseAjaxController
{
    public function run()
    {
        return Response()->data(OurCompany::finalizeAddingFileToDetails($_POST['data']))->success();
    }
}