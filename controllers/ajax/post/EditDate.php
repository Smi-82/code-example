<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Exception;
use Task;

class EditDate extends BaseAjaxController
{
    public function run()
    {
        $key = clear_post($_POST['type']);
        $err = '';
        try {
            $result = Task::getById($_POST['task_id'], ['start', 'estimateTime', 'created', 'estimate']);
            if (strtotime($_POST['timestamp']) < strtotime($result['created'])) {
                $err = mb_ucfirst(lng('the_start_date_must_be_at_least')) . ' ' . date('d-n-Y H:i', strtotime($result['created']));
                return Response()->data($result[$key])->error($err);
            }
            switch ($key) {
                case 'start':
                    $range = strtotime($result['estimateTime']) - strtotime($_POST['timestamp']);
                    $err = mb_ucfirst(lng('the_start_date_must_not_be_greater_than')) . ' ' . date('d-n-Y H:i', strtotime($result['estimateTime']) - (strtotime($result['estimate']) + (intval($result['estimate']) / 3600 / 8) * 57600));
                    break;
                case 'estimateTime':
                    $range = strtotime($_POST['timestamp']) - strtotime($result['start']);
                    $err = mb_ucfirst(lng('the_start_date_must_be_at_least')) . ' ' . date('d-n-Y H:i', strtotime($result['start']) + (intval($result['estimate']) / 3600 / 8) * 57600);
                    break;
            }
            if ($range < (strtotime($result['estimate']) + (intval($result['estimate']) / 3600 / 8) * 57600)) {
                return Response()->data($result[$key])->error($err);
            }
            else {
                $res = Task::updateTasksData($_POST['task_id'], [$key => date('Y-m-d H:i', strtotime($_POST['timestamp']))]);
                taskHistory($_POST['task_id'], 'Change date');
                return Response()->success();
            }
        } catch (Exception $e) {
            return Response()->error($e->getMessage());
        }
    }
}