<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Exception;
use Templates;
use User;

class GetTemplate extends BaseAjaxController
{
    public function run()
    {
        try {
            if ($_POST['name'] == '')
                return Response()->error();
            $data = $_POST;
            unset($data['key']);
            unset($data['name']);
            $res = '';
            if (isset($_POST['clientid']))
                $res = User::getById($_POST['clientid']);

            if ($res)
                $data = array_merge($data, $res);
            return Response()->data(Templates::getTemplate(clear_post($_POST['name']), $data))->success();
        } catch (Exception $e) {
            return Response()->error($e->getMessage());
        }
    }
}