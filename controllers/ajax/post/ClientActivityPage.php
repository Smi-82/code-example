<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class ClientActivityPage extends BaseAjaxController
{
    public function run()
    {
        $obj = new \ClientActivityPage();
        return Response()->data($obj->getTableData())->success();
    }
}