<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use TaskAlert;

class TaskCompletionStatusCheckAll extends BaseAjaxController
{
    public function run()
    {
        $task_id = escape_string($_POST['task_id']);
        $user_ids = array_map(
            function ($val, $i) {
                return $val['user_id'];
            },
            getProjectParticipants($task_id, 15)
        );
        array_walk(
            $user_ids,
            function ($val, $key) use ($task_id) {
                TaskAlert::add($val, $task_id, 1);
                $temp = get_user_by_id($val);
                history('task', User()->getSignature() . ' created alert request about participant status to ' . $temp['firstname'] . ' ' . $temp['lastname'] . ' - task ID: ' . $task_id);
            }
        );
        if (!TaskAlert::taskHasAlertType2($task_id)) TaskAlert::deleteByUserTaskType(User()->getId(), $task_id, 2);
        return json_encode(true);
    }
}