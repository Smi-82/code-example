<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use TaskScreenshot;
use UploadDir;

class GetTaskAdminScreenshots extends BaseAjaxController
{
    public function run()
    {
        if (User()->isSupervisor())
            $screenshots = TaskScreenshot::getByTaskId($_POST['task_id']);
        else
            $screenshots = TaskScreenshot::getMyByTaskId($_POST['task_id']);
        $screenshots = array_map(function ($el) {
            $el['src'] = UploadDir::getSrc($el['output_dir'] . DS . $el['filename']);
            $el['whenCreated'] = @date('d-m-y H:i:s', strtotime($el['whenCreated']));
            return $el;
        }, $screenshots);
        return Response()->data($screenshots)->success();
    }
}