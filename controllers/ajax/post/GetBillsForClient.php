<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use PaymentPartsProjectBill;

class GetBillsForClient extends BaseAjaxController
{
    public function run()
    {
        $bills = BillProject()->getLastBillsByClientId(User()->getId());
        $invoces = InvoiceProject()->getByClientId(User()->getId());
        $bills = array_merge($bills, $invoces);
        if (empty($bills)) {
            return Response()->data([])->success();
        }
        $bills = array_map(function ($elem) {
            $elem['isClone'] = 'no';
            return $elem;
        }, $bills);
        if ($invoces) {
            $clone_invoice = array_map(function ($elem) {
                $elem['approved'] = 0;
                $elem['isClone'] = 'yes';
                return $elem;
            }, $invoces);
            $bills = array_merge($bills, $clone_invoice);
        }
        $bills = array_map(function ($elem) {
            $elem['selector'] = md5($elem['id'] . $elem['isClone']);
            if ($elem['when_approved']) {
                $elem['when_approved'] = [
                    'date' => date('d-m-Y', strtotime($elem['when_approved'])),
                    'time' => date('H:i:s', strtotime($elem['when_approved'])),
                ];
            }
            return $elem;
        }, $bills);

        $projects = array_unique(array_column($bills, 'project_id'));
        $projects = Project()->getManyByIds($projects, ['id', 'name']);
//    $bill_ids = array_column($bills, 'id');
        foreach ($bills as &$bill) {
            if (intval($bill['approved']))
                $bill['path']['href'] = InvoiceProject()->getHrefPDF($bill['id'], $bill['project_id']);
            else
                $bill['path']['href'] = BillProject()->getHrefPDF($bill['id'], $bill['project_id']);
            $tmp_proj = array_filter($projects, function ($elem) use ($bill) {
                return intval($bill['project_id']) == intval($elem['id']);
            });
            $bill['project'] = array_shift($tmp_proj);
            $bill['payment_parts'] = PaymentPartsProjectBill::getByBillId($bill['id']);
            $bill['rest'] = $bill['amount'] - array_sum(array_column(array_filter($bill['payment_parts'], function ($elem) {
                    return intval($elem['paid']);
                }), 'amount'));
            if (intval($bill['approved']))
                $bill['name_bdc'] = InvoiceProject()->createNameForShow($bill['id'], User()->getSociete());
            else
                $bill['name_bdc'] = BillProject()->createNameBDC($bill['id'], $bill['created']);

        }
        return Response()->data($bills)->success();
    }
}