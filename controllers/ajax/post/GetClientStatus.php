<?php


namespace controllers\ajax\post;


use ClientStatus;
use controllers\ajax\BaseAjaxController;

class GetClientStatus extends BaseAjaxController
{
    public function run()
    {
        $res = ClientStatus::getAll();
        return $res ? Response()->data($res)->success() : Response()->error($res);
    }
}