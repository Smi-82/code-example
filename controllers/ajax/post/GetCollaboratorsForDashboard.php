<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class GetCollaboratorsForDashboard extends BaseAjaxController
{
    public function run()
    {
        return Response()->data(User()->getAllCollaborators())->success();
    }
}