<?php


namespace controllers\ajax\post;


use Assistant;
use controllers\ajax\BaseAjaxController;

class GetClientsForAssistant extends BaseAjaxController
{
    public function run()
    {
        return Response()->data(array_column(Assistant::getClientsByAssistantId($_POST['master']), 'id'))->success();
    }
}