<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Group;
use Project;
use Templates;
use UploadDir;

class GenerateExpressionsOfNeedsPdf extends BaseAjaxController
{
    public function run()
    {
        $pdfData = [];
        $pdfData['general_project_id'] = $general_project_id = escape_string(clear_post($_POST['project_id']));
        $user_id = User()->isCollaborator() ? User()->getParentId() : User()->getId();
        $groups = [];
        $tasks = [];
        $query_str = 'SELECT p.name, p.price_type, COUNT(g.id) AS cntGr FROM ' .
            Project::getTable() . ' AS p
                        LEFT JOIN ' .
            Group::getTable() . ' AS g
                        ON
                        g.general_project_id = p.id
                        WHERE
                        p.id=' . $general_project_id;

        $project = querySingle($query_str);

        $pdfData['project'] = $project;
        $pdfHtml = Templates::getTemplate('invoice/expression-of-needs-pdf', $pdfData);
        $mpdf = new \Mpdf\Mpdf(['tempDir' => UploadDir::getRootPath() . 'Mpdf/ExpressionsOfNeeds']);
        $mpdf->SetDisplayMode('fullpage');

        $mpdf->WriteHTML($pdfHtml);

        $mpdf->enableImports = true;

        $mpdf->Output('upload/expression-of-needs-' . $general_project_id . '-' . date("Y") . '-' . date("m") . ".pdf", "F");


        return Response()->data($_SERVER['DOCUMENT_ROOT'] . DS . MODULE . DS . 'libs' . DS . 'mpdf' . DS . 'vendor' . DS . 'autoload.php')->success();
    }
}