<?php

namespace controllers\ajax\post;

use controllers\ajax\BaseAjaxController;
use User;

class SetDefaultShowTheClientSingleClient extends BaseAjaxController
{
    public function run()
    {
        $res = User()->update($_POST['client_id'], ['show_the_client' => '']);
        return $res ? Response()->success() : Response()->error($res);
    }

    public function singleType()
    {
        $res = User::getById($_POST['client_id'], ['show_the_client']);
        $res = json_decode(htmlspecialchars_decode($res['show_the_client']), true);
        unset($res[$_POST['type_id']]);
        if (empty($res))
            $res = User()->update($_POST['client_id'], ['show_the_client' => '']);
        else
            $res = User()->update($_POST['client_id'], ['show_the_client' => json_encode($res)]);
        return $res ? Response()->success() : Response()->error($res);
    }
}