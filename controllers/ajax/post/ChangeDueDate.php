<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Task;

class ChangeDueDate extends BaseAjaxController
{
    public function run()
    {
        if ($_POST['task_id'] == '' || $_POST['newDate'] == '') {
            return Response()->error(mb_ucfirst(lng('missed_required_data')));
        }
        $update_note_exe = Task::updateTasksData($_POST['task_id'], ['estimateTime' => $_POST['newDate']]);
        if ($update_note_exe === false) {
            return Response()->error(mb_ucfirst(lng('error_occured')) . ', ' . lng('date not changed'));
        }
        else {
            history('task', User()->getSignature() . ' changed Due Date of the task (ID:' . clear_post($_POST['task_id']) . ') to ' . clear_post($_POST['newDate']));
            $today = date_create(date('Y-m-d'));
            $created = date_create(clear_post($_POST['created']));
            $estimateTime = date_create(clear_post($_POST['newDate']));
            $working = date_diff($created, $today);
            $daysToEnd = date_diff($estimateTime, $today);
            $totaldays = date_diff($estimateTime, $created);
            $totaldays = intval($totaldays->format('%a'));
            $totalworking = intval($working->format('%a'));
            $done = round($totalworking * 100 / $totaldays);
            return Response()->data(array(
                'working' => $daysToEnd->format('%a days'),
                'done' => $done,
                'newDate' => clear_post($_POST['newDate'])
            ))->success();
        }
    }
}