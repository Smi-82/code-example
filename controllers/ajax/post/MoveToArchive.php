<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Task;

class MoveToArchive extends BaseAjaxController
{
    public function run()
    {
        if ($_POST['task_id'] == '' || $_POST['user_id'] == '') {
            return Response()->success();
        }
        history('task', User()->getSignature() . ' moved task (ID:' . clear_post($_POST['task_id']) . ') into archive');
        if (Task::updateTasksData($_POST['task_id'], ['active' => 0])) {
            return Response()->success();
        }
        else {
            return Response()->error();
        }
    }
}