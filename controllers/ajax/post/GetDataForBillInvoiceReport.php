<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Exception;
use Group;
use Task;
use UserTypes;

class GetDataForBillInvoiceReport extends BaseAjaxController
{
    public function run()
    {
        $task_ids = escape_string(clear_post($_SESSION[$_POST['page']]['tasks']));
        $sql = '
SELECT
g.name AS group_name,
g.id AS group_id,
t.id AS task_id,
t.name AS task_name,
t.description AS task_desc,
t.estimate,
t.price_type,
t.category,
t.price,
ut.id AS type_id,
ut.name AS type_name,
ut.tax_min
FROM
' . Task::getTable() . ' AS t
LEFT JOIN
' . Group::getTable() . ' AS g
ON
t.group_id = g.id
LEFT JOIN
' . UserTypes::getTable() . ' AS ut
ON
ut.id = t.collaborator_type_id
WHERE
t.id IN (' . $task_ids . ')
ORDER BY task_id
';
        $participants = Task()->getOwnParticipants(explode(',', $task_ids), ['id, img, firstname, lastname']);
        $tmp = [];
        foreach ($participants as $v) {
            $v['img'] = getImg($v, $v['id']);
            $v['u_name'] = trim($v['firstname'] . ' ' . $v['lastname']);
            unset($v['firstname']);
            unset($v['lastname']);
            $tmp[$v['task_id']][] = $v;
        }
        try {
            $tasks = query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
        $tht_sum = 0;
        $cnt_groups = count(array_unique(array_column($tasks, 'group_id')));
        foreach ($tasks as &$t) {
            $tht_sum += intval($t['price_type'] == 1 ? $t['tht'] : $t['price']);
            $t['group_name'] = strip_tags(html_entity_decode($t['group_name']));
            $t['task_desc'] = strip_tags(html_entity_decode($t['task_desc']));
            $t['task_name'] = strip_tags(html_entity_decode($t['task_name']));
            $t['task_estimation'] = secondsToWorkingTime($t['estimate']);
            $t['participants'] = $tmp[$t['task_id']];
            if (!$t['tax_min'])
                $t['tax_min'] = 0;
        }
        $ttc_sum = $tht_sum + $tht_sum * floatval((OurCompany()->tva) / 100);
        return Response()->data(['tasks' => $tasks, 'cnt_tasks' => count($tasks), 'cnt_groups' => $cnt_groups, 'ttc_sum' => $ttc_sum, 'tht_sum' => $tht_sum])->success();
    }
}