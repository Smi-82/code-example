<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Project;

class GetProjectQuestionnaireData extends BaseAjaxController
{
    public function run()
    {
        $res = Project::getQuestionnaire($_POST['project_id']);
        return $res ? Response()->data($res)->success() : Response()->error();
    }
}