<?php


namespace controllers\ajax\post;


use Consultant;
use controllers\ajax\BaseAjaxController;
use Exception;
use User;
use UserTypes;

class GetCntClients extends BaseAjaxController
{
    public function run()
    {
        $consultant = clear_post($_POST['consultant']);
        $obj = new Consultant();
        $tmp = $obj->getChildrensId($consultant);
        $tmp[] = $consultant;
        try {
            $sql = 'SELECT COUNT(id) as cnt_clients FROM ' . User::getTable() . ' WHERE usertype=' . UserTypes::getTable() . ' AND consultant_id IN (' . implode(',', $tmp) . ')';
            $res = querySingle($sql);
            return $res['cnt_clients'];
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}