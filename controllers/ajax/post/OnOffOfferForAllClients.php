<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Offer;

class OnOffOfferForAllClients extends BaseAjaxController
{
    public function run()
    {
        $res = Offer::displayByProjectId($_POST['project_id'], $_POST['status']);
        return $res ? Response()->data($res)->success() : Response()->error($res);
    }
}