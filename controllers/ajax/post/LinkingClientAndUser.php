<?php

namespace controllers\ajax\post;

use ClientUsers;
use controllers\ajax\BaseAjaxController;

class LinkingClientAndUser extends BaseAjaxController
{
    public function run()
    {
        $res = ClientUsers::add($_POST['client_id'], $_POST['user_id']);
        return is_numeric($res) ? Response()->data($res)->success() : Response()->error($res);
    }
}