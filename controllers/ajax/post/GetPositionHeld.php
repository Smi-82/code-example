<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use PositionHeld;

class GetPositionHeld extends BaseAjaxController
{
    public function run()
    {
        $res = PositionHeld::getAll();
        return is_array($res) ? Response()->data($res)->success() : Response()->error($res);
    }
}