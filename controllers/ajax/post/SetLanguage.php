<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Exception;

class SetLanguage extends BaseAjaxController
{
    public function run()
    {
        $lng = escape_string(clear_post($_POST['lang']));
        try {
            Language()->setLang($lng);
        } catch (Exception $e) {
            return Response()->data($e->getMessage())->error();
        }
        return Response()->success();
    }
}