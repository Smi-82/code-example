<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class EditTaskClientComment extends BaseAjaxController
{
    public function run()
    {
        return Response()->data(Task()->editClientMessage(clear_post($_POST['task_id']), clear_post($_POST['message_id']), $_POST['desc']))->success();
    }
}