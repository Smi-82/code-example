<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class AddProjectClientComment extends BaseAjaxController
{
    public function run()
    {
        return Response()->data(Project()->addClientMessage(clear_post($_POST['project_id']), clear_post($_POST['user_id']), $_POST['desc']))->success();
    }
}