<?php


namespace controllers\ajax\post;


use ClientMenuManager;
use controllers\ajax\BaseAjaxController;

class ChangeSowingClientMenuFormAdmSide extends BaseAjaxController
{
    public function run()
    {
        $obj = new ClientMenuManager($_POST['client']);
        $res = $obj->setShow($_POST['menu_key'], $_POST['value']);
        return $res ? Response()->success() : Response()->error($res);
    }
}