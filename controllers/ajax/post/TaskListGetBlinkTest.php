<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use TaskAccess;

class TaskListGetBlinkTest extends BaseAjaxController
{
    public function run()
    {
        $result = [];
        $tasks_ids = $_POST['tasks_id'];
        $ids = array_map(function ($elem) {
            return escape_string(clear_post($elem));
        }, $tasks_ids);
        $inf = TaskAccess::getManyByTaskIds($ids, ['last_timestamp', 'user_id', 'task_id']);
        foreach ($inf as $key => $val) {
            if (!isset($inf[$val['task_id']]))
                $inf[$val['task_id']] = [];
            $inf[$val['task_id']][] = $val;
            unset($inf[$key]);
        }

        foreach ($tasks_ids as $id) {
            $message = false;
//            if (isset($messages[$id])) {
//                $last_msg = $messages[$id];
//                foreach ($user_message_link as $val) {
//                    if ($val['user_id'] == $current_user->getId() && $val['task_id'] == $id && $val['message_id'] == $last_msg) {
//                        $message = true;
//                        break;
//                    }
//                }
//            }
            $result[$id]['message'] = [
                'has_new' => $message,
                'cnt' => 0
            ];
            if (isset($inf[$id])) {
                foreach ($inf[$id] as $check) {
                    $blink = false;
                    $online = false;
                    if ($check["last_timestamp"] == 0) {
                        $blink = true;
                    }
                    if (time() - $check["last_timestamp"] <= 15) {
                        $online = true;
                    }
                    $result[$id]['users'][] = ["user_id" => $check["user_id"], "blink" => $blink, "online" => $online];
                }
            }
        }

        /*
    foreach ($ids as $id) {
        $last_msg = query("SELECT id FROM tasks_messages WHERE task_id = $id ORDER by id DESC LIMIT 1");
        $message = false;
        if ($last_msg) {
            $last_msg = $last_msg[0]["id"];
            $user_id = $current_user->getId();
            $sql = "SELECT id FROM user_message_link WHERE user_id=$user_id AND task_id=$id AND message_id=$last_msg";
            if (query($sql))
                $message = false;
            else
                $message = true;
        }
        $sql = "SELECT * FROM tasks_access WHERE task_id = " . $id;
        $inf = query($sql);
        foreach ($inf as $check) {
            $blink = false;
            $online = false;
            if ($check["last_timestamp"] == 0) {
                $blink = true;
            }
            if (time() - $check["last_timestamp"] < 10) {
                $online = true;
            }
            $result[$id][] = ["user_id" => $check["user_id"], "blink" => $blink, "online" => $online, "message" => $message];
        }
    }
    */
        return Response()->data($result)->success();
    }
}