<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Exception;
use translation\classes\TranslationManager;

class GetTranslation extends BaseAjaxController
{
    public function run()
    {
        try {
            $res = TranslationManager::getInst()->getAsKeyValue(Language()->getLanguage());
        } catch (Exception $e) {
            $res = $e->getTraceAsString();
        }
        return Response()->data($res)->success();
    }
}