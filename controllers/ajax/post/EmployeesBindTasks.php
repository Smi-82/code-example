<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use TaskAccess;

class EmployeesBindTasks extends BaseAjaxController
{
    public function run()
    {
        $employe_ids = $_POST['employe_ids'];
        $task_ids = $_POST['task_ids'];
        foreach ($employe_ids as $user_id)
            foreach ($task_ids as $task_id)
                TaskAccess::add($task_id, $user_id);
        return Response()->success();
    }
}