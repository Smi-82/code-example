<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class CloneProjectAsOffer extends BaseAjaxController
{
    public function run()
    {
        $res = Project()->cloneAsOffer($_POST['id']);
        return empty($res['error']) ? Response()->data($res)->success() : Response()->data(['place' => 'console'])->error($res['error']);
    }
}