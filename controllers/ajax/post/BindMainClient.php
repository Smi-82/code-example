<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class BindMainClient extends BaseAjaxController
{
    public function run()
    {
        User()->updateClient($_POST['client'], ['parent_id' => $_POST['main_id']]);
        return Response()->data()->success();
    }
}