<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use OurCompany;

class UpdateOurCompanyDetails extends BaseAjaxController
{
    public function run()
    {
        unset($_POST['key']);
        if (OurCompany::updateDetails($_POST['data']))
            return Response()->data(OurCompany::getDetails())->success();
        return Response()->data(OurCompany::getDetails())->error('Could not update form');
    }
}