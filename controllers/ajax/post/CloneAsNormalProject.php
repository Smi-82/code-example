<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class CloneAsNormalProject extends BaseAjaxController
{
    public function run()
    {
        $res = Project()->cloneAsNormalProject($_POST['id']);
        return empty($res['error']) ? Response()->data($res)->success() : Response()->data(['place' => 'console'])->error($res['error']);
    }
}