<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use OurCompany;

class GetOurCompanyImageDetails extends BaseAjaxController
{
    public function run()
    {
        return Response()->data(OurCompany::getImageDetails($_POST['file']))->success();
    }
}