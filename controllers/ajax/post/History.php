<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class History extends BaseAjaxController
{
    public function run()
    {
        $arr = [
            'page',
            'page_url',
            'description'];
        foreach ($arr as &$val) {
            if (!isset($_POST[$val]) || $_POST[$val] == '') {
                echo Response()->error(mb_ucfirst(lng('error')) . ': ' . mb_ucfirst(lng('missed_required_fields')));
                exit();
            }
        }
        $res = \History::add([
            'user_id' => User()->getId(),
            'connection_id' => session_id(),
            'page' => $_POST['page'],
            'page_url' => $_POST['page_url'],
            'description' => $_POST['description'],]);
        return $res ? Response()->data($res)->success() : Response()->error(mb_ucfirst(lng('history_not_added')));
    }
}