<?php


namespace controllers\ajax\post;


use Category;
use controllers\ajax\BaseAjaxController;

class DeleteEntityFromCat extends BaseAjaxController
{
    public function run()
    {
        $res = Category::deleteEntity($_POST['cat'], $_POST['ent']);
        return $res ? Response()->data()->success() : Response()->error();
    }
}