<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Exception;
use Task;

class ChangeDescr extends BaseAjaxController
{
    public function run()
    {
        try {
            Task::updateTasksData($_POST['task_id'], ['description' => $_POST['task_desc']]);
        } catch (Exception $e) {
            return Response()->error($e->getMessage());
        }
        return Response()->data(escape_string(clear_post($_POST['task_desc'])))->success();
    }
}