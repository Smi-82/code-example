<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class DeleteProject extends BaseAjaxController
{
    public function run()
    {
        $res = Project()->delete($_POST['general_project_id']);
        return $res ? Response()->success() : Response()->error();
    }
}