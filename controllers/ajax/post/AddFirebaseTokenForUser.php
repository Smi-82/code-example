<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class AddFirebaseTokenForUser extends BaseAjaxController
{
    public function run()
    {
        if (!$_POST['token'] || !$_POST['user_global_id']) {
            echo json_encode(['status' => 0, 'result' => 101, 'error' => 'User not found']);
            exit;
        }
        $token = json_encode(clear_post($_POST['token']), JSON_UNESCAPED_UNICODE);
        $global_id = clear_post($_POST['user_global_id']);
        $currentToken = User()->getUserByGlobalID($global_id, ['firebase_tokens'])['firebase_tokens'];
        $currentTokenArray = [];
        if ($currentToken) {
            $currentTokenArray = explode(',', $currentToken);
            if (in_array($token, $currentTokenArray)) {
                return json_encode(['status' => 0, 'result' => 101, 'error' => 'Token already exists']);
            }
        }
        $currentTokenArray[] = $token;
        if (User()->updateByGlobalId($global_id, ['firebase_tokens' => implode(',', $currentTokenArray)])) {
            return json_encode(['status' => 0, 'result' => 1]);
        }
    }
}