<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Exception;
use User;
use UserTypes;

class GetConsultantChildIds extends BaseAjaxController
{
    public function run()
    {
        $sql = 'SELECT id FROM ' . User::getTable() . ' WHERE usertype!=' . UserTypes::getTable() . ' AND consultant_id=' . clear_post($_POST['id']);
        try {
            $res = query($sql);
            return $res ? Response()->data(array_map(function ($elem) {
                return $elem['id'];
            }, $res))->success() : Response()->error($sql);
        } catch (Exception $e) {
            return Response()->error($e->getMessage());
        }
    }
}