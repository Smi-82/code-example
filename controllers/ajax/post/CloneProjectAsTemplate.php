<?php

namespace controllers\ajax\post;

use controllers\ajax\BaseAjaxController;

class CloneProjectAsTemplate extends BaseAjaxController
{
    public function run()
    {
        $res = Project()->cloneAsTemplate($_POST['id']);
        return empty($res['error']) ? Response()->data($res)->success() : Response()->data(['place' => 'console'])->error($res['error']);
    }
}