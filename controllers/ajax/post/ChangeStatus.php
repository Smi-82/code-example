<?php


namespace controllers\ajax\post;


use appEvents\subscribers\controller\AppEventsSubscribersController;
use controllers\ajax\BaseAjaxController;
use appEvents\AppEventsController;
use MessengerEventCodes;
use Task;

class ChangeStatus extends BaseAjaxController
{
    public function run()
    {
        $task_id = clear_post($_POST['task_id']);
        $curent_status = Task::getById($_POST["task_id"], ['status']);
        $curent_status = $curent_status['status'];
        $id = clear_post($_POST['id']);
        if (!User()->isSupervisor()) {
            if ($id <= $curent_status) {
                return Response()->error(mb_ucfirst(lng('error')) . ': You can not lower the status!');
            }
        }
        $status_obj = Statuses();
        $res = false;
        if ($status_obj->canUserTypeChange($id, User()->getType(), Task::getTable()))
            $res = Task::updateTasksData($task_id, ['status' => $id]);
        if ($res) {
            $name1 = $status_obj->getStatusesById($curent_status, 'name');
            $desc = 'Changed task status from ' . $name1 . ' to ' . $status_obj->getStatusesById($id, 'name');
            AppEventsController::publish(AppEventsSubscribersController::changeTaskStatusEvent(), array_merge(compact('desc', 'task_id'), ['page' => 'task']));
            $notificationApiResponse = 'no response';
            switch ($id) {
                case $status_obj->waiting_for_validation_by_the_client():
                    $notificationApiResponse = MessengerEvents()->setOuterData('old_status', mb_ucfirst(lng($name1)))->send(MessengerEventCodes::TASK_STATUS_CHANGE_TO_WAITING_FOR_VALIDATION_BY_THE_CLIENT, ['task_id' => $task_id]);
                    break;
                case $status_obj->in_development():
                    $notificationApiResponse = MessengerEvents()->setOuterData('old_status', mb_ucfirst(lng($name1)))->send(MessengerEventCodes::TASK_STATUS_CHANGE_TO_IN_DEVELOPMENT, ['task_id' => $task_id]);
                    break;
                case $status_obj->finalized_not_controlled():
                    $notificationApiResponse = MessengerEvents()->setOuterData('old_status', mb_ucfirst(lng($name1)))->send(MessengerEventCodes::TASK_STATUS_CHANGE_TO_FINALIZED_NOT_CONTROLLED, ['task_id' => $task_id]);
                    break;
                case $status_obj->validated_technical_inspection():
                    $notificationApiResponse = MessengerEvents()->setOuterData('old_status', mb_ucfirst(lng($name1)))->send(MessengerEventCodes::TASK_STATUS_CHANGE_TO_VALIDATED_TECHNICAL_INSPECTION, ['task_id' => $task_id]);
                    break;
                case $status_obj->double_check_validated():
                    $notificationApiResponse = MessengerEvents()->setOuterData('old_status', mb_ucfirst(lng($name1)))->send(MessengerEventCodes::TASK_STATUS_CHANGE_TO_DOUBLE_CHECK_VALIDATED, ['task_id' => $task_id]);
                    break;
                case $status_obj->approved_by_the_customer():
                    $notificationApiResponse = MessengerEvents()->setOuterData('old_status', mb_ucfirst(lng($name1)))->send(MessengerEventCodes::TASK_STATUS_CHANGE_TO_APPROVED_BY_THE_CUSTOMER, ['task_id' => $task_id]);
                    break;
            }
            $notificationData = MessengerEvents()->getOutputData();
        }
        else
            return Response()->error(mb_ucfirst(lng('error')) . ': status was not updated');
        return Response()->data(compact('notificationApiResponse', 'notificationData'))->success();
    }
}