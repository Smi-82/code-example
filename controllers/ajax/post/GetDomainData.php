<?php


namespace controllers\ajax\post;


use Consultant;
use controllers\ajax\BaseAjaxController;
use Domain;
use Templates;

class GetDomainData extends BaseAjaxController
{
    public function run()
    {
        $domain = new Domain((new Consultant())->getDomainByUser_id(clear_post($_POST['client'])));
        return Response()->data([
            'logo' => $domain->getLogo(),
            'bailee' => $domain->getBailee(),
            'phrase' => $domain->getPhrase(),
            'businessAddress' => $domain->getBusinessAddress(),
            'stamp' => $domain->getStamp(),
            'signature' => $domain->getSignature(),
            'footer' => Templates::getTemplate('solution_footer')
        ])->success();
    }
}