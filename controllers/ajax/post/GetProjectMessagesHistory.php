<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Task;

class GetProjectMessagesHistory extends BaseAjaxController
{
    public function run()
    {
        $history = Project()->getMessagesHistoryByProjectId(clear_post($_POST['project_id']));

        $data = [];
//        $data = (new TaskMessageHistoryList())->getDataByTaskId(clear_post($_POST['task_id']));
//        $history = $data['history'];
        $user_ids = array_column($history, 'user_id');
        $users = User()->getManyByIds($user_ids, ['firstname', 'lastname']);

        $history = array_map(function ($item) use ($users) {
            $item['user_firstname'] = $users[$item['user_id']]['firstname'];
            $item['user_lastname'] = $users[$item['user_id']]['lastname'];
            $date_f = date_create($item['date'])->format('d-m-Y H:i');

            switch ($item['action']) {
                case Task::HISTORY_MESSAGES_ACTION_ADD:
                    $item['description'] = [
                        'text' => lngr('task_message_history_description_action_add', implode(' ', [$item['user_firstname'], $item['user_lastname']]), $date_f),
                        'old_message' => null,
                        'added_message' => htmlspecialchars_decode($item['message'])
                    ];
                    break;
                case Task::HISTORY_MESSAGES_ACTION_UPDATE:
                    $item['description'] = [
                        'text' => lngr('task_message_history_description_action_update', implode(' ', [$item['user_firstname'], $item['user_lastname']]), $date_f),
                        'old_message' => htmlspecialchars_decode($item['old_message']),
                        'new_message' => htmlspecialchars_decode($item['message'])
                    ];
                    break;
                case Task::HISTORY_MESSAGES_ACTION_DELETE:
                    $item['description'] = [
                        'text' => lngr('task_message_history_description_action_delete', implode(' ', [$item['user_firstname'], $item['user_lastname']]), $date_f),
                        'old_message' => null,
                        'message' => htmlspecialchars_decode($item['message'])
                    ];
                    break;
            }

            return $item;
        }, $history);

        usort($history, function ($a, $b) {
            return date($b['date']) >= date($a['date']);
        });

        $data['history'] = $history;

        return Response()->data($data)->success();
    }
}