<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use User;

class GetMastersForTeamList extends BaseAjaxController
{
    public function run()
    {
        $sql = 'SELECT u.id, TRIM(CONCAT(u.firstname," ", UPPER(u.lastname))) AS master_name, u.active FROM ' . User::getTable() . ' AS u WHERE u.usertype=' . UserTypes()->getRGPDMaster();
        $masters = query($sql);
        return Response()->data($masters)->success();
    }
}