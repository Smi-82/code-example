<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class GetProjectQuestionnaire extends BaseAjaxController
{
    public function run()
    {
        $res = Project::getQuestionnaire($_POST['project_id']);
        return Response()->data($res)->success();
    }
}