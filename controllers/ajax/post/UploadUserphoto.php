<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class UploadUserphoto extends BaseAjaxController
{
    public function run()
    {
        if (isset($_FILES['myfile'])) {
            $res = json_encode(User()->addFoto($_POST['id']));
            if (intval($_POST['id']) == intval(User()->getId()))
                User()->updateHimself();
            return $res;
        }
    }
}