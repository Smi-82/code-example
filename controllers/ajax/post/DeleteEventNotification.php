<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class DeleteEventNotification extends BaseAjaxController
{
    public function run()
    {
        return Response()->data(MessengerEvents()->deleteNotification($_POST['id']))->success();
    }
}