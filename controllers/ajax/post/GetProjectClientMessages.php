<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class GetProjectClientMessages extends BaseAjaxController
{
    public function run()
    {
        $user_id = $_POST['user_id'];
        $data = array_map(function ($item) use ($user_id) {
            $item['user_img_url'] = getImg(['img' => $item['user_img']], $item['user_id']);
            $item['message_f'] = htmlspecialchars_decode($item['message']);
            return $item;
        }, Project()->getClientMessages($_POST['project_id'], $_POST['user_id']));
        usort($data, function ($a, $b) {
            return date($b['sentat']) >= date($a['sentat']);
        });
        return Response()->data($data)->success();
    }
}