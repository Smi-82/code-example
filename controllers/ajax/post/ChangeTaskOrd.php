<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Task;

class ChangeTaskOrd extends BaseAjaxController
{
    public function run()
    {
        $res = Task::updateOrder($_POST['task_id'], $_POST['ord']);
        return Response()->data($res)->success();
    }
}