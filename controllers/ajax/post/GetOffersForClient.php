<?php


namespace controllers\ajax\post;


use Category;
use controllers\ajax\BaseAjaxController;
use Offer;

class GetOffersForClient extends BaseAjaxController
{
    public function run()
    {
        $offers = Offer::getNotAgreedOfferForClient(User()->getId(), ['project_id', 'id', 'client_agree']);
        if (empty($offers)) {
            return Response()->data([])->success();
        }
        $tmp = [];
        foreach ($offers as $v) {
            $tmp[$v['project_id']] = $v;
        }
        $offers = array_column($offers, 'project_id');
        $proj_obj = Project();
        $offers = $proj_obj->getManyByIds($offers, ['id', 'banner_descr', 'banner_title', 'img', 'price', 'price_type', 'category']);
        $category = array_column($offers, 'category');
        $category = array_unique($category);
        $category = Category::getCategoriesById($category, ['id', 'name']);
        if (isset($_POST['filters']['category'])) {
            $filter = intval($_POST['filters']['category']);
            if ($filter) {
                $offers = array_filter($offers, function ($el) use ($filter) {
                    return intval($el['category']) == $filter;
                });
            }
        }
        foreach ($offers as &$val) {
            $val['img'] = $proj_obj->getImgLink($val['id'], $val['img']);
            $val['banner_descr'] = htmlspecialchars_decode($val['banner_descr']);
            $val['offer_id'] = isset($tmp[$val['id']]) ? $tmp[$val['id']]['id'] : 0;
            $val['client_agree'] = isset($tmp[$val['id']]) ? intval($tmp[$val['id']]['client_agree']) : 0;
            $val['price_type'] = $proj_obj->getPriceType($val['price_type'])['obj'];
        }
        unset($tmp);
        return Response()->data(compact('offers', 'category'))->success();
    }
}