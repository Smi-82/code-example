<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class BindUsersToProject extends BaseAjaxController
{
    public function run()
    {
        $users = $_POST['users'];
        $users = User()->getManyByIds($users, ['id', 'usertype']);
        $fl = true;
        $types = UserTypes();
        foreach ($users as $us_id => $user) {
            switch (intval($user['usertype'])) {
                case $types->getRGPDMaster():
                    if ($fl) {
                        Project()->addMaster($_POST['project'], $us_id);
                        $fl = false;
                    }
                    break;
                default:
                    Project()->addUser($_POST['project'], $us_id);
            }

        }
        return Response()->success();
    }
}