<?php


namespace controllers\ajax\post;


use Consultant;
use controllers\ajax\BaseAjaxController;
use Exception;

class ChangeConsultantMaster extends BaseAjaxController
{
    public function run()
    {
        try {
            $rjson = Consultant::changeMasterForConsultants($_POST['master'], explode(',', $_POST['to']));
            return Response()->data($rjson)->success();
        } catch (Exception $e) {
            return Response()->error($e->getMessage());
        }
    }
}