<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use TaskScreenshot;

class DeleteScreenshot extends BaseAjaxController
{
    public function run()
    {
        TaskScreenshot::delete($_POST['id']);
        return Response()->success();
    }
}