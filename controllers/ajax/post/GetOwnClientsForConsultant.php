<?php


namespace controllers\ajax\post;


use Consultant;
use controllers\ajax\BaseAjaxController;

class GetOwnClientsForConsultant extends BaseAjaxController
{
    public function run()
    {
        $obj = new Consultant();
        return Response()->data(array_column($obj->getOwnClientsForConsultant($_POST['master'], ['id']), 'id'))->success();
    }
}