<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Task;

class ChangeTaskName extends BaseAjaxController
{
    public function run()
    {
        Task::updateTasksData($_POST['task_id'], ['name' => $_POST['task_name']]);
        return Response()->data(escape_string(clear_post($_POST['task_name'])))->success();
    }
}