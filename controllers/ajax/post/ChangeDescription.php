<?php


namespace controllers\ajax\post;


use appEvents\AppEventsController;
use appEvents\subscribers\controller\AppEventsSubscribersController;
use controllers\ajax\BaseAjaxController;
use Task;
use TasksMessages;

class ChangeDescription extends BaseAjaxController
{
    public function run()
    {
        $msg_id = intval(escape_string(clear_post($_POST['msg_id'])));
        if ($msg_id) {
            $res = TasksMessages::updateByIdAndUserId($msg_id, User()->getId(), [
                'message' => $_POST['desc'],
                'changeat' => date('Y-m-d H:i:s')
            ]);
        }
        else
            $res = TasksMessages::add(
                [
                    'user_id' => User()->getId(),
                    'task_id' => $_POST['task_id'],
                    'message' => $_POST['desc']
                ]
            );
        if ($res) {
            AppEventsController::publish(AppEventsSubscribersController::taskCommentsEvent(), ['task_id' => $_POST['task_id']]);
            if (isset($_POST['unset_section']))
                Task::updateSectionComment(clear_post($_POST['task_id']), clear_post($_POST['unset_section']), false);
            return Response()->data(User()->getSignature() . ', (' . date('d - m - Y H:i') . ')')->success();
        }
        else
            return Response()->error(mb_ucfirst(lng('error')) . ': Description was not updated!');
    }
}