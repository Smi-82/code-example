<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Exception;
use User;

class BindMastersToPaymentSystem extends BaseAjaxController
{
    public function run()
    {
        $masters = empty($_POST['masters']) ? 0 : implode(', ', $_POST['masters']);
        $sql_arr = [
            update_query_string(User::getTable(), ['payment_system_id' => 0]) . ' WHERE payment_system_id = ' . clear_post($_POST['payment_id']) . ' AND usertype = ' . UserTypes()->getRGPDMaster(),
            update_query_string(User::getTable(), ['payment_system_id' => $_POST['payment_id']]) . ' WHERE id IN(' . $masters . ')'
        ];
        if (!$masters)
            unset($sql_arr[1]);
        try {
            foreach ($sql_arr as $sql)
                query($sql);
            return Response()->success();
        } catch (Exception $e) {
            return Response()->error($e->getMessage());
        }
    }
}