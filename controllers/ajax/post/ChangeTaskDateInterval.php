<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Exception;
use Task;

class ChangeTaskDateInterval extends BaseAjaxController
{
    public function run()
    {
        try {
            $result = Task::getById($_POST['task_id'], ['start', 'estimateTime']);
            extract($result);
            $estimate = intval($estimate) + 57600;
            if ((strtotime($_POST['estimateTime']) - strtotime($start)) >= $estimate) {
                Task::updateTasksData($_POST['task_id'], ['estimateTime' => date('Y-m-d H:i', strtotime($_POST['estimateTime']))]);
                taskHistory($_POST['task_id'], 'Change task date interval');
                return Response()->success();
            }
            else
                return Response()->error(mb_ucfirst(lng('the_date_can_not_be_less_than')) . ' ' . date('d-m-Y H:i', strtotime($start) + $estimate + 57600));
        } catch (Exception $e) {
            return Response()->error($e->getMessage());
        }
    }
}