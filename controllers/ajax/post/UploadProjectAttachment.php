<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class UploadProjectAttachment extends BaseAjaxController
{
    public function run()
    {
        return Response()->data(Project()->addClientAttachment($_POST['project_id'], $_POST['user_id']))->success();
    }
}