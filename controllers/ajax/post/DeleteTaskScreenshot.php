<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use TaskScreenshot;

class DeleteTaskScreenshot extends BaseAjaxController
{
    public function run()
    {
        TaskScreenshot::delete(clear_post($_POST['id']));
        return Response()->success();
    }
}