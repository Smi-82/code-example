<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class CloneGroupOnly extends BaseAjaxController
{
    public function run()
    {
        Group()->cloneGroup($_POST['group']);
        return Response()->success();
    }
}