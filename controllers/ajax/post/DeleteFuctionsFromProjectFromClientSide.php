<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use ProjectFunctions;

class DeleteFuctionsFromProjectFromClientSide extends BaseAjaxController
{
    public function run()
    {
        ProjectFunctions::delete($_POST['id']);
        return Response()->success();
    }
}