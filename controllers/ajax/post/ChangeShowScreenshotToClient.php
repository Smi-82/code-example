<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use TaskScreenshot;

class ChangeShowScreenshotToClient extends BaseAjaxController
{
    public function run()
    {
        TaskScreenshot::update($_POST['id'], $_POST['data']);
        return Response()->success();
    }
}