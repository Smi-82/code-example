<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class AddTasksFromClientSide extends BaseAjaxController
{
    public function run()
    {
        $tasks = $_POST['data']['tasks'];
        $res = [];
        if (!empty($tasks)) {
            foreach ($tasks as $task) {
                $res[] = Task()->addNewTask($task);
            }
        }
        $err = array_filter($res, function ($el) {
            return !empty($el['errors']);
        });
        return empty($err) ? Response()->success() : Response()->data(array_column($res, 'errors'))->error();
    }
}