<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class UpdatePosition extends BaseAjaxController
{
    public function run()
    {
        return Response()->data(Position()->update(clear_post($_POST['id']), $_POST['data']))->success();
    }
}