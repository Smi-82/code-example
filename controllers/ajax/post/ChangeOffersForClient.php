<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Offer;

class ChangeOffersForClient extends BaseAjaxController
{
    public function run()
    {
        $res = empty($_POST['offers']) ? Offer::deleteClientFromOffer($_POST['client']) : Offer::changeOffersForClient($_POST['client'], $_POST['offers']);
        return $res ? Response()->data($res)->success() : Response()->data($res)->error();
    }
}