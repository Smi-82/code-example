<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Task;

class PriceType extends BaseAjaxController
{
    public function run()
    {
        $res = Task::updateTasksData($_POST['task_id'], ['price' => $_POST['price'], 'price_type' => $_POST['price'] ? $_POST['price_type'] : 0]);
        if ($res) {
            taskHistory($_POST['task_id'], 'Change price');
        }
        return $res ? Response()->data($_POST['price'] ? 1 : 0)->success() : Response()->error(mb_ucfirst(lng('server_error')) . '!');
    }
}