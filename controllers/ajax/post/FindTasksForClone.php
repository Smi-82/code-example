<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Exception;
use Project;
use Task;

class FindTasksForClone extends BaseAjaxController
{
    public function run()
    {
        $tasks_params = ['id', 'cloned_from'];
        if (User()->isSupervisor()) {
            $tasks = Task()->getAll($tasks_params);
        }
        else {
            $projects = Project::getProjectListForUser(User()->getId());
            $tasks = Project::getOwnTasks($projects, $tasks_params);
        }
        $tasks = array_filter($tasks, function ($elem) {
            return intval($elem['cloned_from']) == 0;
        });

        $tasks = array_column($tasks, 'id');

        $where = '';
        if ($_POST['filter']['val'] != '') {
            $where = ' name LIKE ' . '"%' . escape_string(clear_post($_POST['filter']['val'])) . '%"';
            if ($_POST['filter']['category'] != 0) {
                $where .= ' AND ';
            }
        }
        if ($_POST['filter']['category'] != 0) {
            $where .= ' category= ' . escape_string(clear_post($_POST['filter']['category']));
        }

        $sql = Task::baseSelect() . ' WHERE for_cloning=1 AND id IN (' . implode(',', $tasks) . ') ';
        if ($where != '')
            $sql .= ' AND (' . $where . ') ';
        $sql .= ' ORDER BY id ASC';
        try {
            $res = query($sql);
            $res = array_map(function ($elem) {
                if (isset($elem['name']))
                    $elem['name'] = html_entity_decode($elem['name']);
                if (isset($elem['description'])) {
                    $elem['description'] = html_entity_decode($elem['description']);
                    $elem['description'] = strip_tags($elem['description']);
                }

                return $elem;
            }, $res);
        } catch (Exception $e) {
            $res = $e->getMessage();
        }
        return Response()->data($res)->success($sql);
    }
}