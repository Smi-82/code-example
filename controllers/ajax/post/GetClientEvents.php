<?php


namespace controllers\ajax\post;


use ClientActivity;
use controllers\ajax\BaseAjaxController;

class GetClientEvents extends BaseAjaxController
{
    public function run()
    {
        $res = ClientActivity::getData();
        return Response()->data($res)->success();
    }
}