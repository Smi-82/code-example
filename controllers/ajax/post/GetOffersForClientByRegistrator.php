<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Offer;

class GetOffersForClientByRegistrator extends BaseAjaxController
{
    public function run()
    {
        $offers = Offer::getOffersForClientByRegistrator(User()->getId(), ['project_id', 'id', 'client_agree']);

        $tmp = [];
        foreach ($offers as $v) {
            $tmp[$v['project_id']] = $v;
        }
        $offers = array_column($offers, 'project_id');
        $offers = Project()->getManyByIds($offers, ['id', 'banner_descr', 'banner_title', 'img']);
        foreach ($offers as &$val) {
            $val['img'] = Project()->getImgLink($val['id'], $val['img']);
            $val['banner_descr'] = htmlspecialchars_decode($val['banner_descr']);
            $val['offer_id'] = isset($tmp[$val['id']]) ? $tmp[$val['id']]['id'] : 0;
            $val['client_agree'] = isset($tmp[$val['id']]) ? intval($tmp[$val['id']]['client_agree']) : 0;
        }
        unset($tmp);
        return Response()->data($offers)->success();
    }
}