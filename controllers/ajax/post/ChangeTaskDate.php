<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Exception;
use Task;

class ChangeTaskDate extends BaseAjaxController
{
    public function run()
    {
        $result = Task::getById($_POST['task_id'], ['start', 'estimateTime', 'created']);
        $created = $result['created'];
        unset($result['created']);
        if ((strtotime($result['start']) + intval($_POST['step'])) < strtotime($created))
            return Response()->error(date('d-m-Y', strtotime($created)));
        foreach ($result as &$val)
            $val = date('Y-m-d H:i', strtotime($val) + intval($_POST['step']));
        try {
            $res = Task::updateTasksData($_POST['task_id'], $result);
            return $res ? Response()->success() : Response()->error(mb_ucfirst(lng('date_was_not_changed')));
        } catch (Exception $e) {
            return Response()->error($e->getMessage());
        }
    }
}