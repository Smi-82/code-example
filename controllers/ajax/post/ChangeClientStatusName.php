<?php


namespace controllers\ajax\post;


use ClientStatus;
use controllers\ajax\BaseAjaxController;

class ChangeClientStatusName extends BaseAjaxController
{
    public function run()
    {
        $res = ClientStatus::update($_POST['id'], ['name' => $_POST['name']]);
        return is_bool($res) && $res ? Response()->success() : Response()->error($res);
    }
}