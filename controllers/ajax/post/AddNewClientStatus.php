<?php


namespace controllers\ajax\post;


use ClientStatus;
use controllers\ajax\BaseAjaxController;

class AddNewClientStatus extends BaseAjaxController
{
    public function run()
    {
        $res = ClientStatus::add($_POST['name']);
        return !is_bool($res) && intval($res) != 0 ? Response()->data($res)->success() : Response()->error($res);
    }
}