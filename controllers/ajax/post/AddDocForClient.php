<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class AddDocForClient extends BaseAjaxController
{
    public function run()
    {
        return Response()->data(User()->addDocForClient($_POST['user_id'], $_POST['doc']))->success();
    }
}