<?php


namespace controllers\ajax\post;


use abstractClasses\TableData;
use controllers\ajax\BaseAjaxController;
use Project;
use ProjectListPage;

class GetClientProjects extends BaseAjaxController
{
    public function run()
    {
        $obj = new ProjectListPage();
        $result = $obj->getTableData();
        $result['projects'] = array_filter($result['projects'], function ($el) {
            return intval($el['is_template']) == 0 && intval($el['is_offer']) == 0;
        });
        $result['projects'] = array_values($result['projects']);
        if (isset($result['projects']) && !empty($result['projects'])) {
            $project_ids = array_column($result['projects'], 'id');
            $tasks = Project::getOwnTasks($project_ids, ['id', 'status']);
            foreach ($tasks as $key => $val) {
                $p_id = $val['_p_id'];
                unset($val['_p_id']);
                $tasks[$p_id][] = $val;
                unset($tasks[$key]);
            }
            $statuses = Statuses();
            $userTypes = UserTypes()->getAll();
            $userTypes = valueToKey('id', $userTypes);
            foreach ($result['projects'] as &$val) {
                $val['participants'] = array_map(function ($el) use ($userTypes) {
                    $el['usertype_name'] = $userTypes[$el['usertype']]['name'];
                    return $el;
                }, $val['participants']);
                $val['dateCreated'] = date('d-m-Y H:i', strtotime($val['dateCreated']));
                $val['tasks_total'] = isset($tasks[$val['id']]) ? count($tasks[$val['id']]) : 0;
                $val['tasks_ongoing'] = isset($tasks[$val['id']]) ? count(array_filter($tasks[$val['id']], function ($task) use ($statuses) {
                    return !in_array($task['status'], [$statuses->double_check_validated(), $statuses->approved_by_the_customer()]);
                })) : 0;
                $val['tasks_compl'] = $val['tasks_total'] - $val['tasks_ongoing'];
                switch ($val['status']) {
                    case $statuses->in_preparation():
                    case $statuses->waiting_for_validation_by_the_client():
                    case $statuses->in_development():
                    case $statuses->finalized_not_controlled():
                    case $statuses->validated_technical_inspection():
                        $val['status'] = 1;
                        break;
                    case $statuses->double_check_validated():
                    case $statuses->approved_by_the_customer():
                        $val['status'] = 2;
                        break;
                }
            }
        }
        return Response()->data($result)->success();
    }
}