<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Exception;

class Recall extends BaseAjaxController
{
    public function run()
    {
        $rjson = Response()->error(mb_ucfirst(lng('specify_the_phone')));
        if (!empty($_POST['phone'])) {
            $sql = insert_query_string('recall', ['phone' => $_POST['phone']], true);
            try {
                $rjson = query($sql) ? Response()->success() : Response()->error(mb_ucfirst(lng('application_not_added')));
            } catch (Exception $e) {
                $rjson = Response()->error($e->getMessage());
            }
        }
        return $rjson;
    }
}