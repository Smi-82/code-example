<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use OurCompany;

class GetOurCompanyDetails extends BaseAjaxController
{
    public function run()
    {
        return Response()->data(OurCompany::getDetails())->success();
    }
}