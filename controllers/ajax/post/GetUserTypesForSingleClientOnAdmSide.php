<?php

namespace controllers\ajax\post;

use controllers\ajax\BaseAjaxController;
use User;

class GetUserTypesForSingleClientOnAdmSide extends BaseAjaxController
{
    public function run()
    {
        $inst = UserTypes();
        $res = $inst->getAll();
        foreach ($res as $k => $v) {
            if ($v['id'] == $inst->getClient()) {
                unset($res[$k]);
                break;
            }
        }
        $show = User::getById($_POST['client_id'], ['show_the_client']);
        $show = htmlspecialchars_decode($show['show_the_client']);

        if (!empty($show)) {
            $show = json_decode($show, true);
            $res = array_map(function ($el) use ($show) {
                $el['set_show'] = isset($show[$el['id']]);
                if ($el['set_show'])
                    $el['show_the_client'] = $show[$el['id']];
                return $el;
            }, $res);
        }
        $res = array_values($res);
        return Response()->data($res)->success();
    }
}