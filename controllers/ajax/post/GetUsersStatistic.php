<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use UsersStatisticPage;

class GetUsersStatistic extends BaseAjaxController
{
    public function run()
    {
        $obj = new UsersStatisticPage();
        $result = $obj->getTableData();
        return isset($result['error']) ? Response()->error($result['error']) : Response()->data($result)->success();
    }
}