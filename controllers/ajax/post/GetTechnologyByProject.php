<?php


namespace controllers\ajax\post;


use CodeLanguage;
use controllers\ajax\BaseAjaxController;

class GetTechnologyByProject extends BaseAjaxController
{
    public function run()
    {
        $current = Project()->getById($_POST['project'], ['languages']);
        $current = $current['languages'];
        if ($current != 'null') {
            $current = json_decode($current);
            if (is_array($current)) {
                natsort($current);
                $current = array_values($current);
            }
            else
                $current = [];
        }
        else
            $current = [];
        $select = CodeLanguage::getLng();
        $select = array_values($select);
        if (!empty($current))
            $select = array_diff($select, $current);
        $select = array_values($select);
        return Response()->data(compact('current', 'select'))->success();
    }
}