<?php


namespace controllers\ajax\post;


use CollaboratorProjectAccess;
use controllers\ajax\BaseAjaxController;

class AddOldCollaborators extends BaseAjaxController
{
    public function run()
    {
        $collaborators = $_POST['collaborators'];
        $collaborators = clearParams($collaborators);
        foreach ($collaborators as $coll_id) {
            $res = CollaboratorProjectAccess::add($coll_id, $_POST['general_project_id']);
            if ($res == false)
                return Response()->error(mb_ucfirst(lng('error')) . ' ' . mb_ucfirst(lng('collaborator_was_not_added')));

        }
        return Response()->success();
    }
}