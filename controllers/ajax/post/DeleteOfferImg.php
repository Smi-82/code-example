<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class DeleteOfferImg extends BaseAjaxController
{
    public function run()
    {
        return Response()->data(Project()->deleteImg($_POST['id']))->success();
    }
}