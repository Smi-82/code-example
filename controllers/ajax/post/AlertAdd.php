<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use TaskAlert;
use User;

class AlertAdd extends BaseAjaxController
{
    public function run()
    {
        $type = escape_string($_POST['type']);
        $user_id = escape_string($_POST['user_id']);
        $task_id = escape_string($_POST['task_id']);
        if (TaskAlert::add($user_id, $task_id, $type)) {
            $temp = User::getById($user_id);
            history('task', User()->getSignature() . ' created alert request about participant status to ' . $temp['firstname'] . ' ' . $temp['lastname'] . ' - task ID: ' . $task_id);
            if (!TaskAlert::taskHasAlertType2($task_id)) TaskAlert::deleteByUserTaskType(User()->getId(), $task_id, 2);
            return Response()->success();
        }
        else
            return Response()->error();
    }
}