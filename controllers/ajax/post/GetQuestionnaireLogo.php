<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Project;

class GetQuestionnaireLogo extends BaseAjaxController
{
    public function run()
    {
        return Project::getQuestionnaireFiles($_POST['project_id']);
    }
}