<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Group;

class ChangeGroup extends BaseAjaxController
{
    public function run()
    {
        $res = Group::updateParent($_POST['current_gr'], $_POST['parent_gr']);
        return !is_string($res) ? Response()->success() : Response()->error($res);
    }
}