<?php

namespace controllers\ajax\post;

use controllers\ajax\BaseAjaxController;
use UserTypes;

class ChangeShowTypeAllClientsFromAdm extends BaseAjaxController
{
    public function run()
    {
        $res = UserTypes::setShowTheClient($_POST['id'], $_POST['show']);
        return $res ? Response()->success() : Response()->error($res);
    }
}