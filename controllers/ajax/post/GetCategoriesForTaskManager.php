<?php


namespace controllers\ajax\post;


use Category;
use controllers\ajax\BaseAjaxController;

class GetCategoriesForTaskManager extends BaseAjaxController
{
    public function run()
    {
        $res = Category::getAllCategories();
        return is_array($res) ? Response()->data($res)->success() : Response()->error($res);
    }
}