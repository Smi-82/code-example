<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class DeleteQuestionnaireLogo extends BaseAjaxController
{
    public function run()
    {
        return Project::deleteQuestionnaireFiles($_POST['project_id'], $_POST['file']);
    }
}