<?php


namespace controllers\ajax\post;



use controllers\ajax\BaseAjaxController;
use Exception;
use TaskAccess;

class GetBindEmployeTasks extends BaseAjaxController
{
    public function run()
    {
        try {
            $res = TaskAccess::getByUserId($_POST['user_id'], ['task_id']);
            $res = array_column($res, 'task_id');
        } catch (Exception $e) {
            return Response()->error($e->getMessage());
        }
        return Response()->data($res)->success();
    }
}