<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class GetFriendlyUrl extends BaseAjaxController
{
    public function run()
    {
        if ($_POST['alias'] == '') {
            return false;
        }
        return strtolower(fr2lat(clear_post($_POST['alias'])));
    }
}