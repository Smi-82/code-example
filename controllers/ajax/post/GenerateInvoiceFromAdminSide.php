<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class GenerateInvoiceFromAdminSide extends BaseAjaxController
{
    public function run()
    {
        if (!User()->isSuperAdmin()) {
            return Response()->error();
        }
        $res = BillProject()->readTOS($_POST['bdc']);
        if ($res) {
            $res = BillProject()->approve($_POST['bdc']);
        }
        return $res ? Response()->success() : Response()->error();
    }
}