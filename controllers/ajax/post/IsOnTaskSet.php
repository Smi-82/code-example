<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Task;

class IsOnTaskSet extends BaseAjaxController
{
    public function run()
    {
        Task::taskReloadForAccessUser($_POST['task_id'], User()->getId());
        return Response()->success();
    }
}