<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class DeleteTaskInsideMessage extends BaseAjaxController
{
    public function run()
    {
        return Response()->data(Task()->deleteInsideMessage(clear_post($_POST['msg_id'])))->success();
    }
}