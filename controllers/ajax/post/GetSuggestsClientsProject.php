<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use User;

class GetSuggestsClientsProject extends BaseAjaxController
{
    public function run()
    {
        $search_key = escape_string(clear_post($_POST['search_key']));
        $result_clients = [];
        if (!empty($search_key)) {
            $sql = "SELECT a.firstname as name,
                a.lastname as lastname,
                a.img as img,
                a.id as id
                FROM " . User::getTable() . " a
                WHERE 1 and (a.firstname LIKE '%$search_key%' OR a.lastname LIKE '%$search_key%' OR CONCAT_WS(' ', a.firstname, a.lastname) LIKE '%$search_key%') and a.usertype = " . UserTypes()->getClient();
            $result_clients = query($sql);
        }
        if (!empty($result_clients)) {
            foreach ($result_clients as &$val) {
                $val['img'] = getImg($val, $val['id']);
            }
        }
        return Response()->data($result_clients)->success();
    }
}