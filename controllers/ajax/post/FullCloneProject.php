<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class FullCloneProject extends BaseAjaxController
{
    public function run()
    {
        $res = Project()->fullClone($_POST['id']);
        return empty($res['error']) ? Response()->data($res)->success() : Response()->data(['place' => 'console'])->error($res['error']);
    }
}