<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use User;
use UserTypes;

class GetSuggestsClients extends BaseAjaxController
{
    public function run()
    {
        $data['search_key'] = $_POST['search_key'];
        $search_key = clear_post($data['search_key']);
        $strict = substr($search_key, 0, 1) == '(' && substr($search_key, -1) == ')';
        $oneword = count(explode(' ', $search_key)) == 1;
        if ($strict) {
            $search_key = ltrim($search_key, '(');
            $search_key = rtrim($search_key, ')');
        }

        if ($strict) {
            $sql = "SELECT a.firstname as name,
                    a.lastname as lastname,
                    a.id as id
                    FROM " . User::getTable() . " a
                    WHERE 1 and (a.firstname LIKE '$search_key' OR a.lastname LIKE '$search_key' OR CONCAT_WS(' ', a.firstname, a.lastname) LIKE '$search_key')
                    ORDER BY firstname, lastname
                ";
        }
        elseif ($oneword) {
            $sql = "SELECT a.firstname as name,
                    a.lastname as lastname,
                    a.id as id
                    FROM " . User::getTable() . " a
                    WHERE 1 and (CONCAT_WS(' ', a.firstname, a.lastname) LIKE '%$search_key%')
                    ORDER BY firstname, lastname
                ";
        }
        else {
            $sql = "SELECT CONCAT_WS(' ', a.firstname, a.lastname) as name,
                    a.id as id
                    FROM " . User::getTable() . " a WHERE 1;
                ";

            $result = query($sql);

            $ids = getIdsByKey($result, $search_key);

            $sql = "SELECT a.firstname as name,
                    a.lastname as lastname,
                    a.id as id
                    FROM " . User::getTable() . " a WHERE a.id IN ($ids) ORDER BY firstname, lastname
                ";
        }

        $result_clients = query($sql);

        if ($strict) {
            $sql = "SELECT a.name as name,
                    a.id as id
                    FROM " . UserTypes::getTable() . " a
                    WHERE 1 and (a.name LIKE '$search_key')
                    ORDER BY name
                ";
        }
        elseif ($oneword) {
            $sql = "SELECT a.name as name,
                    a.id as id
                    FROM " . UserTypes::getTable() . " a
                    WHERE 1 and (a.name LIKE '%$search_key%')
                    ORDER BY name
                ";
        }
        else {
            $sql = "SELECT a.name as name,
                    a.id as id
                    FROM " . UserTypes::getTable() . " a WHERE 1;
                ";
            $result = query($sql);

            $ids = getIdsByKey($result, $search_key);

            $sql = "SELECT a.name as name,
                    a.id as id
                    FROM " . UserTypes::getTable() . " a WHERE a.id IN ($ids) ORDER BY name
                ";
        }
        $result_usertypes = query($sql);
        $items = array(
            'clients' => array(),
            'usertypes' => array()
        );
        $html = '';
        $item_empty = "<li class = 'search-suggest-item-empty'>Aucun résultat trouvé</li>";
        if (count($result_usertypes)) {
            foreach ($result_usertypes as $row2) {
                $items['usertypes'][] = "<li class = 'search-suggest-item-found' data-nofilter data-page = 'utilisateurs.php' data-skey = '" . $row2['id'] . "' data-s = 'usertype ' data-setype = 'client' data-sepref = 'a'>" . $row2['name'] . "</li>";
            }
        }
        if (count($result_clients)) {
            foreach ($result_clients as $row2) {
                $items['clients'][] = "<li class = 'search-suggest-item-found' data-nofilter data-page = 'utilisateurs.php' data-skey = '" . $row2['id'] . "' data-s = 'id' data-setype = 'client' data-sepref = 'a'>" . $row2['name'] . ' ' . $row2['lastname'] . "</li>";
            }
        }
        $items['clients'] = count($items['clients']) ? $items['clients'] : array($item_empty);
        $items['usertypes'] = count($items['usertypes']) ? $items['usertypes'] : array($item_empty);
        $html = "
                <li class = 'search-suggest-category'>Clients</li>%clients%
                <li class = 'search-suggest-category'>" . mb_ucfirst(lng('usertypes')) . "</li>%usertypes%
                ";
        $html = str_replace('%clients%', implode('', $items['clients']), $html);
        $html = str_replace('%usertypes%', implode('', $items['usertypes']), $html);
        return $html;
    }
}