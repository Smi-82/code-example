<?php

namespace controllers\ajax\post\dateOfCalls;

use controllers\ajax\BaseAjaxController;
use DatesOfCalls;

class GetDateOfCallsForClient extends BaseAjaxController
{
    public function run()
    {
        $res = DatesOfCalls::getAll();
        return is_array($res) ? Response()->data($res)->success() : Response()->error($res);
    }

    public function forCurrent()
    {
        $res = DatesOfCalls::getByWhoAdd(User()->getId());
        return is_array($res) ? Response()->data($res)->success() : Response()->error($res);
    }
}