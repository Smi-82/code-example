<?php

namespace controllers\ajax\post\dateOfCalls;

use controllers\ajax\BaseAjaxController;
use DatesOfCalls;

class AddDateOfCalls extends BaseAjaxController
{
    public function run()
    {
        $res = DatesOfCalls::add(['date' => $_POST['date'], 'client_id'=>$_POST['client'], 'seller'=>$_POST['seller']]);
        return $res ? Response()->data($res)->success() : Response()->error($res);
    }
}