<?php

namespace controllers\ajax\post\dateOfCalls;

use controllers\ajax\BaseAjaxController;
use DatesOfCalls;

class RemoveDateOfCall extends BaseAjaxController
{
    public function run()
    {
        $res = DatesOfCalls::deleteById($_POST['id']);
        return $res ? Response()->data($res)->success() : Response()->error($res);
    }
}