<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Task;

class MoveTaskToGroup extends BaseAjaxController
{
    public function run()
    {
        $res = Task::updateTasksData($_POST['task_id'], ['group_id' => $_POST['group_id']]);
        if ($res)
            taskHistory($_POST['task_id'], 'Move task to group ' . $_POST['group_id']);
        return $res ? Response()->success() : Response()->error(mb_ucfirst(lng('error')) . ': ' . lng('task_was_not_changed') . '!');
    }
}