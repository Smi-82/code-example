<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use PaymentPartsProjectBill;

class GetPaymentParts extends BaseAjaxController
{
    public function run()
    {
        $res = PaymentPartsProjectBill::getByBillId($_POST['bill']);
        return Response()->data($res)->success();
    }
}