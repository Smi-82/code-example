<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class EditProjectClientComment extends BaseAjaxController
{
    public function run()
    {
        return Response()->data(Project()->editClientMessage(clear_post($_POST['message_id']), $_POST['desc']))->success();
    }
}