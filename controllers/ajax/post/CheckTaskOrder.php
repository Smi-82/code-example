<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Task;

class CheckTaskOrder extends BaseAjaxController
{
    public function run()
    {
        $res = Task::isBusyOrder($_POST['task_id'], $_POST['ord']);
        return Response()->data($res)->success();
    }
}