<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use ProjectFunctions;

class UpdateProjectFuction extends BaseAjaxController
{
    public function run()
    {
        $res = ProjectFunctions::update($_POST['id'], $_POST['data']);
        return $res ? Response()->data($res)->success() : Response()->error();
    }
}