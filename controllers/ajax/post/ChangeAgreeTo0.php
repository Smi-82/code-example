<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Offer;

class ChangeAgreeTo0 extends BaseAjaxController
{
    public function run()
    {
        $res = Offer::changeClientAgreeById($_POST['id']);
        return $res !== false ? Response()->success() : Response()->error();
    }
}