<?php


namespace controllers\ajax\post;


use ClientMenuManager;
use controllers\ajax\BaseAjaxController;

class GetClientMenu extends BaseAjaxController
{
    public function run()
    {
        $result = new ClientMenuManager($_POST['client_id']);
        return Response()->data($result->getMenuValue())->success();
    }
}