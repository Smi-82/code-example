<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use ProjectFunctions;

class GetProjectFunctions extends BaseAjaxController
{
    public function run()
    {
        $res = ProjectFunctions::getOneByProjectId($_POST['project']);
        return Response()->data($res)->success();
    }
}