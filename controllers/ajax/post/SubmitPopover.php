<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Task;

class SubmitPopover extends BaseAjaxController
{
    public function run()
    {
        if ($_POST['comment'] == '' || $_POST['section'] == '' || $_POST['task_id'] == '') {
            return Response()->error(mb_ucfirst(lng('error')) . ': ' . lng('missed_required_fields'));
        }

        $section = clear_post($_POST['section']);
        $task_id = clear_post($_POST['task_id']);
        $add_section = ['section' => $section, 'comment' => clear_post($_POST['comment']), 'show' => 1];
        if (Task::updateSectionComment($task_id, $section, $add_section)) {
            history('task', User()->getSignature() . ' added new message for "' . clear_post($_POST['title']) . '" cell. Message : "' . $add_section['comment'] . '" Task ID : ' . $task_id);
            return Response()->success();
        }
        else {
            return Response()->error(mb_ucfirst(lng('error_occured')) . ' ' . lng('during_insert_data_in_database'));
        }
    }
}