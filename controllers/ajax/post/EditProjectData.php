<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class EditProjectData extends BaseAjaxController
{
    public function run()
    {
        if (intval($_POST['project_id']) == 0) {
            return Response()->error('project id = 0');
        }
        $res = Project()->editProjectData($_POST['project_id'], $_POST['projectData']);
        return $res['error'] ? Response()->error($res['msg']) : Response()->data($res)->success();
    }
}