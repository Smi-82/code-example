<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class GetBDCInvoiceListsForAdminSideForClient extends BaseAjaxController
{
    public function run()
    {
        $projects = Project()->getProjectsByClientId($_POST['client'], ['id', 'name']);
        if (empty($projects))
            return Response()->data([])->success();
        $obj = BillProject();
        $obj_inv = InvoiceProject();
        $proj_ids = array_column($projects, 'id');
        $bills = $obj->getLastBDCByProjectId($proj_ids, ['id', 'created', 'project_id']);
        if (!empty($bills)) {
            $bills = array_map(function ($el) use ($obj) {
                $el['href'] = $obj->getHrefPDF($el['id'], $el['project_id']);
                $el['name'] = $obj->createNameBDC($el['id'], $el['created']);
                return $el;
            }, $bills);
            $bills = valueToKey('id', $bills);
        }

        $invoices = $obj_inv->getByProjectId($proj_ids, ['id', 'project_id']);
        if (!empty($invoices)) {
            $invoices = array_map(function ($el) use ($obj_inv) {
                $el['href'] = $obj_inv->getHrefPDF($el['id'], $el['project_id']);
                $el['name'] = $obj_inv->createFileName($el['id']);
                $el['generate'] = false;
                return $el;
            }, $invoices);
            $invoices = valueToKey('id', $invoices);
        }
        if (!empty($bills)) {
            foreach ($bills as $k => $v) {
                if (!isset($invoices[$k])) {
                    $invoices[$k] = ['id' => $v['id'], 'project_id' => $v['project_id'], 'generate' => true];
                }
            }
        }
        $projects = array_map(function ($el) use ($bills, $invoices) {
            $el['bdc'] = array_filter($bills, function ($v) use ($el) {
                return $v['project_id'] == $el['id'];
            });
            $el['bdc'] = array_values($el['bdc']);
            $el['inv'] = array_filter($invoices, function ($v) use ($el) {
                return $v['project_id'] == $el['id'];
            });
            $el['inv'] = array_values($el['inv']);
            return $el;
        }, $projects);
        return Response()->data($projects)->success();
    }
}