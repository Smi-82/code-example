<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use User;

class CustomerAvailabilityCheck extends BaseAjaxController
{
    public function run()
    {
        $data = $_POST['data'];
        $data = array_filter($data);
        $res = User::customer_availability($data, ['id', 'domain', 'societe', 'phone', 'phone2']);
        if (is_array($res)) {
            if (empty($res)) {
                return Response()->data(['type' => 'success', 'msg' => ucfirst(lng('information_for_the_mentioned_fields_hasn_t_been_recorded'))])->success();
            }
            if (isset($data['domain'])) {
                $domain = array_filter($res, function ($el) use ($data) {
                    return $el['domain'] == $data['domain'];
                });
                if (!empty($domain)) {
                    return Response()->data(['type' => 'error', 'msg' => ucfirst(lng('this_domain_name_has_already_been_registered'))])->success();
                }
            }
            if (isset($data['societe'])) {
                $societe = array_filter($res, function ($el) use ($data) {
                    return $el['societe'] == $data['societe'];
                });
                if (!empty($societe)) {
                    return Response()->data(['type' => 'error', 'msg' => ucfirst(lng('this_company_name_has_already_been_registered'))])->success();
                }
            }
            if (isset($data['phone'])) {
                $societe = array_filter($res, function ($el) use ($data) {
                    return in_array($data['phone'], [$el['phone'], $el['phone2']]);
                });
                if (!empty($societe)) {
                    return Response()->data(['type' => 'error', 'msg' => ucfirst(lng('this_phone_number_has_already_been_registered'))])->success();
                }
            }
        }
        return Response()->error($res);
    }
}