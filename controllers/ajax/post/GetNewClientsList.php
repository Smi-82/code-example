<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use NewClientsList;

class GetNewClientsList extends BaseAjaxController
{
    public function run()
    {
        $obj = new NewClientsList();
        $clients = $obj->getTableData();
        return isset($clients['error']) ? Response()->error($clients['error']) : Response()->data($clients)->success();
    }
}