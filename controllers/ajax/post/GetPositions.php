<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class GetPositions extends BaseAjaxController
{
    public function run()
    {
        $lang = Language()->getLanguage();
        $nameKey = 'name_' . $lang;

        $positions = Position()->getAll();

        $positionTitleId = array_unique(array_column($positions, 'position_title_id'));
        $positionCategoryId = array_unique(array_column($positions, 'position_category_id'));
        $positionSpecialityId = array_unique(array_column($positions, 'position_speciality_id'));

//        $positionTitles = PositionTitle()->getById($positionTitleId, ['id', $nameKey]);
//        $positionCategories = PositionCategory()->getById($positionCategoryId, ['id', $nameKey]);
//        $positionSpecialities = PositionSpeciality()->getById($positionSpecialityId, ['id', $nameKey]);

        $positionTitles = PositionTitle()->getById($positionTitleId, ['id', 'name_fr', 'name_en', 'name_ru']);
        $positionCategories = PositionCategory()->getById($positionCategoryId, ['id', 'name_fr', 'name_en', 'name_ru']);
        $positionSpecialities = PositionSpeciality()->getById($positionSpecialityId, ['id', 'name_fr', 'name_en', 'name_ru']);

        $positionTitlesAssoc = arrayToAssocByKey($positionTitles, 'id');
        $positionCategoriesAssoc = arrayToAssocByKey($positionCategories, 'id');
        $positionSpecialitiesAssoc = arrayToAssocByKey($positionSpecialities, 'id');

        foreach ($positions as &$position) {
            $position['title_name_fr'] = $positionTitlesAssoc[$position['position_title_id']]['name_fr'];
            $position['category_name_fr'] = $positionCategoriesAssoc[$position['position_category_id']]['name_fr'];
            $position['speciality_name_fr'] = $positionSpecialitiesAssoc[$position['position_speciality_id']]['name_fr'];

            $position['title_name_en'] = $positionTitlesAssoc[$position['position_title_id']]['name_en'];
            $position['category_name_en'] = $positionCategoriesAssoc[$position['position_category_id']]['name_en'];
            $position['speciality_name_en'] = $positionSpecialitiesAssoc[$position['position_speciality_id']]['name_en'];

            $position['title_name_ru'] = $positionTitlesAssoc[$position['position_title_id']]['name_ru'];
            $position['category_name_ru'] = $positionCategoriesAssoc[$position['position_category_id']]['name_ru'];
            $position['speciality_name_ru'] = $positionSpecialitiesAssoc[$position['position_speciality_id']]['name_ru'];
        }
        return Response()->data($positions)->success();
    }
}