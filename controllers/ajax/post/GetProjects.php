<?php

namespace controllers\ajax\post;

use controllers\ajax\BaseAjaxController;
use ProjectListPage;

class GetProjects extends BaseAjaxController
{
    public function run()
    {
        $obj = new ProjectListPage();
        $res = $obj->getTableData();
        return Response()->data($res)->success();
    }
}