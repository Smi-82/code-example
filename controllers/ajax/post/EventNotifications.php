<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use MessengerEventCodes;
use User;

class EventNotifications extends BaseAjaxController
{
    public function run()
    {
        if(User()->getId() === false)
            return Response()->error('User does not logged');
        $global_user_id = User::getById(User()->getId(), ['global_id']);
        if (empty($global_user_id))
            return Response()->success();
        $global_user_id = $global_user_id['global_id'];
        if (isset($_POST['exclude']))
            $notifications = MessengerEvents()->getNotificationsByGlobalUserId($global_user_id, $_POST['exclude']);
        else
            $notifications = MessengerEvents()->getNotificationsByGlobalUserId($global_user_id);

        if (empty($notifications))
            return Response()->data($notifications)->success();
        if (!is_array($notifications))
            return Response()->error($notifications);
        $userId = array_unique(array_column($notifications, 'user_id'));
        $users = User::getManyUsersByGlobalIds($userId, ['global_id', 'id', 'firstname', 'lastname']);

        $users = arrayToAssocByKey($users, 'global_id');
        foreach ($notifications as &$notification) {
            $notification['user'] = $users[$notification['user_id']];
            $notification['data'] = json_decode($notification['data']);
            $link = '';
            switch ($notification['code']) {
                case MessengerEventCodes::PROJECT_CREATED:
                    $link = 'my-project-detail-page.php?project=' . $notification['project_id'];
                    break;
                case MessengerEventCodes::TASK_STATUS_CHANGE_TO_WAITING_FOR_VALIDATION_BY_THE_CLIENT:
                case MessengerEventCodes::TASK_STATUS_CHANGE_TO_IN_DEVELOPMENT:
                case MessengerEventCodes::TASK_STATUS_CHANGE_TO_FINALIZED_NOT_CONTROLLED:
                case MessengerEventCodes::TASK_STATUS_CHANGE_TO_VALIDATED_TECHNICAL_INSPECTION:
                case MessengerEventCodes::TASK_STATUS_CHANGE_TO_DOUBLE_CHECK_VALIDATED:
                case MessengerEventCodes::TASK_STATUS_CHANGE_TO_APPROVED_BY_THE_CUSTOMER:
                    $link = 'my-task-page.php?id=' . $notification['task_id'];
                    break;
                case MessengerEventCodes::CONFIRM_PROJECT_EXPRESSION_OF_NEEDS:
                    $link = 'expression_of_needs.php?id=' . $notification['project_id'];
                    break;
                case MessengerEventCodes::CONFIRM_PURCHASE_ORDER:
                    $link = 'personal.php#comandes';
                    break;
                case MessengerEventCodes::PAY_INVOICE:
                    $link = 'personal.php#factures';
                    break;
            }
            $notification['link'] = $link;
        }
        return Response()->data($notifications)->success();
    }
}