<?php


namespace controllers\ajax\post;

use controllers\ajax\BaseAjaxController;
use UsersPage;

class GetUsers extends BaseAjaxController
{
    public function run()
    {
        $obj = new UsersPage();
        $result = $obj->getTableData();
        return isset($result['error']) ? Response()->error($result['error']) : Response()->data($result)->success();
    }
}