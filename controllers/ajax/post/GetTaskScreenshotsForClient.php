<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use TaskScreenshot;
use UploadDir;

class GetTaskScreenshotsForClient extends BaseAjaxController
{
    public function run()
    {
        $screenshots = TaskScreenshot::getByTaskIdForClient(clear_post($_POST['task_id']));
        $root = UploadDir::getRelativePath();
        $root = trim($root, '/');
        $screenshots = array_map(function ($el) use ($root) {
            $el['src'] = $el['output_dir'] . DS . $el['filename'];
            $arr = explode($root, $el['output_dir'] . DS . $el['filename']);
            $el['link'] = $root . array_pop($arr);
            $el['whenCreated'] = date('d-m-y H:i:s', strtotime($el['whenCreated']));
            return $el;
        }, $screenshots);

        return Response()->data($screenshots)->success();
    }
}