<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use MessengerEventCodes;

class AddNewGeneralProjectByClient extends BaseAjaxController
{
    public function run()
    {
        $_POST['projectData']['client_id'] = User()->getId();
        $res = Project()->add($_POST['projectData']);
        MessengerEvents()->send(MessengerEventCodes::PROJECT_CREATED, ['project_id' => $res['project']]);
        return empty($res['error']) ? Response()->data(['project' => $res['project']])->success() : Response()->error(mb_ucfirst(lng('error')) . ': ' . lng('project_is_not_saved'));
    }
}