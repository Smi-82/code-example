<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class DeleteAttachmentFileById extends BaseAjaxController
{
    public function run()
    {
        $res = Task()->deleteAttachmentFileById($_POST['id']);
        return $res ? Response()->success() : Response()->error();
    }
}