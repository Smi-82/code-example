<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use TasksMessagesFiles;

class UpdateTaskAttachmentComment extends BaseAjaxController
{
    public function run()
    {
        $comment = escape_string(clear_post($_POST["comment"]));
        $id = escape_string(clear_post($_POST["id"]));
        $res = TasksMessagesFiles::updateById($id, compact('comment'));
        if (is_numeric($res)) {
            history('task', User()->getSignature() . ' added comment to attachment ID:"' . $id . '". Comment : ' . $comment);
            return json_encode($comment, JSON_UNESCAPED_UNICODE);
        }
        return false;
    }
}