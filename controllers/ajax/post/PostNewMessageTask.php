<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use TasksMessages;

class PostNewMessageTask extends BaseAjaxController
{
    public function run()
    {
        if ($_POST['message'] == '' || $_POST['task_id'] == '') {
            return Response()->error('');
        }

        $fromname = User()->getSignature();

        $message_text = nl2br(clear_post($_POST['message']));

//        $data = query("SELECT img FROM " . User::getTable() . " WHERE id = " . User()->getId())[0];
        $data['img'] = User()->getImg();

        $return_array = array(
            array(
                'name' => $fromname,
                'message' => $message_text,
                'img' => $data['img'],
                'sentat' => date('Y-m-d H:i:s')
            )
        );

        $rjson = Response()->data($return_array)->success();

        if (TasksMessages::add([
            'user_id' => User()->getId(),
            'sentat' => date('Y-m-d H:i:s'),
            'name' => $fromname,
            'message' => $message_text,
            'task_id' => $_POST['task_id'],
        ])
        ) {
            $message_id = getLastId();
//            query("UPDATE tasks_access SET last_seen_msg = " . $last_id . " WHERE task_id = " . clear_post($_POST['task_id']) . " and user_id = " . User()->getId());
            $res = query('SELECT id FROM user_message_link WHERE user_id=' . User()->getId() . ' AND task_id=' . clear_post($_POST['task_id']) . ' LIMIT 1');
            if ($res) {
                $res = $res[0]['id'];
                $sql = update_query_string('user_message_link',
                        array(
                            'message_id' => $message_id,
                        )) . ' WHERE id=' . $res;
            }
            else {
                $sql = insert_query_string('user_message_link',
                    array(
                        'user_id' => User()->getId(),
                        'message_id' => $message_id,
                        'task_id' => $_POST['task_id']
                    )
                );
            }

            query($sql);
            history('task', $fromname . ' sent new message in the task chat. Task ID:' . clear_post($_POST['task_id']));
            return $rjson;
        }
        else {
            return Response()->error('');
        }
    }
}