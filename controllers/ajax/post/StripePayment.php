<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use PaymentPartsProjectBill;
use Payments;
use Stripe\Customer;
use Stripe\PaymentIntent;
use Stripe\Stripe;

class StripePayment extends BaseAjaxController
{
    public function run()
    {
        $payment = PaymentPartsProjectBill::getById($_POST['part_id'], ['id', 'amount', 'project_id']);
        try {
            require_once($_SERVER['DOCUMENT_ROOT'] . '/vendor/stripe/stripe-php/init.php');
            Stripe::setApiKey(Stripe_loc()->getSecretKey());
            if (!User()->stripe_acc) {
                $client = Customer::create([
                    'email' => User()->email,
                ]);
                User()->update(User()->id, ['stripe_acc' => $client->id]);
                if (User()->getUser())
                    User()->updateHimself();
            }
            else {
                $client = Customer::retrieve(User()->stripe_acc);
            }

            $p_int = PaymentIntent::create([
                'amount' => floatval($payment['amount']) * 100,
                'currency' => $_POST['currency'],
                'payment_method_types' => ['card'],
                'customer' => $client->id,
            ]);
            Payments::add(['system' => Stripe_loc()->getName(), 'paym_part_id' => $payment['id'], 'transaction_id' => $p_int->id]);
        } catch (Exception $e) {
            return Response()->error($e->getMessage());
        }
        return Response()->data(compact('p_int'))->success();
    }
}