<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Project;

class SaveProjectQuestionnaire extends BaseAjaxController
{
    public function run()
    {
        $res = Project::addQuestionnaire($_POST['project_id'], $_POST['data']);
        return $res['error'] ? Response()->error($res) : Response()->data($res)->success();
    }
}