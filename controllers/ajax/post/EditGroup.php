<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Group;

class EditGroup extends BaseAjaxController
{
    public function run()
    {
        $res = Group::update($_POST['parent_group_id'], ['name' => $_POST['new_group_name']]);
        return $res !== false ? Response()->success() : Response()->error(mb_ucfirst(lng('error')) . ': Group was not updated!');
    }
}