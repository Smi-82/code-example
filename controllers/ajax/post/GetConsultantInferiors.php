<?php


namespace controllers\ajax\post;


use Consultant;
use controllers\ajax\BaseAjaxController;
use Exception;
use User;
use UserTypes;

class GetConsultantInferiors extends BaseAjaxController
{
    public function run()
    {
        $obj = new Consultant();
        $consultants = $obj->getChildrensId(clear_post($_POST['master']));
        try {
            $sql = '
SELECT
u.id,
u.usertype,
u.img,
TRIM(CONCAT(u.firstname," ", u.lastname)) AS user_name,
ut.name AS ut_name,
(SELECT COUNT(id) FROM ' . User::getTable() . ' WHERE usertype=' . UserTypes::getTable() . ' AND consultant_id=u.id) AS cnt_clients
FROM
' . User::getTable() . ' AS u
LEFT JOIN
' . UserTypes::getTable() . ' AS ut
ON
u.usertype = ut.id
WHERE
u.id
IN';
            if (empty($consultants))
                $consultants[] = 0;
            $sql .= '(' . implode(',', $consultants) . ')
ORDER BY u.usertype ASC';
            $consultants = query($sql);
            $rjson = Response()->data($consultants)->success();
        } catch (Exception $e) {
            $rjson = Response()->error($e->getMessage());
        }
        return $rjson;
    }
}