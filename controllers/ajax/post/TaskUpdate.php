<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Exception;
use Task;

class TaskUpdate extends BaseAjaxController
{
    public function run()
    {
        try {
            Task::updateTasksData($_POST['task_id'], $_POST['data']);
        } catch (Exception $e) {
            return Response()->error($e->getMessage());
        }
        return Response()->data($_POST['data'])->success();
    }
}