<?php


namespace controllers\ajax\post;


use appEvents\subscribers\controller\AppEventsSubscribersController;
use controllers\ajax\BaseAjaxController;
use appEvents\AppEventsController;
use Priorities;
use Task;

class ChangePriority extends BaseAjaxController
{
    public function run()
    {
        if (Task::updateTasksData($_POST['task_id'], ['priority' => $_POST['id']])) {
            $desc = User()->getSignature() . ' changed priority of the task ID:' . clear_post($_POST['task_id']) . ' to ' . Priorities::getKeyById(clear_post($_POST['id']));
            AppEventsController::publish(AppEventsSubscribersController::changeTaskPriorityEvent(), ['task_id' => $_POST['task_id'], 'desc' => $desc, 'page' => 'task']);
        }
        else
            return Response()->error(mb_ucfirst(lng('error')) . ': tasks wos not updated!');

        return Response()->success();
    }
}