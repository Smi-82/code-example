<?php

namespace controllers\ajax\post;

use controllers\ajax\BaseAjaxController;

class ChangeShowTheClientSingleClient extends BaseAjaxController
{
    public function run()
    {
        $userTypes = UserTypes()->getAll();
        $userTypes = valueToKey('id', $userTypes);
        $arr = $_POST['arr'];
        foreach ($arr as $k => $v) {
            if (intval($v['show_the_client']) == intval($userTypes[$v['id']]['show_the_client'])) {
                unset($arr[$k]);
            }
        }
        if (empty($arr))
            $arr = '';
        else {
            $arr = valueToKey('id', $arr, true);
            $arr = array_map(function ($el) {
                return intval($el['show_the_client']);
            }, $arr);
            $arr = json_encode($arr);
        }
        $res = User()->update($_POST['client_id'], ['show_the_client' => $arr]);
        return $res ? Response()->success() : Response()->error($arr);
    }
}