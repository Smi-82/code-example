<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class UploadQuestionnaireLogo extends BaseAjaxController
{
    public function run()
    {
        Project::addQuestionnaireFiles($_POST['project_id']);
        return $_POST;
    }
}