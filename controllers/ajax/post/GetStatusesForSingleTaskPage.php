<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;
use Task;

class GetStatusesForSingleTaskPage extends BaseAjaxController
{
    public function run()
    {
        $status_obj = Statuses();
        $statuses = $status_obj->getAllStatuses('asc');
        $current_user = User();
        $statuses = array_map(function ($status) use ($current_user, $status_obj) {
            $click = '';
            $disabled = true;
            $checked = '';
            if ($status_obj->canUserTypeChange($status['id'], $current_user->getType(), Task::getTable())) {
                $click = "check-status";
                $disabled = false;
            }
            if (intval($_POST['task_status']) >= intval($status['id'])) {
                $checked = 'checked';
            }
            if ($_POST['task_active'] != 1) {
                $disabled = true;
            }
            $status['checked'] = $checked;
            $status['disabled'] = $disabled;
            $status['click'] = $click;
            return $status;
        }, $statuses);
        return Response()->data(array_values($statuses))->success();
    }
}