<?php


namespace controllers\ajax\post;


use controllers\ajax\BaseAjaxController;

class GetFoundTaskColaborators extends BaseAjaxController
{
    public function run()
    {
        $res = Task()->getOwnParticipants($_POST['task_id'], ['id', 'firstname', 'lastname', 'img']);
        $res = array_map(function ($elem) {
            $elem['img'] = getImg($elem, $elem['id']);
            $elem['name'] = trim($elem['firstname'] . ' ' . $elem['lastname']);
            unset($elem['firstname']);
            unset($elem['lastname']);
            unset($elem['id']);
            return $elem;
        }, $res);
        return Response()->data($res)->success();
    }
}