<?php


namespace controllers\ajax\get;


use controllers\ajax\BaseAjaxController;
use Socket;

class GetClientNotificationsCount extends BaseAjaxController
{
    public function run()
    {
        $obj = new Socket(function ($thisSocket) {
            return MessengerEvents()->getNotificationsCountByUserId(User()->getId());
        });
        $obj->setSleep(5)->run();
    }
}