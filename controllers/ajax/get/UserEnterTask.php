<?php

namespace controllers\ajax\get;

use controllers\ajax\BaseAjaxController;
use Logger;
use Socket;
use Task;

class UserEnterTask extends BaseAjaxController
{
    public function run()
    {
//        Logger::add('socket', 'controller run. Line ' . __LINE__);
        $obj = new Socket(function ($thisSocket) {

            $task_id = Task::isUserOnTask(User()->getId(), $_GET['task']);
            $task_id = intval($task_id);
//            if ($task_id == 0)
//                unset($thisSocket);
//            Logger::add('socket', 'construct socket run. Line ' . __LINE__ . '. Task - ' . $task_id . '. User - ' . User()->getId());
            $res = false;
            if (intval($thisSocket->task_id) && connection_status() == CONNECTION_NORMAL) {
//                Logger::add('socket', 'Task- ' . $thisSocket->task_id . ' time count. Status - ' . connection_status() . '. Line ' . __LINE__);
                $res = Task::updateTimeSpendByTaskAndUserIds($task_id, User()->getId(), $thisSocket->getSleep());
            }
            else
//                Logger::add('socket', 'connection aborted. Status - ' . connection_status() . '.Line ' . __LINE__);
            if ($task_id && !$thisSocket->task_id)
                $thisSocket->task_id = $task_id;

            return compact('task_id');
        });
//        Logger::add('socket', 'row before Destruct. Line ' . __LINE__);
        $obj->setDestructFunc(function ($thisSocket) {
//            Logger::add('socket', 'DEstruct socket run. Line ' . __LINE__ . '. Task - ' . $thisSocket->task_id . '. User - ' . User()->getId());
            Task::userNotOnTask(User()->getId(), $thisSocket->task_id);
        })->setSleep(5)->run();
//        Logger::add('socket', 'row AFTER Destruct. Line ' . __LINE__);
    }
}