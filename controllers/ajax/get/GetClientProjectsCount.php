<?php


namespace controllers\ajax\get;


use controllers\ajax\BaseAjaxController;
use Socket;

class GetClientProjectsCount extends BaseAjaxController
{
    public function run()
    {
        $obj = new Socket(function ($thisSocket) {
            return Project()->getClientProjectsCount();
        });
        $obj->setSleep(5)->run();
    }
}