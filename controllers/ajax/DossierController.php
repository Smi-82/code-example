<?php


namespace controllers\ajax;


use Consultant;
use Domain;
use Dossier;
use DossierMeetingHistory;
use User;

class DossierController extends BaseAjaxController
{
    public static function setDossierMeeting()
    {
        try {
            $meeting = Dossier::getById($_POST['dossier_id'], ['meeting']);
            $meeting = $meeting['meeting'];
            if (strtotime($_POST['meeting']) != strtotime($meeting))
                DossierMeetingHistory::add($_POST['dossier_id'], date('Y-m-d H:i:s', strtotime($_POST['meeting'])));

            $res = Dossier::update($_POST['dossier_id'], ['meeting' => date('Y-m-d H:i:s', strtotime($_POST['meeting'])), 'meeting_done' => 0]);
            if ($res)
                dossierHistory($_POST['dossier_id'], 'Set dossier meeting');
            $client = querySingle('SELECT u.email, d.client_id FROM ' . User::getTable() . ' AS u LEFT JOIN ' . Dossier::getTable() . ' AS d ON d.client_id=u.id WHERE d.id=' . clear_post($_POST['dossier_id']));
            $vendor = User::getById(User()->getId(), ['phone', 'phone2', 'gender', 'firstname', 'lastname', 'email']);

            (bool)preg_match('#mrs#i', $vendor['gender']) ? $vendor['gender'] = 'mrs' : $vendor['gender'] = 'mr';
            $domain = new Domain((new Consultant())->getDomainByUser_id($client['client_id']));
            $mail_body = getHtmlTemplate($domain->meeting_mail_template(),
                [
                    'date_meeting' => date('d-m-Y à H:i', strtotime($_POST['meeting'])),
                    'user_type' => User()->getUsertypeSignature(),
                    'user_name' => mb_ucfirst($vendor['gender'])
                        . ' ' . mb_ucfirst($vendor['firstname']) . ' ' . mb_strtoupper($vendor['lastname']),
                    'name' => mb_ucfirst($vendor['gender'])
                        . ' ' . mb_ucfirst($vendor['firstname']) . ' ' . mb_strtoupper($vendor['lastname']),
                    'phones' => $vendor['phone'] . (!empty($vendor['phone2']) ? ' ' . lng('or') . ' ' : '') . $vendor['phone2'],
                    'email' => $vendor['email']
                ]);

            $res = Mails()->send_one_mail($domain->getContactMailAddress(), '', $client['email'], '', mb_ucfirst(lng('afsmd_phone_appointment_confirmation')), $mail_body, '');
            return $res ? Response()->success($res) : Response()->error(mb_ucfirst(lng('meeting_time_is_not_set')) . '!');
        } catch (Exception $e) {
            return Response()->error($e->getMessage());
        }
    }

    public static function clearDossierAnswers()
    {
        $arr = [
            'answer' => '',
            'result_text' => '',
            'formula_id' => '',
            'amount' => '',
            'payment' => 0,
            'total_discount' => 0,
            'isSigned' => 0,
            'isRead' => 0,
            'isSent' => 0,
        ];
        $res = Dossier::update($_POST['dossier_id'], $arr);
        return $res ? Response()->success() : Response()->error();
    }

    public static function showContractToTheClient()
    {
        $res = Dossier::isSent($_POST['dossier_id']);
        return $res ? Response()->success() : Response()->error(mb_ucfirst(lng('error_occured')) . '! ' . mb_ucfirst(lng('the_certificate_is_not_visible_to_the_client')));
    }
}