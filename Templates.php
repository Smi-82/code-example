<?php


class Templates
{
    const DIRECTORY = 'templates';
    const PATH_TO_TEMPLATES = ROOT . DS . self::DIRECTORY;

    public static function getHtmlComponents($template, $data = [])
    {
        return self::getTemplate('html_components' . DS . $template, $data);
    }

    public static function getTemplate($template, $data = [])
    {
        if (file_exists(self::PATH_TO_TEMPLATES . DS . $template . '.html')) {
            $path = self::PATH_TO_TEMPLATES . DS . $template . '.html';
        } elseif (file_exists(self::PATH_TO_TEMPLATES . DS . $template . '.php')) {
            $path = self::PATH_TO_TEMPLATES . DS . $template . '.php';
        } else {
            return 'File ' . self::PATH_TO_TEMPLATES . DS . $template . '.php||.html Not found';
        }

        ob_start();
        include($path);
        return ob_get_clean();
    }
}