<?php

use appEvents\AppEventsController;
use appEvents\subscribers\controller\AppEventsSubscribersController;
use models\ProjectAccessModel;

class ProjectAccess
{
    public static function baseSelect(array $columns = ['*'], $aggregate = null)
    {
        return ProjectAccessModel::baseSelect($columns, $aggregate);
    }

    public static function getTable()
    {
        return ProjectAccessModel::getTable();
    }

    public static function add($project_id, $user_id)
    {
        try {
            $res = ProjectAccessModel::add($project_id, $user_id);
            if ($res) {
                AppEventsController::publish(AppEventsSubscribersController::addAccessToProjectEvent(), compact('project_id', 'user_id'));
                $user_to_add = User::getById($user_id, ['usertype', 'global_id']);
                if (in_array($user_to_add['usertype'], UserTypes()->getSupportTypes())) {
                    $clientdata = Project()->getClient($project_id, ['id', 'global_id']);
                    if (!empty($clientdata)) {
                        require_once($_SERVER['DOCUMENT_ROOT'] . DS . MODULE . DS . 'messenger_tm/VMSG_messenger_functions.php');
                        $dialogid = V_Messenger::getDialogidByProjectidAndClientid($project_id, $clientdata['id']);
                        if ($dialogid) {
                            require_once($_SERVER['DOCUMENT_ROOT'] . DS . MODULE . DS . 'messenger_tm/glooda_api.php');
                            $ga = new GloodaAPI('logged_in_user', array('supportUsersData' => array(), 'supportDialogID' => 0, 'token' => User()->accessToken));
                            if ($ga->login_OK) {
                                $res_add = $ga->AddUserToGroup(array('id' => $user_to_add['global_id']), $dialogid);
                            }
                        }
                    }
                }
            }
            return $res;
        } catch (Exception $e) {
            return $e->getTrace();
        }
    }

    public static function delete($project_id, $user_id)
    {
        $res = ProjectAccessModel::delete($project_id, $user_id);
        if ($res) {

            /* $user_to_delete = User::getById($user_id, ['usertype', 'global_id']);
             if($user_to_delete){
                 if( in_array($user_to_delete['usertype'], UserTypes()->getSupportTypes()) ){
                     $clientdata = Project()->getClient($project_id,['id', 'global_id']);
                     if( !empty($clientdata) ){
                         require_once($_SERVER['DOCUMENT_ROOT'] . DS . MODULE . DS . 'messenger_tm/VMSG_messenger_functions.php');
                         $dialogid = V_Messenger::getDialogidByProjectidAndClientid($project_id, $clientdata['id']);
                         if($dialogid){
                             require_once($_SERVER['DOCUMENT_ROOT'] . DS . MODULE . DS . 'messenger_tm/glooda_api.php');
                             $ga = new GloodaAPI( 'logged_in_user', array('supportUsersData' => array(), 'supportDialogID'  => 0, 'token' => $_SESSION[MODULE]['user']['accessToken']) );
                             if ($ga->login_OK) {
                                 $res_delete = $ga->removeUserFromGroups(array('id'=>$user_to_delete['global_id']), [$dialogid]);
                             }
                         }
                     }
                 }
             }*/

            $tasks = Project::getOwnTasks($project_id, ['id']);
            $tasks = array_column($tasks, 'id');
            Task()->deleteAccess($tasks, $user_id);
        }
        return $res;
    }

    public static function getAccessDataByProjectIdS($project_ids, array $params = ['*'])
    {
        return ProjectAccessModel::getAccessDataByProjectIdS($project_ids, $params);
    }

    public static function getAccessData($project_id, array $params = ['*'])
    {
        return ProjectAccessModel::getAccessData($project_id, $params);
    }

    public static function getAccessDataForUser($user_id, array $params = ['*'])
    {
        $params = clearParams($params);
        $user_id = escape_string(clear_post($user_id));
        $params = array_map(function ($elem) {
            return 'a.' . $elem;
        }, $params);
        $sql = ProjectAccessModel::baseSelect($params) . ' AS a LEFT JOIN ' . Project::getTable() . ' AS p ON p.id=a.general_project_id  WHERE p.active=1 AND a.user_id=' . $user_id;
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}