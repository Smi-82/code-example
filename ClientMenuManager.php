<?php


class ClientMenuManager
{
    private $client_id;
    private $column = 'client_menu';
    private $menu_tmp = [
        'header_menu_dashboard' => [
            'show' => 1
        ],
        'header_menu_my_projects' => [
            'show' => 1
        ],
        'header_menu_create_project' => [
            'show' => 1
        ],
        'header_menu_offers' => [
            'show' => 1
        ],
        'header_menu_library' => [
            'show' => 1
        ],
        'header_menu_my_account' => [
            'show' => 1
        ],
        'collaborator_s_rates' => [
            'show' => 1
        ],
    ];
    private $menu;

    public function __construct($client_id)
    {
        $this->client_id = $client_id;
        $res = User::getById($client_id, [$this->column]);
        if (!empty($res) && !is_null($res[$this->column])) {
            $res = htmlspecialchars_decode($res[$this->column]);
            $res = json_decode($res, true);
            $this->menu = array_merge($this->menu_tmp, $res);
        }
        else
            $res = [];
        $this->menu = array_merge($this->menu_tmp, $res);
        foreach ($this->menu as $key => &$val){
            $val['show'] = intval($val['show']);
            $val['key'] = $key;
        }
    }

    public function getMenu()
    {
        return $this->menu;
    }

    public function getMenuValue()
    {
        return array_values($this->getMenu());
    }

    public function is_show($key)
    {
        return $this->menu[$key]['show'];
    }

    public function setShow($key, $show = 1)
    {
        $this->menu[$key]['show'] = $show;
        $data = json_encode($this->menu);
        return User()->update($this->client_id, [$this->column => $data]);
    }
}