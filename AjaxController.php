<?php


class AjaxController
{
    private static function action($key, $class, $method = null)
    {
        if (!class_exists($class))
            throw new Exception($class . ' not exists');
//        AjaxStat::update($_SERVER['REQUEST_METHOD'], $key);
        $obj = new $class();
        echo is_null($method) ? $obj->run() : $obj->$method();
        exit();
    }

    public static function get($key, $class, $method = null)
    {
        return ($_GET['key'] !== $key) ? false : self::action($key, $class, $method);
    }

    public static function post($key, $class, $method = null)
    {
        return ($_POST['key'] !== $key) ? false : self::action($key, $class, $method);
    }

    public static function request($key, $class, $method = null)
    {
        return ($_REQUEST['key'] !== $key) ? false : self::action($key, $class, $method);
    }
}