<?php

use abstractClasses\TableData;

class TaskListPage extends TableData
{
    protected function addFilters()
    {
        $filters = '';
        if ($this->_filters != null && is_array($this->_filters)) {

            foreach ($this->_filters as $field => $val) {
                if (!empty($val)) {
                    switch ($field) {
                        case 'only_my':
                            $filters .= ' AND t.id IN (' . TaskAccess::baseSelect(['task_id']) . ' WHERE user_id = ' . User()->getId() . ') ';
                            break;
                        default:
                            $filters .= ' AND t.' . $field . ' IN (' . implode(', ', $val) . ') ';
                    }
                }
            }
        }
        return $filters;
    }

    protected function addFind()
    {
        if (!empty($this->_findBy)) {
            switch ($this->_findField) {
                case 'project':
                    $findByName = ' AND p.name LIKE "%' . $this->_findBy . '%" ';
                    break;
                case 'group':
                    $findByName = ' AND g.name LIKE "%' . $this->_findBy . '%" ';
                    break;
                default:
                    $findByName = ' AND t.' . $this->_findField . ' LIKE "%' . $this->_findBy . '%" ';
            }
        }
        else
            $findByName = '';
        return $findByName;
    }

    protected function addSort()
    {
        if ($this->_sortWay && $this->_sortBy && $this->_sortWay != 'both') {
            switch ($this->_sortBy) {
                default:
                    $name = 't.' . $this->_sortBy;
            }
            $sort = ' ORDER BY ' . $name . ' ' . $this->_sortWay;
        }
        else
            $sort = ' ORDER BY t.ord ASC, t.id DESC';
        return $sort;
    }

    public static function getColumnsForPage()
    {
        $statuses = Statuses();
        return [
            1 => [
                'title' => 'new_tasks',
                'color' => 'red',
                'chB' => 'danger',
                'statuses' => [
                    $statuses->in_preparation()
                ],
            ],
            2 => [
                'title' => 'tasks_assigned_and_in_progress_2',
                'color' => 'orange',
                'chB' => 'warning',
                'statuses' => [
                    $statuses->in_development(),
                    $statuses->waiting_for_validation_by_the_client()
                ],
            ],
            3 => [
                'title' => 'waiting_for_control_2',
                'color' => 'blue',
                'chB' => 'primary',
                'statuses' => [
                    $statuses->double_check_validated(),
                    $statuses->finalized_not_controlled(),
                    $statuses->validated_technical_inspection()
                ],
            ],
            4 => [
                'title' => 'finished_finished',
                'color' => 'green',
                'chB' => 'success',
                'statuses' => [
                    $statuses->approved_by_the_customer()
                ],
            ],
        ];
    }

    public function getTableData($general_project_id = null)
    {
        $current_user = User();
        $user_id = $current_user->isCollaborator() ? $current_user->getParentId() : $current_user->getId();

        $query_str = '
            SELECT
            t.*,
            cat.name as category_name,
            g.name as group_name,
            g.id as group_id,
            p.name as project_name,
            p.id as project_id,
            GROUP_CONCAT(tm.file_id,",") AS fcnt
            FROM
            ' . Task::getTable() . ' AS t
            LEFT JOIN
            ' . Category::getTable() . ' AS cat
            ON
            cat.id=t.category
            LEFT JOIN
            ' . Group::getTable() . ' AS g
            ON
            g.id = t.group_id
            LEFT JOIN
            ' . Project::getTable() . ' as p
            ON
            p.id = g.general_project_id
            LEFT JOIN
            ' . TasksMessages::getTable() . ' AS tm
            ON
            tm.task_id=t.id
            WHERE
            t.active =1';
        if ($current_user->isClient() || $current_user->isCollaborator())
            $query_str .= ' AND p.show_tasks_to_client=1 ';

        $query_str .= $findByName = $this->addFind();
        $query_str .= $this->addFilters();
        if (is_numeric($general_project_id)) {
            $query_str .= ' AND p.id = ' . $general_project_id . ' AND p.active = 1 ';
        }
        if ($current_user->isClient()) {
            $query_str .= ' AND p.client_id=' . $user_id . ' ';
        }
        elseif (!($current_user->isSupervisor() || $current_user->isAssistant())) {
            if (!isset($this->_filters['only_my'])) {
                $query_str .= ' AND ( ' . $user_id . ' IN (SELECT ta.user_id FROM ' . TaskAccess::getTable() . ' AS ta WHERE ta.task_id = t.id)) ';
            }
        }

        if (isset($_POST['group_id']) && is_numeric($_POST['group_id'])) {
            $group_id = escape_string(clear_post($_POST['group_id']));
            $query_str .= " AND g.id = $group_id";
        }
        $query_str .= ' GROUP BY t.id ';
        $query_str .= $this->addSort();
//        if (!empty($this->_currentPage) && $this->_cntRows) {
//            $query_str .= ' LIMIT ' . ($this->_currentPage * $this->_cntRows - $this->_cntRows) . ', ' . ($this->_cntRows);
//        }
        try {
            $result = query($query_str);
            if (!empty($result)) {
                $result = Task::order0ToTheEnd($result);
                if (!empty($this->_currentPage) && $this->_cntRows) {
                    $result = array_page($result, $this->_currentPage, $this->_cntRows);
                }
            }

            $sql1 = '
SELECT
COUNT(*) AS cnt_task
FROM
' . Task::getTable() . ' AS t
LEFT JOIN
' . Group::getTable() . ' AS g
ON
g.id = t.group_id
LEFT JOIN
' . Project::getTable() . ' as p
ON
p.id = g.general_project_id
WHERE
t.active =1 ';
            if (is_numeric($general_project_id)) {
                $sql1 .= ' AND p.id = ' . $general_project_id . ' AND p.active = 1 ';
            }
            if ($current_user->isClient()) {
                $query_str .= ' AND p.client_id=' . $user_id . ' ';
            }
            elseif (!($current_user->isSupervisor() || $current_user->isAssistant())) {
                if (!isset($this->_filters['only_my'])) {
                    $sql1 .= ' AND ( ' . $user_id . ' IN (SELECT ta.user_id FROM ' . TaskAccess::getTable() . ' AS ta WHERE ta.task_id = t.id)) ';
                }
            }

            if (isset($_POST['group_id']) && is_numeric($_POST['group_id'])) {
                $group_id = escape_string(clear_post($_POST['group_id']));
                $sql1 .= " AND g.id = $group_id ";
            }
            $sql1 .= $findByName;
            $sql1 .= $this->addFilters();
            $cnt_tasks = querySingle($sql1)['cnt_task'];
            $donePercent = 0;
            if (is_numeric($general_project_id)) {
                $donePercent = Project::getDonePercent($general_project_id);
            }
            if ($result) {

                $tasks_id = array_column($result, 'id');
                $users = Task()->getOwnParticipants($tasks_id, ['id', 'lastname', 'firstname', 'img', 'usertype']);
                $time_spent = Task::getTimeSpent($tasks_id);
                $users = array_map(function ($elem) {
                    $elem['img'] = getImg($elem, $elem['id']);
                    $elem['blink'] = false;
                    $elem['online'] = false;
                    return $elem;
                }, $users);

                if (isset($_POST['project_id'])) {
                    $types = UserTypes();
                    $users = array_filter($users, function ($elem) use ($types) {
                        return $elem['usertype'] != $types->getClient();
                    });
                }

                foreach ($result as &$val) {
                    $val['count_msg'] = 0;
                    $val['new_id'] = Task::getFakeId($val['id']);
                    $val['count_file'] = 0;
                    if ($val['fcnt'] != null) {
                        $arr = array_filter(explode(',', $val['fcnt']), function ($element) {
                            return $element != '';
                        });
                        $val['count_msg'] = count(array_filter($arr, function ($element) {
                            return intval($element) == 0;
                        }));
                        $val['count_file'] = count(array_filter($arr, function ($element) {
                            return intval($element) != 0;
                        }));
                    }
                    unset($val['fcnt']);
                    $val['participants'] = array_filter($users, function ($elem) use ($val) {
                        return intval($elem['task_id']) == intval($val['id']);
                    });
                    if ($current_user->isClient())
                        $val['participants'] = array_filter($val['participants'], function ($elem) use ($current_user) {
                            return intval($elem['id']) != $current_user->getId();
                        });
                    $val['message'] = false;
                    $val['created'] = date('d-m-Y', strtotime($val['created']));
                    $val['description'] = strip_tags(html_entity_decode($val['description']));
                    $val['name'] = strip_tags(html_entity_decode($val['name']));
                    $val['status_class'] = Statuses()->getStatusesById($val['status'], 'class');
                    $val['priority_class'] = Priorities::getColorClassById($val['priority']);
                    $val['status_name'] = Statuses()->getStatusesById($val['status'], 'name');
                    $val['prioriry_name'] = Priorities::getKeyById($val['priority']);
                    $val['time_spent'] = isset($time_spent[$val['id']]) ? getHoursMinutesFromSeconds($time_spent[$val['id']]) : '';
                    $today = date_create(date('Y-m-d'));
                    $created = date_create($val['created']);
                    $estimateTime = date_create($val['estimateTime']);
                    $working = date_diff($created, $today);
                    $totaldays = date_diff($estimateTime, $created);
                    $daysToEnd = date_diff($estimateTime, $today);
                    $daysToEnd = $daysToEnd->format('%R%a');
                    if ($daysToEnd < 0) {
                        $daysToEnd = substr($daysToEnd, 1, 5);
                        $totaldays = intval($totaldays->format('%a'));
                        $totalworking = intval($working->format('%a'));
                        $done = round($totalworking * 100 / $totaldays);
                    }
                    else {
                        $done = 100;
                    }
                    $val['daysToEnd'] = intval($daysToEnd);
                    $val['done'] = intval($done);
                    $val['color'] = empty($val['color']) ? '#ffffff' : $val['color'];
                }
            }
            else
                $result = [];
            return ['tasks' => $result, 'cnt_tasks' => $cnt_tasks, 'donePercent' => $donePercent];
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function getDataForTemplateProject($general_project_id = null)
    {
        $current_user = User();
        $user_id = $current_user->isCollaborator() ? $current_user->getParentId() : $current_user->getId();
        if ($this->_sortWay && $this->_sortBy && $this->_sortWay != 'both') {
            switch ($this->_sortBy) {
                default:
                    $name = 't.' . $this->_sortBy;
            }
            $sort = ' ORDER BY ' . $name . ' ' . $this->_sortWay;
        }
        else
            $sort = ' ORDER BY t.id DESC';
        $query_str = '
            SELECT
            t.*,
            cat.name as category_name,
            g.name as group_name,
            g.id as group_id,
            p.name as project_name,
            p.id as project_id,
            GROUP_CONCAT(tm.file_id,",") AS fcnt
            FROM
            ' . Task::getTable() . ' AS t
            LEFT JOIN
            ' . Category::getTable() . ' AS cat
            ON
            cat.id=t.category
            LEFT JOIN
            ' . Group::getTable() . ' AS g
            ON
            g.id = t.group_id
            LEFT JOIN
            ' . Project::getTable() . ' as p
            ON
            p.id = g.general_project_id
            LEFT JOIN
            ' . TasksMessages::getTable() . ' AS tm
            ON
            tm.task_id=t.id
            WHERE
            t.active =1';
        if ($current_user->isClient() || $current_user->isCollaborator())
            $query_str .= ' AND p.show_tasks_to_client=1 ';

        $query_str .= $findByName = $this->addFind();
        $query_str .= $this->addFilters();
        if (is_numeric($general_project_id)) {
            $query_str .= ' AND p.id = ' . $general_project_id . ' AND p.active = 1 ';
        }

        if (isset($_POST['group_id']) && is_numeric($_POST['group_id'])) {
            $group_id = escape_string(clear_post($_POST['group_id']));
            $query_str .= " AND g.id = $group_id";
        }
        $query_str .= ' GROUP BY t.id ';
        $query_str .= $sort;
        if (!empty($this->_currentPage) && $this->_cntRows) {
            $query_str .= ' LIMIT ' . ($this->_currentPage * $this->_cntRows - $this->_cntRows) . ', ' . ($this->_cntRows);
        }
        try {
            $result = query($query_str);
            $sql1 = '
SELECT
COUNT(*) AS cnt_task
FROM
' . Task::getTable() . ' AS t
LEFT JOIN
' . Group::getTable() . ' AS g
ON
g.id = t.group_id
LEFT JOIN
' . Project::getTable() . ' as p
ON
p.id = g.general_project_id
WHERE
t.active =1 ';
            if (is_numeric($general_project_id)) {
                $sql1 .= ' AND p.id = ' . $general_project_id . ' AND p.active = 1 ';
            }

            if (isset($_POST['group_id']) && is_numeric($_POST['group_id'])) {
                $group_id = escape_string(clear_post($_POST['group_id']));
                $sql1 .= " AND g.id = $group_id ";
            }
            $sql1 .= $findByName;
            $sql1 .= $this->addFilters();
            $cnt_tasks = querySingle($sql1)['cnt_task'];
            if (is_numeric($general_project_id)) {
                $donePercent = Project::getDonePercent($general_project_id);
            }
            if ($result) {

                $tasks_id = array_column($result, 'id');
                $users = Task()->getOwnParticipants($tasks_id, ['id', 'lastname', 'firstname', 'img', 'usertype']);
                $users = array_map(function ($elem) {
                    $elem['img'] = getImg($elem, $elem['id']);
                    $elem['blink'] = false;
                    $elem['online'] = false;
                    return $elem;
                }, $users);

                if (isset($_POST['project_id'])) {
                    $types = UserTypes();
                    $users = array_filter($users, function ($elem) use ($types) {
                        return $elem['usertype'] != $types->getClient();
                    });
                }

                foreach ($result as &$val) {
                    $val['count_msg'] = 0;
                    $val['count_file'] = 0;
                    $val['new_id'] = Task::getFakeId($val['id']);
                    if ($val['fcnt'] != null) {
                        $arr = array_filter(explode(',', $val['fcnt']), function ($element) {
                            return $element != '';
                        });
                        $val['count_msg'] = count(array_filter($arr, function ($element) {
                            return intval($element) == 0;
                        }));
                        $val['count_file'] = count(array_filter($arr, function ($element) {
                            return intval($element) != 0;
                        }));
                    }
                    unset($val['fcnt']);
                    $val['participants'] = array_filter($users, function ($elem) use ($val) {
                        return intval($elem['task_id']) == intval($val['id']);
                    });
                    if ($current_user->isClient())
                        $val['participants'] = array_filter($val['participants'], function ($elem) use ($current_user) {
                            return intval($elem['id']) != $current_user->getId();
                        });
                    $val['message'] = false;
                    $val['created'] = date('d-m-Y', strtotime($val['created']));
                    $val['description'] = strip_tags(html_entity_decode($val['description']));
                    $val['name'] = strip_tags(html_entity_decode($val['name']));
                    $val['status_class'] = Statuses()->getStatusesById($val['status'], 'class');
                    $val['priority_class'] = Priorities::getColorClassById($val['priority']);
                    $val['status_name'] = Statuses()->getStatusesById($val['status'], 'name');
                    $val['prioriry_name'] = Priorities::getKeyById($val['priority']);
                    $today = date_create(date('Y-m-d'));
                    $created = date_create($val['created']);
                    $estimateTime = date_create($val['estimateTime']);
                    $working = date_diff($created, $today);
                    $totaldays = date_diff($estimateTime, $created);
                    $daysToEnd = date_diff($estimateTime, $today);
                    $daysToEnd = $daysToEnd->format('%R%a');
                    if ($daysToEnd < 0) {
                        $daysToEnd = substr($daysToEnd, 1, 5);
                        $totaldays = intval($totaldays->format('%a'));
                        $totalworking = intval($working->format('%a'));
                        $done = round($totalworking * 100 / $totaldays);
                    }
                    else {
                        $done = 100;
                    }
                    $val['daysToEnd'] = intval($daysToEnd);
                    $val['done'] = intval($done);
                }
            }
            else
                $result = [];
            return ['tasks' => $result, 'cnt_tasks' => $cnt_tasks, 'donePercent' => $donePercent];
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}