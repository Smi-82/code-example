<?php

use models\HistoryModel;

class History
{
    public static function getArr($page, $description)
    {
        return compact('page', 'description');
    }

    public static function baseSelect(array $columns = ['*'], $aggregate = null)
    {
        return HistoryModel::baseSelect($columns, $aggregate);
    }

    public static function getTable()
    {
        return HistoryModel::getTable();
    }

    public static function add($data)
    {
        $arr = [
            'user_id' => User()->getId(),
            'connection_id' => session_id(),
            'page_url' => SiteData()->getAll('fullurl')
        ];
        return HistoryModel::add(array_merge($arr, $data));
    }
}