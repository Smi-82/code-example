<?php
use abstractClasses\AbstractPaymentSystems;
use traits\SingleTone;

class Easytransac_loc extends AbstractPaymentSystems
{
    use SingleTone;

    private function __construct()
    {
        $this->mode = $this->setTestMode();
        $this->data = [
            'name' => 'easytransac',
            'pay_by_link_url' => '', //not implemented yet
            'APIpassword' => 'MmRmNTk5Yzc2Njk2MWVjY2U0YzlmMWY3OWMwZGFhMDMzNTkwZTA2ODM0YmUxYWE0ODM4MjMwMDRkNmE0OGJlZg==' //test
        ];
        if ($this->isLiveMode()) {
            $this->data = array_merge($this->data, [
                'APIpassword' => 'NjUyNGIzOTdiNThmYmVjMTYwZmJiNDQwNjE2NWM4NzMzYWNiOTkyZWRlMzgxNjg4MjM2MGZkNzY2NDUwYTNlNA==' //live
            ]);
        }
    }
}