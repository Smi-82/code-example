<?php

use abstractClasses\AbstractPaymentSystems;
use traits\SingleTone;

class Gateway_loc extends AbstractPaymentSystems
{
    use SingleTone;

    private function __construct()
    {
        $this->mode = $this->setTestMode();
        $this->data = [
            'name' => 'gateway',
            'mid' => '1849',
            'ccy' => 'EUR',
            'lang' => 'EN',
            'table' => 'gateway_payment_system',
            'test' => true,
            'pay_by_link_url' => '', //not implemented yet
            'PGPayment' => $this->getGatewayPayUrl(),
            'PGResult' => $this->getGatewayResultUrl(),
            'responses' => [
                'NAK' => 'unsuccessful payment (e.g. not enough funds on the account)',
                'UTX' => 'unknown transaction ID',
                'PEN' => 'no result yet, the request has to be repeated',
                'ERR' => 'error (e.g. unauthentic signature, not matching transaction details)',
                'CAN' => 'cardholder clicked on the “Mégsem” (Cancel) button',
                'EXP' => 'payment time expired (25 minutes)',
                'ACK' => 'successful transaction',
                'PE2' => 'request accepted',
                'VOI' => 'reversed (void)',
            ]
        ];
        foreach ($this->data['responses'] as $k => $v)
            $this->data['responses'][$k] = lng($k);
    }

    private function getSignatureForGateway($txid, $amount, $type)
    {
        $mid = $this->getGateway('mid');
        $data_p = http_build_query([
            'mid' => $mid,
            'txid' => $txid,
            'type' => $type,
            'amount' => $amount,
            'ccy' => $this->getGateway('ccy'),
        ]);
        $fp = fopen($_SERVER['DOCUMENT_ROOT'] . '/keys/private_key.pem', "r");
        $priv_key = fread($fp, 8192);
        fclose($fp);
        $pkeyid = openssl_get_privatekey($priv_key);
        openssl_sign($data_p, $signature, $pkeyid);
        openssl_free_key($pkeyid);
        return bin2hex($signature);
    }

    public function getGateway($param = '')
    {
        return $this->getPayment($this->gateway, $param);
    }

    public function getGatewayPayUrl()
    {
        $str = '';
        if (!$this->isLiveMode())
            $str = 'Test';
        return 'https://ebank.khb.hu/PaymentGateway' . $str . '/PGPayment';
    }

    public function getGatewayResultUrl()
    {
        $str = '';
        if (!$this->isLiveMode())
            $str = 'Test';
        return 'https://ebank.khb.hu/PaymentGateway' . $str . '/PGResult';
    }

    public function askGatewayResponse($txid)
    {
        $params = http_build_query([
            'mid' => $this->getGateway('mid'),
            'txid' => $txid,
        ]);
        $answ = file_get_contents($this->getGatewayResultUrl() . '?' . $params);
//        $answ = substr($res, 0, 3);
        $this->addLogGateway($txid, $answ);
        return $answ;
    }

    public function askForReverseGateway($txid, $amount)
    {
        $type = 'RE';
        $sign = $this->getSignatureForGateway($txid, $amount * 100, $type);
        $params = http_build_query([
            'txid' => $txid,
            'type' => $type,
            'mid' => $this->getGateway('mid'),
            'amount' => $amount * 100,
            'ccy' => $this->getGateway('ccy'),
            'sign' => $sign
        ]);
        $answ = file_get_contents($this->getGatewayPayUrl() . '?' . $params);
//        $answ = substr($res, 0, 3);
        $this->addLogGateway($txid, $answ);
        return $answ;
    }

    public function updateGatewayTable($txid, $answer)
    {
        $sql = QueryStr()->update_query_string('gateway_payment_system', ['response' => $answer]) . ' WHERE txid=' . clear_post($txid);
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function addLogGateway($txid, $answer, $dossier_id = 0, $client_id = 0)
    {
        if (!$dossier_id || !$client_id) {
            $sql = 'SELECT gps.dossier_id, gps.response, d.client_id FROM gateway_payment_system AS gps LEFT JOIN dossiers AS d ON d.id=gps.dossier_id WHERE gps.txid=' . $txid;
            $res = querySingle($sql);
            if ($res) {
                $dossier_id = $res['dossier_id'];
                $client_id = $res['client_id'];
            }
        }
        if ($res['response'] != substr($answer, 0, 3)) {
            $path = $_SERVER['DOCUMENT_ROOT'] . '/pay/transactions/' . $this->gateway;
            Folder::create($path);
            $path .= '/client-' . $client_id;
            Folder::create($path);
            $message = 'answer - ' . $answer . ' ' . date('d-m-Y H:i:s');
            $file = $path . '/transaction_' . $dossier_id . '_' . $txid . '.txt';
            $fp = fopen($file, "a+");
            $res = fwrite($fp, PHP_EOL . $message);
            fclose($fp);
        }
        return $res;
    }

    public function gatewayEmailToClient($data = [])
    {
        $domain = new Domain((new Consultant())->getDomainByUser_id($data['client_id']));
        $mailbody_html = Templates::getTemplate('email_gateway_unswer', [
            'first_prase' => $data['response'] == 'ACK' ? 'Merci d’avoir effectué' : 'Vous avez essayé d’effectuer',
            'txid' => $data['txid'],
            'mid' => $this->getGateway('mid'),
            'answer' => $data['response'],
            'transl_status' => $this->getGateway('responses')[$data['response']],
            'amount' => $data['payment'],
        ]);
        $mailbody_txt = '';
        $subject = 'service';
        return Mails()->sendmail($domain->getContactMailAddress(), $data['email'], '', $subject, $mailbody_html, $mailbody_txt);
    }

    public function gatewayEmailReversToClient($data = [])
    {
        $domain = new Domain((new Consultant())->getDomainByUser_id($data['client_id']));
        $mailbody_html = Templates::getTemplate('email_gateway_unswer', [
            'first_prase' => $data['response'] == 'VOI' ? 'Merci d’avoir effectué' : 'Vous avez essayé d’effectuer',
            'txid' => $data['txid'],
            'mid' => $this->getGateway('mid'),
            'status' => $data['response'],
            'transl_status' => $this->getGateway('responses')[$data['response']],
            'amount' => $data['payment'],
        ]);
        $mailbody_txt = '';
        $subject = 'service';
        return Mails()->sendmail($domain->getContactMailAddress(), $data['email'], '', $subject, $mailbody_html, $mailbody_txt);
    }

    public function deleteGatewayResult($txid)
    {
        $sql = 'SELECT dossier_id FROM dossier_fact_payment WHERE transaction_id=' . clear_post($txid);
        $dossier_id = query($sql)[0]['dossier_id'];
        $sql = 'DELETE FROM dossier_fact_payment WHERE transaction_id=' . clear_post($txid);
        $res = query($sql);
        DossierStatuses()->changeDossierStatus($dossier_id);
        return $res;
    }

    public function insertGatewayTable($txid, $amount, $dossier_id)
    {
        $sql = QueryStr()->insert_query_string('gateway_payment_system', [
            'txid' => clear_post($txid),
            'payment' => clear_post($amount) / 100,
            'dossier_id' => clear_post($dossier_id)
        ]);
        try {
            query($sql);
            return getLastId();
        } catch (Exception $e) {
            return false;
        }
    }

    public function saveGatewayResult($txid)
    {
        try {
            $sql = 'SELECT COUNT(id) as cnt FROM dossier_fact_payment WHERE transaction_id=' . clear_post($txid);
            $res = query($sql)[0]['cnt'];
            if (!intval($res)) {
                $sql = '
SELECT
gps.dossier_id,
gps.payment,
u.id AS client_id,
u.consultant_id,
d.status_id
FROM
gateway_payment_system AS gps
LEFT JOIN
dossiers AS d
ON
d.id=gps.dossier_id
LEFT JOIN
' . User::getTable() . ' AS u
ON
u.id=d.client_id
WHERE
gps.txid=' . clear_post($txid);
                $data = query($sql)[0];
                $sql = QueryStr()->insert_query_string('dossier_fact_payment', [
                    'client_id' => $data['client_id'],
                    'dossier_id' => $data['dossier_id'],
                    'payment_name' => $this->gateway,
                    'transaction_id' => $txid,
                    'vendor_id' => $data['consultant_id'],
                    'payment' => $data['payment'],
                    'action' => 'auto',
                    'text' => ''
                ]);
                query($sql);
                $doc = new Documents($data['dossier_id'], $data['client_id']);
                $dfp_id = getLastId();
                $doc->createBill(['dfp_id' => $dfp_id]);
                $files = array(array(
                    'name' => $doc->getFileName($doc->bill),
                    'path' => $doc->getBillOutputDir(['dfp_id' => $dfp_id])
                ));
                $domain = new Domain((new Consultant())->getDomainByUser_id($data['client_id']));
                $reply = Mails()->send_one_mail_with_attachments($domain->getSupportMailAddress(), '', 'danhouri90@gmail.com', '', 'Bill', '<p></p>', '', $files);
                DossierStatuses()->changeDossierStatus($data['dossier_id']);
            }
        } catch (Exception $e) {
            return false;
        }
        return true;
    }
}