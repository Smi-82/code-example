<?php


use traits\BaseSelect;
use traits\GetById;
use traits\GetTable;
use traits\Update;

class Dossier
{
    use GetTable;
    use BaseSelect;
    use Update;
    use GetById;

    private static $table = 'dossiers';

    public static function isSent($id, $isSent = 1)
    {
        return self::update($id, compact('isSent'));
    }

    public static function setMeeting()
    {

    }

    public static function delete($id)
    {
        $id = escape_string(clear_post($id));
        $path = UploadDir::getRootPath() . self::getTable() . DS . $id . DS;
        Folder::delete($path);
        $sql = 'DELETE FROM ' . self::getTable() . ' WHERE id = ' . $id;
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}