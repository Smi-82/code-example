<?php

use abstractClasses\TableData;

class UsersPage extends TableData
{
    protected function addFind()
    {
        if (!empty($this->_findBy)) {
            switch ($this->_findField) {
                case 'userName':
                    $findByName = ' AND (u.firstname LIKE "%' . $this->_findBy . '%" OR u.lastname LIKE "%' . $this->_findBy . '%") ';
                    break;
                case 'phones':
                    $findByName = ' AND (u.phone LIKE "%' . $this->_findBy . '%" OR u.phone2 LIKE "%' . $this->_findBy . '%") ';
                    break;
                default:
                    $findByName = ' AND u.' . $this->_findField . ' LIKE "%' . $this->_findBy . '%" ';
            }
        }
        else
            $findByName = '';
        return $findByName;
    }

    protected function addFilters()
    {
        $filters = '';
        if ($this->_filters != null && is_array($this->_filters)) {
            foreach ($this->_filters as $field => $val) {
                if (!empty($val)) {
                    switch ($field) {
                        default:
                            $filters .= ' AND u.' . $field . ' IN (' . implode(', ', $val) . ') ';
                    }
                }
            }
        }
        return $filters;
    }

    protected function addSort()
    {
        if ($this->_sortWay == 'both') {
            $sort = ' ORDER BY u.id ';
        }
        else {
            $sort = ' ORDER BY ' . $this->_sortBy . ' ' . $this->_sortWay . ' ';
        }
        return $sort;
    }

    public function getTableData()
    {
        $sql = '
SELECT
u.id,
TRIM(CONCAT(u.firstname," ",u.lastname)) AS userName,
u.usertype,
u.email,
TRIM(CONCAT(u.phone, IF(u.phone != "" AND u.phone2 != "", ", ", ""), u.phone2)) AS phones,
u.country,
u.img,
u.lastlogin,
ut.name AS userTypeName
FROM
' . User::getTable() . ' AS u
LEFT JOIN
' . UserTypes::getTable() . ' AS ut
ON
ut.id = u.usertype
WHERE u.usertype !=' . UserTypes()->getClient() . ' ';
        if (User()->isRGPDMaster()) {

            $obj = new Consultant();
            $tmp = $obj->getChildrensId(User()->getId());
            $tmp[] = User()->getId();
//            $clients = $obj->getOwnClientsForConsultant($tmp, ['id']);
//            $users = array_merge($tmp, array_column($clients, 'id'));
            $users = $tmp;
            $users = array_unique($users, SORT_NUMERIC);
            $users = array_filter($users, function ($elem) {
                return $elem != User()->getId();
            });
            $sql .= ' AND u.id IN (' . implode(', ', $users) . ') ';

        }

        $sql .= $findByName = $this->addFind();
        $sql .= $this->addFilters();
        $sql .= $this->addSort();
        $sql .= $this->addPage($findByName);
        try {
            $sql1 = 'SELECT COUNT(u.id) AS cntRows FROM ' . User::getTable() . ' AS u WHERE u.usertype !=' . UserTypes()->getClient();
            if (User()->isRGPDMaster()) {
                $sql1 .= ' AND u.id IN (' . implode(', ', $users) . ') ';
            }
            $sql1 .= $this->addFilters();
            $sql1 .= $findByName;
            $cntRows = querySingle($sql1)['cntRows'];
            $users = query($sql);
            $users = array_map(function ($elem) {
                $elem['img'] = getImg($elem, $elem['id']);
                $lastlogin = strtotime($elem['lastlogin']);
                $elem['lastlogin'] = $lastlogin > 0 ? date('d-m-Y H:i', $lastlogin) : 0;
                return $elem;
            }, $users);
            return compact('users', 'cntRows');
        } catch (Exception $e) {
            return ['error' => $e->getMessage()];
        }
    }
}