<?php

use traits\BaseSelect;
use traits\GetTable;

class TimePackagesProject
{
    private static $table = 'project_time_packages';
    use GetTable;
    use BaseSelect;

    private static function clearTimeSpent($time_spent)
    {
        return array_filter(array_map(function ($el) {
            return array_filter($el);
        }, $time_spent));
    }

    public static function add($data)
    {
        if (!isset($data['user_id']))
            $data['user_id'] = User()->getId();
        $sql = insert_query_string(self::getTable(), $data);
        try {
            $res = query($sql);
            return $res ? getLastId() : $res;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function delete($ids)
    {
        $ids = getAsArray($ids);
        $res = self::getByManyId($ids, ['project_bill_id', 'project_id']);
        $res = array_filter($res, function ($el) {
            return !is_null($el['project_bill_id']);
        });
        if (!empty($res)) {
            foreach ($res as $bill_id) {
                BillProject()->delete($bill_id['project_bill_id'], $bill_id['project_id']);
            }
        }
        $sql = 'DELETE FROM ' . self::getTable() . ' WHERE id IN (' . implode(',', $ids) . ')';
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function updateTimeSpent($id, $tasks_time_spent)
    {
        return self::update($id, [
            'time_spent' => array_sum(Task::calculateTimeSpent($tasks_time_spent)),
            'time_detailing' => json_encode($tasks_time_spent)
        ]);
    }

    public static function updateBilId($id, $val)
    {
        return self::update($id, ['project_bill_id' => $val]);
    }

    public static function update($id, $data)
    {
        $id = escape_string(clear_post($id));
        if (!isset($data['updated']))
            $data['updated'] = date('Y-m-d H:i:s');
        $data['updated'] = date('Y-m-d H:i:s', strtotime($data['updated']));
        $sql = update_query_string(self::getTable(), $data) . ' WHERE id = ' . $id;
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getTrace();
        }
    }

    public static function getAll(array $params = ['*'])
    {
        try {
            return query(self::baseSelect($params));
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function getById($id, array $params = ['*'])
    {
        $res = self::getByManyId($id, $params);
        return empty($res) ? [] : array_shift($res);
    }

    public static function getByManyId($ids, array $params = ['*'])
    {
        $ids = getAsArray($ids);
        try {
            return query(self::baseSelect($params) . ' WHERE id IN (' . implode(',', $ids) . ')');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function getByProjectId($id, array $params = ['*'])
    {
        $id = escape_string(clear_post($id));
        try {
            return query(self::baseSelect($params) . ' WHERE project_id = ' . $id);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function getByBillId($id, array $params = ['*'])
    {
        $id = escape_string(clear_post($id));
        try {
            return querySingle(self::baseSelect($params) . ' WHERE project_bill_id = ' . $id);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function getByProjectIdFullData($id)
    {
        $id = escape_string(clear_post($id));
        $sql = '
            SELECT
            ptp.*,
            TRIM(CONCAT(u.firstname," ",u.lastname)) AS user_name,
            u.id AS user_id,
            ut.name AS ut_name,
            pp.id AS pp_id,
            pp.paid
            FROM ' .
            self::getTable() . ' AS ptp
            LEFT JOIN ' .
            User::getTable() . ' AS u
            ON
            ptp.user_id = u.id
            LEFT JOIN ' .
            UserTypes::getTable() . ' AS ut
            ON
            u.usertype=ut.id
            LEFT JOIN
            ' . PaymentPartsProjectBill::getTable() . ' AS pp
            ON
            pp.project_bill_id = ptp.project_bill_id
            WHERE
            ptp.project_id = ' . $id . ' ORDER BY ptp.id DESC';
        try {
            $res = query($sql);
            $res = array_map(function ($el) {
                $el['created'] = date('d-m-Y H:i', strtotime($el['created']));
                $el['updated'] = date('d-m-Y H:i', strtotime($el['updated']));
                if (is_null($el['paid']))
                    $el['paid'] = 0;
                if (is_null($el['pp_id']))
                    $el['pp_id'] = 0;
                return $el;
            }, $res);
            return $res;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function getCurrentByProjectId($project_id, array $params = ['*'])
    {
        $project_id = escape_string(clear_post($project_id));
        $sql = self::baseSelect($params) . ' WHERE project_id=' . $project_id . ' AND paid=1 ';
        $sql .= ' ORDER BY paid_date ';
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function setPaid($id)
    {
        return self::update($id, ['paid' => 1, 'paid_date' => date('Y-m-d H:i:s')]);
    }

    public static function setUnpaid($id)
    {
        return self::update($id, ['paid' => 0, 'paid_date' => '']);
    }

    public static function getStatisticsByProjectId($proj_id)
    {
        $current = self::getCurrentByProjectId($proj_id, ['id', 'paid', 'val']);
        if (!empty($current)) {
            $tasks = Project::getOwnTasks($proj_id, ['id']);
            $tasks = array_column($tasks, 'id');
            $time_spent = Task::getTimeSpentWithUsers($tasks);
            $time_spent = self::clearTimeSpent($time_spent);
            $sum_by_tasks = Task::calculateTimeSpent($time_spent);
            $sum = array_sum($sum_by_tasks);
            foreach ($current as &$pack) {
                $pack['val'] = intval($pack['val']);
                if ($sum == 0) {
                    break;
                }
                if ($pack['val'] >= $sum) {
                    $pack['time_spent'] = $sum;
                    $pack['time_detailing'] = $time_spent;
                    break;
                } else {
                    $pack['time_spent'] = $pack['val'];
                    $sum -= $pack['val'];
                    $tmp = $pack['val'];
                    $pack['time_detailing'] = [];
                    foreach ($time_spent as $task_id => $users) {
                        foreach ($users as $user_id => $time) {
                            if ($tmp == 0) {
                                break 2;
                            }
                            if ($tmp >= $time) {
                                $pack['time_detailing'][$task_id][$user_id] = $time;
                                $time_spent[$task_id][$user_id] = 0;
                                $tmp -= $time;
                            } else {
                                $pack['time_detailing'][$task_id][$user_id] = $tmp;
                                $time_spent[$task_id][$user_id] = $time - $tmp;
                                $tmp = 0;
                            }
                        }
                    }
                    $time_spent = self::clearTimeSpent($time_spent);
                }
            }
            $time_detailing = array_filter(array_column($current, 'time_detailing'));
            $task_ids = [];
            $user_ids = [];
            foreach ($time_detailing as $tasks) {
                $task_ids = array_merge($task_ids, array_keys($tasks));
                $user_ids = array_merge(array_reduce(array_map(function ($el) {
                    return array_keys($el);
                }, $tasks), function ($arr, $val) {
                    return array_merge($arr, $val);
                }, []));
            }
            $tasks = Task::getManyByIds(array_unique($task_ids), ['id', 'name']);
            $users = User::getManyByIds(array_unique($user_ids), ['id', 'firstname', 'lastname', 'usertype', 'img']);
            $userTypes = UserTypes();
            $users = array_map(function ($el) use ($userTypes) {
                $el['name'] = trim($el['firstname'] . ' ' . $el['lastname']);
                $el['usertype'] = $userTypes->getTypeById($el['usertype'], 'name');
                $el['img'] = getImg($el, $el['id']);
                return $el;
            }, $users);
            $screenshots = array_map(function ($el) {
                $tmp = [];
                foreach ($el as $val) {
                    $src = explode(DS . MODULE . DS, $val['output_dir']);
                    $src = array_pop($src);
                    $val['src'] = DS . MODULE . DS . $src . DS . $val['filename'];
                    $val['whenCreated'] = date('d-m-Y H:i:s', strtotime($val['whenCreated']));
                    $tmp[$val['user_id']][] = $val;
                }
                return $tmp;
            }, TaskScreenshot::getByManyTaskIdForClient($task_ids));
            $current = array_map(function ($el) use ($tasks, $users, $screenshots) {
                if (isset($el['time_detailing'])) {
                    $tmp['tasks'] = [];
                    foreach ($el['time_detailing'] as $task_id => $us) {
                        $tmp['tasks'][$task_id] = array_merge($tasks[$task_id], ['users' => []]);
                        foreach ($us as $us_id => $time) {
                            $tmp['tasks'][$task_id]['users'][] = array_merge($users[$us_id], ['time' => getHoursMinutesFromSeconds($time)], ['screenshots' => isset($screenshots[$task_id][$us_id]) ? $screenshots[$task_id][$us_id] : []]);
                        }
                        $tmp['tasks'] = array_values($tmp['tasks']);
                    }
                    $el['time_detailing'] = $tmp;
                } else {
                    $el['time_detailing'] = [];
                    $el['time_spent'] = 0;
                }
                $el['val'] = getHoursMinutesFromSeconds($el['val']);
                $el['time_spent'] = getHoursMinutesFromSeconds($el['time_spent']);
                return $el;
            }, $current);
            return array_filter($current, function ($el) {
                return !empty($el['time_detailing']);
            });
        }
    }
}