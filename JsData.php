<?php

use traits\SingleTone;

class JsData
{
    use SingleTone;

    private $brackets;

    private function __construct()
    {
        $this->set([
            'langs_login_page' => Language()->getLangData(),
            'delete' => '',
            'fullurl' => SiteData()->getAll('fullurl'),
            'module' => MODULE,
            'language' => Language()->getLanguage(),
            'connection_id' => session_id()
        ]);
    }

    private function isAccos($arr)
    {
        $fkey = 0;
        if ($arr)
            $fkey = array_keys($arr)[0];

        if (!is_numeric($fkey)) return true;

        $st = $fkey - 1;
        foreach ($arr as $key => &$val) {
            if (preg_match('/\D/', $key) || (intval($key) - $st) != 1) {
                return true;
            }
            $st = intval($key);
        }
        return false;
    }

    private function createJsObjectFromArray($arr)
    {
        $str = end($this->brackets)[0];
        foreach ($arr as $key => $val) {
            if (is_array($val)) {
                $isAccos = $this->isAccos($val);
                $this->brackets[] = $isAccos ? ['{', '}'] : ['[', ']'];
                $str .= $key . ': ' . $this->createJsObjectFromArray($val) . ', ';
            }
            else {
                switch (end($this->brackets)[0]) {
                    case '{':
                        $str .= $key . ': "' . $val . '", ';
                        break;
                    case '[':
                        $str .= '"' . $val . '", ';
                        break;
                }
            }
        }
        $str = trim($str);
        $str = trim($str, ',');
        $str .= end($this->brackets)[1];
        array_pop($this->brackets);
        return $str;
    }

    public function getJsData($param = null)
    {
        try {
            return $_SESSION[MODULE]['JsData'] ?? null;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function delete($key = [])
    {
        if (is_array($key) && !empty($key) && count($key) == 1)
            $key = array_shift($key);
        if (!is_array($key) && isset($_SESSION[MODULE]['JsData'][$key])) {
            unset($_SESSION[MODULE]['JsData'][$key]);
            return true;
        }
        elseif (is_array($key) && !empty($key)) {
            $tmp_arr = [];
            if (!isset($_SESSION[MODULE]['JsData'][$key[0]]))
                return false;
            $tmp_arr[$key[0]] = $_SESSION[MODULE]['JsData'][$key[0]];
            for ($i = 1; $i < count($key) - 1; $i++) {
                $tmp = end($tmp_arr);
                if (!isset($tmp[$key[$i]]))
                    return false;
                $tmp_arr[$key[$i]] = $tmp[$key[$i]];
                unset($tmp);
            }
            unset($tmp_arr[$key[count($key) - 2]][$key[count($key) - 1]]);
            $key = array_reverse($key);
            array_shift($key);
            for ($i = 0; $i < count($key) - 1; $i++) {
                $tmp_arr[$key[$i + 1]][$key[$i]] = $tmp_arr[$key[$i]];
            }
            $last_key = array_pop($key);
            $_SESSION[MODULE]['JsData'][$last_key] = $tmp_arr[$last_key];
        }
        return true;
    }

    public function set($arr = [])
    {
        $this->brackets = [];
        $this->brackets[] = ['{', '}'];
        if (!isset($_SESSION[MODULE]['JsData']))
            $_SESSION[MODULE]['JsData'] = [];

        $_SESSION[MODULE]['JsData'] = array_merge($_SESSION[MODULE]['JsData'], $arr);
        $str = '<script>';
        $str .= 'var jsData = ';
        $str .= $this->createJsObjectFromArray($_SESSION[MODULE]['JsData']);
        $str .= '</script>';
        return $str;
    }
}