<?php

use appEvents\AppEventsController;
use models\TaskAccessModel;
use models\TaskModel;
use traits\BaseSelect;
use traits\ClassName;
use traits\GetTable;
use traits\SingleTone;

class Task
{
    use ClassName;
    use GetTable;
    use BaseSelect;
    use SingleTone;

    private static $table = 'tasks';
    const HISTORY_MESSAGES_ACTION_ADD = 'HISTORY_MESSAGES_ACTION_ADD';
    const HISTORY_MESSAGES_ACTION_UPDATE = 'HISTORY_MESSAGES_ACTION_UPDATE';
    const HISTORY_MESSAGES_ACTION_DELETE = 'HISTORY_MESSAGES_ACTION_DELETE';
    private static $fake_id_prefix = 97000;
    private $_fields;
    private $variables = [];

    private static $task_messages_files_output_dir = 'upload' . DS . 'messages' . DS . 'projects';

    public static function getFakeIdPrefix()
    {
        return self::$fake_id_prefix;
    }

    public static function getFakeId($id)
    {
        return intval($id) + self::getFakeIdPrefix();
    }

    public static function getRealId($fake_id)
    {
        return intval($fake_id) - self::getFakeIdPrefix();
    }

    public static function getTable()
    {
        return TaskModel::getTable();
    }

    public static function changeCategory($from, $to)
    {
        return TaskModel::changeCategory($from, $to);
    }

    public static function isBusyOrder($task_id, $ord)
    {
        $project = self::getProjectId($task_id);
        $tasks = Project::getOwnTasks($project);
        try {
            return array_filter($tasks, function ($el) use ($task_id, $ord) {
                return $el['id'] != $task_id && $el['ord'] == $ord;
            });
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function getAll($columns = ['*'])
    {
        return TaskModel::getAll($columns);
    }

    public static function updateSectionComment($task_id, $unset_section = false, $add_section = false)
    {
        $section_comments = self::getById($task_id, ['section_comments']);
        $section_comments = $section_comments['section_comments'];
        try {
            $section_comments = unserialize($section_comments);
        } catch (Exception $e) {
            return $e;
        }

        if ($unset_section !== false) {
            $section_comments[$unset_section]['show'] = 0;
        }

        if ($add_section !== false) {
            $section_comments[$add_section['section']]['comment'] = $add_section["comment"];
            $section_comments[$add_section['section']]['show'] = $add_section["show"];
        }
        $res = self::updateTasksData($task_id, ['section_comments' => serialize($section_comments)]);
        return (bool)$res;
    }

    public static function getByGroupIds($group_ids, $columns = ['*'])
    {
        return TaskModel::getByGroupIds($group_ids, $columns);
    }

    public static function getById($id, $columns = ['*'])
    {
        try {
            $res = self::getManyByIds($id, $columns);
            return $res ? $res[$id] : $res;
        } catch (Exception $e) {
            return false;
        }
    }

    public static function getManyByIds($ids, $columns = ['*'])
    {
        return TaskModel::getManyByIds($ids, $columns);
    }

    public function getOwnParticipants($task_ids, $columns = ['*'])
    {
        return TaskModel::getOwnParticipants($task_ids, $columns);
    }

    public static function order0ToTheEnd($task)
    {
        $with0 = array_filter($task, function ($el) {
            return $el['ord'] == 0;
        });
        $not0 = array_filter($task, function ($el) {
            return $el['ord'] != 0;
        });
        return array_merge($not0, $with0);
    }

    private static function getProjectId($task_id)
    {
        $project_id = self::getInst()->getProject($task_id, ['id']);
        return $project_id['id'];
    }

    public static function geMainFolderName()
    {
        return 'tasks';
    }

    public static function getByWhoCreated($user_id, $columns = ['*'])
    {
        return TaskModel::getByWhoCreated($user_id, $columns);
    }

    public static function getDirectory($task_id, $project_id = 0)
    {
        if (!$project_id) {
            $project_id = self::getProjectId($task_id);
        }
        $str = Project::geMainFolder() . DS . $project_id . DS . self::geMainFolderName() . DS . $task_id;
        Folder::create($str);
        return $str;
    }

    public static function getScreenshotDirectory($task_id, $project_id = 0)
    {
        if (!$project_id) {
            $project_id = self::getProjectId($task_id);
        }
        $str = self::getDirectory($task_id, $project_id) . DS . 'screenshots';
        Folder::create($str);
        return $str;
    }

    public static function saveScreenshot($global_user_id)
    {
        $user_id = User::getUserByGlobalID($global_user_id, ['id']);
        if (empty($user_id))
            return 0;
        $user_id = $user_id['id'];
        $task_id = self::getWorkTaskIdByUserId($user_id);
        if (!intval($task_id))
            return 0;
        $path = self::getScreenshotDirectory($task_id);

        $d = new Datetime("now");
        $ts = $d->format('U');
        $file_name = $user_id . '-' . $task_id . '-' . $ts . '.jpg';
        $file = $path . DS . $file_name;
        $entityBody = file_get_contents('php://input');
        $file_w = fopen($file, 'w+');
        fwrite($file_w, $entityBody);
        fclose($file_w);
        TaskScreenshot::add([
            'task_id' => $task_id,
            'user_id' => $user_id,
            'output_dir' => $path,
            'filename' => $file_name,
        ]);
        return $task_id;
    }

    public static function getWorkTaskIdByUserId($user_id)
    {
        return TaskAccessModel::getWorkTaskIdByUserId($user_id);
    }

    public static function delete($ids)
    {
        $ids = getAsArray($ids);
        $proj_id = [];
        foreach ($ids as $id) {
            $tmp = self::getInst()->getProject($id, ['id']);
            $proj_id[$tmp['id']][] = $id;
        }
        try {
            $res = TaskModel::delete($ids);
            if ($res) {
                foreach ($proj_id as $p_id => $task_ids) {
                    foreach ($task_ids as $t_id) {
                        $path = self::getDirectory($t_id, $p_id);
                        Folder::delete($path);
                    }
                }
            }
            return $res;
        } catch (Exception $e) {
            return false;
        }
    }

    public function getCntParticipantsForOneTask($task_id)
    {
        $res = $this->getCntParticipantsForManyTasks($task_id);
        return $res[$task_id];
    }

    public function getCntParticipantsForManyTasks($task_ids)
    {
        return TaskAccessModel::getCntParticipantsForManyTasks($task_ids);
    }

    public function getOwnParticipantsByTask($task_ids, $columns = ['*'])
    {
        $participants = $this->getOwnParticipants($task_ids, $columns);
        $tmp = [];
        foreach ($participants as $v) {
            $tmp[$v['task_id']][] = array_diff_key($v, ['task_id' => '']);
        }
        return $tmp;

    }

    public function deleteAccess($tasks, $users)
    {
        return TaskAccess::delete($tasks, $users);
    }

    public function addAccess($task_id, $user_id)
    {
        return TaskAccess::add($task_id, $user_id);
    }

    public function getProject($task_id, $columns = ['*'])
    {
        return TaskModel::getProject($task_id, $columns);
    }

    public function getClient($task_id, $columns = ['*'])
    {
        return Project()->getClient($this->getProject($task_id, ['id'])['id'], $columns);
    }

    public function getByAttachmentId($attachment_id, $columns = ['*'])
    {
        return TaskModel::getByAttachmentId($attachment_id, $columns);
    }

    public function getByMessageId($message_id, $columns = ['*'])
    {
        return TaskModel::getByMessageId($message_id, $columns);
    }

    public function cloneTask($task_id, array $data)
    {
        $task_id = escape_string(clear_post($task_id));
        $task = self::getById($task_id);
        $required_fields = [
            'status' => Statuses()->in_preparation(),
            'user_id' => User()->getId(),
            'start' => $data['start'] ?? date('Y-m-d H:i:s'),
            'created' => date('Y-m-d H:i:s'),
            'section_comments' => '',
            'system_access' => '',
            'cloned_from' => $task_id,
            'estimate' => 0,
            'estimateTime' => date("Y-m-d H:i:s", strtotime("+1 month")),
        ];
        $task = array_merge($task, $data, $required_fields);
        return self::getInst()->addNewTask($task);
    }

    public static function updateOrder($task_id, $ord)
    {
        $ord = intval($ord);
        $project = self::getProjectId($task_id);
        $tasks = Project::getOwnTasks($project, ['id', 'ord']);
        $tasks = valueToKey('id', $tasks);
        $arr_for_update = [];
        if ($ord == 0) {
            $arr_for_update[$task_id] = ['id' => $task_id, 'ord' => $ord];
        }
        else {
            $arr_for_update[$task_id] = ['id' => $task_id, 'ord' => $ord];
            do {
                $tmp = array_filter($tasks, function ($el) use ($ord) {
                    return intval($el['ord']) == $ord;
                });
                if (!empty($tmp)) {
                    $tmp = array_shift($tmp);
                    $ord = ++$tmp['ord'];
                    if (!isset($arr_for_update[$tmp['id']])) {
                        $arr_for_update[$tmp['id']] = ['id' => $tmp['id'], 'ord' => $ord];
                    }
                }
            } while (!empty($tmp));
        }
        return TaskModel::updateOrder($arr_for_update);
    }

    public static function updateTasksData($task_id, $data = [])
    {
        if (empty($data))
            return false;
        $columns = DBManager()->getTableColumns(self::getTable());
        $columns = array_flip($columns);
        $data = array_intersect_key($data, $columns);
        foreach ($data as $key => &$val) {
            if (in_array($key, ['start', 'created', 'estimateTime'])) {
                $data[$key] = date('Y-m-d H:i:s', strtotime($val));
            }
        }
        return TaskModel::update($task_id, $data);
    }

    public function addNewTask(array $data = [])
    {
        $current_user = User();
        if (isset($data['id']))
            unset($data['id']);
        $return = [
            'errors' => []
        ];
        foreach (['name', 'estimateTime', 'category', 'priority', 'group_id'] as &$val) {
            if ($data[$val] == '') {
                $return['errors'][] = mb_ucfirst(lng('missed_required_data')) . ' ' . $val;
            }
        }
        if (!empty($return['errors']))
            return $return;

        $user_id = $current_user->isCollaborator() ? $current_user->getParentId() : $current_user->getId();
        $tmp_data = [];
        foreach (['estimateTime', 'startTime'] as $v) {
            $data[$v] = @date('Y-m-d H:i:s', strtotime($data[$v]));
        }
        if (!$this->_fields) {
            $this->_fields = DBManager()->getTableColumns(self::getTable());
            $this->_fields = array_flip($this->_fields);
        }
        if (strtotime($data['estimateTime']) < strtotime($data['startTime']))
            $data['estimateTime'] = date('Y-m-d H:i:s', strtotime($data['startTime'] . ' +1 month'));
        $tmp_data = array_intersect_key($data, $this->_fields);
        $task_id = TaskModel::add(array_merge(
            [
                'user_id' => $user_id,
                'start' => $data['startTime'],
            ], $tmp_data));
        if ($task_id && is_numeric($task_id)) {
            AppEventsController::publish(AppEventsSubscribersController::newTask(), compact('task_id', 'user_id'));
            if (!isset($data['project'])) {
                $data['project'] = $this->getProject($task_id, ['id', 'is_template', 'is_offer']);
            }
            if (!(intval($data['project']['is_template']) || intval($data['project']['is_offer']))) {
                $general_project_id = intval(escape_string(clear_post($data['project']['id'])));
                if (!isset($this->general_project_id) || $this->general_project_id != $general_project_id) {
                    $this->general_project_id = $general_project_id;
                    $client_id = Project()->getClient($general_project_id, ['id']);
                    $users = [$user_id];
                    if (!empty($client_id)) {
                        $users[] = $client_id['id'];;
                    }
                    $res = Project::getAccessData($general_project_id, ['user_id']);
                    if ($res) {
                        $res = array_column($res, 'user_id');
                        $users = array_merge($users, $res);
                        $users = array_unique($users);
                    }
                    $this->users_for_access = $users;
                }

                foreach ($this->users_for_access as $u_id) {
                    TaskAccess::add($task_id, $u_id);
                }
            }
            $return['responce'] = ['task' => $task_id, 'category' => escape_string(clear_post($data['category']))];
        }
        else {
            $return['errors'][] = mb_ucfirst(lng('missed_required_data')) . ': ' . lng('task_is_not_saved');
        }
        history('add_new_task', $current_user->getSignature() . ' Add task (ID:' . $task_id);
        return $return;

    }

    public function __set($name, $value)
    {
        $this->variables[$name] = $value;
    }

    public function __get($name)
    {
        return $this->variables[$name] ?? false;
    }

    public function deleteAttachmentFileById($att_id)
    {
        $attachments = TasksMessagesFiles::getById($att_id, ['id', 'output_dir', 'filename']);
        Folder::delete(UploadDir::getRootPath($attachments['output_dir']));
        TasksMessagesFiles::delete($att_id);
        return true;
    }

    public function getAttachments($task_id)
    {
        return TasksMessagesFiles::getByTaskId($task_id);
    }

    public function getAttachmentById($attachment_id, $columns = ['*'])
    {
        return TasksMessagesFiles::getById($attachment_id);
    }

    public function getMessageById($message_id, $columns = ['*'])
    {
        return TasksMessages::getById($message_id, $columns);
    }

    public function getAttachmentUrl($output_dir, $filename)
    {
        return changeSlash(ltrim(UploadDir::getRelativePath() . $output_dir . $filename, '/'));
    }

    /*
        public function buildAttachmentUploadDirectoryName()
        {
            $date = date_create();
            return $date->format("Y") . DS . $date->format("Y-m-d") . DS . $date->format('H-i-s');
        }

        public function createAttachmentsUploadDirectory()
        {
            $dir = ROOT . DS . self::$task_messages_files_output_dir . DS . $this->buildAttachmentUploadDirectoryName();
            if (!file_exists($dir)) {
                mkdir($dir, 0775, true);
                chmod($dir, 0775);
            }
            return $dir;
        }
    */
    public function addAttachment($task_id)
    {
        $task_id = escape_string(clear_post($task_id));
        $user_id = User()->getId();
        $ret = [];
        if (isset($_FILES['myfile'])) {
            $error = $_FILES['myfile']['error'];
            if (!is_array($_FILES['myfile']['name'])) //single file
            {
                $fileName = $_FILES['myfile']['name'];
                $attachment_id = TasksMessagesFiles::add([
                    'task_id' => $task_id,
                    'output_dir' => '',
                    'filename' => $fileName,
                    'filesize' => $_FILES['myfile']['size'],
                    'added_by_user' => $user_id,
                ]);
                if (!$this->project_id_for_task_attachments) {
                    $this->project_id_for_task_attachments = $this->getProject($task_id, ['id']);
                    $this->project_id_for_task_attachments = $this->project_id_for_task_attachments['id'];
                }
                $dir = self::getDirectory($task_id, $this->project_id_for_task_attachments);
                $dir .= DS . 'task_attachments' . DS . $attachment_id . DS;
                $dir = changeSlash($dir);
                Folder::create($dir);
                $tmp_arr = explode(changeSlash(UploadDir::getRelativePath()), $dir);
                TasksMessagesFiles::updateById($attachment_id, ['output_dir' => array_pop($tmp_arr)]);
                unset($tmp_arr);
                $dir .= $fileName;
                move_uploaded_file($_FILES['myfile']['tmp_name'], $dir);
                $ret[] = $fileName;
            }
            return $ret;
        }
    }

    public function getMessages($task_id)
    {
        return TasksMessages::getMessagesByTask($task_id);
    }

    public function getInsideMessages($task_id)
    {
        return TasksMessagesInside::getMessagesByTask($task_id);
    }

    public function addClientMessage($task_id, $user_id, $message)
    {
        try {
            $message_id = TasksMessages::add([
                'user_id' => escape_string($user_id),
                'task_id' => escape_string($task_id),
                'message' => escape_string($message)
            ]);
            if (is_numeric($message_id)) {
                $project_id = $this->getProject($task_id, ['id'])['id'];
                $this->addToMessagesHistory($project_id, $task_id, $message_id, self::HISTORY_MESSAGES_ACTION_ADD, $message, null);

                ClientActivity::add($project_id, self::getClassName(), $task_id);
            }
        } catch (Exception $e) {
            return $e->getMessage();
        }

    }

    public function addInsideMessage($task_id, $user_id, $message)
    {
        try {
            $message_id = TasksMessagesInside::add([
                'user_id' => escape_string($user_id),
                'task_id' => escape_string($task_id),
                'message' => escape_string($message)
            ]);
        } catch (Exception $e) {
            return $e->getMessage();
        }

    }

    public function editClientMessage($task_id, $message_id, $message)
    {
        $old_message = $this->getMessageById($message_id, ['message'])['message'];

        try {
            $res = TasksMessages::updateById($message_id, compact('message'));
            if ($res) {
                $project_id = $this->getProject($task_id, ['id'])['id'];
                $this->addToMessagesHistory($project_id, $task_id, $message_id, self::HISTORY_MESSAGES_ACTION_UPDATE, $message, $old_message);

                ClientActivity::add($project_id, self::getClassName(), $task_id);
            }
        } catch (Exception $e) {
            return $e->getMessage();
        }

    }

    public function editInsideMessage($task_id, $message_id, $message)
    {

        try {
            return TasksMessagesInside::updateById($message_id, compact('message'));
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function deleteClientMessage($message_id)
    {
        $message_id = escape_string($message_id);
        $message = $this->getMessageById($message_id, ['message'])['message'];
        $task_id = $this->getByMessageId($message_id, ['id'])['id'];
        $project_id = $this->getProject($task_id, ['id'])['id'];
        try {
            $res = TasksMessages::deleteByIdAndUserId($message_id, User()->getId());
            if ($res) {
                $this->addToMessagesHistory($project_id, $task_id, $message_id, self::HISTORY_MESSAGES_ACTION_DELETE, $message, null);

                ClientActivity::add($project_id, self::getClassName(), $task_id);
            }
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function deleteInsideMessage($message_id)
    {
        return TasksMessagesInside::deleteByIdAndUserId($message_id, User()->getId());
    }

    public function renameAttachment($id, $filename)
    {
        $id = escape_string(clear_post($id));
        $filename = trim(escape_string(clear_post($filename)));
        $file_att = TasksMessagesFiles::getById($id, ['output_dir', 'filename']);
        $path = UploadDir::getRootPath($file_att['output_dir'] . $file_att['filename']);
        $path = changeSlash($path);
        if (!file_exists($path))
            return false;
        $fileInfo = new SplFileInfo($path);
        $oldname = $fileInfo->getFilename();
        $newName = $filename . "." . $fileInfo->getExtension();
        if (rename($path, ($fileInfo->getPath()) . DS . $newName)) {
            TasksMessagesFiles::updateById($id, ['filename' => $newName]);
            $task_id = $this->getByAttachmentId($id, ['id']);
            $project_id = $this->getProject($task_id['id'], ['id']);
            ClientActivity::add($project_id['id'], self::getClassName(), $task_id['id']);
            history('task', User()->getSignature() . ' renamed attachment file from "' . $oldname . '" to "' . $newName . '". Task ID : ' . $id);
            return $newName;
        }
        else {
            TasksMessagesFiles::updateById($id, ['filename' => $oldname]);
            return false;
        }
    }

    public function commentAttachment($id, $comment)
    {
        $comment = escape_string($comment);
        $id = escape_string($id);
        $res = TasksMessagesFiles::updateById($id, ['comment' => $comment]);
        if ($res) {
            history('task', User()->getSignature() . ' added comment to attachment ID:"' . $id . '". Comment : ' . $comment);
            $task_id = $this->getByAttachmentId($id, ['id']);
            $project_id = $this->getProject($task_id['id'], ['id']);
            ClientActivity::add($project_id['id'], self::getClassName(), $task_id['id']);
            return $comment;
        }
        return $res;
    }

    public function addToMessagesHistory($project_id, $task_id, $message_id, $action, $message, $old_message)
    {
        $user_id = User()->getId();
        return HistoryTaskMessages::add(compact('project_id', 'task_id', 'message_id', 'action', 'message', 'old_message', 'user_id'));
    }

    public function getMessagesHistoryByTaskId($task_id, $columns = ['*'])
    {
        return HistoryTaskMessages::getByTaskId($task_id, $columns);
    }

    public static function taskReloadForAccessUser($task_id, $user_id)
    {
        $res = self::isUserOnTask($user_id, $task_id);
        $res = intval($res);
        if ($res != intval($task_id)) {
            self::userNotOnTask($user_id);
            self::userOnTask($task_id, $user_id);
        }
        return compact('res', 'task_id', 'user_id');
    }

    public static function userOnTask($task_id, $user_id)
    {
        try {
            $res = TaskAccess::updateByTaskAndUserIds(['is_on_task' => 1], $user_id, $task_id);
            if ($res)
                MessengerEvents()->send(MessengerEventCodes::START_SCREENSHOT_TASK, ['user_id' => $user_id, 'task_id' => $task_id]);
            return $res;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function userNotOnTask($user_id, $task_id = 0)
    {
        try {
            $res = TaskAccess::updateByTaskAndUserIds(['is_on_task' => 0], $user_id, $task_id);
            if ($res)
                MessengerEvents()->send(MessengerEventCodes::END_SCREENSHOT_TASK, ['user_id' => $user_id, 'task_id' => $task_id]);
            return $res;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function isUserOnTask($user_id, $task_id = 0)
    {
        return TaskAccess::isUserOnTask($user_id, $task_id);
    }

    public static function isBindUserToTask($user_id, $task_id = 0)
    {
        return TaskAccess::isBindUserToTask($user_id, $task_id);
    }

    public static function updateTimeSpendByTaskAndUserIds($task_id, $user_id, $interval = 10)
    {
        return TaskAccess::updateTimeSpendByTaskAndUserIds($task_id, $user_id, $interval);
    }

    public static function getTimeSpentWithUsers($ids)
    {
        $res = TaskAccess::getManyByTaskIds($ids, ['user_id', 'task_id', 'time_spent']);
        $tmp = [];
        foreach ($res as &$v)
            $tmp[$v['task_id']][$v['user_id']] = $v['time_spent'];
        return $tmp;
    }

    public static function getFullDataTimeSpentWithUsers($timeSpentWithUsers)
    {
        $task_ids = array_keys($timeSpentWithUsers);
        $user_ids = array_unique(array_merge(array_reduce(array_map(function ($el) {
            return array_keys($el);
        }, $timeSpentWithUsers), function ($arr, $val) {
            return array_merge($arr, $val);
        }, [])));
        $tasks = self::getManyByIds(array_unique($task_ids), ['id', 'name']);
        $users = User::getManyByIds(array_unique($user_ids), ['id', 'firstname', 'lastname', 'usertype', 'img']);
        $userTypes = UserTypes();
        $users = array_map(function ($el) use ($userTypes) {
            $el['name'] = trim($el['firstname'] . ' ' . $el['lastname']);
            $el['usertype'] = $userTypes->getTypeById($el['usertype'], 'name');
            $el['img'] = getImg($el, $el['id']);
            return $el;
        }, $users);
        $tmp = [];
        $screenshots = array_map(function ($el) {
            $tmp = [];
            foreach ($el as $val) {
                $src = explode(DS . MODULE . DS, $val['output_dir']);
                $src = array_pop($src);
                $val['src'] = DS . MODULE . DS . $src . DS . $val['filename'];
                $val['whenCreated'] = date('d-m-Y H:i:s', strtotime($val['whenCreated']));
                $tmp[$val['user_id']][] = $val;
            }
            return $tmp;
        }, TaskScreenshot::getByManyTaskIdForClient($task_ids));
        foreach ($timeSpentWithUsers as $task_id => $us) {
            $tmp[$task_id] = array_merge($tasks[$task_id], ['users' => [], 'time' => getHoursMinutesFromSeconds(self::calculateTimeSpent($timeSpentWithUsers)[$task_id])]);
            foreach ($us as $us_id => $time) {
                $tmp[$task_id]['users'][$us_id] = array_merge($users[$us_id], ['time' => getHoursMinutesFromSeconds($time), 'screenshots' => $screenshots[$task_id][$us_id] ?? '']);
            }
        }
        return $tmp;
    }

    public static function getTimeSpent($ids)
    {
        return self::calculateTimeSpent(self::getTimeSpentWithUsers($ids));
    }

    public static function calculateTimeSpent($timeSpentWithUsers)
    {
        return array_map(function ($el) {
            return array_sum($el);
        }, $timeSpentWithUsers);
    }
}