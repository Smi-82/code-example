<?php
class Domain
{
    private $domain_id;

    private function getTemplate($name)
    {
        $arr = [
            1 => [
                'for_client_create_template' => 'mail_body_new_client.html',
                'when_client_added_in_dossier_template' => 'mail_to_the_client_added_in_dossier.html',
                'meeting_mail_template' => 'meeting_mail.html',
                'program_of_accompaniment_template' => 'program_of_accompaniment.html',
                'send_the_sanctions_of_the_cnil_template' => 'send_the_sanctions_of_the_cnil.html',
                'service_details_tempate' => 'service_details.html',
                'presentations_mail_to_the_client_template' => 'template_presentations_mail_to_the_client.html',
                'service_details_template' => 'service_details.html',
                'mail_body_pdf_send' => 'mail_body_pdf_send.html',
            ],
            2 => [
                'for_client_create_template' => 'mail_body_new_client_solution.html',
                'when_client_added_in_dossier_template' => 'mail_to_the_client_added_in_dossier_solution.html',
                'meeting_mail_template' => 'meeting_mail_solution.html',
                'program_of_accompaniment_template' => 'program_of_accompaniment_solution.html',
                'send_the_sanctions_of_the_cnil_template' => 'send_the_sanctions_of_the_cnil_solution.html',
                'service_details_tempate' => 'service_details_solution.html',
                'presentations_mail_to_the_client_template' => 'template_presentations_mail_to_the_client_solution.html',
                'service_details_template' => 'service_details_solution.html',
                'mail_body_pdf_send' => 'mail_body_pdf_send_solution.html',
            ],
        ];
        switch ($this->domain_id) {
            case 0:
            case 1:
                return $arr[1][$name];
        }
        return $arr[$this->domain_id][$name];
    }

    public function __construct($domain_id)
    {
        $this->domain_id = $domain_id;
    }

    public function getDomain()
    {
        return $this->domain_id;
    }

    public function getContactMailAddress()
    {
        $arr = [
            1 => 'contact@afsmd.fr',
            2 => 'contact@solution-rgpd.net',
        ];
        switch ($this->domain_id) {
            case 0:
            case 1:
                return $arr[1];
        }
        return $arr[$this->domain_id];
    }

    public function getLogo()
    {
        $arr = [
            1 => 'header_logo.png',
            2 => 'Logo-solution-rgpd.net_V2.png',
        ];
        switch ($this->domain_id) {
            case 0:
            case 1:
                return $arr[1];
        }
        return $arr[$this->domain_id];
    }

    public function getBusinessAddress()
    {
        $arr = [
            1 => '10 rue de Penthièvre, 75008 PARIS',
            2 => '',
        ];
        switch ($this->domain_id) {
            case 0:
            case 1:
                return $arr[1];
        }
        return $arr[$this->domain_id];
    }

    public function getBailee()
    {
        $arr = [
            1 => 'Christophe Rébière',
            2 => '',
        ];
        switch ($this->domain_id) {
            case 0:
            case 1:
                return $arr[1];
        }
        return $arr[$this->domain_id];
    }

    public function getStamp()
    {
        $arr = [
            1 => 'stamp.png',
            2 => '',
        ];
        switch ($this->domain_id) {
            case 0:
            case 1:
                return $arr[1];
        }
        return $arr[$this->domain_id];
    }

    public function getSignature()
    {
        $arr = [
            1 => 'signature.png',
            2 => '',
        ];
        switch ($this->domain_id) {
            case 0:
            case 1:
                return $arr[1];
        }
        return $arr[$this->domain_id];
    }

    public function getFooter()
    {
        $arr = [
            1 => 'afsmd_footer',
            2 => 'solution_footer',
        ];
        switch ($this->domain_id) {
            case 0:
            case 1:
                return $arr[1];
        }
        return $arr[$this->domain_id];
    }

    public function getHeader()
    {
        $arr = [
            1 => 'afsmd_header.html',
            2 => '',
        ];
        switch ($this->domain_id) {
            case 0:
            case 1:
                return $arr[1];
        }
        return $arr[$this->domain_id];
    }

    public function getSupportMailAddress()
    {
        $arr = [
            1 => 'support@afsmd.fr',
            2 => 'support@solution-rgpd.net',
        ];
        switch ($this->domain_id) {
            case 0:
            case 1:
                return $arr[1];
        }
        return $arr[$this->domain_id];
    }

    public function getPhrase()
    {
        $arr = [
            1 => 'AFSMD',
            2 => 'Solution-RGPD',
        ];
        switch ($this->domain_id) {
            case 0:
            case 1:
                return $arr[1];
        }
        return $arr[$this->domain_id];
    }

    public function getHref()
    {
        $arr = [
            1 => 'https://rgpdfacile.com',
            2 => 'https://solution-rgpd.net',
        ];
        switch ($this->domain_id) {
            case 0:
            case 1:
                return $arr[1];
        }
        return $arr[$this->domain_id];
    }

    public function mail_body_pdf_send()
    {
        return $this->getTemplate(__FUNCTION__);
    }

    public function for_client_create_template()
    {
        return $this->getTemplate(__FUNCTION__);
    }

    public function when_client_added_in_dossier_template()
    {
        return $this->getTemplate(__FUNCTION__);
    }

    public function meeting_mail_template()
    {
        return $this->getTemplate(__FUNCTION__);
    }

    public function program_of_accompaniment_template()
    {
        return $this->getTemplate(__FUNCTION__);
    }

    public function send_the_sanctions_of_the_cnil_template()
    {
        return $this->getTemplate(__FUNCTION__);
    }

    public function service_details_tempate()
    {
        return $this->getTemplate(__FUNCTION__);
    }

    public function presentations_mail_to_the_client_template()
    {
        return $this->getTemplate(__FUNCTION__);
    }

    public function service_details_template()
    {
        return $this->getTemplate(__FUNCTION__);
    }
}