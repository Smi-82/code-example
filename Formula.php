<?php

use traits\SingleTone;

class Formula
{
    private $variables;
    use SingleTone;

    public function __get($name)
    {
        return $this->variables[$name];
    }

    public function __construct()
    {
        $this->variables = [];
    }

    public function getFile($file, $variables = [])
    {
        $this->variables = $variables;
        ob_start();
        include ROOT . '/templates/' . $file;
        return ob_get_clean();
    }
}