<?php
class BillRGPD
{
    private $bill_path;

    public function createBill($dossier_id, $client_id, $dfp_id)
    {
        $sql = 'SELECT text FROM dossier_fact_payment WHERE id=' . $dfp_id;
        $res = querySingle($sql);

        $fl = false;
        if ($res && !empty($res['text'])) {
            $res = $res['text'];
            $res = unserialize(html_entity_decode($res));
            $fl = true;
        }
        Folder::create($this->bill_path);
        Folder::create($this->bill_path . DS . $client_id);
        $output_dir = $this->bill_path . DS . $client_id . DS;
        $mpdf = new \Mpdf\Mpdf(['tempDir' => UploadDir::getRootPath() . 'Mpdf/billRGPD']);


        $footer = $fl ? $res['footer'] : Templates::getTemplate('afsmd_footer');

        $mpdf->SetHTMLFooter($footer);

        $body = $fl ? $res['body'] : Templates::getTemplate('rgpd_bill', ['$dossier_id' => $dossier_id]);

        $mpdf->writeHTML($body);

        if ($mpdf->Output($output_dir . 'bill-' . $dossier_id . $client_id . $dfp_id . '.pdf', 'F') == '') {
            if (!$fl)
                query(update_query_string('dossier_fact_payment', ['text' => serialize(['body' => $body, 'footer' => $footer])]) . ' WHERE id=' . $dfp_id);
            return true;
        } else {
            return false;
        }
    }

    public function __construct()
    {
        $this->bill_path = UploadDir::getRootPath() . 'bill';
    }

    public function createLastBill($dossier_id)
    {
        $sql = 'SELECT MAX(id) AS id, client_id  FROM dossier_fact_payment WHERE dossier_id=' . $dossier_id;
        $res = querySingle($sql);
        if ($res) {
            $path = $this->bill_path . DS . $res['client_id'] . DS . 'bill-' . $dossier_id . $res['client_id'] . $res['id'] . '.pdf';
            if (!file_exists($path)) {
                $this->createBill($dossier_id, $res['client_id'], $res['id']);
            }
            return true;
        } else {
            return false;
        }
    }

    public function createAllBills($dossier_id)
    {
        $sql = 'SELECT id, client_id  FROM dossier_fact_payment WHERE dossier_id=' . $dossier_id;
        $res = query($sql);
        if ($res) {
            foreach ($res as $val) {
                $path = $this->bill_path . DS . $val['client_id'] . DS . 'bill-' . $dossier_id . $val['client_id'] . $val['id'] . '.pdf';
                if (!file_exists($path)) {
                    $this->createBill($dossier_id, $val['client_id'], $val['id']);
                }
            }
            return true;
        } else {
            return false;
        }
    }
}