<?php

use models\HistoryProjectMessagesModel;

class HistoryProjectMessages
{
    public static function baseSelect(array $columns = ['*'], $aggregate = null)
    {
        return HistoryProjectMessagesModel::baseSelect($columns, $aggregate);
    }

    public static function getTable()
    {
        return HistoryProjectMessagesModel::getTable();
    }

    public static function add($data)
    {
        return HistoryProjectMessagesModel::add($data);
    }

    public static function getByProjectId($project_id, $columns = ['*'])
    {
        return HistoryProjectMessagesModel::getByProjectId($project_id, $columns);
    }
}