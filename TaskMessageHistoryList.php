<?php

use abstractClasses\TableData;

class TaskMessageHistoryList extends TableData
{
    protected function addSort()
    {
        if ($this->_sortWay == 'both') {
            $sort = ' ORDER BY id ';
        }
        else {
            $sort = ' ORDER BY ' . $this->_sortBy . ' ' . $this->_sortWay . ' ';
        }
        return $sort;
    }

    public function getDataByTaskId($task_id, $params = ['*'])
    {
        $sql = HistoryTaskMessages::baseSelect($params) . ' WHERE task_id = ' . $task_id;
        $sql .= $this->addSort();
        try {
            $sql1 = 'SELECT COUNT(id) AS cntRows FROM ' . HistoryTaskMessages::getTable() . ' WHERE task_id = ' . $task_id;
            $cntRows = querySingle($sql1)['cntRows'];
            $history = query($sql);
            return compact('history', 'cntRows', 'sql');
        } catch (Exception $e) {
            return ['error' => $e->getTrace()];
        }
    }
}