<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use traits\SingleTone;

class Mails
{
    use SingleTone;

    private $mail;

    private function __construct()
    {
//        require_once $_SERVER['DOCUMENT_ROOT'] . '/libs/sendmail/PHPMailerAutoload.php';
        require $_SERVER['DOCUMENT_ROOT'] . '/vendor/phpmailer/phpmailer/src/Exception.php';
        require $_SERVER['DOCUMENT_ROOT'] . '/vendor/phpmailer/phpmailer/src/PHPMailer.php';
        require $_SERVER['DOCUMENT_ROOT'] . '/vendor/phpmailer/phpmailer/src/SMTP.php';
        date_default_timezone_set('Europe/PARIS');
//        $mail_settings = [
//            'Host' => 'afsmd.fr',
//            'Port' => '2566',
//            'SMTPSecure' => 'tls',
//            'AuthType' => 'PLAIN',
//            'SmtpVerify' => true,
//            'Username' => 'support@afsmd.fr',
//            'Password' => '!df2gRs6dfgs8Bdf',
//            'sender_email' => 'support@afsmd.fr',
//            'sender_name' => 'website_name',
//            'SMTPDebug' => '0'
//        ];
        $mail_settings = [
            'Host' => 'mail.glooda.com',
            'Port' => '465',
            'SMTPSecure' => 'ssl',
            'SmtpVerify' => true,
            'Username' => 'support@glooda.com',
            'Password' => 'RzOS39a8kbeLj8blVjr3a',
            'sender_email' => 'support@glooda.com',
            'sender_name' => 'website_name'
        ];

        $this->mail = new PHPMailer;
        $this->mail->CharSet = 'utf-8';
        $this->mail->isSMTP();
        if (isset($mail_settings['SMTPDebug']))
            $this->mail->SMTPDebug = $mail_settings['SMTPDebug'];
        if ($mail_settings['SmtpVerify'] === false) {
            $this->mail->smtpConnect(
                array(
                    "ssl" => array(
                        "verify_peer" => false,
                        "verify_peer_name" => false,
                        "allow_self_signed" => true
                    )
                )
            );
        }
//-----------------------------------------------
        $this->mail->Host = $mail_settings['Host'];
        $this->mail->Port = $mail_settings['Port'];
        $this->mail->SMTPSecure = $mail_settings['SMTPSecure'];
        $this->mail->SMTPAuth = true;
        if (isset($mail_settings['AuthType'])) {
            $this->mail->AuthType = $mail_settings['AuthType'];
        } else {
            $this->mail->AuthType = "PLAIN";
        }
        $this->mail->Username = $mail_settings['Username'];
        $this->mail->Password = $mail_settings['Password'];
        $this->mail->ContentType = 'text/plain';
        $this->mail->IsHTML(true);
    }

    public function send_one_mail_Saler($from_email, $from_name, $to_email, $to_name, $subject, $mailbody_html, $mailbody_txt, $files_arr = '', $pdfvirement = '', $pdfcheque = '')
    {
        $this->mail->setFrom($from_email, $from_name);
        $this->mail->addAddress($to_email, $to_name);
        $this->mail->Subject = $subject;
        $this->mail->Body = $mailbody_html;
        $this->mail->AltBody = $mailbody_txt;
//Attach an image file

        if ($files_arr != '') {
            foreach ($files_arr as $file_id) {
                $exeSelectUser = query('select * from sent_mails_files where id=' . $file_id);

                if ($exeSelectUser) {
                    $qwq = $exeSelectUser[0];
                    $fileName = $qwq['filename'];
                    $output_dir = $qwq['output_dir'];
                    $this->mail->addAttachment($output_dir . $fileName, $fileName);
                }
            }
        }

        if ($pdfvirement != '') {
            $this->mail->addAttachment($pdfvirement, "Virement.pdf");
        }
        if ($pdfcheque != '') {
            $this->mail->addAttachment($pdfcheque, "Cheque.pdf");
        }
//    return [$pdfvirement, $pdfcheque];
        $this->mail->addAttachment("files/conditions-d'utilisation-de-AFSMD.pdf", "Conditions-d'utilisation-de-AFSMD.pdf");

//send the message, check for errors
        if (!$this->mail->send()) {
//        return "Mailer Error: " . $mail->ErrorInfo;
//$message11="Mailer Error!";
//echo $message11;
            return false;
        } else {
//$message11="Newsletter subscription was successful!";
//echo $message11;
            return true;
        }

    }

    public function send_dossier_pdf_by_mail($from_email, $from_name, $to_email, $to_name, $subject, $mailbody_html, $mailbody_txt, $dossier_id = 0)
    {
        $this->mail->setFrom($from_email, $from_name);

        $this->mail->addAddress($to_email, $to_name);
        $this->mail->Subject = $subject;
        $this->mail->Body = $mailbody_html;
        $this->mail->AltBody = $mailbody_txt;


        $fileName = 'Prise en charge pour conformité RGPD.pdf';
        $output_dir = PATH_TO_PDF_DOSSIERS . $dossier_id . '/';
        if (file_exists($output_dir . $fileName))
            $this->mail->addAttachment($output_dir . $fileName, $fileName);

        return $this->mail->send();
    }

    public function sendmail($from_email, $to_email, $to_name, $subject, $mailbody_html, $mailbody_txt)
    {
        $this->mail->setFrom($from_email);
        $this->mail->addAddress($to_email, $to_name);
        $this->mail->Subject = $subject;
        $this->mail->Body = $mailbody_html;
        $this->mail->AltBody = $mailbody_txt;
        return $this->mail->send();
    }

    public function send_one_mail($from_email, $from_name, $to_email, $to_name, $subject, $mailbody_html, $mailbody_txt, $files_arr = [])
    {
        $this->mail->setFrom($from_email, $from_name);
        $this->mail->addAddress($to_email, $to_name);
        $this->mail->Subject = $subject;
        $this->mail->Body = $mailbody_html;
        $this->mail->AltBody = $mailbody_txt;

        foreach ($files_arr as $file_id) {
            $exeSelectUser = query("select * from sent_mails_files where id=" . $file_id . " limit 0,1");
            if ($exeSelectUser) {
                $qwq = $exeSelectUser[0];
                $fileName = $qwq['filename'];
                $output_dir = $qwq['output_dir'];
                $this->mail->addAttachment($output_dir . $fileName, $fileName);
            }
        }
        try {
            return $this->mail->send();
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /*
$files = array(
    array(
        'name'=>'Filename1',
        'path'=>'path to file'
    ),
    array(
        'name'=>'Filename2',
        'path'=>'path to file'
    )
)
     */
    public function send_one_mail_with_attachments($from_email, $from_name, $to_email, $to_name, $subject, $mailbody_html, $mailbody_txt, $files = [])
    {
        $this->mail->setFrom($from_email, $from_name);
        $this->mail->addAddress($to_email, $to_name);
        $this->mail->Subject = $subject;
        $this->mail->Body = $mailbody_html;
        $this->mail->AltBody = $mailbody_txt;

        foreach ($files as $file) {
            if (file_exists($file['path'])) {
                @$this->mail->addAttachment($file['path'], $file['name']);
            }
        }

        try {
            return $this->mail->send();
        } catch (Exception $e) {
            return $e->getMessage();
        }

    }

}