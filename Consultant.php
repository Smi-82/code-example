<?php

class Consultant
{
    private $arr;
    private $params;
    private $res;

    public function __construct()
    {
        $this->arr = [];
        $this->res = [];
        $this->params = '*';
    }

    private function getChild($user_id, &$params)
    {
        $sql = 'SELECT ' . implode(',', $params) . ', id AS tmp_id FROM ' . User::getTable() . ' WHERE usertype != ' . UserTypes()->getClient() . ' AND consultant_id IN (' . $user_id . ')';

        try {
            $res = query($sql);

            if ($res) {
                $res_id = array_column($res, 'tmp_id');

                if (in_array($user_id, $res_id))
                    throw new Exception();
                foreach ($res as &$val)
                    unset($val['tmp_id']);
            }
        } catch (Exception $e) {
            $res = [];
            $res_id = [];
        }
        $this->res = array_merge($this->res, $res);
        if ($res) {
            $this->arr = array_merge($this->arr, $res);
            $this->getChild(implode(', ', $res_id), $params);
        }
    }

    public static function getHierarchy($user_id)
    {
        $user_id = escape_string(clear_post($user_id));
        $sql = 'SELECT ut.hierarchy FROM ' . UserTypes::getTable() . ' AS ut LEFT JOIN ' . User::getTable() . ' AS u ON u.usertype=ut.id WHERE u.id=' . $user_id . ' LIMIT 1';
        try {
            return querySingle($sql)['hierarchy'];
        } catch (Exception $e) {
            return $e->getMessage();
        }

    }

    public static function changeMasterForConsultants($master_id, $consultant_ids)
    {
        $consultant_ids = getAsArray($consultant_ids);
        $master_id = escape_string(clear_post($master_id));
        $sql = update_query_string(User::getTable(), [
                'consultant_id' => $master_id
            ]) . ' WHERE id IN (' . implode(',', $consultant_ids) . ')';
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    private function masterId($user_id)
    {
        $res = User::getById($user_id, ['consultant_id']);
        if (!empty($res)) {
            $res = intval($res['consultant_id']);
            if ($res != 0) {
                $this->arr[] = $res;
                $this->masterId($res);
            }
        }
    }

    private function masterByType($user_id, $master_type, array $params): void
    {
        $sql = 'SELECT ' . implode(',', $params) . ', consultant_id AS c_id, usertype AS u_t FROM ' . User::getTable() . ' WHERE id = ' . $user_id . ' LIMIT 1';
        $res = querySingle($sql);

        if ($res) {
            if (intval($res['u_t']) == intval($master_type)) {
                unset($res['u_t']);
                unset($res['c_id']);
                $this->res = $res;
            }
            else {
                $this->masterByType($res['c_id'], intval($master_type), $params);
            }
        }
    }

    public function getDomainByUser_id($user_id)
    {
        $key = 'domain_id';
        $res = $this->getMasterByType($user_id, UserTypes()->getRGPDMaster(), [$key]);
        return empty($res) ? 0 : intval($res[$key]);
    }

    public function getPaymentSystemByUser_id($user_id)
    {
        $key = 'payment_system_id';
        $res = $this->getMasterByType($user_id, UserTypes()->getRGPDMaster(), [$key]);
        return empty($res) ? 0 : intval($res[$key]);
    }

    public function getChildrensId($user_id, array $params = ['id'])
    {
        $params = clearParams($params);
        $this->getChild($user_id, $params);
        return array_column($this->arr, 'id');
    }

    public function getChildrens($user_id, array $params = ['*'])
    {
        $params = clearParams($params);
        $this->getChild($user_id, $params);
        return $this->res;
    }

    public function getMasterIds($user_id)
    {
        $this->masterId($user_id);
        return $this->arr;
    }

    public function getMasterByType($user_id, $master_type, array $params = ['*'])
    {
        $this->masterByType($user_id, $master_type, clearParams($params));
        return $this->res ?? [];
    }

    public function getAllHierarchyHigher($user_id)
    {
        $user_id = escape_string(clear_post($user_id));
        $sql = 'SELECT u.id FROM ' . User::getTable() . ' AS u LEFT JOIN ' . UserTypes::getTable() . ' AS ut ON u.usertype=ut.id WHERE ut.hierarchy>=' . self::getHierarchy($user_id);
        $res = query($sql);
        return array_column($res, 'id');
    }

    public function getAllHierarchyBelow($user_id)
    {
        $sql = 'SELECT u.id FROM ' . User::getTable() . ' AS u LEFT JOIN ' . UserTypes::getTable() . ' AS ut ON u.usertype=ut.id WHERE ut.hierarchy != 0 AND ut.hierarchy<=' . self::getHierarchy($user_id);
        $res = query($sql);
        return array_column($res, 'id');
    }

    public function getOwnClientsForConsultant($consultants_id, array $params = ['*'])
    {
        User::getClientsForConsultant($consultants_id, $params);
    }

    public function getCntClients($consultants_id)
    {
        $consultants_id = getAsArray($consultants_id);
        return querySingle('SELECT COUNT(id) as cnt_clients FROM ' . User::getTable() . ' WHERE usertype=' . UserTypes()->getClient() . ' AND consultant_id IN (' . implode(',', $consultants_id) . ')')['cnt_clients'];
    }
}