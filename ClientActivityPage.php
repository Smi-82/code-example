<?php

use abstractClasses\TableData;

class ClientActivityPage extends TableData
{
    protected function addFilters()
    {
        $filters = '';
        if ($this->_filters != null && is_array($this->_filters)) {

            foreach ($this->_filters as $field => $val) {
                if (!empty($val)) {
                    switch ($field) {
                        case 'withAssistant':
                            $filters .= ' AND c.assistant_id != 0 ';
                            break;
                        case 'withoutAssistant':
                            $filters .= ' AND c.assistant_id = 0 ';
                            break;
                    }
                }
            }
        }
        return $filters;
    }

    protected function addFind()
    {
        if (!empty($this->_findBy)) {
            switch ($this->_findField) {
                case 'id':
                    $findByName = ' AND c.id LIKE "%' . $this->_findBy . '%" ';
                    break;
                case 'clientName':
                    $findByName = ' AND (c.firstname LIKE "%' . $this->_findBy . '%" OR c.lastname LIKE "%' . $this->_findBy . '%") ';
                    break;
                case 'assistant':
                    $findByName = ' AND (a.firstname LIKE "%' . $this->_findBy . '%" OR a.lastname LIKE "%' . $this->_findBy . '%") ';
                    break;
                case 'societe':
                    $findByName = ' AND c.societe LIKE "%' . $this->_findBy . '%" ';
                    break;
                case 'phones':
                    $findByName = ' AND (c.phone LIKE "%' . $this->_findBy . '%" OR c.phone2 LIKE "%' . $this->_findBy . '%") ';
                    break;
                case 'email':
                    $findByName = ' AND c.email LIKE "%' . $this->_findBy . '%" ';
                    break;
                default:
                    $findByName = '';
            }

        }
        else
            $findByName = '';
        return $findByName;
    }

    protected function addSort()
    {
        if ($this->_sortWay == 'both') {
            $sort = ' ORDER BY client_name ASC';
        }
        else {
            switch ($this->_sortBy) {
                case 'id':
                    $sort = ' ORDER BY client_id ' . $this->_sortWay . ' ';
                    break;
                case 'clientName':
                    $sort = ' ORDER BY client_name ' . $this->_sortWay . ' ';
                    break;
                case 'societe':
                    $sort = ' ORDER BY c.societe ' . $this->_sortWay . ' ';
                    break;
                case 'phones':
                    $sort = ' ORDER BY phones ' . $this->_sortWay . ' ';
                    break;
                case 'email':
                    $sort = ' ORDER BY c.email ' . $this->_sortWay . ' ';
                    break;
                default:
                    $sort = '';
            }
        }
        return $sort;
    }

    protected function addPage($findByName = '')
    {
        if (!empty($this->_currentPage) && $this->_cntRows) {
            if (
                empty($findByName)
                &&
                $this->_sortWay == 'both'
                ||
                $this->_sortBy != 'ready'
            ) {
                return ' LIMIT ' . ($this->_currentPage * $this->_cntRows - $this->_cntRows) . ', ' . ($this->_cntRows);
            }
        }
        return '';
    }

    public function getTableData()
    {
        $sql = '
SELECT
c.id AS client_id,
TRIM(CONCAT(c.firstname," ",c.lastname)) AS client_name,
c.img AS c_img,
a.id AS assistant_id,
TRIM(CONCAT(a.firstname," ",a.lastname)) AS assistant_name,
a.img AS a_img
FROM
' . User::getTable() . ' AS c
LEFT JOIN
' . User::getTable() . ' AS a
 ON
 c.assistant_id=a.id
WHERE c.usertype=' . UserTypes()->getClient() . ' ';
        if (User()->isAssistant()) {
            $client_ids = Assistant::getClientsByAssistantId(User()->getId(), ['id']);
            $client_ids = array_column($client_ids, 'id');
            $sql .= ' AND c.id IN (' . implode(',', $client_ids) . ') ';
        }

        $sql .= $findByName = $this->addFind();
        $sql .= ' ' . $this->addFilters();
        $sql .= $this->addSort();
        $sql .= $this->addPage($findByName);

        try {
            $res = query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
        $res = array_map(function ($elem) {
            $elem['c_img'] = getImg($elem, $elem['client_id']);
            $elem['a_img'] = getImg($elem, $elem['assistant_id']);
            $elem['checked'] = false;
            $elem['open'] = true;
            return $elem;
        }, $res);
        $sql1 = 'SELECT COUNT(c.id) AS cnt_clients FROM ' . User::getTable() . ' AS c WHERE c.usertype=' . UserTypes()->getClient() . '  ';
        $sql1 .= $this->addFilters();
        if (isset($client_ids)) {
            $sql1 .= ' AND c.id IN (' . implode(',', $client_ids) . ') ';
        }
        try {
            $cnt_rows = querySingle($sql1);
            $cnt_rows = $cnt_rows['cnt_clients'];
        } catch (Exception $e) {
            return $e->getMessage();
        }
        return ['result' => $res, 'cntData' => $cnt_rows];
    }
}