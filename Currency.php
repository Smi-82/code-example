<?php

use traits\SingleTone;

class Currency
{
    private $default;
    private $arr;
    private $exeptionStr;
    use SingleTone;

    private function __construct()
    {
        $this->default = 'usd';
        $this->exeptionStr = [
            'no_name' => 'No such name - '
        ];
        $this->arr = [
            'usd' => [
                'symbol' => '$',
                'fontawesome_icon' => 'fa fa-dollar',
            ],
            'eur' => [
                'symbol' => '€',
                'fontawesome_icon' => 'fa fa-euro',
            ],
        ];
    }

    private function checkName($name = null)
    {
        if (is_null($name))
            $name = $this->default;
        if (!isset($this->arr[$name]))
            throw new Exception($this->exeptionStr['no_name'] . $name);
        return $name;
    }

    public function getSymbol($name = null)
    {
        $name = $this->checkName($name);
        return $this->arr[$name]['symbol'];
    }

    public function getFontawesomeIcon($name = null)
    {
        $name = $this->checkName($name);
        return $this->arr[$name]['fontawesome_icon'];
    }

    public function getKey($name = null)
    {
        $name = $this->checkName($name);
        return $name;
    }

    public function getKeys()
    {
        $res = array_keys($this->arr);
        sort($res);
        return $res;
    }
}