<?php

use abstractClasses\TableData;
use models\UserModel;

class ProjectListPage extends TableData
{
    protected function addFilters()
    {
        $filters = '';
        if ($this->_filters != null && is_array($this->_filters)) {
            foreach ($this->_filters as $field => $val) {
                if (!empty($val)) {
                    switch ($field) {
                        case 'template':
                            $filters .= ' AND p.is_template = 1 ';
                            break;
                        case 'offer':
                            $filters .= ' AND p.is_offer = 1 ';
                            break;
                        case 'ordinary_projects':
                            $filters .= ' AND p.is_offer = 0 AND p.is_template = 0';
                            break;
                        case 'project_language':
                            $tmp = Language()->getLangs();
                            $val = array_map(function ($el) use ($tmp) {
                                return '"' . $tmp[intval($el)] . '"';
                            }, $val);
                            $filters .= ' AND p.lng IN (' . implode(', ', $val) . ') ';
                            break;
                        case 'status':
                        case 'category':
                        case 'priority':
                        case 'price_type':
                            $filters .= ' AND p.' . $field . ' IN (' . implode(', ', $val) . ') ';
                            break;
                        default:
                            $filters .= '';
                    }
                }
            }
        }
        return $filters;
    }

    protected function addFind()
    {
        $findByName = '';
        if (!empty($this->_findBy)) {
            switch ($this->_findField) {
                case 'participant':
                    $_res = User::findBy(['firstname' => $this->_findBy, 'lastname' => $this->_findBy, 'email' => $this->_findBy], ['id']);
                    $findByName = ' AND p.id = -1 ';
                    if (!empty($_res)) {
                        $_res = array_column($_res, 'id');
                        $_res = array_shift($_res);
                        $project_ids = Access()->getProjectListForUser($_res);
                        if (!empty($project_ids)) {
                            $project_ids = array_unique($project_ids);
                            $project_ids = implode(', ', $project_ids);
                            $findByName = ' AND p.id IN (' . $project_ids . ') ';
                        }
                    }
                    break;
                case 'client':
                    $findByName = ' AND (u.firstname ' . ' LIKE "%' . $this->_findBy . '%" OR u.lastname ' . ' LIKE "%' . $this->_findBy . '%") ';
                    break;
                case 'master':
                    $findByName = ' AND (m.firstname ' . ' LIKE "%' . $this->_findBy . '%" OR m.lastname ' . ' LIKE "%' . $this->_findBy . '%") ';
                    break;
                default:
                    $findByName = ' AND p.' . $this->_findField . ' LIKE "%' . $this->_findBy . '%" ';
            }
        }
        return $findByName;
    }

    protected function addSort()
    {
        if ($this->_sortWay && $this->_sortBy &&
            $this->_sortWay != 'both'
            &&
            $this->_sortBy != 'done'
        ) {
            switch ($this->_sortBy) {
                case 'cntTasks':
                case 'cntGroups':
                    $name = $this->_sortBy;
                    break;
                case 'client':
                    $name = 'client';
                    break;
                case 'master':
                    $name = 'master';
                    break;
                case 'category':
                    $name = 'c.name';
                    break;
                default:
                    $name = 'p.' . $this->_sortBy;
            }
            $sort = ' ORDER BY ' . $name . ' ' . $this->_sortWay;
        }
        else
            $sort = ' ORDER BY p.id DESC';
        return $sort;
    }

    protected function addPage($findByName = '')
    {
        if (!empty($this->_currentPage) && $this->_cntRows) {
            if (
                empty($findByName)
                &&
                $this->_sortWay == 'both'
                ||
                $this->_sortBy != 'done'
            ) {
                return ' LIMIT ' . ($this->_currentPage * $this->_cntRows - $this->_cntRows) . ', ' . ($this->_cntRows);
            }
            else return '';
        }
    }

    public function getTableData($columns = ['*'])
    {
        $columns = clearParams($columns);
        $columns_p = array_map(function ($elem) {
            return 'p.' . $elem;
        }, $columns);
        $current_user = User();
        $findByName = $this->addFind();
        $sql = '
SELECT
' . implode(',', $columns_p) . ',
TRIM(CONCAT(u.firstname, " ", u.lastname)) AS client,
TRIM(CONCAT(m.firstname, " ", m.lastname)) AS master,
(SELECT COUNT(g.id) FROM ' . Group::getTable() . ' AS g WHERE g.general_project_id = p.id) AS cntGroups,
(SELECT COUNT(t.id) FROM ' . Task::getTable() . ' AS t LEFT JOIN ' . Group::getTable() . ' AS g ON g.id=t.group_id LEFT JOIN ' . Project::getTable() . ' AS gp ON gp.id=g.general_project_id WHERE t.active=1 AND gp.id=p.id) as cntTasks,
c.name AS cat_name
FROM
' . Project::getTable() . ' AS p
LEFT JOIN
' . User::getTable() . ' AS u
ON
u.id=p.client_id
LEFT JOIN
' . User::getTable() . ' AS m
ON
m.id=p.master_id
LEFT JOIN
' . Category::getTable() . ' AS c
ON
c.id=p.category
WHERE
p.active = 1 ' . $findByName;
        $projects = [];
        $local_filters = '';
        if (isset($this->_filters['only_my'])) {
            $projects = Project::getProjectListForUser(User()->getId(), true);
        }
        if ($current_user->isRGPDCurator()) {
            $consultant = new Consultant();
            $children_with_current = [];
            $children = $consultant->getChildrensId($current_user->getId());
            if ($children)
                $children_with_current = $children;
            $children_with_current[] = $current_user->getId();
            $local_filters .= ' AND u.consultant_id IN (' . implode(',', $children_with_current) . ') ';
            $sql .= $local_filters;
        }
        else if ($current_user->isRGPDVendor()) {
            $local_filters .= ' AND u.consultant_id = ' . $current_user->getId() . ' ';
            $sql .= $local_filters;
        }
        $current_user_id = $current_user->isCollaborator() ? $current_user->getParentId() : $current_user->getId();
        if ($current_user->isClient()) {
            $projects = Project()->getProjectsByClientId($current_user_id, ['id']);
            $projects = array_column($projects, 'id');
            $projects = implode(',', $projects);
            if (empty($projects))
                $projects = '-1';
        }
        else if (!($current_user->isSupervisor() || $current_user->isClient() || $current_user->isRGPDUser())) {
            $projects = Project()->getProjectListForUser($current_user_id, true);
            if (empty($projects))
                $projects = '-1';
        }
        if (!empty($projects)) {
            $local_filters .= ' AND p.id IN (' . $projects . ') ';
            $sql .= $local_filters;
        }
        $sql .= ' ' . $this->addFilters();
        $sql .= $this->addSort();

        $sql .= $this->addPage($findByName);
        $participants = [];
        try {
            $projects_data = query($sql);
            $project_ids = array_column($projects_data, 'id');
            if (!empty($projects_data)) {
                $participants = Project()->getOwnParticipants($project_ids, ['id', 'img', 'firstname', 'lastname', 'usertype']);
                if ($current_user->isClient()) {
                    foreach ($participants as &$v) {
                        $v = array_filter($v, function ($el) use ($current_user) {
                            return $el['id'] != $current_user->getId();
                        });
                    }
                }
            }
            $projectsDoneData = Project::getDonePercentManyProjects($project_ids);
            $statuses = Statuses();
            $isShowAsOfferToClient = Offer::getCntDisplayedByProjects($project_ids);
            foreach ($projects_data as &$v) {
                $v['displayed_to_client'] = isset($isShowAsOfferToClient[$v['id']]);
                $v['isDefault'] = $v['id'] == Project::getDefaultId();
                $v['done'] = $projectsDoneData[$v['id']];
                if ($this->issetColumnOrAll('dateCreated', $columns))
                    $v['dateCreated'] = date('d-m-Y, H:i', strtotime($v['dateCreated']));
                if ($this->issetColumnOrAll('start', $columns))
                    $v['start'] = date('d-m-Y, H:i', strtotime($v['start']));
                if ($this->issetColumnOrAll('end', $columns))
                    $v['end'] = date('d-m-Y, H:i', strtotime($v['end']));
                if ($this->issetColumnOrAll('priority', $columns))
                    $v['priority'] = Priorities::getKeyById($v['priority']);
                $v['is_template'] = (bool)$v['is_template'];
                $v['is_offer'] = (bool)$v['is_offer'];
                if ($this->issetColumnOrAll('status', $columns))
                    $v['status_name'] = $statuses->getStatusesById($v['status'])['aliase'];
                if ($this->issetColumnOrAll('price_type', $columns)) {
                    $v['price_type_tmp'] = $v['price_type'];
                    $v['price_type'] = Project()->getPriceType(intval($v['price_type']));
                    if (isset($v['price_type']['obj']))
                        $v['price_type'] = $v['price_type']['obj'];
                }
                $v['new_id'] = Project::getFakeId($v['id'] );

                if (isset($participants[$v['id']]) && is_array($participants[$v['id']])) {
                    try {
                        $participants[$v['id']] = array_map(function ($elem) {
                            if (!empty($elem))
                                $elem['img'] = getImg($elem, $elem['id']);
                            return $elem;
                        }, $participants[$v['id']]);
                    } catch (Exception $e) {
                        $participants[$v['id']] = [];
                    }
                    $v['participants'] = $participants[$v['id']];
                }
                else
                    $v['participants'] = [];

            }
            if ($this->_sortWay && $this->_sortBy && $this->_sortBy == 'done' && $this->_sortWay != 'both') {
                usort($projects_data, function ($a, $b) {
                    if ($a[$this->_sortBy] == $b[$this->_sortBy])
                        return 0;
                    switch (strtolower($this->_sortWay)) {
                        case 'asc':
                            return ($a[$this->_sortBy] < $b[$this->_sortBy]) ? -1 : 1;
                        case 'desc':
                            return ($a[$this->_sortBy] > $b[$this->_sortBy]) ? -1 : 1;
                        default:
                            return 0;
                    }
                });
                $projects_data = array_page($projects_data, $this->_currentPage, $this->_cntRows);
            }

            $sql1 = 'SELECT COUNT(p.id) AS cntProjects FROM ' . Project::getTable() . ' AS p LEFT JOIN ' . User::getTable() . ' AS u ON u.id=p.client_id WHERE 1 ';
            $sql1 .= $findByName;
            $sql1 .= $this->addFilters();
            if (!empty($local_filters))
                $sql1 .= $local_filters;
            $sql1 .= ' AND p.active=1 ';

            $cntProjects = querySingle($sql1)['cntProjects'];
            if (empty($cntProjects))
                $cntProjects = 0;
            $error = [];

        } catch (Exception $e) {
            $cntProjects = 0;
            $projects_data = [];
            $error = [$e->getMessage(), $sql];
        }
        return ['projects' => $projects_data, 'part' => $participants, 'cntProjects' => $cntProjects, 'error' => $error, 'sql' => $sql];
    }
}