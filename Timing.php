<?php

class Timing
{
    private static $tokens = [
        31536000 => 'year',
        2592000 => 'month',
        604800 => 'week',
        86400 => 'day',
        3600 => 'hour',
        60 => 'minute',
        1 => 'seconds'
    ];

    public static function get($time, $arr = [])
    {
//        if (empty($arr))
//            $time = time() - $time;
        $time = ($time < 1) ? 1 : $time;
        foreach (self::$tokens as $unit => $text) {
            if ($time < $unit) continue;
            $numberOfUnits = floor($time / $unit);
            $arr[] = ['val' => $numberOfUnits, 'key' => lng($text)];
            break;
        }

        $time -= $numberOfUnits * $unit;
        if ($time <= 1)
            return $arr;
        return self::get($time, $arr);
    }

    public static function asString($arr)
    {
        return trim(array_reduce($arr, function ($result, $elem) {
            $result .= $elem['val'] . mb_substr($elem['key'], 0, 1) . ', ';
            return $result;
        }, ''), ', ');
    }
}