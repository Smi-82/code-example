<?php

use models\ClientUsersModel;

class ClientUsers
{
    private static function _getArray($client_id, $user_id, $display_duration)
    {
        return compact('client_id', 'user_id', 'display_duration');
    }

    public static function setDuration($id, $display_duration)
    {
        $display_duration = $display_duration['h'] * 3600 + $display_duration['d'] * 24 * 3600;
        return ClientUsersModel::update($id, compact('display_duration'));
    }

    public static function add($client_id, $user_id)
    {
        $data = compact('client_id', 'user_id');
        $data['who_added'] = User()->getId();
        return ClientUsersModel::add($data);
    }

    public static function removeBasicByClientId($client_id)
    {
        $res = ClientUsersModel::updateByClient($client_id, ['is_basic' => 0]);
        if (!$res)
            return false;
        return User::setDateForPopup($client_id);
    }

    public static function setAsBasic($id, $is_basic = 1)
    {
        $client_id = ClientUsersModel::getById($id, ['client_id']);
        $client_id = $client_id['client_id'];
        self::removeBasicByClientId($client_id);
        if ($is_basic)
            return ClientUsersModel::update($id, ['is_basic' => $is_basic]);
        else
            return true;
    }


    public static function getDurationCurrentByClientId($client_id)
    {
        $res = ClientUsersModel::getCurrentByClientId($client_id, ['display_duration']);
        if (empty($res))
            return 0;
        return intval($res['display_duration']);

    }

    public static function getByClientId($client_id)
    {
        $res = ClientUsersModel::getByClientId($client_id);
        if (empty($res))
            return $res;
        $user_ids = array_reduce($res, function ($r, $item) {
            $r[] = $item['user_id'];
            $r[] = $item['who_added'];
            return $r;
        }, []);
        $user_ids[] = $client_id;
        $user_ids = array_unique($user_ids);
        $users = User::getManyByIds($user_ids, ['id', 'firstname', 'lastname', 'usertype', 'img']);
        $secInDay = 24 * 3600;
        foreach ($res as &$val) {
            foreach (['client_id', 'user_id', 'who_added'] as $v) {
                $val[$v] = $users[$val[$v]];
                $val[$v]['user_type_name'] = UserTypes()->getTypeById($val[$v]['usertype'], 'name');
                $val[$v]['user_name'] = trim($val[$v]['firstname'] . ' ' . $val[$v]['lastname']);
                $val[$v]['img'] = getImg($val[$v], $val[$v]['id']);
            }
            $val['duration']['d'] = floor(intval($val['display_duration']) / $secInDay);
            $val['duration']['h'] = (intval($val['display_duration']) - $val['duration']['d'] * $secInDay) / 3600;
        }
        return $res;
    }
}