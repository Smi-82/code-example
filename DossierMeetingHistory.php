<?php


use traits\GetTable;

class DossierMeetingHistory
{
    use GetTable;

    private static $table = 'dossier_meeting_history';

    public static function add($dossier_id, $meeting)
    {
        $sql = insert_query_string(self::getTable(), compact('dossier_id', 'meeting'));
        try {
            $res = query($sql);
            return $res ? getLastId() : $res;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}