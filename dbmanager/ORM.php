<?php

namespace dbmanager;



use dbmanager\abstracts\ADBWorker;
use dbmanager\interfaces\iDBManager;
use traits\SingleTone;

class ORM extends ADBWorker implements iDBManager
{
    use SingleTone;

    private function __construct()
    {
        $this->setConnection();
        if ($this->getLink() == null) {
            $connection = new Opis\Database\Connection(
                'mysql:host=' . $this->getDbhost() . ';dbname=' . $this->getDbname(),
                $this->getDblogin(),
                $this->getDbpass()
            );
            $this->link = new Opis\Database\Database($connection);
        }
    }

    public function getLink()
    {
        return $this->link;
    }
}