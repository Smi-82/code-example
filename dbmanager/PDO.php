<?php

namespace dbmanager;


use dbmanager\abstracts\ADBWorker;
use dbmanager\interfaces\iDBManager;
use traits\SingleTone;

class PDO extends ADBWorker implements iDBManager
{
    use SingleTone;

    private function __construct()
    {
        $this->setConnection();
        if ($this->getLink() == null) {
            try {
                $this->link = new PDO('mysql:host=' . $this->getDbhost() . ';dbname=' . $this->getDbname(), $this->getDblogin(), $this->getDbpass());
                $this->getLink()->exec("SET NAMES UTF8");
            } catch (PDOException $e) {
                print "Error!: " . $e->getMessage() . "<br/>";
                die();
            }
        }
    }

    public function getLink()
    {
        return $this->link;
    }
}