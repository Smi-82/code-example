<?php

namespace dbmanager\interfaces;

interface iDBManager
{
    public function getLink();
}