<?php

namespace dbmanager\abstracts;

/**
 *
 */
class ADBWorker
{
    protected $dbhost;
    protected $dblogin;
    protected $dbpass;
    protected $dbname;
    protected $link;

    protected function setConnection()
    {
        $server_name = $_SERVER['SERVER_NAME'];
        include_once $_SERVER['DOCUMENT_ROOT'] . '/' . MODULE . '/dbSettings/myHost.php';
        $localDomains = getDevLocalDomains();
        if (in_array($server_name, $localDomains)) {
            $hosts = getDbSettings('local');
        }
        else {
            $server_name = explode('.', $server_name);
            $server_name = array_shift($server_name);
            $key = $server_name == 'test' ? 'test' : 'prod';
            $hosts = getDbSettings($key);
        }
        $this->dbhost = $hosts['host'];
        $this->dblogin = $hosts['dblogin'];
        $this->dbpass = $hosts['dbpass'];
        $this->dbname = $hosts['dbname'];
        $this->link = null;
    }

    /**
     * @return mixed
     */
    public function getDbname()
    {
        return $this->dbname;
    }

    /**
     * @return mixed
     */
    public function getDbhost()
    {
        return $this->dbhost;
    }

    /**
     * @return mixed
     */
    public function getDblogin()
    {
        return $this->dblogin;
    }

    /**
     * @return mixed
     */
    public function getDbpass()
    {
        return $this->dbpass;
    }
}