<?php

namespace dbmanager;

use dbmanager\abstracts\ADBWorker;
use dbmanager\interfaces\iDBManager;
use traits\SingleTone;

class Native extends ADBWorker implements iDBManager
{
    use SingleTone;

    private function __construct()
    {
        $this->setConnection();
        if ($this->getLink() == null) {
            $this->link = mysqli_connect($this->getDbhost(), $this->getDblogin(), $this->getDbpass());
            mysqli_set_charset($this->getLink(), 'utf8');
            mysqli_select_db($this->getLink(), $this->getDbname()) or die(mysqli_error($this->getLink()));
        }
    }

    public function getLink()
    {
        return $this->link;
    }
}