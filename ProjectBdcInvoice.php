<?php

class ProjectBdcInvoice
{
    public static function getDataForPDF($bill_id, array $columns = [])
    {
        $tmp_arr = [
            Project::getTable() => ['id', 'price', 'name', 'client_id', 'start', 'end', 'price_type', 'descr', 'evaluation'],
            User::getTable() => ['id', 'firstname', 'lastname', 'societe', 'address', 'nomero', 'nomero_tva', 'city', 'zip'],
            Task::getTable() => ['id', 'name', 'group_id', 'description', 'category', 'ord'],
            PaymentPartsProjectBill::getTable() => [],
            BillProject::getTable() => [],
        ];
        foreach ($tmp_arr as $key => $params) {
            if (isset($columns[$key])) {
                $columns[$key] = array_merge($params, $columns[$key]);
                $columns[$key] = array_unique($columns[$key]);
            } else {
                $columns[$key] = $params;
            }
            if (empty($columns[$key]))
                $columns[$key][] = '*';
        }
        $res = BillProject::getById($bill_id, $columns[BillProject::getTable()]);
        $res['project'] = Project()->getById($res['project_id'], $columns[Project::getTable()]);
        if ($res['project']['price_type'] == Project::getIdPriceTypeByHourlyPayment()) {
            $res['project']['time_package'] = TimePackagesProject::getByBillId($bill_id, ['id', 'val']);
            $res['project']['evaluation'] = getHoursMinutesFromSeconds($res['project']['time_package']['val']);
        }
        $res['client'] = User::getById($res['project']['client_id'], $columns[User::getTable()]);
        $tmp_tasks_arr = $columns[Task::getTable()];
        if (empty($res['tasks'])) {
            $res['tasks'] = Project::getOwnTasks($res['project_id'], $tmp_tasks_arr);
            $res['tasks'] = Task::order0ToTheEnd($res['tasks']);
            $res['tasks'] = array_map(function ($elem) {
                unset($elem['_p_id']);
                return $elem;
            }, $res['tasks']);
            $tmp_task_ids = array_column($res['tasks'], 'id');
        } else {
            $tmp_task_ids = !is_array($res['tasks']) ? explode(',', $res['tasks']) : $res['tasks'];
            $res['tasks'] = array_values(Task::getManyByIds($tmp_task_ids, $tmp_tasks_arr));
        }
        $res['cnt_groups'] = 0;
        if (!empty($res['tasks'])) {
            $res['cnt_groups'] = count(array_unique(array_column($res['tasks'], 'group_id')));
            $cntTasks = Task()->getCntParticipantsForManyTasks($tmp_task_ids);
            $categories_id = array_column($res['tasks'], 'category');
            $categories_id = array_unique($categories_id);
            $categories = Category::getCategoriesById($categories_id, ['id', 'name']);
            $categories = array_combine(array_column($categories, 'id'), $categories);
            foreach ($res['tasks'] as &$val) {
                $val['cnt_users'] = isset($cntTasks[$val['id']]) ? $cntTasks[$val['id']] : 0;
                $val['category'] = $categories[$val['category']]['name'];
            }
        }
        $res['payment_parts'] = PaymentPartsProjectBill::getByBillId($res['id'], $columns[PaymentPartsProjectBill::getTable()]);
        return $res;
    }

    private static function asByTasks($proj)
    {

    }

    private static function asBySubscription($proj)
    {
        $date = date('Y-m-d H:i:s');
        if (!empty($proj['schedule'])) {
            $schedule = Project::createPaymentSchedule($proj['schedule'], $date, Project::createPrice($proj, OurCompany()->tva), $proj['moment_of_payment'], $proj['prepaid'], $proj['price_type']);
            $schedule = ['start' => array_shift($schedule), 'end' => array_pop($schedule)];
        } else {
            $nextShed['current']['price'] = Project::createPrice($proj, OurCompany()->tva);
            if (intval($proj['prepaid']))
                $date = $proj['start'];
            else
                $date = $proj['end'];
            $nextShed['next'] = false;
        }
        $schedule = array_map(function ($el) {
            return date('Y-m-d H:i:s', strtotime($el['date']));
        }, $schedule);
        $res = Project()->editProjectData($proj['id'], array_merge([
            'schedule_create' => $date,
            'prepaid' => 1,
            'is_offer' => 0,
            'offer_client_id' => $proj['client_id'],
        ], $schedule));
        $proj = Project()->getById($proj['id']);
        self::createNextBdcInvoice($proj);
    }

    private static function asByHourlyPayment($proj, $data = [])
    {
        $val = BillProject()->getBillDataForBill($proj);
        $statuses = Statuses();
        $val['tasks'] = array_filter($val['tasks'], function ($el) use ($statuses) {
            return !in_array($el['status'], [$statuses->approved_by_the_customer()]);
        });
        $bill_id = BillProject()->add($val);
        TimePackagesProject::updateBilId($data[TimePackagesProject::getTable()], $bill_id);
        $bill_obj = BillProject::getById($bill_id);

        $pp_id = PaymentPartsProjectBill::add([
            'payment_date' => intval($proj['prepaid']) ? date('Y-m-d') : $proj['end'],
            'amount' => $bill_obj['amount'],
            'percent' => 100,
            'project_id' => $proj['id'],
            'project_bill_id' => $bill_id,
        ]);
        BillProject()->createPDF(BillProject()->getDataForPDF($bill_id));
    }

    private static function asByPackage($proj)
    {
        $val = BillProject()->getBillDataForBill($proj);
        $bill_id = BillProject()->add($val);
        MessengerEvents()->send(MessengerEventCodes::CONFIRM_PURCHASE_ORDER, ['bill_id' => $bill_id]);
        $bill_obj = BillProject::getById($bill_id);
        PaymentPartsProjectBill::add([
            'payment_date' => intval($proj['prepaid']) ? date('Y-m-d') : $proj['end'],
            'amount' => $bill_obj['amount'],
            'percent' => 100,
            'project_id' => $proj['id'],
            'project_bill_id' => $bill_id,
        ]);
        BillProject()->createPDF(BillProject()->getDataForPDF($bill_id));
    }

    public static function cron()
    {
        $sql = '
SELECT
p.*
FROM ' .
            Project::getTable() . ' AS p
    WHERE
    p.price_type=' . Project::getIdPriceTypeBySubscription() . '
    AND
    p.paid=0
    AND
    p.is_template=0
    AND
    p.is_offer=0
    AND
    p.client_id != 0
AND
(
  TIMESTAMPDIFF(DAY,NOW(),p.schedule_next_payment_date) < 7
  OR
  p.schedule_next_payment_date = 0
  )
AND
(SELECT COUNT(id) FROM ' . BillProject::getTable() . ' WHERE project_id=p.id AND created=p.schedule_next_payment_date) = 0
';
        try {
            $res = query($sql);
            if (!empty($res)) {
                foreach ($res as $proj) {
                    self::asBySubscription($proj);
                }
            }
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function createNextBdcInvoice($proj)
    {
        try {
            $schedule = Project::createPaymentSchedule($proj['schedule'], $proj['schedule_create'], Project::createPrice($proj, OurCompany()->tva), $proj['moment_of_payment'], $proj['prepaid']);
            $nextShed = Project::nextPayment($schedule, $proj['schedule_next_payment_date']);
            $date = date('Y-m-d H:i:s', strtotime($nextShed['current']['date']));
            Project()->editProjectData($proj['id'], ['schedule_next_payment_date' => $date]);
            $bill = BillProject()->add([
                'amount' => $nextShed['current']['price'],
                'created' => $date,
                'project_id' => $proj['id'],
                'tasks' => '',
                'readTOS' => 1,
                'whoCreate' => date('Y-m-d H:i:s'),
            ]);
            PaymentPartsProjectBill::add([
                'payment_date' => $date,
                'amount' => $nextShed['current']['price'],
                'percent' => 100,
                'project_id' => $proj['id'],
                'project_bill_id' => $bill,
            ]);
            BillProject()->createPDF(BillProject()->getDataForPDF($bill));
            BillProject()->approve($bill);
            if ($nextShed['next'] === false)
                Project()->editProjectData($proj['id'], ['paid' => 1]);
            else {
                $date = date('Y-m-d H:i:s', strtotime($nextShed['next']['date']));
                Project()->editProjectData($proj['id'], ['schedule_next_payment_date' => $date]);
            }
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public static function createByProjectPriceType($proj_id, $data = [])
    {
        $project = Project()->getById($proj_id);
        $priceType = intval($project['price_type']);
        switch ($priceType) {
            case Project::getIdPriceTypeByTasks():
                return self::asByTasks($project);
            case Project::getIdPriceTypeBySubscription():
                return self::asBySubscription($project);
            case Project::getIdPriceTypeByHourlyPayment():
                if (isset($data[TimePackagesProject::getTable()])) {
                    $time = TimePackagesProject::getById($data[TimePackagesProject::getTable()], ['val']);
                    $project['evaluation'] = $time['val'];
                }
                return self::asByHourlyPayment($project, $data);
            case Project::getIdPriceTypeByPackage():
                return self::asByPackage($project);
        }
    }
}