<?php
use abstractClasses\TableData;
class ConsultantHierarchePage extends TableData
{
    protected function addSort()
    {
        if ($this->_sortWay == 'both') {
            $sort = ' ORDER BY u.id ';
        } else {
            switch ($this->_sortBy) {
                case 'id':
                    $sort = ' ORDER BY u.id ' . $this->_sortWay . ' ';
                    break;
                case 'consultantName':
                    $sort = ' ORDER BY user_name ' . $this->_sortWay . ' ';
                    break;
                case 'status':
                    $sort = ' ORDER BY u.usertype ' . $this->_sortWay . ' ';
                    break;
                default:
                    $sort = '';
            }

        }
        return $sort;
    }
    public function getTableData()
    {
        $current_user = User();
        $sql = '
SELECT
u.id,
u.usertype,
u.img,
TRIM(CONCAT(u.firstname," ", u.lastname)) AS user_name,
TRIM(CONCAT(c.firstname," ", c.lastname)) AS curator_name,
u.consultant_id,
ut.name AS ut_name
FROM
' . User::getTable() . ' AS u
LEFT JOIN
' . UserTypes::getTable() . ' AS ut
ON
u.usertype = ut.id
LEFT JOIN
' . User::getTable() . ' AS c
ON
c.id = u.consultant_id
WHERE 1 ';
        if ($current_user->isRGPDCurator()) {
            $obj = new Consultant();
            $consultants = $obj->getChildrensId($current_user->getId());
            if (empty($consultants))
                $consultants[] = 0;
            $sql .= 'AND u.id IN (' . implode(',', $consultants) . ')';
        } elseif ($current_user->isSupervisor()) {
            $sql .= ' AND u.usertype IN (' . UserTypes()->getRGPDUsers(true) . ')';
        }
        $sql .= $this->addSort();
        try {
            $consultants = query($sql);
        } catch (Exception $e) {
            $consultants = [];
        }
        foreach ($consultants as &$val) {
            $obj = new Consultant();
            $tmp = $obj->getChildrensId($val['id']);
            $tmp[] = $val['id'];
            $val['cntClients'] = $obj->getCntClients($tmp);
            $val['checked'] = false;
            $val['show'] = true;
            $val['usertypeName'] = UserTypes()->getTypeById($val['usertype'])['name'];
            unset($obj);
        }
        if ($this->_sortBy == 'cntClients' && $this->_sortWay != 'both') {
            usort($consultants, function ($a, $b) {
                $a = intval($a[$this->_sortBy]);
                $b = intval($b[$this->_sortBy]);
                if (intval($a) == intval($b))
                    return 0;
                switch ($this->_sortWay) {
                    case 'asc':
                        return (intval($a) < intval($b)) ? -1 : 1;
                    case 'desc':
                        return (intval($a) > intval($b)) ? -1 : 1;
                    default:
                        return 0;
                }
            });
        }
        return $consultants;
    }
}