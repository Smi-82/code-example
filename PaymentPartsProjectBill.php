<?php

use traits\BaseSelect;
use traits\GetTable;
use traits\SingleTone;

class PaymentPartsProjectBill
{
    private static $table = 'project_bill_payment_parts';
    use SingleTone;
    use GetTable;
    use BaseSelect;

    public static function getByProjectId($project_id, $columns = ['*'])
    {
        $project_id = escape_string(clear_post($project_id));
        $sql = self::baseSelect($columns) . ' WHERE project_id=' . $project_id;
        try {
            $res = query($sql);
            $res = array_map(function ($elem) {
                $elem['payment_date'] = date('d-m-Y', strtotime($elem['payment_date']));
                return $elem;
            }, $res);
            return $res;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function setPaid($id, $val = 1)
    {
        return self::update($id, ['paid' => $val, 'paid_date' => date('Y-m-d H:i:s'), 'who_paid' => User()->getUser() ? User()->getId() : 0]);
    }

    public static function setUnPaid($id)
    {
        $res = self::setPaid($id, 0);
        return $res ? self::update($id, ['paid_date' => 0, 'who_paid' => 0]) : $res;
    }

    public static function getByBillId($bill_id, $columns = ['*'])
    {
        $bill_id = escape_string(clear_post($bill_id));
        $sql = self::baseSelect($columns) . ' WHERE project_bill_id=' . $bill_id . ' ORDER BY payment_date';
        try {
            $res = query($sql);
            $res = array_map(function ($elem) {
                $elem['payment_date'] = date('d-m-Y', strtotime($elem['payment_date']));
                return $elem;
            }, $res);
            return $res;
        } catch (Exception $e) {
            return $sql;
            return $e->getMessage();
        }
    }

    public static function getById($id, $columns = ['*'])
    {
        $id = escape_string(clear_post($id));
        $sql = self::baseSelect($columns) . ' WHERE id=' . $id . ' LIMIT 1';
        try {
            $res = querySingle($sql);
            if ($res && isset($res['payment_date']))
                $res['payment_date'] = date('d-m-Y', strtotime($res['payment_date']));
            return $res;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function update($id, $data)
    {
        $id = escape_string(clear_post($id));
        $data = array_map(function ($elem) {
            if (isset($elem['payment_date']))
                $elem['payment_date'] = date('Y-m-d', strtotime($elem['payment_date']));
            return $elem;
        }, $data);
        unset($data['id']);
        $sql = update_query_string(self::getTable(), $data) . ' WHERE id=' . $id;
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function add($data)
    {
        if (isset($data['payment_date']))
            $data['payment_date'] = date('Y-m-d', strtotime($data['payment_date']));
        unset($data['id']);
        $sql = insert_query_string(self::getTable(), $data);
        try {
            query($sql);
            return getLastId();
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function get($columns = ['*'])
    {
        $sql = self::baseSelect($columns);
        try {
            $res = query($sql);
            return array_map(function ($elem) {
                $elem['payment_date'] = date('d-m-Y', strtotime($elem['payment_date']));
                return $elem;
            }, $res);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function getLastUnpaidByBillId($bill_id, $columns = ['*'])
    {
        if (!in_array('*', $columns)) {
            if (!in_array('payment_date', $columns)) $columns[] = 'payment_date';
            if (!in_array('paid', $columns)) $columns[] = 'paid';
        }
        $payment_parts = self::getByBillId($bill_id, $columns);

        if (empty($payment_parts)) return [];
        usort($payment_parts, function ($a, $b) {
            return date_create($a['payment_date']) < $b['payment_date'];
        });

        foreach ($payment_parts as $part) {
            if (!intval($part['paid'])) return $part;
        }
        return [];
    }

    public static function getAllUnpaidOfInvoices($columns = ['*'])
    {

        $sql = 'SELECT ' . columnsArrayToQueryStringWithAlias('a', $columns) . ' '
            . ' FROM ' . self::getTable() . ' a '
            . ' LEFT JOIN ' . BillProject::getTable() . ' b '
            . ' ON a.`project_bill_id` = b.`id` '
            . ' WHERE b.`approved` = 1';
        try {
            $res = query($sql);
            return array_map(function ($elem) {
                $elem['payment_date'] = date('d-m-Y', strtotime($elem['payment_date']));
                return $elem;
            }, $res);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}