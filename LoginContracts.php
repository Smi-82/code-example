<?php
class LoginContracts
{
    private $output_dir;
    private $folder;
    private $client_id;
    private $contracts;
    private $contracts_template_path;

    private function getContractForlder($name)
    {
        $tmp = $this->output_dir;
        $tmp .= $name . DS;
        Folder::create($tmp);
        return $tmp;
    }
    private function getbody($file)
    {
        $dom = new DOMDocument;
        $dom->loadHTML($file);
        $bodies = $dom->getElementsByTagName('body');
        assert($bodies->length === 1);
        $body = $bodies->item(0);
        for ($i = 0; $i < $body->children->length; $i++) {
            $body->remove($body->children->item($i));
        }
        $stringbody = $dom->saveHTML($body);
        return $stringbody;
    }
    public function __construct($client_id = 0)
    {
        $this->contracts_template_path = 'user_contracts/clients/';
        $this->contracts = [
            'nda' => [
                'field' => 'is_nda',
                'name' => 'nda',
                'hash' => md5(uniqid()),
                'text' => '',
                'file_name' => 'nda'
            ],
            'cooperation' => [
                'field' => 'is_cooperation',
                'name' => 'cooperation agreement',
                'hash' => md5(uniqid()),
                'text' => '',
                'file_name' => ''
            ],
        ];
        $this->client_id = escape_string(clear_post($client_id));
        $this->folder = 'login_pdf_contracts';
        $this->output_dir = ROOT . DS . 'upload' . DS . $this->folder . DS;
        Folder::create($this->output_dir);
        if ($this->client_id != 0) {
            $this->output_dir .= $this->client_id . DS;
            Folder::create($this->output_dir);
        }
    }

    public function getContractsData($user_data = [])
    {
        foreach ($this->contracts as &$val) {
            if (!empty($val['file_name']) && !empty($user_data)) {
                $val['text'] = $this->getbody(Templates::getTemplate($this->contracts_template_path . $val['file_name'], $user_data));
            }
        }
        return $this->contracts;
    }

    public function getLink($contract_name)
    {

    }

    public function deleteContract($contract_name = '')
    {
        if (!empty($contract_name) && file_exists($this->output_dir . $contract_name . DS . 'contract_' . $contract_name . '.pdf'))
            Folder::delete($this->output_dir . $contract_name . DS);
        else
            Folder::delete($this->output_dir);

        if (!empty($contract_name) && isset($this->contracts[strtolower($contract_name)])) {
            $field = $this->contracts[strtolower($contract_name)]['field'];
        } else {
            $field = array_column($this->contracts, 'field');
        }
        if (is_array($field))
            $sql = update_query_string(User::getTable(), array_fill_keys($field, 0)) . ' WHERE id=' . $this->client_id;
        else
            $sql = update_query_string(User::getTable(), [$field => 0]) . ' WHERE id=' . $this->client_id;
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }

    }

    public function addContract(array $contractData = [])
    {
        if ($this->client_id == 0)
            return false;
        $hash = clear_post($contractData['hash']);
        $contract_name = clear_post($contractData['contract_name']);
        $output_dir = $this->getContractForlder($contract_name);
        try {
            $user_data = User::getById($this->client_id);
            Pdf::Init(htmlspecialchars_decode(Templates::getTemplate('user_contracts/clients/' . $contract_name, $user_data)))
                ->makePDFWithSignature(
                    $output_dir . 'signature_' . $contract_name . '.png'
                    , $hash
                    , $output_dir . 'contract_' . $contract_name . '.pdf'
                );
            if (isset($_POST['signature'])) {
                $data = $_POST['signature'];

                list($type, $data) = explode(';', $data);
                list(, $data) = explode(',', $data);
                $data = base64_decode($data);
                $field = $this->contracts[strtolower($contract_name)]['field'];
                file_put_contents($output_dir . 'signature_' . $contract_name . '.png', $data);
                $mailbody_html = '<p></p>';
                $from_name = 'Accueil';
                Mails()->send_one_mail_with_attachments('contact@taskeo.net', $from_name, $user_data['email'], $user_data['firstname'], $contract_name, $mailbody_html, '', [['path' => $output_dir . 'contract_' . $contract_name . '.pdf', 'name' => 'contract_' . $contract_name . '.pdf']]);
                return User()->updateClient($this->client_id, [$field => 1]);
            }

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}