<?php

use models\CategoryEntityModel;

class CategoryEntity
{
    public static function baseSelect(array $columns = ['*'], $aggregate = null)
    {
        return CategoryEntityModel::baseSelect($columns, $aggregate);
    }

    public static function getTable()
    {
        return CategoryEntityModel::getTable();
    }
}