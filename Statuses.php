<?php

use traits\SingleTone;

class Statuses
{
    private $_statuses;
    private $_statuses_by_aliase;
    use SingleTone;

    private function __construct()
    {
        $this->_statuses_by_aliase = [];
        $uT = UserTypes();
        $ut_all = $uT->getAll();
        $ut_all_id = array_column($ut_all, 'id');
        $this->_statuses = [
            1 => [
                'aliase' => 'in_preparation',
                'class' => '',
                'ord' => '1',
                'show' => true,
                'descr' => [
                    Project::getTable() => 'status_in_preparation_project_desc',
                    Task::getTable() => 'status_in_preparation_task_desc',
                ],
                'who_can_change' => [
                    Project::getTable() => [$uT->getTeamLead(), $uT->getChef(), $uT->getAssistant()],
                    Task::getTable() => array_diff($ut_all_id, [$uT->getClient()])
                ],
                'weight' => 0,
            ],
            2 => [
                'aliase' => 'waiting_for_validation_by_the_client',
                'class' => 'tomato',
                'ord' => '2',
                'show' => false,
                'descr' => [
                    Project::getTable() => '',
                    Task::getTable() => '',
                ],
                'who_can_change' => [
                    Project::getTable() => [],
                    Task::getTable() => []
                ],
                'weight' => 0,
            ],
            3 => [
                'aliase' => 'in_development',
                'class' => 'orange',
                'ord' => '3',
                'show' => true,
                'descr' => [
                    Project::getTable() => 'status_in_progress_project_desc',
                    Task::getTable() => 'status_in_progres_task_desc',
                ],
                'who_can_change' => [
                    Project::getTable() => [$uT->getTeamLead(), $uT->getChef(), $uT->getAssistant()],
                    Task::getTable() => array_diff($ut_all_id, [$uT->getClient()])
                ],
                'weight' => 15,
            ],
            4 => [
                'aliase' => 'finalized_not_controlled',
                'class' => 'grey',
                'ord' => '4',
                'show' => true,
                'descr' => [
                    Project::getTable() => 'status_finalized_uncontrolled_project_desc',
                    Task::getTable() => 'status_finalized_uncontrolled_task_desc',
                ],
                'who_can_change' => [
                    Project::getTable() => [$uT->getTeamLead(), $uT->getChef(), $uT->getAssistant()],
                    Task::getTable() => array_diff($ut_all_id, [$uT->getClient()])
                ],
                'weight' => 90,
            ],
            5 => [
                'aliase' => 'validated_technical_inspection',
                'class' => 'purple',
                'ord' => '5',
                'show' => true,
                'descr' => [
                    Project::getTable() => 'status_technical_inspection_project_desc',
                    Task::getTable() => 'status_technical_inspection_task_desc',
                ],
                'who_can_change' => [
                    Project::getTable() => [$uT->getTeamLead(), $uT->getChef(), $uT->getAssistant()],
                    Task::getTable() => [$uT->getTeamLead(), $uT->getChef(), $uT->getAssistant()]
                ],
                'weight' => 100,
            ],
            6 => [
                'aliase' => 'double_check_validated',
                'class' => 'yellow',
                'ord' => '6',
                'show' => false,
                'descr' => [
                    Project::getTable() => '',
                    Task::getTable() => '',
                ],
                'who_can_change' => [
                    Project::getTable() => [],
                    Task::getTable() => []
                ],
                'weight' => 0,
            ],
            7 => [
                'aliase' => 'approved_by_the_customer',
                'class' => 'green',
                'ord' => '7',
                'show' => true,
                'descr' => [
                    Project::getTable() => 'status_aproved_by_customer_project_desc',
                    Task::getTable() => 'status_aproved_by_customer_task_desc',
                ],
                'who_can_change' => [
                    Project::getTable() => [$uT->getClient(), $uT->getChef(), $uT->getAssistant()],
                    Task::getTable() => [$uT->getClient(), $uT->getChef(), $uT->getAssistant()]
                ],
                'weight' => 0,
            ],
        ];
        foreach ($this->_statuses as $k => &$v) {
            $v['id'] = $k;
            $v['name'] = $v['aliase'];
            $this->_statuses_by_aliase[$v['aliase']] = $v;
            foreach ($v['who_can_change'] as &$val)
                $val[] = $uT->getSupervisor();
        }
    }

    private function _getAliaseById($name)
    {
        return $this->_statuses_by_aliase[$name]['id'];
    }

    public function canUserTypeChange($status, $usertype, $entity)
    {
        return in_array($usertype, $this->_statuses[$status]['who_can_change'][$entity]);
    }

    public function getCondition($status_id = 1)
    {
        $status_id = intval($status_id);
        switch ($status_id) {
            case $this->waiting_for_validation_by_the_client():
            case $this->in_development():
            case $this->finalized_not_controlled():
            case $this->validated_technical_inspection():
                return 2;
            case $this->double_check_validated():
            case $this->approved_by_the_customer():
                return 3;
        }
        return $status_id;
    }

    public function getStatusesByAliase($aliase, $param = '')
    {
        return $this->_statuses[$aliase][$param] ?? $this->_statuses[$aliase];
    }

    public function getStatusesById($id, $param = '')
    {
        return (!empty($param) && isset($this->_statuses[$id][$param])) ? $this->_statuses[$id][$param] : $this->_statuses[$id];
    }

    public function getDescrById($id, $entity)
    {
        $res = $this->getStatusesById($id, 'descr');
        return $res[$entity];
    }

    public function getAllStatuses($ord = null)
    {
        $statuses = [];
        foreach ($this->_statuses as $key => $val) {
            if ($val['show'])
                $statuses[$key] = $val;
        }
        switch ($ord) {
            case 'asc':
                uasort($statuses, function ($a, $b) {
                    $a = $a['ord'];
                    $b = $b['ord'];
                    if ($a == $b) {
                        return 0;
                    }
                    return ($a < $b) ? -1 : 1;
                });
                break;
            case 'desc':
                uasort($statuses, function ($a, $b) {
                    $a = $a['ord'];
                    $b = $b['ord'];
                    if ($a == $b) {
                        return 0;
                    }
                    return ($a > $b) ? -1 : 1;
                });
                break;
        }
        return $statuses;
    }

    public function in_preparation()
    {
        return $this->_getAliaseById(__FUNCTION__);
    }

    public function waiting_for_validation_by_the_client()
    {
        return $this->_getAliaseById(__FUNCTION__);
    }

    public function in_development()
    {
        return $this->_getAliaseById(__FUNCTION__);
    }

    public function finalized_not_controlled()
    {
        return $this->_getAliaseById(__FUNCTION__);
    }

    public function validated_technical_inspection()
    {
        return $this->_getAliaseById(__FUNCTION__);
    }

    public function double_check_validated()
    {
        return $this->_getAliaseById(__FUNCTION__);
    }

    public function approved_by_the_customer()
    {
        return $this->_getAliaseById(__FUNCTION__);
    }
}