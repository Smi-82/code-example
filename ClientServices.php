<?php

use models\ClientServicesModel;
use traits\SingleTone;

class ClientServices
{
    use SingleTone;

    public static function baseSelect(array $columns = ['*'], $aggregate = null)
    {
        return ClientServicesModel::baseSelect($columns, $aggregate);
    }

    public static function getTable()
    {
        return ClientServicesModel::getTable();
    }

    public static function add($data)
    {
        if (isset($data['id']))
            unset($data['id']);
        return ClientServicesModel::add($data);
    }

    public static function multiAddForClientId($client_id, $services)
    {
        $client_id = escape_string(clear_post($client_id));
        $data = array_map(function ($el) use ($client_id) {
            return ['user_id' => $client_id, 'service_key' => $el];
        }, $services);
        return ClientServicesModel::multiAdd($data);
    }

    public static function updateServicesForClient($client_id, $services)
    {
        $res = ClientServicesModel::deleteByClientAndService($client_id, $services);
        return $res ? self::multiAddForClientId($client_id, $services) : $res;
    }

}