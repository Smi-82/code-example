<?php

class Priorities
{
    private static $priorities = [
        '1' => [
            'key' => 'very_urgent',
            'color_class' => 'status_red'
        ],
        '2' => [
            'key' => 'urgent',
            'color_class' => 'status_orange'
        ],
        '3' => [
            'key' => 'normal',
            'color_class' => 'status_blue'
        ],
        '4' => [
            'key' => 'low',
            'color_class' => 'status_green'
        ]
    ];

    public static function getId()
    {
        return array_keys(self::$priorities);
    }

    public static function getIdByKey($key)
    {
        foreach (self::$priorities as $id => $item) {
            if ($item['key'] == $key) return $id;
        }
    }

    public static function getIdByColorClass($colorClass)
    {
        foreach (self::$priorities as $id => $item) {
            if ($item['color_class'] == $colorClass) return $id;
        }
    }

    public static function getKeyById($id)
    {
        return self::$priorities[$id]['key'] ?? null;
    }

    public static function getColorClassById($id)
    {
        return self::$priorities[$id]['color_class'] ?? null;
    }

    public static function getAll()
    {
        return self::$priorities;
    }
}