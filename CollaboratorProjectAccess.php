<?php

use models\CollaboratorProjectAccessModel;
use traits\SingleTone;

class CollaboratorProjectAccess
{
    use SingleTone;

    public static function baseSelect(array $columns = ['*'], $aggregate = null)
    {
        return CollaboratorProjectAccessModel::baseSelect($columns, $aggregate);
    }

    public static function getTable()
    {
        return CollaboratorProjectAccessModel::getTable();
    }

    public static function add($user_id, $general_project_id)
    {
        return CollaboratorProjectAccessModel::add(compact('user_id', 'general_project_id'));
    }

    public static function getByUserId($id, $columns = ['*'])
    {
        return CollaboratorProjectAccessModel::getByUserId($id, $columns);
    }

    public static function deleteByUserIdProjectId($user_id, $general_project_id)
    {
        return CollaboratorProjectAccessModel::deleteByUserIdProjectId($user_id, $general_project_id);
    }
}