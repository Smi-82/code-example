<?php


namespace models;


use Exception;
use ProjectAccess;
use User;
use UserTypes;

class AssistantModel
{
    public static function getTable()
    {
        return ProjectAccess::getTable();
    }

    public static function getByOneProjectId($id, array $columns = ['*'])
    {
        $res = self::getByManyProjectId($id, $columns);
        return $res[$id];
    }

    public static function getAll(array $columns = ['*'])
    {
        return User::getByTypeId(UserTypes()->getAssistant(), $columns);
    }

    public static function getByManyProjectId($id, array $columns = ['*'])
    {
        $id = getAsArray($id);
        asort($id);
        $columns = clearParams($columns);
        $columns = array_map(function ($elem) {
            return 'u.' . $elem;
        }, $columns);
        $sql = '
SELECT
' . implode(',', $columns) . ', pa.general_project_id AS _p_id
FROM
' . self::getTable() . ' AS pa
LEFT JOIN
' . User::getTable() . ' AS u
ON
u.id=pa.user_id
LEFT JOIN
' . UserTypes::getTable() . ' AS ut
ON
ut.id=u.usertype
WHERE
u.usertype IN (' . implode(',', [
                UserTypes()->getAssistant()
            ]) . ')
AND
pa.general_project_id IN (' . implode(',', $id) . ')';
        try {
            $res = query($sql);
            $tmp = [];
            foreach ($res as &$val) {
                $_p_id = $val['_p_id'];
                unset($val['_p_id']);
                $tmp[$_p_id][] = $val;
            }
            return $tmp;
        } catch (Exception $e) {
            return [];
        }
    }
}