<?php

namespace models;

use Exception;
use traits\BaseSelect;
use traits\GetTable;

class ProjectAccessModel
{
    private static $table = 'project_access';
    use GetTable;
    use BaseSelect;

    public static function getAccessData($project_id, array $params = ['*'])
    {
        $params = clearParams($params);
        $project_id = escape_string(clear_post($project_id));
        $sql = self::baseSelect($params) . ' WHERE general_project_id=' . $project_id;
        try {
            return query($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function getAccessDataByProjectIdS($project_ids, array $params = ['*'])
    {
        $params = clearParams($params);
        if (!in_array('general_project_id', $params))
            $params[] = 'general_project_id';
        $project_ids = getAsArray($project_ids);
        $sql = self::baseSelect($params) . ' WHERE general_project_id IN (' . implode(',', $project_ids) . ')';
        try {
            return query($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function delete($project_id, $user_id)
    {
        $user_id = getAsArray($user_id);
        $project_id = getAsArray($project_id);
        $sql = 'DELETE FROM ' . self::getTable() . ' WHERE general_project_id IN (' . implode(',', $project_id) . ') AND user_id IN (' . implode(',', $user_id) . ')';
        try {
            return query($sql);
        } catch (Exception $e) {
            return false;
        }
    }

    public static function add($general_project_id, $user_id)
    {
        $general_project_id = escape_string(clear_post($general_project_id));
        $user_id = escape_string(clear_post($user_id));
        $sql = insert_query_string(self::getTable(), compact('general_project_id', 'user_id'), true);
        try {
            $res = query($sql);
            return $res ? getLastId() : $res;
        } catch (Exception $e) {
            return false;
        }
    }
}