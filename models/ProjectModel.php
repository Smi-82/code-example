<?php


namespace models;


use BillProject;
use Exception;
use Group;
use Project;
use ProjectClientMessages;
use Task;
use TaskAccess;
use traits\BaseSelect;
use traits\GetTable;
use User;

class ProjectModel
{
    use GetTable;
    use BaseSelect;

    private static $table = 'projects';

    public static function delete($project_id)
    {
        $project_id = escape_string(clear_post($project_id));
        $sql = 'DELETE FROM ' . self::getTable() . ' WHERE id = "' . $project_id . '"';
        try {
            return query($sql);
        } catch (Exception $e) {
            return false;
        }
    }

    public static function getMainClients($params = ['*'])
    {
        $sql = 'SELECT DISTINCT client_id FROM ' . self::getTable() . ' WHERE client_id != 0';
        try {
            $res = query($sql);
            return !empty($res) ? array_column($res, 'client_id') : $res;
        } catch (Exception $e) {
            return [];
        }
    }

    public static function getClientProjectsCount($client_id)
    {
        $client_id = escape_string(clear_post($client_id));
        $sql = 'SELECT COUNT(p.id) as cnt
                FROM
                ' . self::getTable() . ' AS p
                LEFT JOIN users AS u ON u.id=p.client_id
                LEFT JOIN users AS m ON m.id=p.master_id
                WHERE
                p.active = 1 AND p.is_template=0 AND p.is_offer=0 AND u.id = ' . $client_id;
        try {
            $result = querySingle($sql);
            return $result['cnt'] ?? 0;
        } catch (Exception $e) {
            return 0;
        }
    }

    public static function getTemplateProjects(array $columns = ['*'])
    {
        $sql = self::baseSelect($columns) . ' WHERE is_template = 1';
        try {
            return query($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function getOwnParticipants($project_ids, $params = ['*'])
    {
        $project_ids = getAsArray($project_ids);
        $sql = 'SELECT p.id AS p_id, u.id AS user_id FROM ' . self::getTable() . ' AS p LEFT JOIN ' . Group::getTable() . ' AS g ON p.id=g.general_project_id RIGHT JOIN ' . Task::getTable() . ' AS t ON t.group_id=g.id LEFT JOIN ' . TaskAccess::getTable() . ' AS ta ON ta.task_id=t.id LEFT JOIN ' . User::getTable() . ' AS u ON u.id=ta.user_id WHERE p.id IN (' . implode(', ', $project_ids) . ')';
        try {
            return query($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function getCntTasksForManyProjects($project_ids)
    {
        $project_ids = getAsArray($project_ids);
        $sql = 'SELECT COUNT(t.id) AS cnt_tasks, p.id FROM ' . Task::getTable() . ' AS t LEFT JOIN ' . Group::getTable() . ' AS g ON t.group_id=g.id LEFT JOIN ' . self::getTable() . ' AS p ON p.id=g.general_project_id WHERE p.id IN (' . implode(',', $project_ids) . ' ) GROUP BY p.id';
        try {
            return query($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function getOwnTasks($project_ids, $params = ['*'])
    {
        $project_ids = getAsArray($project_ids);
        $params = clearParams($params);
        $params = array_map(function ($elem) {
            return 't.' . $elem;
        }, $params);
        $sql = 'SELECT ' . implode(',', $params) . ', p.id AS _p_id FROM ' . Task::getTable() . ' AS t LEFT JOIN ' . Group::getTable() . ' AS g ON t.group_id=g.id LEFT JOIN ' . self::getTable() . ' AS p ON p.id=g.general_project_id WHERE p.id IN (' . implode(',', $project_ids) . ' ) ORDER BY t.ord ASC, t.id';
        try {
            return query($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function update($project_id, $data)
    {
        $project_id = getAsArray($project_id);
        $sql = update_query_string(self::getTable(), $data) . ' WHERE id IN (' . implode(',', $project_id) . ')';
        try {
            return query($sql);
        } catch (Exception $e) {
            return false;
        }
    }

    public static function add($data)
    {
        $sql = insert_query_string(self::getTable(), $data);
        try {
            $res = query($sql);
            return $res ? getLastId() : $res;
        } catch (Exception $e) {
            return false;
        }
    }

    public static function changeCategory($from, $to)
    {
        $from = escape_string(clear_post($from));
        $to = escape_string(clear_post($to));
        $sql = update_query_string(self::getTable(), ['category' => $to]) . ' WHERE category=' . $from;
        try {
            return query($sql);
        } catch (Exception $e) {
            return false;
        }
    }

    public static function addUser($project_id, $user_id, $field = null)
    {
        $user_id = escape_string(clear_post($user_id));
        $project_id = escape_string(clear_post($project_id));
        if (!is_null($field)) {
            $field = escape_string(clear_post($field));
            $sql = update_query_string(self::getTable(), [$field => $user_id]) . ' WHERE id=' . $project_id;
            try {
                return query($sql);
            } catch (Exception $e) {
                return false;
            }
        }
    }

    public static function getByPriceType($type, array $columns = ['*'])
    {
        $type = escape_string(clear_post($type));
        $sql = self::baseSelect($columns) . ' WHERE price_type=' . $type;
        try {
            return query($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function getManyByIds($ids, $params = ['*'])
    {
        $ids = getAsArray($ids);
        $sql = self::baseSelect($params) . ' WHERE id IN (' . implode(',', $ids) . ')';
        try {
            return query($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function getByClientId($client_ids, $params = ['*'])
    {
        $params = clearParams($params);
        $sql = 'SELECT ' . implode(',', $params) . ' FROM ' . self::getTable() . ' WHERE client_id =' . escape_string(clear_post($client_ids));
        try {
            return query($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function getAllProjects($params = ['*'])
    {
        $sql = self::baseSelect($params) . ' WHERE active = 1';
        try {
            return query($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function getProjectInWhichThereAreTasksToWhichTheUserHasBeenAdded($user_id)
    {
        $user_id = escape_string(clear_post($user_id));
        $query_str = 'SELECT DISTINCT general_project_id FROM ' . Group::getTable() . ' AS g LEFT JOIN ' . self::getTable() . ' AS p ON p.id=g.general_project_id RIGHT JOIN ' . Task::getTable() . ' AS t ON t.group_id=g.id LEFT JOIN ' . TaskAccess::getTable() . ' AS ta ON ta.task_id=t.id WHERE p.active=1 AND ta.user_id=' . $user_id;
        try {
            $res = query($query_str);
            return $res ? array_column($res, 'general_project_id') : [];
        } catch (Exception $e) {
            return [];
        }
    }

    public static function getProjectListForUser($user_id)
    {
        $user_id = escape_string(clear_post($user_id));
        $fields = ['user_id', 'client_id', 'master_id'];
        foreach ($fields as &$val)
            $val .= '=' . $user_id;
        $fields = implode(' OR ', $fields);
        $query_str = '
SELECT
id
FROM
' . self::getTable() . '
WHERE active=1 AND (' . $fields . ')';

        try {
            return query($query_str);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function getCntProjectsForMainClient($client_id)
    {
        $client_id = escape_string(clear_post($client_id));
        $sql = 'SELECT COUNT(id) AS cnt FROM ' . self::getTable() . ' WHERE client_id=' . $client_id;
        try {
            $res = querySingle($sql);
            $res = $res['cnt'];
            return intval($res);
        } catch (Exception $e) {
            return 0;
        }
    }

    public static function getByWhoCreated($user_id, $columns = ['*'])
    {
        $user_id = escape_string(clear_post($user_id));
        $columns = clearParams($columns);
        try {
            return query(self::baseSelect($columns) . ' WHERE user_id = ' . $user_id);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function getProjectsSubscrForPayment()
    {
        $sql = '
SELECT
p.*
FROM ' .
            self::getTable() . ' AS p
    WHERE
    p.price_type=' . Project::getIdPriceTypeBySubscription() . '
    AND
    p.paid=0
    AND
    p.is_template=0
    AND
    p.is_offer=0
    AND
    p.client_id != 0
AND
(
  TIMESTAMPDIFF(DAY,NOW(),p.schedule_next_payment_date) < 7
  OR
  p.schedule_next_payment_date = 0
  )
AND
(SELECT COUNT(id) FROM ' . BillProject::getTable() . ' WHERE project_id=p.id AND created=p.schedule_next_payment_date) = 0
';
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function getDonePercentManyProjects($ids)
    {
        $sql = '
                    SELECT
                    COUNT(*) AS cnt_task,
                    t.status,
                    p.id
                    FROM
                    ' . Task::getTable() . ' AS t
                    LEFT JOIN
                    ' . Group::getTable() . ' AS g
                    ON
                    g.id = t.group_id
                    LEFT JOIN
                    ' . self::getTable() . ' as p
                    ON
                    p.id = g.general_project_id
                    WHERE
                    t.active =1 AND p.id IN (' . implode(',', $ids) . ') AND p.active = 1 GROUP BY p.id, t.status ';
        try {
            return query($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function getAllOffers($params = ['*'])
    {
        $sql = self::baseSelect($params) . ' WHERE is_offer = 1 AND is_template=0';
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function getReady($project_id)
    {
        $project_id = escape_string(clear_post($project_id));
        $sql = 'SELECT COUNT(t.id) AS cnt FROM ' . Task::getTable() . ' AS t LEFT JOIN ' . Group::getTable() . ' AS g ON g.id=t.group_id LEFT JOIN ' . self::getTable() . ' AS gp ON gp.id=g.general_project_id WHERE t.active=1 AND t.status=' . Statuses()->approved_by_the_customer() . ' AND gp.id=' . $project_id;
        try {
            return querySingle($sql)['cnt'];
        } catch (Exception $e) {
            return 0;
        }
    }

    public static function getByMessageId($message_id, $params = ['*'])
    {
        $message_id = escape_string($message_id);
        $params = array_map(function ($elem) {
            return 'a.' . $elem;
        }, $params);
        $sql = 'SELECT ' . implode(',', $params) . ' FROM ' . self::getTable() . ' a LEFT JOIN ' . ProjectClientMessages::getTable() . ' b ON a.id = b.project_id WHERE b.id = ' . $message_id . ' LIMIT 0, 1';
        try {
            return querySingle($sql);
        } catch (Exception $e) {
            return [];
        }
    }
}