<?php

namespace models;

use Exception;
use traits\BaseSelect;
use traits\GetTable;

class DatesOfCallsModel
{
    use GetTable;
    use BaseSelect;

    private static $table = 'dates_of_calls';

    public static function deleteById($id)
    {
        $id = escape_string(clear_post($id));
        $sql = 'DELETE FROM ' . self::getTable() . ' WHERE id=' . $id;
        try {
            return query($sql);
        } catch (Exception $e) {
            return false;
        }
    }

    public static function add(array $data)
    {
        $sql = insert_query_string(self::getTable(), $data);
        try {
            $res = query($sql);
            return $res ? getLastId() : $res;
        } catch (Exception $e) {
            return false;
        }
    }

    public static function update($id, $data)
    {
        $id = escape_string(clear_post($id));
        $sql = update_query_string(self::getTable(), $data) . ' WHERE id=' . $id;
        try {
            return query($sql);
        } catch (Exception $e) {
            return false;
        }
    }

    public static function getAll(array $params = ['*'])
    {
        try {
            return query(self::baseSelect($params) . ' ORDER BY d_calls');
        } catch (Exception $e) {
            return [];
        }
    }

    public static function getByClientId($id, array $params = ['*'])
    {
        $id = escape_string(clear_post($id));
        try {
            return query(self::baseSelect($params) . ' WHERE client_id=' . $id . ' ORDER BY d_calls');
        } catch (Exception $e) {
            return [];
        }
    }

    public static function getByWhoAdd($id, array $params = ['*'])
    {
        $id = escape_string(clear_post($id));
        try {
            return query(self::baseSelect($params) . ' WHERE who_add=' . $id . ' ORDER BY d_calls');
        } catch (Exception $e) {
            return [];
        }
    }
}