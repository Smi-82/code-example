<?php

namespace models;

use traits\BaseSelect;
use traits\GetTable;

class CollaboratorProjectAccessModel
{
    private static $table = 'collaborator_project_access';
    use GetTable;
    use BaseSelect;

    public static function deleteByUserIdProjectId($user_id, $general_project_id)
    {
        $user_id = escape_string(clear_post($user_id));
        $general_project_id = escape_string(clear_post($general_project_id));
        $sql = 'DELETE FROM ' . self::getTable() . ' WHERE user_id=' . $user_id . ' AND general_project_id=' . $general_project_id;
        try {
            return query($sql);
        } catch (Exception $e) {
            return false;
        }
    }

    public static function getByUserId($id, $columns = ['*'])
    {
        $id = escape_string(clear_post($id));
        $sql = self::baseSelect($columns) . ' WHERE user_id=' . $id;
        try {
            return query($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function add(array $data)
    {
        $sql = insert_query_string(self::getTable(), $data);
        try {
            $res = query($sql);
            return $res ? getLastId() : $res;
        } catch (Exception $e) {
            return false;
        }
    }
}