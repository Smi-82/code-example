<?php

namespace models;

use Exception;
use traits\BaseSelect;
use traits\GetTable;

class GroupModel
{
    private static $table = 'groups';
    use GetTable;
    use BaseSelect;

    private static function _getByField($field, $id, $columns)
    {
        $id = escape_string(clear_post($id));
        $columns = clearParams($columns);
        try {
            return query(self::baseSelect($columns) . ' WHERE ' . $field . ' = ' . $id);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function delete($group_ids)
    {
        $sql = 'DELETE FROM ' . self::getTable() . ' WHERE id IN (' . implode(', ', $group_ids) . ')';
        try {
            return query($sql);
        } catch (Exception $e) {
            return false;
        }
    }

    public static function deleteByProjectIds($ids)
    {
        $ids = getAsArray($ids);
        $sql = 'DELETE FROM ' . self::getTable() . ' WHERE general_project_id IN (' . implode(', ', $ids) . ')';
        try {
            return query($sql);
        } catch (Exception $e) {
            return false;
        }
    }

    public static function add($data)
    {
        $sql = insert_query_string(self::getTable(), $data);
        try {
            $res = query($sql);
            return $res ? getLastId() : $res;
        } catch (Exception $e) {
            return false;
        }
    }

    public static function getByManyProjectsId($project_id, array $params = ['*'])
    {
        $project_id = getAsArray($project_id);
        $params[] = 'general_project_id AS _tmp_p_id';
        $sql = self::baseSelect($params) . ' WHERE general_project_id IN (' . implode(',', $project_id) . ') ORDER BY parent_id, id';
        try {
            return query($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function getByManyIds($ids, array $params = ['*'])
    {
        $ids = getAsArray($ids);
        $sql = self::baseSelect($params) . ' WHERE id IN (' . implode(', ', $ids) . ') ORDER BY parent_id, id';
        try {
            return query($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function update($id, $data)
    {
        $id = getAsArray($id);
        $sql = update_query_string(self::getTable(), $data) . ' WHERE id IN (' . implode(',', $id) . ')';
        try {
            return query($sql);
        } catch (Exception $e) {
            return false;
        }
    }

    public static function getByWhoCreated($user_id, $columns = ['*'])
    {
        return self::_getByField('user_id', $user_id, $columns);
    }

    public static function getByParentId($user_id, $columns = ['*'])
    {
        return self::_getByField('parent_id', $user_id, $columns);
    }

    public static function getById($id, $columns = ['*'])
    {
        return self::_getByField('id', $id, $columns);
    }

}