<?php

namespace models;

use Exception;
use traits\BaseSelect;
use traits\GetTable;

class ClientActivityModel
{
    private static $table = 'client_events';
    use GetTable;
    use BaseSelect;

    public static function updateByToken($token, $data)
    {
        $token = escape_string(clear_post($token));
        $sql = update_query_string(self::getTable(), $data) . ' WHERE token="' . $token . '"';
        try {
            return query($sql);
        } catch (Exception $e) {
            return false;
        }
    }

    public static function getByToken($token, array $columns = ['*'])
    {
        $token = escape_string(clear_post($token));
        $sql = self::baseSelect($columns) . ' WHERE token="' . $token . '" LIMIT 1';
        try {
            return querySingle($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function getById($id, array $columns = ['*'])
    {
        $id = escape_string(clear_post($id));
        $sql = self::baseSelect($columns) . ' WHERE id=' . $id . ' LIMIT 1';
        try {
            return querySingle($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function updateById($id, $data)
    {
        $id = escape_string(clear_post($id));
        $sql = update_query_string(self::getTable(), $data) . ' WHERE id=' . $id;
        try {
            return query($sql);
        } catch (Exception $e) {
            return false;
        }
    }

    public static function getGetedByUserId($id, array $columns = ['*'])
    {
        $id = escape_string(clear_post($id));
        $sql = self::baseSelect($columns) . ' WHERE whoGet=' . $id . ' AND isClose=0';
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function add($data)
    {
        $sql = insert_query_string(self::getTable(), $data);
        try {
            $res = query($sql);
            return $res ? getLastId() : $res;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}