<?php

namespace models;

use Exception;
use traits\BaseSelect;
use traits\GetTable;

class CategoryEntityModel
{
    private static $table = 'category_entity';
    use GetTable;
    use BaseSelect;

    public static function getEntityByCategoryId($cat_id)
    {
        $cat_id = getAsArray($cat_id);
        $sql = 'SELECT GROUP_CONCAT(entity) AS entity, category_id FROM ' . self::getTable() . ' WHERE category_id IN (' . implode(',', $cat_id) . ') GROUP BY category_id';
        try {
            return query($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function getCategoriesByEntity($entity_id, array $params = ['*'])
    {
        $entity_id = escape_string(clear_post($entity_id));
        $sql = self::baseSelect($params) . ' WHERE entity=' . $entity_id;
        try {
            return query($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function add($category_id, $entity)
    {
        $category_id = escape_string(clear_post($category_id));
        $entity = escape_string(clear_post($entity));
        $sql = insert_query_string(self::getTable(), compact('category_id', 'entity'), true);
        try {
            return query($sql);
        } catch (Exception $e) {
            return false;
        }
    }

    public static function deleteByCategoryIdAndEntityId($cat_id, $ent_id)
    {
        $cat_id = escape_string(clear_post($cat_id));
        $ent_id = escape_string(clear_post($ent_id));
        $sql = 'DELETE FROM ' . self::getTable() . ' WHERE category_id=' . $cat_id . ' AND entity=' . $ent_id;
        try {
            return query($sql);
        } catch (Exception $e) {
            return false;
        }
    }
}