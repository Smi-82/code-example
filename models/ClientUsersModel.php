<?php

namespace models;

use Exception;
use traits\BaseSelect;
use traits\GetTable;

class ClientUsersModel
{
    use GetTable;
    use BaseSelect;

    private static $table = 'client_users';

    public static function getCurrentByClientId($client_id, $params = ['*'])
    {
        $sql = 'SELECT ' . implode(',', $params) . ' FROM ' . self::getTable() . ' WHERE client_id =' . escape_string(clear_post($client_id)) . ' AND is_basic=1 LIMIT 1';
        try {
            return querySingle($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function getById($id, $params = ['*'])
    {
        $params = clearParams($params);
        $sql = 'SELECT ' . implode(',', $params) . ' FROM ' . self::getTable() . ' WHERE id =' . escape_string(clear_post($id));
        try {
            return querySingle($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function getByClientId($client_ids, $params = ['*'])
    {
        $params = clearParams($params);
        $sql = 'SELECT ' . implode(',', $params) . ' FROM ' . self::getTable() . ' WHERE client_id =' . escape_string(clear_post($client_ids));
        try {
            return query($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function add($data)
    {
        if (!isset($data['who_added']))
            $data['who_added'] = User()->getId();
        $sql = insert_query_string(self::getTable(), $data);
        try {
            $res = query($sql);
            return $res ? getLastId() : $res;
        } catch (Exception $e) {
            return false;
        }
    }

    public static function update($id, $data)
    {
        $id = getAsArray($id);
        $sql = update_query_string(self::getTable(), $data) . ' WHERE id IN (' . implode(',', $id) . ')';
        try {
            return query($sql);
        } catch (Exception $e) {
            return false;
        }
    }

    public static function updateByClient($id, $data)
    {
        $id = getAsArray($id);
        $sql = update_query_string(self::getTable(), $data) . ' WHERE client_id IN (' . implode(',', $id) . ')';
        try {
            return query($sql);
        } catch (Exception $e) {
            return false;
        }
    }

    public static function delete($ids)
    {
        $ids = getAsArray($ids);
        $sql = 'DELETE FROM ' . self::getTable() . ' WHERE id IN (' . implode(',', $ids) . ')';
        try {
            return query($sql);
        } catch (Exception $e) {
            return false;
        }
    }
}