<?php

namespace models;

use Exception;
use traits\BaseSelect;
use traits\GetTable;

class ClientStatusModel
{
    private static $table = 'client_status';
    use GetTable;
    use BaseSelect;

    public static function update($id, $data)
    {
        $id = escape_string(clear_post($id));
        $sql = update_query_string(self::getTable(), $data) . ' WHERE id=' . $id;
        try {
            return query($sql);
        } catch (Exception $e) {
            return false;
        }
    }

    public static function add(array $data)
    {
        $sql = insert_query_string(self::getTable(), $data);
        try {
            $res = query($sql);
            return $res ? getLastId() : $res;
        } catch (Exception $e) {
            return false;
        }
    }

    public static function getById($ids, array $params = ['*'])
    {
        $ids = getAsArray($ids);
        try {
            return query(self::baseSelect($params) . ' WHERE id IN (' . implode(',', $ids) . ') ORDER BY name');
        } catch (Exception $e) {
            return [];
        }
    }

    public static function getAll(array $params = ['*'])
    {
        try {
            return query(self::baseSelect($params) . ' ORDER BY name');
        } catch (Exception $e) {
            return [];
        }
    }
}