<?php


namespace models;


use Exception;
use traits\BaseSelect;
use traits\GetTable;

class TaskAccessModel
{
    private static $table = 'tasks_access';
    use GetTable;
    use BaseSelect;

    private static function getByField($field, $id, $columns = ['*'])
    {
        $id = escape_string(clear_post($id));
        $sql = self::baseSelect($columns) . ' WHERE `' . $field . '`=' . $id;
        try {
            return query($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function getManyByTaskIds($task_ids, $columns = ['*'])
    {
        $task_ids = getAsArray($task_ids);
        $sql = self::baseSelect($columns) . ' WHERE task_id IN (' . implode(',', $task_ids) . ')';
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function getManyByTaskAndUserIds($task_ids, $user_ids, $columns = ['*'])
    {
        $task_ids = getAsArray($task_ids);
        $user_ids = getAsArray($user_ids);
        $task_ids = implode(',', $task_ids);
        $user_ids = implode(',', $user_ids);
        $sql = self::baseSelect($columns) . ' WHERE task_id IN (' . $task_ids . ') AND user_id IN (' . $user_ids . ')';
        try {
            return query($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function getByTaskAndUserIds($task_id, $user_id, $columns = ['*'])
    {
        $task_id = escape_string(clear_post($task_id));
        $user_id = escape_string(clear_post($user_id));
        $sql = self::baseSelect($columns) . ' WHERE task_id=' . $task_id . ' AND user_id=' . $user_id;
        try {
            $res = query($sql);
            return $res ? array_shift($res) : $res;
        } catch (Exception $e) {
            return [];
        }
    }

    public static function getManyByIds($ids, $columns = ['*'])
    {
        $ids = getAsArray($ids);
        $columns[] = 'id AS _tmp_id';
        $sql = self::baseSelect($columns) . ' WHERE id IN (' . implode(',', $ids) . ')';
        try {
            return query($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function updateByTaskAndUserIds($data, $user_id, $task_id = 0)
    {
        $task_id = escape_string(clear_post($task_id));
        $user_id = escape_string(clear_post($user_id));
        unset($data['task_id']);
        unset($data['user_id']);
        unset($data['id']);
        $sql = update_query_string(self::getTable(), $data) . ' WHERE user_id=' . $user_id;
        if (intval($task_id))
            $sql .= ' AND task_id = ' . $task_id;
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function update($ids, $data)
    {
        $ids = getAsArray($ids);
        unset($data['task_id']);
        unset($data['user_id']);
        unset($data['id']);
        $sql = update_query_string(self::getTable(), $data) . ' WHERE id IN (' . implode(',', $ids) . ')';
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function add($task_id, $user_id, $last_timestamp)
    {
        try {
            return query(insert_query_string(self::getTable(),
                [
                    'task_id' => $task_id,
                    'user_id' => $user_id,
                    'time_spent' => 0,
                    'last_timestamp' => $last_timestamp,
                    'last_seen_msg' => 0
                ], true));
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function delete($tasks, $users)
    {
        $tasks = getAsArray($tasks);
        $users = getAsArray($users);
        $sql = 'DELETE FROM ' . self::getTable() . ' WHERE task_id IN (' . implode(',', $tasks) . ') AND user_id IN (' . implode(',', $users) . ')';
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function getByUserIdAndTaskId($user_id, $task_id, $columns = ['*'])
    {
        $task_id = escape_string(clear_post($task_id));
        $user_id = escape_string(clear_post($user_id));
        $sql = self::baseSelect($columns) . ' WHERE user_id=' . $user_id . ' AND task_id=' . $task_id . ' LIMIT 1';
        try {
            return query($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function getByTaskId($task_id, $columns = ['*'])
    {
        return self::getByField('task_id', $task_id, $columns);
    }

    public static function getByUserId($user_id, $columns = ['*'])
    {
        return self::getByField('user_id', $user_id, $columns);
    }

    public static function getWorkTaskIdByUserId($user_id)
    {
        $user_id = escape_string(clear_post($user_id));
        $sql = self::baseSelect(['task_id']) . ' WHERE 	is_on_task=1  AND user_id=' . $user_id;
        try {
            $res = querySingle($sql);
            return $res ? $res['task_id'] : 0;
        } catch (Exception $e) {
            return 0;
        }
    }

    public static function getCntParticipantsForManyTasks($task_ids)
    {
        $task_ids = getAsArray($task_ids);
        $sql = 'SELECT COUNT(user_id) AS cnt_users, task_id FROM ' . self::getTable() . ' WHERE task_id IN (' . implode(',', $task_ids) . ') GROUP BY task_id';
        try {
            $res = query($sql);
        } catch (Exception $e) {
            return [];
        }
        $res = valueToKey('task_id', $res);
        foreach ($res as &$val)
            $val = $val['cnt_users'];
        return $res;
    }
}