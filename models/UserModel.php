<?php


namespace models;


use Exception;
use traits\BaseSelect;
use traits\GetTable;
use traits\Query;
use UserTypes;

class UserModel
{
    use GetTable;
    use BaseSelect;
    use Query;

    private static $table = 'users';

    public static function getUserIdByHashAndPrefix($hash, $prefix)
    {
        $sql = self::baseSelect(['id']) . " WHERE MD5(CONCAT('$prefix', `id`)) = '$hash'";
        try {
            $result = querySingle($sql);
            return $result['id'] ?? null;
        } catch (Exception $e) {
            return null;
        }
    }

    public static function getNewArrivals($count, $params = ['*'])
    {
        $sql = self::baseSelect($params) . ' ORDER BY id desc LIMIT ' . $count;
        try {
            return query($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function getCollaboratorsByClientId($clientId, $params = ['*'])
    {
        $clientId = getAsArray($clientId);
        $sql = self::baseSelect($params) . ' WHERE parent_id in (' . implode(',', $clientId) . ') AND parent_id != 0';
        try {
            return query($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function getManyUsersByGlobalIds($ids, $params = ['*'])
    {
        $ids = getAsArray($ids);
        $params = clearParams($params);
        $params[] = 'id AS _u_id';
        $sql = self::baseSelect($params) . ' WHERE global_id IN (' . implode(',', $ids) . ')';
        try {
            $res = query($sql);
            return valueToKey('_u_id', $res, true);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function customer_availability($filters, $columns = ['*'])
    {
        $filters1 = array_map(function ($el) {
            $el['phone2'] = $el['phone'];
            return $el;
        }, $filters);
        $filters1 = clearParams($filters1);
        $filters1 = array_filter($filters1);
        if (empty($filters1)) {
            return $filters1;
        }
        $filters1 = array_map(function ($key, $val) {
            return $key . ' LIKE "' . $val . '"';
        }, array_keys($filters1), array_values($filters1));
        $filters1 = implode(' OR ', $filters1);
        $sql = self::baseSelect($columns) . ' WHERE ' . $filters1;
        try {
            return query($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function getManyByIds($ids, $params = ['*'])
    {
        $ids = getAsArray($ids);
        $params = clearParams($params);
        $params[] = 'id AS _u_id';
        asort($ids);
        $ids = array_filter($ids);
        $sql = self::baseSelect($params) . ' WHERE id IN (' . implode(',', $ids) . ')';
        try {
            $res = query($sql);
        } catch (Exception $e) {
            return [];
        }
        return valueToKey('_u_id', $res, true);
    }

    public static function findBy($arr, $columns = ['*'])
    {
        $where = '';
        foreach ($arr as $k => $v) {
            $where .= $k . ' LIKE "%' . $v . '%" ';
            if (next($arr))
                $where .= ' OR ';
        }
        $sql = self::baseSelect($columns) . ' WHERE ' . $where;
        try {
            return query($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function getClientsForConsultant($consultants_id, array $params = ['*'])
    {
        $consultants_id = getAsArray($consultants_id);
        $params = clearParams($params);
        $sql = self::baseSelect($params) . ' WHERE usertype = ' . UserTypes()->getClient() . ' AND consultant_id IN (' . implode(', ', $consultants_id) . ')';
        try {
            return query($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function getById($id, array $params = ['*'])
    {
        $id = escape_string(clear_post($id));
        $sql = self::baseSelect($params) . ' WHERE id=' . $id . ' LIMIT 1';
        try {
            return querySingle($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function getCntByType($ids)
    {
        $ids = getAsArray($ids);
        $sql = 'SELECT COUNT(*) AS cnt, usertype FROM ' . self::getTable() . ' WHERE usertype IN ( ' . implode(',', $ids) . ') GROUP BY usertype';
        try {
            $res = query($sql);
            $res = valueToKey('usertype', $res, true);
            $res = array_map(function ($el) {
                return $el['cnt'];
            }, $res);
            foreach ($ids as $v) {
                if (!isset($res[$v]))
                    $res[$v] = 0;
            }
            return $res;
        } catch (Exception $e) {
            return 0;
        }
    }

    public static function getByEncrypt($encrypt, $params = ['*'])
    {
        $encrypt = escape_string(clear_post($encrypt));
        $sql = self::baseSelect($params) . ' WHERE encrypt =' . $encrypt;
        try {
            return querySingle($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function getByManyTypeIds($ids, $params = ['*'])
    {
        $ids = getAsArray($ids);
        $sql = self::baseSelect($params) . ' WHERE usertype IN ( ' . implode(',', $ids) . ')';
        try {
            return query($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function getHierarchyById($user_id)
    {
        $user_id = escape_string(clear_post($user_id));
        $sql = 'SELECT ut.hierarchy FROM ' . UserTypes::getTable() . ' AS ut LEFT JOIN ' . self::getTable() . ' AS u ON ut.id=u.usertype WHERE u.id=' . $user_id;
//        $sql = 'SELECT ut.hierarchy FROM ' . self::getTable() . ' AS u LEFT JOIN '. UserTypes::getTable() . ' AS ut ON ut.id=u.usertype WHERE u.id=' . $user_id;
//        return $sql;
        try {
            $res = querySingle($sql);
            return empty($res) ? false : $res['hierarchy'];
        } catch (Exception $e) {
            return false;
        }
    }

    public static function getByGlobalID($global_id, $columns = ['*'])
    {
        $global_id = escape_string(clear_post($global_id));
        $columns = clearParams($columns);
        $sql = self::baseSelect($columns) . ' WHERE global_id = ' . $global_id . ' LIMIT 1';
        try {
            return querySingle($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function getByEmail($email, $columns = ['*'])
    {
        $columns = clearParams($columns);
        $email = escape_string(clear_post($email));
        $sql = self::baseSelect($columns) . ' WHERE email = "' . $email . '" LIMIT 1';
        try {
            return querySingle($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function getParticipantsWithoutTypes(array $columns = ['*'], array $excluded_types = [])
    {
        $sql = self::baseSelect($columns) . ' WHERE 1 ';
        if (!empty($excluded_types))
            $sql .= ' AND usertype NOT IN (' . implode(',', $excluded_types) . ') ';
        $sql .= ' ORDER BY usertype ASC, user_name ASC';
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function updateByGlobalIds($ids, $data)
    {
        $ids = getAsArray($ids);
        $sql = update_query_string(self::getTable(), $data) . ' WHERE global_id IN (' . implode(',', $ids) . ')';
        try {
            return query($sql);
        } catch (Exception $e) {
            return false;
        }
    }

    public static function getByLogin($login, array $columns = ['*'])
    {
        $login = escape_string(clear_post($login));
        $sql = self::baseSelect($columns) . ' WHERE login = "' . $login . '" LIMIT 1';
        try {
            return querySingle($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function add($data)
    {
        $sql = insert_query_string(self::getTable(), $data);
        try {
            $res = query($sql);
            return $res ? getLastId() : $res;
        } catch (Exception $e) {
            return false;
        }
    }

    public static function update($ids, $data)
    {
        $ids = getAsArray($ids);
        $data = clearParams($data);
        if (isset($data['id']))
            unset($data['id']);
        $sql = update_query_string(self::getTable(), $data) . ' WHERE id IN (' . implode(',', $ids) . ')';
        try {
            return query($sql);
        } catch (Exception $e) {
            return false;
        }
    }

    public static function getOnLine(array $columns = ['*'], array $types = [])
    {
        $sql = self::baseSelect($columns) . ' WHERE is_online=1 ';
        if (!empty($types))
            $sql .= ' AND usertype IN (' . implode(',', $types) . ') ';
        try {
            return query($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function updateAll($data)
    {
        $sql = update_query_string(self::getTable(), $data);
        try {
            return query($sql);
        } catch (Exception $e) {
            return false;
        }
    }

    public static function getClientsRegisteredByHimself(array $columns = ['*'])
    {
        $sql = self::baseSelect($columns) . ' WHERE usertype=' . UserTypes()->getClient() . ' AND who_registered=0';
        try {
            return query($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function getAll(array $columns = ['*'])
    {
        $sql = self::baseSelect($columns);
        try {
            return query($sql);
        } catch (Exception $e) {
            return [];
        }
    }
}