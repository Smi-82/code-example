<?php

namespace models;

use Exception;
use traits\BaseSelect;
use traits\GetTable;

class HistoryProjectMessagesModel
{
    private static $table = 'history_project_messages';
    use getTable;
    use baseSelect;

    public static function add($data)
    {
        $sql = insert_query_string(self::getTable(), $data);
        try {
            $res = query($sql);
            return $res ? getLastId() : $res;
        } catch (Exception $e) {
            return false;
        }
    }

    public static function getByProjectId($project_id, $columns = ['*'])
    {
        $project_id = escape_string(clear_post($project_id));
        $sql = self::baseSelect($columns) . ' WHERE project_id = ' . $project_id;
        try {
            return query($sql);
        } catch (Exception $e) {
            return [];
        }
    }
}