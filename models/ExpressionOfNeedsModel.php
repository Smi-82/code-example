<?php

namespace models;

use Exception;
use traits\BaseSelect;
use traits\GetTable;

class ExpressionOfNeedsModel
{
    private static $table = 'expression_of_needs';
    use GetTable;
    use BaseSelect;

    public static function getForAdminByProjectId($id, array $columns = ['*'])
    {
        $id = escape_string(clear_post($id));
        $sql = self::baseSelect($columns) . ' WHERE project_id =' . $id . ' AND as_draft=0 LIMIT 1';
        try {
            return querySingle($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function getByProjectId($id, array $columns = ['*'])
    {
        $id = escape_string(clear_post($id));
        $sql = self::baseSelect($columns) . ' WHERE project_id =' . $id . ' LIMIT 1';
        try {
            return querySingle($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function update($id, $data)
    {
        $id = escape_string(clear_post($id));
        $sql = update_query_string(self::getTable(), $data) . ' WHERE id=' . $id;
        try {
            return query($sql);
        } catch (Exception $e) {
            return false;
        }
    }

    public static function getById($id, $columns = ['*'])
    {
        $id = escape_string(clear_post($id));
        $sql = self::baseSelect($columns) . ' WHERE id =' . $id . ' LIMIT 1';
        try {
            return querySingle($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function add(array $data)
    {
        $sql = insert_query_string(self::getTable(), $data);
        try {
            $res = query($sql);
            return $res ? getLastId() : $res;
        } catch (Exception $e) {
            return false;
        }
    }
}