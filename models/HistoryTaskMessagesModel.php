<?php

namespace models;

use Exception;
use traits\BaseSelect;
use traits\GetTable;

class HistoryTaskMessagesModel
{
    private static $table = 'history_task_messages';
    use getTable;
    use baseSelect;

    public static function add($data)
    {
        $sql = insert_query_string(self::getTable(), $data);
        try {
            $res = query($sql);
            return $res ? getLastId() : $res;
        } catch (Exception $e) {
            return false;
        }
    }

    public static function getById($id, $columns = ['*'])
    {
        $id = escape_string($id);
        $sql = self::baseSelect($columns) . " WHERE $id = '$id'";
        try {
            return querySingle($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function updateById($ids, $data)
    {
        $ids = getAsArray($ids);
        $sql = update_query_string(self::getTable(), $data) . ' WHERE id IN (' . implode(',', $ids) . ')';
        try {
            return query($sql);
        } catch (Exception $e) {
            return false;
        }
    }

    public static function getByTaskId($task_id, $columns = ['*'])
    {
        $sql = self::baseSelect($columns) . ' WHERE task_id = ' . $task_id;
        try {
            return query($sql);
        } catch (Exception $e) {
            return [];
        }
    }
}