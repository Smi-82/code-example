<?php

namespace models;

use Exception;
use traits\BaseSelect;
use traits\GetTable;

class MailHistoryModel
{
    private static $table = 'mail_history';
    use GetTable;
    use BaseSelect;

    public static function add($data)
    {
        $sql = insert_query_string(self::getTable(), $data);
        try {
            $res = query($sql);
            return $res ? getLastId() : $res;
        } catch (Exception $e) {
            return false;
        }
    }

    public static function getById($id, array $columns = ['*'])
    {
        $id = escape_string(clear_post($id));
        $sql = self::baseSelect($columns) . ' WHERE id =' . $id . ' LIMIT 1';
        try {
            return querySingle($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function getAll(array $columns = ['*'])
    {
        $sql = self::baseSelect($columns);
        try {
            return query($sql);
        } catch (Exception $e) {
            return [];
        }
    }
}