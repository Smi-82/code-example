<?php

namespace models;

use Exception;
use traits\BaseSelect;
use traits\GetTable;

class ClientServicesModel
{
    private static $table = 'client_services';
    use GetTable;
    use BaseSelect;

    public static function deleteByClientAndService($client_id, $services)
    {
        $services = array_map(function ($elem) {
            return "'" . $elem . "'";
        }, $services);
        $client_id = escape_string(clear_post($client_id));
        $sql = 'DELETE FROM ' . self::getTable() . ' WHERE user_id=' . $client_id . ' AND service_key NOT IN (' . implode(', ', $services) . ')';
        try {
            return query($sql);
        } catch (Exception $e) {
            return false;
        }
    }

    public static function multiAdd($data)
    {
        $sql = multiple_insert_str(self::getTable(), $data);
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getTraceAsString();
        }
    }

    public static function add($data)
    {
        $sql = insert_query_string(self::getTable(), $data);
        try {
            $res = query($sql);
            return $res ? getLastId() : $res;
        } catch (Exception $e) {
            return $e->getTraceAsString();
        }
    }
}