<?php

namespace models;

use Exception;
use traits\BaseSelect;
use traits\GetTable;

class ProjectBillModel
{
    private static $table = 'project_bill';
    use GetTable;
    use BaseSelect;

    public static function delete($bill_id)
    {
        $bill_id = escape_string(clear_post($bill_id));
        $sql = 'DELETE FROM ' . self::getTable() . ' WHERE id=' . $bill_id;
        try {
            return query($sql);
        } catch (Exception $e) {
            return false;
        }
    }

    public static function getByProjectId($project_id, array $columns = ['*'])
    {
        $project_id = escape_string(clear_post($project_id));
        $sql = self::baseSelect($columns) . ' WHERE project_id=' . $project_id;
        try {
            return query($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function getApprovedByProjectId($projects_id, array $columns = ['*'])
    {
        $projects_id = getAsArray($projects_id);
        $sql = self::baseSelect($columns) . ' WHERE approved=1 AND project_id IN (' . implode(',', $projects_id) . ')';
        try {
            return query($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function getLastBDCByProjectId($projects_id, array $columns = ['*'])
    {
        $projects_id = getAsArray($projects_id);
        $sql = self::baseSelect($columns) . ' WHERE id IN ((SELECT MAX(id) FROM ' . self::getTable() . ' WHERE approved=0 AND project_id IN (' . implode(',', $projects_id) . ') GROUP BY token))';
        try {
            return query($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function getAll(array $columns = ['*'])
    {
        $sql = self::baseSelect($columns);
        try {
            return query($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function getAllApprovedByProjectId($project_id, array $columns = ['*'])
    {
        $project_id = escape_string(clear_post($project_id));
        $sql = self::baseSelect($columns) . ' WHERE approved=1 AND project_id=' . $project_id;
        try {
            return query($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function getLastApprovedByProjectId($project_id, array $columns = ['*'])
    {
        $project_id = escape_string(clear_post($project_id));
        $sql = self::baseSelect($columns) . ' WHERE approved=1 AND project_id=' . $project_id . ' ORDER BY id DESC LIMIT 1';
        try {
            $res = query($sql);
            return $res ? array_shift($res) : $res;
        } catch (Exception $e) {
            return [];
        }
    }

    public static function getAllApproved(array $columns = ['*'])
    {
        $sql = self::baseSelect($columns) . ' WHERE approved=1';
        try {
            return query($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function getById($id, array $columns = ['*'])
    {
        $id = escape_string(clear_post($id));
        $sql = self::baseSelect($columns) . ' WHERE id=' . $id . ' LIMIT 1';
        try {
            return querySingle($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function add($data)
    {
        $sql = insert_query_string(self::getTable(), $data);
        try {
            $res = query($sql);
            return $res ? getLastId() : $res;
        } catch (Exception $e) {
            return false;
        }
    }

    public static function createTable()
    {
        $sql = 'CREATE TABLE `taskeo`.' . self::getTable() . '(
                        `id` INT(11) NOT NULL AUTO_INCREMENT ,
                        `project_id` INT(11) NOT NULL ,
                        `created` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ,
                        `city` VARCHAR(255) NOT NULL ,
                        `discount` DECIMAL(10.2) NOT NULL DEFAULT \'0\' ,
                        `discount_type` TINYINT(1) NOT NULL DEFAULT \'0\' COMMENT \'0-percent, 1-amount\' ,
                        `comment` TEXT NULL DEFAULT NULL , PRIMARY KEY (`id`)
                        ) ENGINE = InnoDB;
                        ';
    }

    public static function getSumPaymentByProjectId($project_id)
    {
        $project_id = escape_string(clear_post($project_id));
        $sql = 'SELECT SUM(amount) as balance FROM ' . self::getTable() . ' WHERE project_id=' . $project_id;
        try {
            $res = querySingle($sql);
            return $res['balance'];
        } catch (Exception $e) {
            return 0;
        }
    }

    public static function update($bill_id, array $data)
    {
        $bill_id = escape_string(clear_post($bill_id));
        $sql = update_query_string(self::getTable(), $data) . ' WHERE id=' . $bill_id;
        try {
            return query($sql);
        } catch (Exception $e) {
            return false;
        }
    }
}