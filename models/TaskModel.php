<?php


namespace models;


use Exception;
use Group;
use Project;
use TaskAccess;
use TasksMessages;
use TasksMessagesFiles;
use traits\BaseSelect;
use traits\GetTable;
use User;

class TaskModel
{
    use GetTable;
    use BaseSelect;

    private static $table = 'tasks';

    private static function _getBySomeId($table, $id, $columns = ['*'])
    {
        $id = escape_string(clear_post($id));
        $columns = array_map(function ($elem) {
            return 'a.' . $elem;
        }, $columns);
        $sql = 'SELECT ' . implode(',', $columns) . ' FROM ' . self::getTable() . ' a LEFT JOIN ' . $table . ' b ON a.id = b.task_id WHERE b.id = ' . $id . ' LIMIT 0, 1';
        try {
            return querySingle($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function getByGroupIds($group_ids, $columns = ['*'])
    {
        $group_ids = getAsArray($group_ids);
        $sql = self::baseSelect($columns) . ' WHERE group_id IN (' . implode(',', $group_ids) . ')';
        try {
            return query($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function getByWhoCreated($user_id, $columns = ['*'])
    {
        $user_id = escape_string(clear_post($user_id));
        $columns = clearParams($columns);
        try {
            return query(self::baseSelect($columns) . ' WHERE user_id = ' . $user_id);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function add($data)
    {
        $sql = insert_query_string(self::getTable(), $data);
        try {
            $res = query($sql);
            return $res ? getLastId() : $res;
        } catch (Exception $e) {
            return false;
        }
    }

    public static function update($task_id, $data = [])
    {
        $task_id = getAsArray($task_id);
        $sql = update_query_string(self::getTable(), $data) . ' WHERE id IN (' . implode(',', $task_id) . ')';
        try {
            return query($sql);
        } catch (Exception $e) {
            return false;
        }
    }

    public static function updateOrder($arr)
    {
        $arr = array_map(function ($el) {
            return ' WHEN id=' . escape_string($el['id']) . ' THEN ' . escape_string($el['ord']);
        }, $arr);
        $sql = 'UPDATE ' . self::getTable() . ' SET ord=CASE ' . implode(' ', $arr) . ' ELSE ord END';
        try {
            return query($sql);
        } catch (Exception $e) {
            return false;
        }
    }

    public static function getByMessageId($id, $columns = ['*'])
    {
        return self::_getBySomeId(TasksMessages::getTable(), $id, $columns);
    }

    public static function getByAttachmentId($id, $columns = ['*'])
    {
        return self::_getBySomeId(TasksMessagesFiles::getTable(), $id, $columns);
    }

    public static function getProject($task_id, $columns = ['*'])
    {
        $task_id = escape_string(clear_post($task_id));
        $columns = clearParams($columns);
        $columns = array_map(function ($elem) {
            return 'p.' . $elem;
        }, $columns);
        $sql = 'SELECT ' . implode(',', $columns) . ' FROM ' . self::getTable() . ' AS t LEFT JOIN ' . Group::getTable() . ' AS g ON t.group_id=g.id LEFT JOIN ' . Project::getTable() . ' AS p ON g.general_project_id=p.id WHERE t.id=' . $task_id;
        try {
            return querySingle($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function delete($ids)
    {
        $sql = 'DELETE FROM ' . self::getTable() . ' WHERE id IN (' . implode(',', $ids) . ')';
        try {
            return query($sql);
        } catch (Exception $e) {
            return false;
        }
    }

    public static function getOwnParticipants($task_ids, $columns = ['*'])
    {
        $task_ids = getAsArray($task_ids);
        $columns = clearParams($columns);
        $columns = array_map(function ($elem) {
            return 'u.' . $elem;
        }, $columns);
        $sql = 'SELECT ta.task_id, ' . implode(',', $columns) . ' FROM ' . TaskAccess::getTable() . ' AS ta LEFT JOIN ' . User::getTable() . ' AS u ON u.id=ta.user_id WHERE ta.task_id IN (' . implode(', ', $task_ids) . ')';
        try {
            return query($sql);
        } catch (Exception $e) {
            return [];
        }

    }

    public static function getManyByIds($ids, $columns = ['*'])
    {
        $ids = getAsArray($ids);
        $columns[] = ' id as t_id_smi ';
        $sql = self::baseSelect($columns) . ' WHERE id IN (' . implode(',', $ids) . ')';
        try {
            $res = query($sql);
            return valueToKey('t_id_smi', $res, true);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function getAll($columns = ['*'])
    {
        $sql = self::baseSelect($columns);
        try {
            return query($sql);
        } catch (Exception $e) {
            return [];
        }
    }

    public static function changeCategory($from, $to)
    {
        $from = escape_string(clear_post($from));
        $to = escape_string(clear_post($to));
        $sql = update_query_string(self::getTable(), ['category' => $to]) . ' WHERE category=' . $from;
        try {
            return query($sql);
        } catch (Exception $e) {
            return false;
        }
    }

}