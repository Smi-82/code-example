<?php


namespace models;


use Exception;
use traits\BaseSelect;
use traits\GetTable;
use User;
use UserTypes;

class ProjectClientMessagesModel
{
    private static $table = 'project_client_messages';
    use getTable;
    use baseSelect;

    public static function add($data)
    {
        $sql = insert_query_string(self::getTable(), $data);
        try {
            $res = query($sql);
            return $res ? getLastId() : $res;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function updateById($ids, $data)
    {
        if (isset($data['id']))
            unset($data['id']);
        $ids = getAsArray($ids);
        $sql = update_query_string(self::getTable(), $data) . ' WHERE id IN (' . implode(',', $ids) . ')';
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function getById($message_id, $columns = ['*'])
    {
        $message_id = escape_string($message_id);
        $sql = self::baseSelect($columns) . ' WHERE id = ' . $message_id . ' LIMIT 0, 1';
        try {
            return querySingle($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function deleteByIdAndUserId($id, $user_id)
    {
        $id = escape_string(clear_post($id));
        $user_id = escape_string(clear_post($user_id));
        $sql = 'DELETE FROM ' . self::getTable() . ' WHERE id=' . $id . ' AND user_id=' . $user_id;
        try {
            return query($sql);
        } catch (Exception $e) {
            return false;
        }
    }

    public static function getClientMessagesByProjectId($project_id, $user_id = null)
    {
        $project_id = escape_string(clear_post($project_id));
        if ($user_id !== null)
            $user_id = escape_string(clear_post($user_id));
        $sql = 'SELECT a.*,
                b.`firstname` as `user_name`,
                b.`lastname` as `user_lastname`,
                b.`usertype` as `user_usertype`,
                b.`img` as `user_img`,
                c.`name` as `usertype_name`
                FROM ' . self::getTable() . ' a
                LEFT JOIN ' . User::getTable() . ' b ON a.`user_id` = b.`id`
                LEFT JOIN ' . UserTypes::getTable() . ' c ON b.`usertype` = c.`id`
                WHERE `project_id` = ' . $project_id;
        try {
            return query($sql);
        } catch (Exception $e) {
            return [];
        }
    }
}