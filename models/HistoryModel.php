<?php

namespace models;

use Exception;
use traits\BaseSelect;
use traits\GetTable;

class HistoryModel
{
    private static $table = 'history';
    use getTable;
    use baseSelect;

    public static function add($data)
    {
        $sql = insert_query_string(self::getTable(), $data);
        try {
            $res = query($sql);
            return $res ? getLastId() : $res;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}