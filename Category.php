<?php

use appEvents\AppEventsController;
use appEvents\subscribers\controller\AppEventsSubscribersController;
use models\CategoryEntityModel;
use models\CategoryModel;

class Category
{
    private static $baseCategoryId = 48;

    public static function getTable()
    {
        return CategoryModel::getTable();
    }

    public static function isBase($cat_id)
    {
        return intval($cat_id) == self::getBaseCategory();
    }

    public static function getBaseCategory()
    {
        return self::$baseCategoryId;
    }

    public static function getEntity()
    {
        return [
            Project::getTable() => [
                'id' => 1,
                'name' => 'projects',
                'className' => Project::getClassName()
            ],
            Task::getTable() => [
                'id' => 2,
                'name' => 'tasks',
                'className' => Task::getClassName()
            ],
        ];
    }

    public static function baseSelect(array $columns = ['*'])
    {
        $columns = clearParams($columns);
        return 'SELECT ' . implode(',', $columns) . ', id AS tmp_id, IF(id=' . self::getBaseCategory() . ',"1","0") as isBase FROM ' . CategoryModel::getTable();
    }

    public static function getHTMLNames($key)
    {
        $arr = [
            'name' => 'data[name]',
            'entity' => 'data[entity][]'
        ];
        return $arr[$key] ?? $arr;
    }

    public static function deleteEntity($cat_id, $ent_id)
    {
        try {
            $res = CategoryEntityModel::deleteByCategoryIdAndEntityId($cat_id, $ent_id);
            if ($res) {
                $class = self::getEntityClassNameById($ent_id);
                $res = $class::changeCategory($cat_id, self::getBaseCategory());
            }
            return $res;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function getEntityClassNameById($ent_id)
    {
        $res = self::getOneEntityById($ent_id);
        $res = array_shift($res);
        return $res['className'];
    }

    public static function getEntityKeyById($ent_id)
    {
        $res = self::getOneEntityById($ent_id);
        $res = array_keys($res);
        return array_shift($res);
    }

    public static function addEntity($category_id, $entity)
    {
        return CategoryEntityModel::add($category_id, $entity);
    }

    public static function getOneEntityById($id)
    {
        return self::getManyEntityById_s($id);
    }

    public static function getManyEntityById_s($ids)
    {
        $ids = getAsArray($ids);
        return array_filter(self::getEntity(), function ($elem) use ($ids) {
            return in_array($elem['id'], $ids);
        });
    }

    public static function getEntityIdByName($name)
    {
        return self::getEntity()[$name]['id'];
    }

    public static function getEntityByName($names)
    {
        $names = getAsArray($names);
        return array_intersect_key(self::getEntity(), array_flip($names));
    }

    public static function getCategoriesByEntity($entity_table_name, array $params = ['*'])
    {
        $entity = self::getEntity();
        if (!isset($entity[$entity_table_name]))
            return [];
        $cat_ids = CategoryEntityModel::getCategoriesByEntity($entity[$entity_table_name]['id'], ['category_id']);
        if (empty($cat_ids)) return [];
        $cat_ids = array_column($cat_ids, 'category_id');
        return self::getCategoriesById($cat_ids, $params);
    }

    public static function getEntityByCategoryId($cat_id)
    {
        $res = CategoryEntityModel::getEntityByCategoryId($cat_id);
        $res = valueToKey('category_id', $res, true);
        return array_map(function ($elem) {
            $elem['entity'] = explode(',', $elem['entity']);
            $elem['entity'] = self::getManyEntityById_s($elem['entity']);
            return $elem;
        }, $res);
    }

    public static function getAllCategoriesWithEntity(array $params = ['*'])
    {
        try {
            $res = self::getAllCategories($params);
            $cat_id = array_column($res, 'id');
            $entity = self::getEntityByCategoryId($cat_id);
            return array_map(function ($elem) use ($entity) {
                $elem['entity'] = $entity[$elem['id']]['entity'];
                return $elem;
            }, $res);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function getAllCategories(array $params = ['*'])
    {
        return CategoryModel::getAll($params);
    }

    public static function getCategoriesById($ids, array $params = ['*'])
    {
        return CategoryModel::getCategoriesById($ids, $params);
    }

    public static function getOneCategoryById($id, array $params = ['*'])
    {
        try {
            $res = self::getCategoriesById($id, $params);
            return $res ? array_shift($res) : '';
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function add($data)
    {
        $entity = '';
        if (isset($data['entity'])) {
            $entity = $data['entity'];
            unset($data['entity']);
        }
        $data['whoCreate'] = User()->getId();
        try {
            $res = CategoryModel::add($data);
            if ($res && is_numeric($res)) {
                AppEventsController::publish(AppEventsSubscribersController::newCategory(), ['id' => $res]);
                if (!empty($entity)) {
                    foreach ($entity as $ent_id) {
                        self::addEntity($res, intval($ent_id));
                    }
                }
            }
            return $res;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function update($id, $data)
    {
        return CategoryModel::update($id, $data);
    }
}