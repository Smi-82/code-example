<?php

use abstractClasses\TableData;

class UsersStatisticPage extends TableData
{
    protected function addFilters()
    {
        $filters = '';
        if ($this->_filters != null && is_array($this->_filters)) {
            foreach ($this->_filters as $field => $val) {
                if (!empty($val)) {
                    switch ($field) {
                        default:
                            $filters .= ' AND u.' . $field . ' IN (' . implode(', ', $val) . ') ';
                    }
                }
            }
        }
        return $filters;
    }

    protected function addFind()
    {
        if (!empty($this->_findBy)) {
            switch ($this->_findField) {
                case 'userName':
                    $findByName = ' AND (u.firstname LIKE "%' . $this->_findBy . '%" OR u.lastname LIKE "%' . $this->_findBy . '%") ';
                    break;
                case 'phones':
                    $findByName = ' AND (u.phone LIKE "%' . $this->_findBy . '%" OR u.phone2 LIKE "%' . $this->_findBy . '%") ';
                    break;
                default:
                    $findByName = ' AND u.' . $this->_findField . ' LIKE "%' . $this->_findBy . '%" ';
            }
        }
        else
            $findByName = '';
        return $findByName;
    }

    protected function addSort()
    {
        if ($this->_sortWay == 'both') {
            $sort = ' ORDER BY u.id ';
        }
        else {
            $sort = ' ORDER BY ' . $this->_sortBy . ' ' . $this->_sortWay . ' ';
        }
        return $sort;
    }

    protected function addPage($findByName = '')
    {
        if (!empty($this->_currentPage) && $this->_cntRows != 0) {
            return ' LIMIT ' . ($this->_currentPage * $this->_cntRows - $this->_cntRows) . ', ' . ($this->_cntRows);
        }
        return '';
    }

    public function getTableData()
    {
        $sql = '
SELECT
u.id,
TRIM(CONCAT(u.firstname," ",u.lastname)) AS userName,
u.usertype,
u.email,
TRIM(CONCAT(u.phone, IF(u.phone != "" AND u.phone2 != "", ", ", ""), u.phone2)) AS phones,
u.country,
u.img,
ut.name AS userTypeName
FROM
' . User::getTable() . ' AS u
LEFT JOIN
' . UserTypes::getTable() . ' AS ut
ON
ut.id = u.usertype
WHERE u.usertype !=' . UserTypes()->getClient() . ' ';
        /*
        if (User()->isRGPDMaster()) {

            $obj = new Consultant();
            $tmp = $obj->getChildrensId(User()->getId());
            $tmp[] = User()->getId();
//            $clients = $obj->getOwnClientsForConsultant($tmp, ['id']);
//            $users = array_merge($tmp, array_column($clients, 'id'));
            $users = $tmp;
            $users = array_unique($users, SORT_NUMERIC);
            $users = array_filter($users, function ($elem) {
                return $elem != User()->getId();
            });
            $sql .= ' AND u.id IN (' . implode(', ', $users) . ') ';

        }
        */

        $sql .= $findByName = $this->addFind();
        $sql .= $this->addFilters();
        $sql .= $this->addSort();
        $sql .= $this->addPage($findByName);
        try {
            $sql1 = 'SELECT COUNT(u.id) AS cntRows FROM ' . User::getTable() . ' AS u WHERE u.usertype !=' . UserTypes()->getClient();
            if (User()->isRGPDMaster()) {
                $sql1 .= ' AND u.id IN (' . implode(', ', $users) . ') ';
            }
            $sql1 .= $this->addFilters();
            $sql1 .= $findByName;
            $cntRows = querySingle($sql1)['cntRows'];
            $users = query($sql);

            //-----------------------------------
            if (!empty($users)) {

                $users_ids = array_column($users, 'id');
                $statuses_obj = Statuses();
                $statuses = $statuses_obj->getAllStatuses();
                unset($statuses[$statuses_obj->approved_by_the_customer()]);
                $statuses_ids = array_flip(array_keys($statuses));
                $status_excl = [$statuses_obj->approved_by_the_customer()];
                $sql = '
SELECT
ta.user_id,
t.status,
COUNT(t.status) AS cnt_status
FROM
' . TaskAccess::getTable() . ' AS ta
LEFT JOIN
' . Task::getTable() . ' AS t
ON
ta.task_id=t.id
WHERE
ta.user_id
IN
(' . implode(',', $users_ids) . ')
AND
t.status
NOT IN (' . implode(',', $status_excl) . ')
GROUP BY
ta.user_id,
t.status';
                $res = query($sql);
                $tmp = [];
                if (!empty($res)) {
                    foreach ($res as $v) {
                        $tmp[$v['user_id']][$v['status']] = $v['cnt_status'];
                    }
                    foreach ($tmp as $u_id => &$statuses) {
                        $dif = array_diff_key($statuses_ids, $statuses);
                        if (!empty($dif)) {
                            foreach ($dif as $k => $v) {
                                $statuses[$k] = 0;
                            }
                        }
                    }
                }
                $statuses_ids = array_fill_keys(array_keys($statuses_ids), 0);
                foreach ($users_ids as $id) {
                    if (!isset($tmp[$id])) {
                        $tmp[$id] = $statuses_ids;
                    }
                }
                $users = array_map(function ($el) use ($tmp, $statuses_ids) {
                    $el['img'] = getImg($el, $el['id']);
                    $el['statuses'] = $tmp[$el['id']] ?? $statuses_ids;
                    return $el;
                }, $users);
            }

            //-----------------------------------
//            $users = array_map(function($elem){
//                $elem['userTypeName'] = UserTypes()->getTypeById($elem['usertype'],'name');
//                return $elem;
//            }, $users);
            return ['users' => $users, 'cntRows' => $cntRows];
        } catch (Exception $e) {
            return ['error' => $e->getMessage()];
        }
    }

    public static function getUserStatisticsDetails($user_id)
    {
        $user_id = escape_string(clear_post($user_id));
        $statuses_obj = Statuses();
        $statuses = $statuses_obj->getAllStatuses();
        unset($statuses[$statuses_obj->approved_by_the_customer()]);
        $statuses = array_column($statuses, 'id');
        $sql = '
SELECT
p.id AS p_id,
p.name AS p_name,
TRIM(CONCAT(c.firstname, " ",c.lastname)) AS client_name,
t.name AS t_name,
t.status,
t.id AS t_id
FROM
' . TaskAccess::getTable() . ' AS ta
LEFT JOIN
' . Task::getTable() . ' AS t
ON
ta.task_id=t.id
LEFT JOIN
' . Group::getTable() . ' AS g
ON
t.group_id=g.id
LEFT JOIN
' . Project::getTable() . ' AS p
ON
g.general_project_id=p.id
LEFT JOIN
' . User::getTable() . ' AS c
ON
c.id=p.client_id
WHERE
t.status IN (' . implode(',', $statuses) . ')
AND
ta.user_id =' . $user_id . ' ORDER BY p_name, status, t_id';
        try {
            $res = query($sql);
            $tmp = [];
            if (!empty($res)) {
                foreach ($res as $v) {
                    $v['status_name'] = ($statuses_obj->getStatusesById($v['status']));
                    $v['status_name'] = $v['status_name']['aliase'];
                    $tmp[$v['p_id']]['p_name'] = $v['p_name'];
                    $tmp[$v['p_id']]['p_id'] = $v['p_id'];
                    $tmp[$v['p_id']]['client_name'] = $v['client_name'];
                    $tmp[$v['p_id']]['tasks'][$v['t_id']]['t_id'] = $v['t_id'];
                    $tmp[$v['p_id']]['tasks'][$v['t_id']]['t_name'] = $v['t_name'];
                    $tmp[$v['p_id']]['tasks'][$v['t_id']]['status'] = $v['status'];
                    $tmp[$v['p_id']]['tasks'][$v['t_id']]['status_name'] = $v['status_name'];
                }
                $tmp = array_values($tmp);
                $tmp = array_map(function ($el) {
                    $el['tasks'] = array_values($el['tasks']);
                    return $el;
                }, $tmp);
            }
            return $tmp;
        } catch (Exception $e) {
//            console($sql);
            return $e->getMessage();
        }
    }
}