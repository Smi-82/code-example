<?php

use traits\BaseSelect;
use traits\GetTable;
use traits\SingleTone;

class PositionSpeciality
{
    private static $table = 'position_specialities';
    use SingleTone;
    use GetTable;
    use BaseSelect;

    public function getAll($params = ['*'])
    {
        $sql = self::baseSelect($params);
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function getById($id, $params = ['*'])
    {
        if (is_scalar($id)) $id = [$id];
        $sql = self::baseSelect($params) . ' WHERE id in (' . implode(',', $id) . ')';
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function add($data)
    {
        $sql = insert_query_string(self::getTable(), $data);
        try {
            query($sql);
            return getLastId();
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function update($id, $data)
    {
        $id = escape_string($id);
        $sql = update_query_string(self::getTable(), $data) . ' WHERE id = ' . $id;
        try {
            query($sql);
            return getLastId();
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function delete($id)
    {
        $sql = 'DELETE FROM ' . self::getTable() . ' WHERE id = ' . $id;
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}