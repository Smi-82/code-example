<?php

use traits\BaseSelect;
use traits\GetTable;

class TaskHistory
{
    private static $table = 'tasks_history';
    use GetTable;
    use BaseSelect;

    public static function add($data)
    {
        if (!$data['user_id'])
            $data['user_id'] = User()->getId();
        $sql = insert_query_string(self::getTable(), $data);
        try {
            $res = query($sql);
            return $res ? getLastId() : $res;
        } catch (Exception $e) {
            return $e->getTrace();
        }
    }

    public static function delete($id)
    {
        $id = escape_string(clear_post($id));
        $sql = 'DELETE FROM' . self::getTable() . 'WHERE id=' . $id;
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}