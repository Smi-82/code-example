<?php

use traits\SingleTone;

class Access
{
    use SingleTone;

    private function editResult($arr, $fl)
    {
        if ($arr && $fl) {
            $arr = implode(', ', $arr);
        }
        return $arr;
    }

    public function hasOpenTask()
    {
        $query_str = TaskAccess::baseSelect(['id']) . ' WHERE user_id = ' . User()->getId() . ' AND is_on_task=1 LIMIT 1';
        try {
            return array_column(query($query_str), 'id');
        } catch (Exception $e) {
            return [];
        }
    }

    public function toTask($user_id, $task_id)
    {
        $access = TaskAccess::getByTaskAndUserIds($task_id, $user_id, ['id']);
        if (!empty($access))
            return true;
        else {
            $p_id = Task()->getProject($task_id, ['id'])['id'];
            $p_id = Project()->getById($p_id, ['client_id', 'user_id']);
            if (empty($p_id))
                return false;
            return in_array($user_id, [$p_id['client_id'], $p_id['user_id']]);
        }
    }

    public function toGroup($user_id, $group_id)
    {
        return in_array($group_id, self::getGroupListForUser($user_id));
    }

    public function getGroupListForUser($user_id, $asStr = false)
    {
        /**
         * groups created by the user
         */
        $result = Group::getByWhoCreated($user_id, ['id']);
        if ($result) {
            $result = array_column($result, 'id');
            return self::editResult($result, $asStr);
        }
//        $query_str = '';
        /**
         * groups to which the user has been added
         */
        /*
        if ($user_id == User()->getId()) {
            if (User()->isCollaborator()) {
                $query_str = 'SELECT group_id FROM collaborator_group_access WHERE user_id=' . $user_id;
            } else if (User()->isEmploye()) {
                $query_str = 'SELECT group_id FROM employe_group_access WHERE user_id=' . $user_id;
            }
            if (!empty($query_str)) {
                try {
                    $result = query($query_str);
                } catch (Exception $e) {
                    $result = [];
                }
                if (!empty($result)) {
                    $result = array_column($result, 'group_id');
                    return self::editResult($result, $asStr);
                }
            }
        }
        */
        /**
         * Groups in which there are tasks to which the user has been added
         */
        $task_ids = TaskAccess::getByUserId($user_id, ['task_id']);
        $task_ids = array_column($task_ids, 'task_id');
        $result = Task::getManyByIds($task_ids, ['group_id']);
        if ($result) {
            $result = array_column($result, 'group_id');
            $result = array_unique($result);
            return self::editResult($result, $asStr);
        }
    }

    public function toProject($user_id, $project_id)
    {
        return in_array($project_id, $this->getProjectListForUser($user_id));
    }

    public function getProjectListForUser($user_id, $asStr = false)
    {
        return Project::getProjectListForUser($user_id, $asStr);
    }

    public function getTaskListForUser($user_id, $asStr = false)
    {
        $result = Task::getByWhoCreated($user_id, ['id']);
        $result = array_column($result, 'id');
        return self::editResult($result, $asStr);
    }
}