<?php

use models\AssistantModel;
use traits\SingleTone;

class Assistant
{
    use SingleTone;

    public static function getTable()
    {
        return ProjectAccess::getTable();
    }

    public static function getByOneProjectId($id, array $columns = ['*'])
    {
        $res = self::getByManyProjectId($id, $columns);
        return $res[$id];
    }

    public static function getAll(array $columns = ['*'])
    {
        return User::getByTypeId(UserTypes()->getAssistant(), $columns);
    }

    public static function getByManyProjectId($id, array $columns = ['*'])
    {
        try {
            return AssistantModel::getByManyProjectId($id, $columns);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}