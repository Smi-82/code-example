<?php

use traits\BaseSelect;
use traits\GetTable;
use traits\SingleTone;

class UserTypes
{
    use SingleTone;
    use GetTable;
    use BaseSelect;

    private static $table = 'usertypes';
    private static $usertype_data = null;

    private function __construct()
    {
//        if (!self::checkUserTypeData()) {
//            self::setUserTypeData();
//        }
    }

    private static function checkUserTypeData()
    {
        return !is_null(self::$usertype_data) && !empty(self::$usertype_data);
    }

    private static function setUserTypeData()
    {
        try {
            $res = query(self::baseSelect());
            $res = valueToKey('id', $res);
        } catch (Exception $e) {
            $res = [];
        }
        self::$usertype_data = $res;
    }

    private static function getUserTypeData()
    {
        if (!self::checkUserTypeData())
            self::setUserTypeData();

        return self::$usertype_data;
    }

    public function getUserTypesForJsData()
    {
        $UserTypes = self::getOwnMethodsWithExceptions();
        $obj = $this;
        $UserTypes_val = array_map(function ($elem) use ($obj) {
            return $obj->$elem();
        }, $UserTypes);
        return array_combine($UserTypes, $UserTypes_val);
    }

    public static function changeChatAccess()
    {
        $sql = update_query_string(self::getTable(), ['allow_сhat_with_client' => $_POST['allow_сhat_with_client']]) . ' WHERE id=' . escape_string(clear_post($_POST['id']));
        try {
            return query($sql);
        } catch (Exception $e) {
            consoleJS($e->getTrace());
        }

    }


    public static function getOwnMethods()
    {
        return get_class_methods(self::getInst());
    }

    public static function getOwnMethodsWithExceptions()
    {
        $res = self::getOwnMethods();
        $res = array_diff($res, [
            __FUNCTION__,
            '__clone',
            '__wakeup',
            '__construct',
            'getTable',
            'getAll',
            'getInst',
            'getTypeById',
            'changeChatAccess',
            'addType',
            'getOwnMethods',
            'getTypesHierarchyBelow',
            'getTypesHierarchyHigher',
            'getTypesHierarchyHigher',
            'baseSelect',
            'setUserTypeData',
            'checkUserTypeData',
            'getUserTypeData',
            'getUserTypesForJsData',
            'update',
            'setShowTheClient',
        ]);
        return $res;
    }

    public static function addType()
    {
        $type_id = escape_string(clear_post($_POST['type_id']));
        if ($type_id) {
            if (!isset($_POST['types']['allow_сhat_with_client']))
                $_POST['types']['allow_сhat_with_client'] = 0;
            $sql = update_query_string(self::getTable(), $_POST['types']) . ' WHERE id = ' . $type_id;
        }
        else {
            $sql = insert_query_string(self::getTable(), $_POST['types']);
        }
        try {
            $res = query($sql);
            self::setUserTypeData();
            return $res;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function setShowTheClient($id, $val)
    {
        return self::update($id, ['show_the_client' => $val]);
    }

    public static function update($id, $data)
    {
        $id = getAsArray($id);
        $sql = update_query_string(self::getTable(), $data) . ' WHERE id IN (' . implode(',', $id) . ')';
        try {
            return query($sql);
        } catch (Exception $e) {
            return false;
        }
    }

    public function getAll($column = null)
    {
        $data = self::getUserTypeData();
        usort($data, function ($a, $b) {
            $a = $a['name'];
            $b = $b['name'];
            if ($a == $b)
                return 0;
            return ($a < $b) ? -1 : 1;
        });
        if (!is_null($column) && isset($data[0][$column]))
            return array_column($data, $column);
        return $data;
    }

    public function getTypeById($id, $param = '')
    {
        $id = intval($id);
        if (empty($param))
            return self::getUserTypeData()[$id];
        return isset(self::getUserTypeData()[$id][$param]) ? self::getUserTypeData()[$id][$param] : false;
    }

    public function getTypesHierarchyBelow($hierarchy)
    {
        $hierarchy = escape_string(clear_post($hierarchy));
        $tmp = [];
        foreach (self::getUserTypeData() as $k => $v) {
            if (intval($v['hierarchy']) < intval($hierarchy) && in_array($k, $this->getRGPDUsers()))
                $tmp[$k] = $v;
        }
        usort($tmp, function ($a, $b) {
            $a = $a['name'];
            $b = $b['name'];
            if ($a == $b)
                return 0;
            return ($a < $b) ? -1 : 1;
        });
        return $tmp;

    }

    public function getTypesHierarchyHigher($hierarchy)
    {
        $hierarchy = escape_string(clear_post($hierarchy));
        $tmp = [];
        foreach (self::getUserTypeData() as $k => $v) {
            if (intval($v['hierarchy']) > intval($hierarchy) && in_array($k, $this->getRGPDUsers()))
                $tmp[$k] = $v;
        }
        usort($tmp, function ($a, $b) {
            $a = $a['name'];
            $b = $b['name'];
            if ($a == $b)
                return 0;
            return ($a < $b) ? -1 : 1;
        });
        return $tmp;

    }

    public function getSupervisor()
    {
        return 1;
    }

    public function getChef()
    {
        return 2;
    }

    public function getAssistant()
    {
        return 3;
    }

    public function getTeamLead()
    {
        return 4;
    }

    public function getDeveloper1()
    {
        return 5;
    }

    public function getDeveloper2()
    {
        return 6;
    }

    public function getDeveloper3()
    {
        return 7;
    }

    public function getDesigner1()
    {
        return 8;
    }

    public function getDesigner2()
    {
        return 9;
    }

    public function getEngineer1()
    {
        return 10;
    }

    public function getEngineer2()
    {
        return 11;
    }

    public function getContentEditor()
    {
        return 12;
    }

    public function getTranslator()
    {
        return 13;
    }

    public function getCommunityManager1()
    {
        return 14;
    }

    public function getClient()
    {
        return 15;
    }

    public function getArtDirector()
    {
        return 16;
    }

    public function getAnimator()
    {
        return 17;
    }

    public function getMarketingDirector()
    {
        return 18;
    }

    public function getMarketingAssistant()
    {
        return 19;
    }

    public function getSecretory()
    {
        return 22;
    }

    public function getRGPDSenior()
    {
        return 23;
    }

    public function getRGPDJunior()
    {
        return 24;
    }

    public function getTeleprospector()
    {
        return 25;
    }

    public function getRGPDMaster()
    {
        return 26;
    }

    public function getRGPDManager()
    {
        return 27;
    }

    public function getRGPDTechnician()
    {
        return 28;
    }

    public function getRGPDAdministrator()
    {
        return 29;
    }

    public function getEmployes($as_string = false)
    {
        $arr = [
            $this->getSupervisor(), // type 1
            $this->getClient(), // type 15
            $this->getTeamLead(), // type 4
            $this->getAssistant(), // type 3
            $this->getChef(), //type 2
        ];
        $arr = array_merge($arr, $this->getRGPDUsers());
        $arr = array_diff($this->getAll('id'), $arr);
        if ($as_string) {
            $arr = implode(',', $arr);
        }
        return $arr;
    }

    public function getRGPDUsers($as_string = false)
    {
        $arr = [
            $this->getRGPDJunior(),
            $this->getRGPDManager(),
            $this->getRGPDSenior(),
            $this->getRGPDMaster(),
            $this->getRGPDTechnician(),
            $this->getRGPDAdministrator()
        ];
        if ($as_string) {
            $arr = array_map(function ($e) {
                return '"' . $e . '"';
            }, $arr);
            $arr = implode(',', $arr);
        }
        return $arr;
    }

    public function getManagers($as_string = false)
    {
        $arr = [
            $this->getChef(),
            $this->getAssistant(),
        ];
        if ($as_string) {
            $arr = array_map(function ($e) {
                return '"' . $e . '"';
            }, $arr);
            $arr = implode(',', $arr);
        }
        return $arr;
    }

    public function getSupportTypes($as_string = false)
    {
        $arr = [
            $this->getSupervisor(),
            $this->getChef(),
            $this->getAssistant()
        ];
        if ($as_string) {
            $arr = array_map(function ($e) {
                return '"' . $e . '"';
            }, $arr);
            $arr = implode(',', $arr);
        }
        return $arr;
    }

    public function getRGPDCurators($as_string = false)
    {
        $arr = [
            $this->getRGPDSenior(),
            $this->getRGPDMaster()
        ];
        if ($as_string) {
            $arr = array_map(function ($e) {
                return '"' . $e . '"';
            }, $arr);
            $arr = implode(',', $arr);
        }
        return $arr;
    }

    public function getRGPDVendors($as_string = false)
    {
        $arr = [
            $this->getRGPDManager(),
            $this->getRGPDJunior(),
        ];
        if ($as_string) {
            $arr = array_map(function ($e) {
                return '"' . $e . '"';
            }, $arr);
            $arr = implode(',', $arr);
        }
        return $arr;
    }
}