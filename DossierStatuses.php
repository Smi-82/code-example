<?php

use traits\SingleTone;

class DossierStatuses
{
    use SingleTone;

    private $_statuses_by_id;
    private $_statuses_by_key;
    private $_enum;

    private function __construct()
    {
        $this->_statuses_by_id = [
            1 => [
                'name' => 'Jamais contacté',
                'class' => '',
                'ord' => 1,
                'key' => 'Not contacted'
            ],
            2 => [
                'name' => 'À rappeler',
                'class' => 'green',
                'ord' => 2,
                'key' => 'To call back'
            ],
            3 => [
                'name' => 'Indécis',
                'class' => 'yellow',
                'ord' => 3,
                'key' => 'Hesitating'

            ],
            4 => [
                'name' => 'Intéressé',
                'class' => 'orange',
                'ord' => 4,
                'key' => 'Interested'

            ],
            5 => [
                'name' => 'Client potentiel',
                'class' => 'green',
                'ord' => 5,
                'key' => 'Possible client'

            ],
            6 => [
                'name' => 'Déjà installé',
                'class' => 'grey',
                'ord' => 6,
                'key' => 'Already launched'

            ],
            7 => [
                'name' => 'Pas intéressé',
                'class' => 'grey',
                'ord' => 7,
                'key' => 'Not interested'

            ],
            8 => [
                'name' => 'Blacklisté',
                'class' => 'tomato',
                'ord' => 8,
                'key' => 'Blacklisted'

            ],
            9 => [
                'name' => 'Faux numéro',
                'class' => 'tomato',
                'ord' => 9,
                'key' => 'False number'

            ],
            10 => [
                'name' => 'Remboursement: demandé',
                'class' => 'purple',
                'ord' => 13,
                'key' => 'Refund: required'

            ],
            11 => [
                'name' => 'Paiement partiel',
                'class' => 'green',
                'ord' => 10,
                'key' => 'Partially payed'

            ],
            12 => [
                'name' => 'Paiement acquitté',
                'class' => 'green',
                'ord' => 11,
                'key' => 'Completely payed'

            ],
            14 => [
                'name' => 'Litige/Dispute',
                'class' => '',
                'ord' => 12,
                'key' => 'Dispute'

            ],
            15 => [
                'name' => 'Technicien 1',
                'class' => '',
                'ord' => 16,
                'key' => 'Technician 1'

            ],
            16 => [
                'name' => 'Technicien 2',
                'class' => '',
                'ord' => 17,
                'key' => 'Technician 2'

            ],
            17 => [
                'name' => 'Technicien 3',
                'class' => '',
                'ord' => 18,
                'key' => 'Technician 3'

            ],
            18 => [
                'name' => 'Technicien 4',
                'class' => '',
                'ord' => 19,
                'key' => 'Technician 4'

            ],
            19 => [
                'name' => 'Technicien 5',
                'class' => '',
                'ord' => 20,
                'key' => 'Technician 5'

            ],
            20 => [
                'name' => 'Conformité finalisé',
                'class' => '',
                'ord' => 21,
                'key' => 'Bringing to conformity finalized'

            ],
            21 => [
                'name' => 'Remboursement: exécuté',
                'class' => '',
                'ord' => 14,
                'key' => 'Refund: executed'

            ],
            22 => [
                'name' => 'Remboursement: annulé/refusé',
                'class' => '',
                'ord' => 15,
                'key' => 'Refund: refused'

            ],
        ];
        $this->_statuses_by_key = [];
        $this->_statuses_by_aliases = [];
        foreach ($this->_statuses_by_id as $id => $val) {
            $key = strtolower(preg_replace('/\W+/', '_', $val['key']));
            $this->_statuses_by_id[$id]['key'] = $key;
            unset($val['key']);
            $val['id'] = $id;
            $this->_statuses_by_key[$key] = $val;
            $this->_enum[$key] = $id;
        }
    }

    public function not_contacted()
    {
        return $this->getEnum(__FUNCTION__);
    }

    public function to_call_back()
    {
        return $this->getEnum(__FUNCTION__);
    }

    public function hesitating()
    {
        return $this->getEnum(__FUNCTION__);
    }

    public function interested()
    {
        return $this->getEnum(__FUNCTION__);
    }

    public function possible_client()
    {
        return $this->getEnum(__FUNCTION__);
    }

    public function already_launched()
    {
        return $this->getEnum(__FUNCTION__);
    }

    public function not_interested()
    {
        return $this->getEnum(__FUNCTION__);
    }

    public function blacklisted()
    {
        return $this->getEnum(__FUNCTION__);
    }

    public function false_number()
    {
        return $this->getEnum(__FUNCTION__);
    }

    public function refund_required()
    {
        return $this->getEnum(__FUNCTION__);
    }

    public function partially_payed()
    {
        return $this->getEnum(__FUNCTION__);
    }

    public function completely_payed()
    {
        return $this->getEnum(__FUNCTION__);
    }

    public function dispute()
    {
        return $this->getEnum(__FUNCTION__);
    }

    public function technician_1()
    {
        return $this->getEnum(__FUNCTION__);
    }

    public function technician_2()
    {
        return $this->getEnum(__FUNCTION__);
    }

    public function technician_3()
    {
        return $this->getEnum(__FUNCTION__);
    }

    public function technician_4()
    {
        return $this->getEnum(__FUNCTION__);
    }

    public function technician_5()
    {
        return $this->getEnum(__FUNCTION__);
    }

    public function bringing_to_conformity_finalized()
    {
        return $this->getEnum(__FUNCTION__);
    }

    public function refund_executed()
    {
        return $this->getEnum(__FUNCTION__);
    }

    public function refund_refused()
    {
        return $this->getEnum(__FUNCTION__);
    }

    public function getAllStatusesByIds($ord = '')
    {
        $arr = $this->_statuses_by_id;
        switch ($ord) {
            case 'ASC':
                uasort($arr, function ($a, $b) {
                    if ($a['ord'] == $b['ord']) {
                        return 0;
                    }
                    return ($a['ord'] < $b['ord']) ? -1 : 1;
                });
                break;
            case 'DESC':
                uasort($arr, function ($a, $b) {
                    if ($a['ord'] == $b['ord']) {
                        return 0;
                    }
                    return ($a['ord'] > $b['ord']) ? -1 : 1;
                });
                break;

        }
        return $arr;

    }

    public function getStatusNameById($id)
    {
        return isset($this->_statuses_by_id[$id]) ? $this->_statuses_by_id[$id]['name'] : '';
    }

    public function getStatusById($id, $params = [])
    {
        return isset($this->_statuses_by_id[$id]) ? empty($params) ? $this->_statuses_by_id[$id] : array_intersect_key($this->_statuses_by_id[$id], array_flip($params)) : [];
    }

    public function getAllStatusesByKeys($ord = '')
    {
        $arr = $this->_statuses_by_key;
        switch ($ord) {
            case 'ASC':
                uasort($arr, function ($a, $b) {
                    if ($a['ord'] == $b['ord']) {
                        return 0;
                    }
                    return ($a['ord'] < $b['ord']) ? -1 : 1;
                });
                break;
            case 'DESC':
                uasort($arr, function ($a, $b) {
                    if ($a['ord'] == $b['ord']) {
                        return 0;
                    }
                    return ($a['ord'] > $b['ord']) ? -1 : 1;
                });
                break;

        }
        return $arr;
    }

    public function getEnum($key = '')
    {
        return empty($key) ? $this->_enum : $this->_enum[$key];
    }

    public function changeDossierStatus($dossier_id)
    {
        $current_user = User();
        if ($current_user->getUser()) {
            $current_user = $current_user->getId();
        } else {
            $current_user = query('SELECT client_id FROM dossiers WHERE id=' . $dossier_id)[0]['client_id'];
        }
        $payments = getDossierPaidAmount($dossier_id);
        if (floatval($payments['total_paid']) > 0 && floatval($payments['total_paid']) < floatval($payments['total_price'])) {
            $dossier_status = $this->partially_payed();
        } else if (floatval($payments['total_paid']) >= floatval($payments['total_price'])) {
            $dossier_status = $this->completely_payed();
        } else {
            $dossier_status = 0;
        }
        $notify = new AdmNotify();
        $notify->set($notify->getTableForStatus(), $dossier_id);
        $sql = QueryStr()->update_query_string('dossiers',
                [
                    'status_id' => $dossier_status,
                    'dateChange' => date('Y-m-d H:i:s'),
                    'whoChange' => $current_user
                ]) . ' WHERE id=' . $dossier_id;
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}