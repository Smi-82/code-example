<?php

use appEvents\AppEventsController;
use appEvents\subscribers\controller\AppEventsSubscribersController;
use traits\SingleTone;

class Login
{
    use SingleTone;

    private $upass;
    private $salt;
    const MD5_PREFIX = 'fhQYBpS5FNs4';

    private function getToken($email)
    {
        return md5($this->salt[0] . $email . $_SERVER['REMOTE_ADDR'] . $this->salt[1]);
    }

    private function checkUser(&$row)
    {
        return $row['password'] === $this->upass;
    }

    public function encrypt_password($password)
    {
        return md5(self::MD5_PREFIX . escape_string(clear_post($password)));
    }

    private function keepMe(&$row)
    {
        ini_set("session.cookie_domain", "." . SiteData()->getAll('sitenamelight')); //зробити присвоєння сесій по всіх піддоменах для основного домена
        $keepMe = $this->getToken($row['email']);
        setcookie("cId", $keepMe, time() + getSeconds(1, 'y'), '/', '.' . SiteData()->getAll('sitenamelight'));
        setcookie("cU", $row['id'], time() + getSeconds(1, 'y'), '/', '.' . SiteData()->getAll('sitenamelight'));
        $_COOKIE['cId'] = $keepMe;
        $_COOKIE['cU'] = $row['id'];
        $update_user = update_query_string(User::getTable(), ['cookie' => $keepMe]) . ' WHERE id=' . $row['id'];
        User()->keepMe = true;
        try {
            query($update_user);
        } catch (Exception $e) {
        }
    }

    private function __construct()
    {
        $this->salt = ['jk7sTy', '5iLhOd'];
    }

    public function logout()
    {
        try {
            if (isset($_COOKIE['cId'])) {
                setcookie('cId', null, 1, '/', '.' . SiteData()->getAll('sitenamelight'));
                setcookie('cU', null, 1, '/', '.' . SiteData()->getAll('sitenamelight'));
                unset($_COOKIE['cId']);
                unset($_COOKIE['cU']);
            }
            if (isset($_COOKIE['PHPSESSID'])) {
                setcookie('PHPSESSID', null, 1, '/', '.' . SiteData()->getAll('sitenamelight'));
                setcookie('PHPSESSID', null, 1, '/', '.' . SiteData()->getAll('sitenamelight'));
                unset($_COOKIE['PHPSESSID']);
                unset($_COOKIE['PHPSESSID']);
            }

            if (isset($_SERVER['HTTP_X_ACCESS_TOKEN']) && isset($_SERVER['HTTP_X_MOBILE_BY'])) {
                require_once($_SERVER['DOCUMENT_ROOT'] . DS . MODULE . DS . 'messenger_tm/glooda_api.php');
                $ga = new GloodaAPI('logout', array('token' => $_SERVER['HTTP_X_ACCESS_TOKEN']));
            }
            AppEventsController::publish(AppEventsSubscribersController::userLogout(), ['user' => User()]);
            $_SESSION = [];
            session_destroy();
            return true;
        } catch (Exception $e) {
            return false;
        }

    }

    public function checkCookie()
    {
        $user = User::getById($_COOKIE['cU']);
        if (!$user)
            return false;
        if ($this->getToken($user['email']) == $user['cookie'] && $_COOKIE['cId'] == $user['cookie']) {
            User::setSession($user);
            return true;
        }
        return false;
    }

    public function loginForMessanger($login, $pass)
    {
        try {
            $uname = escape_string(clear_post($login));
            $this->upass = $this->encrypt_password(clear_post($pass));
            $row = User::getUserByEmail($uname, ['password']);
            if (empty($row))
                return 0;
            return $this->checkUser($row) ? 1 : 0;
        } catch (Exception $e) {
            return 0;
        }
    }

    public static function setOffLine()
    {
        $users = User::getOnLine(['id', 'session_id']);
        if (empty($users))
            return false;
        $path = session_save_path();
        $arr = scandir($path);
        $arr = array_diff($arr, ['.', '..']);
        $obj = self::getInst();
        $arr = array_map(function ($el) use ($obj) {
            $el = explode('_', $el);
            return $obj->encrypt_password(array_pop($el));
        }, $arr);
        $users = array_filter($users, function ($el) use ($arr) {
            return !empty($el['session_id']) && !in_array($el['session_id'], $arr);
        });
        if (empty($users))
            return false;
        $users = array_column($users, 'id');
        foreach ($users as $id)
            Task::userNotOnTask($id);
        User::setOffLine($users);
        return $users;
    }

    public function authorize($user)
    {
        User::setSession($user);
        User::setOnLine($user['id']);
        if ($user['usertype'] == UserTypes()->getClient()) {
            User::setTimeToShowPopup($user['id']);
        }
        if (isset($_REQUEST['keepMeSigned'])) {
            $this->keepMe($user);
        }
        AppEventsController::publish(AppEventsSubscribersController::userLogin(), compact('user'));
        return true;
    }

    function loginById($id)
    {
        return $this->authorize(User::getById(clear_post($id)));
    }

    public function login($login, $pass, $lang = null)
    {

        $uname = escape_string(clear_post($login));
        $this->upass = $this->encrypt_password(clear_post($pass));

        $user = User::getUserByEmail($uname);

        if (empty($user))
            return false;

        if ($this->checkUser($user)) {
            $res = $this->authorize($user);
            if ($res && !is_null($lang)) {
                Language()->setLang($lang);
            }
            return $res;
        }
        return false;
    }

    public function loginFromMobile()
    {
        //if( isset($_SERVER['HTTP_X_MOBILE_BY']) && $_SERVER['HTTP_X_MOBILE_BY']=='android' ) {
        //    if( isset($_SERVER['HTTP_X_ACCESS_TOKEN']) && !empty($_SERVER['HTTP_X_ACCESS_TOKEN']) ){
        require_once($_SERVER['DOCUMENT_ROOT'] . DS . MODULE . DS . 'messenger_tm/glooda_api.php');

        $ga = new GloodaAPI(
            'checkToken',
            array('token' => $_SERVER['HTTP_X_ACCESS_TOKEN'])
        );

        if ($ga->login_OK) {

            //отдаю этот токен мобильному устройству
            //получаю другой токен только для мессенджера и загоняю его в сессию
            //$newToken = $ga->auth_duplicateToken();
            //if ($newToken) $ga->set_accessToken($newToken['token']);

            $globaluserdata = $ga->get_userdata();
            $data = array(
                'accessToken' => $ga->get_accessToken(),
                'global_id' => $ga->get_ID(),
                'profileid' => $ga->get_profileid(),
                'login' => $ga->get_login(),  //maybe I'll delete this field in task manager
                'email' => $ga->get_email(),
                'password' => '111',//todo: убрать костыль //$_POST['password'], //?? for SynchronizeUserDataByGlobalID
                'firstname' => $globaluserdata['login'],//$globaluserdata['firstname'],
                'lastname' => $globaluserdata['lastname']
            );
            // todo: add to $data: [gender], [aboutme], [phone], [address], [country], [city], [zip], [birthday]


            if ($this->loginnew($data, $_POST['lng'] ?? null)) {

                if (User()->getUser()) {
                    $UserTypes = UserTypes()->getUserTypesForJsData();
                    JsData()->set(['module' => MODULE, 'language' => Language()->getLanguage(), 'userTypes' => $UserTypes, 'user' => [
                        'firstname' => User()->getFName(),
                        'lastname' => User()->getLName(),
                        'type' => User()->getType(),
                        'user_id' => User()->getId(),
                        'user_signature' => User()->getSignature(),
                    ],]);
                }

                /**/
                if (true) {
                    return Pages::getClientDefault();
                }
                else {
                    if (User()->isRGPDUser()) {
                        return 'dossier_client_list.php';
                    }
                    else {
                        return Pages::getAdminDefault();
                    }
                }/**/

            }

        }
        else {
            return false;
        }
    }

    public function loginnew($data, $lang = null)
    {
        //$this->upass = $data['password'];
        $user = User::getUserByGlobalID($data['global_id']);

        if (empty($user)) {  //if client-user already has global account but do not have account in task manager

            $data['password1'] = $data['password'];
            $data['registeredBy'] = 'himself';
            $data['firstname'] = empty($data['firstname']) ? 'unknown' : $data['firstname'];
            $data['lastname'] = empty($data['lastname']) ? 'unknown' : $data['lastname'];
            $data['sent_to_user'] = 0;

            $userid = User()->addClient($data);


            if (isset($_SERVER['HTTP_X_MOBILE_BY']) && $_SERVER['HTTP_X_MOBILE_BY'] == 'android') {
                if (isset($_SERVER['HTTP_X_ACCESS_TOKEN']) && !empty($_SERVER['HTTP_X_ACCESS_TOKEN'])) {
                    //console($userid);// return;
                }
            }

            //$userid = User::addNewClientAccount($data);

            if ($userid) {
                $user = User::getUserByGlobalID($data['global_id']);
            }
            else return false;

        }
        else {
            //////if (isset($data['password'])) $data['password'] = $this->encrypt_password(clear_post($data['password']));
            //todo  User::SynchronizeUserDataByGlobalID($global_id, $data);
        }

        $user['accessToken'] = $data['accessToken'];
        $user['profileid'] = $data['profileid'];
        $res = $this->authorize($user);
        if ($res && !is_null($lang)) {
            Language()->setLang($lang);
        }
        return $res;
    }

    /*public function loginnew($global_id, $pass, $data, $lang = null)
    {
        $this->upass = $this->encrypt_password(clear_post($pass));
        $user = User::getUserByGlobalID($global_id);
        consoleJS($user);

        if (empty($user))
            return false;

        if ($this->checkUser($user)) {
            $res = $this->authorize($user);
            if ($res && !is_null($lang)) {
                Language()->setLang($lang);
            }
            return $res;
        }
        return false;
    }*/

    public function loginByIdAndRedirect($id, $url = '/')
    {
        if ($this->loginById($id)) {
            redirect($url);
        }
    }

    public function redirectParam($url = null)
    {
        return ['redirect' => $url];
    }

    public function doRedirectAfterLogIn()
    {
        $redirect = $this->redirectParam();
        return isset($_GET[key($redirect)]);
    }

}