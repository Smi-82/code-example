<?php

use traits\BaseSelect;
use traits\GetTable;

class TasksMessagesFiles
{
    private static $table = 'tasks_messages_files';
    use getTable;
    use baseSelect;

    public static function add($data)
    {
        $sql = insert_query_string(self::getTable(), $data);
        try {
            $res = query($sql);
            return $res ? getLastId() : $res;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function getByTaskId($task_id, $columns = ['*'])
    {
        $task_id = escape_string(clear_post($task_id));
        $sql = self::baseSelect($columns) . ' WHERE task_id = ' . $task_id;
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function getById($id, $columns = ['*'])
    {
        $id = escape_string(clear_post($id));
        $sql = self::baseSelect($columns) . ' WHERE id = ' . $id;
        try {
            return querySingle($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function getAll($columns = ['*'])
    {
        $sql = self::baseSelect($columns);
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function updateById($ids, $data)
    {
        if (isset($data['id']))
            unset($data['id']);
        $ids = getAsArray($ids);
        $sql = update_query_string(self::getTable(), $data) . ' WHERE id IN (' . implode(',', $ids) . ')';
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function delete($id)
    {
        $id = intval(escape_string(clear_post($id)));
        $sql = 'DELETE FROM ' . self::getTable() . ' WHERE id=' . $id;
        try {
            return query($sql);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}