<?php
namespace traits;
trait ClassName
{
    public static function getClassName()
    {
        return get_called_class();
    }
}