<?php
namespace traits;
trait GetById
{
    public static function getById($id, $params = ['*'])
    {
        try {
            $res = self::getManyByIds($id, $params);
            return !empty($res) ? array_shift($res) : $res;
        } catch (Exception $e) {
            return false;
        }
    }

    public static function getManyByIds($ids, $params = ['*'])
    {
        $ids = getAsArray($ids);
        $sql = self::baseSelect($params) . ' WHERE id IN (' . implode(',', $ids) . ')';
        try {
            return query($sql);
        } catch (Exception $e) {
            return false;
        }
    }
}