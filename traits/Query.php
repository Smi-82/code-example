<?php


namespace traits;


use Exception;

trait Query
{
    public static function query($sql, $single = false)
    {
        try {
            return $single ? querySingle($sql) : query($sql);
        } catch (Exception $e) {
            return [];
        }
    }
}