<?php
namespace traits;
trait BaseSelect
{
    public static function baseSelect(array $columns = ['*'], $aggregate = null)
    {
        $columns = clearParams($columns);
        $sql = 'SELECT ' . implode(',', $columns);
        if (!is_null($aggregate)) {
            $sql .= ', ' . $aggregate . ' ';
        }
        $sql .= ' FROM ' . self::getTable();
        return $sql;
    }
}