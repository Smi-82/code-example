<?php


namespace traits;


use Exception;

trait PropertyContainer
{
    private $property_container = [];

    public function addProperty($key, $val)
    {
        $this->property_container[$key] = $val;
    }

    public function getAllProperties()
    {
        return $this->property_container;
    }

    public function getProperty($key)
    {
        if (!isset($this->property_container[$key]))
            throw new Exception('no such property ' . $key . ' in container');
        return $this->property_container[$key];
    }

    public function deleteProperty($key)
    {
        unset($this->property_container[$key]);
    }
}