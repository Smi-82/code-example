<?php
namespace traits;
trait SingleTone
{
    private static $inst = null;

    private function __construct()
    {
    }

    public function __clone()
    {
    }

    public function __wakeup()
    {
    }

    public static function getInst()
    {
        if (self::$inst === null) {
            self::$inst = new self;
        }
        return self::$inst;
    }
}