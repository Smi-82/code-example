<?php


namespace traits;


use Exception;

trait Update
{
    public static function update($id, $data)
    {
        $id = getAsArray($id);
        $sql = update_query_string(self::getTable(), $data) . ' WHERE id IN (' . implode(',', $id) . ')';
        try {
            $res = query($sql);
            return $res ? getLastId() : $res;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}